﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace App_ChuongTrinhSinhCodeTuDong.CodeWebFormLinQ
{
    internal static class csGeneratorAdminMasterPage
    {

        #region Tạo trang MASTER PAGE
       /// <summary>
       /// 
       /// </summary>
       /// <param name="path"></param>
       /// <param name="targetNamespace"></param>
       /// <param name="tableList"></param>
       /// <param name="daoSuffix"></param>
       /// <param name="dtoSuffixh"></param>
        public static void createMasterPage_aspx(string path, string targetNamespace, List<Table> tableList)
        {

            using (StreamWriter streamWriter = new StreamWriter(Path.Combine(path, "admin.Master")))
            {
                var sb = new StringBuilder();

                sb.Append("\n <%@ Master Language=\"C#\" AutoEventWireup=\"true\" CodeBehind=\"admin.master.cs\" Inherits=\"" + targetNamespace + "\" %>");
                sb.Append("\n <!DOCTYPE html PUBLIC \" -//W3C//DTD XHTML 1.0 Strict//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd\">");
                sb.Append("\n <html xmlns=\"http://www.w3.org/1999/xhtml\" xml:lang=\"en\">");
                sb.Append("\n <head runat=\"server\">");
                sb.Append("\n\t <title>" + targetNamespace + "</title>");
                sb.Append("\n\t <link href=\"../css/SiteAdmin.css\" rel=\"stylesheet\" type=\"text/css\" />");
                sb.Append("\n\t <asp:ContentPlaceHolder ID=\"HeadContent\" runat=\"server\">");
                sb.Append("\n\t </asp:ContentPlaceHolder>");
                sb.Append("\n </head>");
                sb.Append("\n <body>");
                sb.Append("\n  <form runat=\"server\">");
                sb.Append("\n <div class=\"page\">");
                sb.Append("\n <div class=\"header\">");
                sb.Append("\n  <div class=\"textBanner\">Admin</div>");
                sb.Append("\n  <div class=\"loginDisplay\"> Welcome <span class=\"bold\">");
                sb.Append("\n  <asp:LoginName ID=\"HeadLoginName\" runat=\"server\" />");
                sb.Append("\n   </span>![");
                sb.Append("\n <asp:LoginStatus ID=\"HeadLoginStatus\" runat=\"server\" LogoutAction=\"Redirect\" LogoutText=\"Log Out\" LogoutPageUrl=\"~/ Acc / login.aspx\" />");
                sb.Append("\n ]");
                sb.Append("\n </div>");
                sb.Append("\n </div>");
                sb.Append("\n <div class=\"clear\"></div>");
 
                sb.Append("\n <div class=\"wrapper\">");
                sb.Append("\n <div class=\"sidebar\"");
                sb.Append("\n  <asp:Menu ID=\"MenuSideBar11\" runat=\"server\" CssClass=\"menuSideBar\" EnableViewState=\"false\" IncludeStyleBlock=\"false\"");
                sb.Append("\n <Items>");
                foreach (Table table in tableList)
                {
                    //string className = Utility.FormatClassName(table.Name);
                    sb.Append("\n <asp:MenuItem NavigateUrl=\"~/admin/" + table.Name + ".aspx\" Text=\"" + table.Name + "\" />");
                }
                sb.Append("\n </Items>");
                sb.Append("\n </asp:Menu>");
                sb.Append("\n </div>");
                sb.Append("\n <div class=\"main\">");
                sb.Append("\n <asp:ContentPlaceHolder ID=\"MainContent\" runat=\"server\" />");
                sb.Append("\n </div>");
                sb.Append("\n </div>");
                sb.Append("\n <div class=\"footer\">");
                sb.Append("\n </div>");
                sb.Append("\n </div>");
                sb.Append("\n </form>");
                sb.Append("\n </body>");
                sb.Append("\n </html>");
                sb.Append("\n ");
                streamWriter.WriteLine(sb);
            }
        }

        /// <summary>
        ///  /// Tạo Code phần thêm sửa xóa kiểu Click Gridview tới textbox
        /// </summary>
        /// <param name="databaseName"></param>
        /// <param name="table"></param>
        /// <param name="targetNamespace"></param>
        /// <param name="storedProcedurePrefix"></param>
        /// <param name="daoSuffix"></param>
        /// <param name="dtoSuffix"></param>
        /// <param name="path"></param>

        public static void createMasterPage_cs(string path, string targetNamespace, List<Table> tableList)
        {
            using (StreamWriter streamWriter = new StreamWriter(Path.Combine(path,"admin.Master.cs")))
            {
                var sb = new StringBuilder();
                sb.Append("using System;");
                sb.Append("\n using System.Collections.Generic;");
                sb.Append("\n using System.Linq;");
                sb.Append("\n using System.Web;");
                sb.Append("\n using System.Web.UI;");
                sb.Append("\n using System.Web.UI.WebControls;");
                sb.Append("\n using Business;");
                sb.Append("\n using Entity;");
                sb.Append("\n using System.Data;");
                sb.Append("\n using Common;");
                sb.Append("\n namespace " + targetNamespace + "");
                sb.Append("\n {");
                sb.Append("\n\t public partial class admin : System.Web.UI.MasterPage");
                sb.Append("\n\t {");
 
                sb.Append("\n\t\t protected void Page_Load(object sender, EventArgs e)");
                sb.Append("\n\t\t {");
                
                sb.Append("\n\t\t } ");
                sb.Append("\n\t }");
                sb.Append("\n }");

                streamWriter.WriteLine(sb);
            }
        }


        public static void createMasterPage_designer_cs(string path, string targetNamespace, List<Table> tableList)
        { 
            using (StreamWriter streamWriter = new StreamWriter(Path.Combine(path,"admin.Master.designer.cs")))
            {
                var sb = new StringBuilder();
                sb.Append("\n//------------------------------------------------------------------------------");
                sb.Append("\n// <auto-generated>");
                sb.Append("\n //     This code was generated by a tool of Toantv.");
                sb.Append("\n //     Changes to this file may cause incorrect behavior and will be lost if");
                sb.Append("\n  //     the code is regenerated. ");
                sb.Append("\n // </auto-generated>");
                sb.Append("\n //------------------------------------------------------------------------------");

                sb.Append("\n namespace " + targetNamespace + "");
                sb.Append("\n {");
                sb.Append("\n\t public partial class admin");
                sb.Append("\n\t{");
                sb.Append("\n\t protected global::System.Web.UI.WebControls.ContentPlaceHolder HeadContent;");
                sb.Append("\n\t protected global::System.Web.UI.WebControls.LoginView HeadLoginView;");
                sb.Append("\n\t protected global::System.Web.UI.WebControls.LoginStatus HeadLoginStatus;");
                sb.Append("\n\t protected global::System.Web.UI.WebControls.Menu MenuSideBar11;");
                sb.Append("\n\t protected global::System.Web.UI.WebControls.ContentPlaceHolder MainContent;");
               
                sb.Append("\n\t ");
                
                sb.Append("\n\t }");
                sb.Append("\n }");

                streamWriter.WriteLine(sb);
            }
        }
        #endregion
    }
}
