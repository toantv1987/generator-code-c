﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
namespace App_ChuongTrinhSinhCodeTuDong.CodeWebFormLinQ
{
	/// <summary>
	/// Generates C# data access and data transfer classes.
	/// </summary>
	public static class CsGenerator
	{
		/// <summary>
		/// Creates a project file that references each generated C# code file for data access.
		/// </summary>
		/// <param name="path">The path where the project file should be created.</param>
		/// <param name="projectName">The name of the project.</param>
		/// <param name="tableList">The list of tables code files were created for.</param>
		/// <param name="daoSuffix">The suffix to append to the name of each data access class.</param>
        /// <param name="dtoSuffix">The suffix to append to the name of each data transfer class.</param>
		public static void CreateProjectFile_DAL(string path, string projectName, List<Table> tableList, string daoSuffix, string dtoSuffix)
		{
			string projectXml = Utility.GetResource(projectName);
			XmlDocument document = new XmlDocument();
			document.LoadXml(projectXml);

			XmlNamespaceManager namespaceManager = new XmlNamespaceManager(document.NameTable);
			namespaceManager.AddNamespace(String.Empty, "http://schemas.microsoft.com/developer/msbuild/2003");
			namespaceManager.AddNamespace("msbuild", "http://schemas.microsoft.com/developer/msbuild/2003");

			document.SelectSingleNode("/msbuild:Project/msbuild:PropertyGroup/msbuild:ProjectGuid", namespaceManager).InnerText = "{" + Guid.NewGuid().ToString() + "}";
			document.SelectSingleNode("/msbuild:Project/msbuild:PropertyGroup/msbuild:RootNamespace", namespaceManager).InnerText = projectName;
			document.SelectSingleNode("/msbuild:Project/msbuild:PropertyGroup/msbuild:AssemblyName", namespaceManager).InnerText = projectName;

			XmlNode itemGroupNode = document.SelectSingleNode("/msbuild:Project/msbuild:ItemGroup[msbuild:Compile]", namespaceManager);
			foreach (Table table in tableList)
			{
				string className = Utility.FormatClassName(table.Name);
				
				//XmlNode dtoCompileNode = document.CreateElement("Compile", "http://schemas.microsoft.com/developer/msbuild/2003");
				//XmlAttribute dtoAttribute = document.CreateAttribute("Include");
				//dtoAttribute.Value = className + dtoSuffix + ".cs";
				//dtoCompileNode.Attributes.Append(dtoAttribute);
				//itemGroupNode.AppendChild(dtoCompileNode);
				
				XmlNode dataCompileNode = document.CreateElement("Compile", "http://schemas.microsoft.com/developer/msbuild/2003");
				XmlAttribute dataAttribute = document.CreateAttribute("Include");
                dataAttribute.Value = Path.Combine(Utility.FormatClassName(table.Name) + daoSuffix + ".cs");
				dataCompileNode.Attributes.Append(dataAttribute);
				itemGroupNode.AppendChild(dataCompileNode);
			}
			 
			document.Save(Path.Combine(path, "DataAccess.csproj"));
		}

        /// <summary>
		/// Creates a project file that references each generated C# code file for data access.
		/// </summary>
		/// <param name="path">The path where the project file should be created.</param>
		/// <param name="projectName">The name of the project.</param>
		/// <param name="tableList">The list of tables code files were created for.</param>
		/// <param name="daoSuffix">The suffix to append to the name of each data access class.</param>
        /// <param name="dtoSuffix">The suffix to append to the name of each data transfer class.</param>
		public static void CreateProjectFile_BUS(string path, string projectName, List<Table> tableList, string busSuffix, string dtoSuffix)
        {
            string projectXml = Utility.GetResource(projectName);
            XmlDocument document = new XmlDocument();
            document.LoadXml(projectXml);

            XmlNamespaceManager namespaceManager = new XmlNamespaceManager(document.NameTable);
            namespaceManager.AddNamespace(String.Empty, "http://schemas.microsoft.com/developer/msbuild/2003");
            namespaceManager.AddNamespace("msbuild", "http://schemas.microsoft.com/developer/msbuild/2003");

            document.SelectSingleNode("/msbuild:Project/msbuild:PropertyGroup/msbuild:ProjectGuid", namespaceManager).InnerText = "{" + Guid.NewGuid().ToString() + "}";
            document.SelectSingleNode("/msbuild:Project/msbuild:PropertyGroup/msbuild:RootNamespace", namespaceManager).InnerText = projectName;
            document.SelectSingleNode("/msbuild:Project/msbuild:PropertyGroup/msbuild:AssemblyName", namespaceManager).InnerText = projectName;

            XmlNode itemGroupNode = document.SelectSingleNode("/msbuild:Project/msbuild:ItemGroup[msbuild:Compile]", namespaceManager);
            foreach (Table table in tableList)
            {
                string className = Utility.FormatClassName(table.Name);

                //XmlNode dtoCompileNode = document.CreateElement("Compile", "http://schemas.microsoft.com/developer/msbuild/2003");
                //XmlAttribute dtoAttribute = document.CreateAttribute("Include");
                //dtoAttribute.Value = className + dtoSuffix + ".cs";
                //dtoCompileNode.Attributes.Append(dtoAttribute);
                //itemGroupNode.AppendChild(dtoCompileNode);

                XmlNode dataCompileNode = document.CreateElement("Compile", "http://schemas.microsoft.com/developer/msbuild/2003");
                XmlAttribute dataAttribute = document.CreateAttribute("Include");
                dataAttribute.Value = Path.Combine(Utility.FormatClassName(table.Name) + busSuffix + ".cs");
                dataCompileNode.Attributes.Append(dataAttribute);
                itemGroupNode.AppendChild(dataCompileNode);
            }

            document.Save(Path.Combine(path, "Business.csproj"));
        }

        /// <summary>
		/// Creates a project file that references each generated C# code file for data access.
		/// </summary>
		/// <param name="path">The path where the project file should be created.</param>
		/// <param name="projectName">The name of the project.</param>
		/// <param name="tableList">The list of tables code files were created for.</param>
		/// <param name="daoSuffix">The suffix to append to the name of each data access class.</param>
        /// <param name="dtoSuffix">The suffix to append to the name of each data transfer class.</param>
		public static void CreateProjectFileEntity(string path, string projectName, List<Table> tableList, string daoSuffix, string dtoSuffix)
        {
            string projectXml = Utility.GetResource(projectName);
            XmlDocument document = new XmlDocument();
            document.LoadXml(projectXml);

            XmlNamespaceManager namespaceManager = new XmlNamespaceManager(document.NameTable);
            namespaceManager.AddNamespace(String.Empty, "http://schemas.microsoft.com/developer/msbuild/2003");
            namespaceManager.AddNamespace("msbuild", "http://schemas.microsoft.com/developer/msbuild/2003");

            document.SelectSingleNode("/msbuild:Project/msbuild:PropertyGroup/msbuild:ProjectGuid", namespaceManager).InnerText = "{" + Guid.NewGuid().ToString() + "}";
            document.SelectSingleNode("/msbuild:Project/msbuild:PropertyGroup/msbuild:RootNamespace", namespaceManager).InnerText = projectName;
            document.SelectSingleNode("/msbuild:Project/msbuild:PropertyGroup/msbuild:AssemblyName", namespaceManager).InnerText = projectName;

            XmlNode itemGroupNode = document.SelectSingleNode("/msbuild:Project/msbuild:ItemGroup[msbuild:Compile]", namespaceManager);
            foreach (Table table in tableList)
            {
                string className = Utility.FormatClassName(table.Name);

                //XmlNode dtoCompileNode = document.CreateElement("Compile", "http://schemas.microsoft.com/developer/msbuild/2003");
                //XmlAttribute dtoAttribute = document.CreateAttribute("Include");
                //dtoAttribute.Value = className + dtoSuffix + ".cs";
                //dtoCompileNode.Attributes.Append(dtoAttribute);
                //itemGroupNode.AppendChild(dtoCompileNode);

                XmlNode dataCompileNode = document.CreateElement("Compile", "http://schemas.microsoft.com/developer/msbuild/2003");
                XmlAttribute dataAttribute = document.CreateAttribute("Include");
                dataAttribute.Value = Path.Combine(Utility.FormatClassName(table.Name) + dtoSuffix + ".cs");
                dataCompileNode.Attributes.Append(dataAttribute);
                itemGroupNode.AppendChild(dataCompileNode);
            }

            document.Save(Path.Combine(path, "Entity.csproj"));
        }

        /// <summary>
        /// Creates the AssemblyInfo.cs file for the project.
        /// </summary>
        /// <param name="path">The root path of the project.</param>
        /// <param name="assemblyTitle">The title of the assembly.</param>
        /// <param name="databaseName">The name of the database the assembly provides access to.</param>
        public static void CreateAssemblyInfo(string path, string assemblyTitle, string databaseName)
		{
			string assemblyInfo = Utility.GetResource("AssemblyInfo.txt");
			assemblyInfo.Replace("#AssemblyTitle", assemblyTitle);
			assemblyInfo.Replace("#DatabaseName", databaseName);

			string propertiesDirectory = Path.Combine(path, "Properties");
			if (Directory.Exists(propertiesDirectory) == false)
			{
				Directory.CreateDirectory(propertiesDirectory);
			}

			File.WriteAllText(Path.Combine(propertiesDirectory, "AssemblyInfo.cs"), assemblyInfo);
		}

		/// <summary>
		/// Creates the SharpCore DLLs required by the generated code.
		/// </summary>
		/// <param name="path">The root path of the project</param>
		public static void CreateSharpCore(string path)
		{
			string sharpCoreDirectory = Path.Combine(Path.Combine(path, "Lib"), "SharpCore");
			if (Directory.Exists(sharpCoreDirectory) == false)
			{
				Directory.CreateDirectory(sharpCoreDirectory);
			}

			Utility.WriteResourceToFile("DataTierGenerator.Resources.SharpCore.SharpCore.Data.dll", Path.Combine(sharpCoreDirectory, "SharpCore.Data.dll"));
			Utility.WriteResourceToFile("DataTierGenerator.Resources.SharpCore.SharpCore.Data.pdb", Path.Combine(sharpCoreDirectory, "SharpCore.Data.pdb"));
			Utility.WriteResourceToFile("DataTierGenerator.Resources.SharpCore.SharpCore.Extensions.dll", Path.Combine(sharpCoreDirectory, "SharpCore.Extensions.dll"));
			Utility.WriteResourceToFile("DataTierGenerator.Resources.SharpCore.SharpCore.Extensions.pdb", Path.Combine(sharpCoreDirectory, "SharpCore.Extensions.pdb"));
			Utility.WriteResourceToFile("DataTierGenerator.Resources.SharpCore.SharpCore.Utilities.dll", Path.Combine(sharpCoreDirectory, "SharpCore.Utilities.dll"));
			Utility.WriteResourceToFile("DataTierGenerator.Resources.SharpCore.SharpCore.Utilities.pdb", Path.Combine(sharpCoreDirectory, "SharpCore.Utilities.pdb"));
		}

		/// <summary>
		/// Creates a C# class for all of the table's stored procedures.
		/// </summary>
		/// <param name="table">Instance of the Table class that represents the table this class will be created for.</param>
		/// <param name="targetNamespace">The namespace that the generated C# classes should contained in.</param>
		/// <param name="daoSuffix">The suffix to be appended to the data access class.</param>
		/// <param name="path">Path where the class should be created.</param>
		public static void CreateDataTransferClass(Table table, string targetNamespace, string dtoSuffix, string path)
		{
			string className = Utility.FormatClassName(table.Name) + dtoSuffix;
            path = Path.Combine(path, "Entity");
            using (StreamWriter streamWriter = new StreamWriter(Path.Combine(path, className + ".cs")))
			{
				// Create the header for the class
				streamWriter.WriteLine("using System;");
				streamWriter.WriteLine();
				//streamWriter.WriteLine("namespace " + targetNamespace);
                streamWriter.WriteLine("namespace Entity");
                streamWriter.WriteLine("{");

				streamWriter.WriteLine("\tpublic class " + className);
				streamWriter.WriteLine("\t{");
                //Append the public properties and Method
                streamWriter.WriteLine("\t\t#region Properties");
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];
                    string parameter = Utility.CreateMethodParameter(column);
                    string type = parameter.Split(' ')[0];
                    string name = parameter.Split(' ')[1];
                    switch (type)
                    {
                        case "DateTime":
                            streamWriter.WriteLine("\t\tpublic " + type + "? " + Utility.FormatPascal(name) + " { get; set; }");
                            break;
                        default:
                            streamWriter.WriteLine("\t\tpublic " + type + " " + Utility.FormatPascal(name) + " { get; set; }");
                            break;
                    }
                   

                    if (i < (table.Columns.Count - 1))
                    {
                        streamWriter.WriteLine();
                    }
                }

                streamWriter.WriteLine();
                streamWriter.WriteLine("\t\t#endregion");

                /*
                 * // Kiểu Khai báo thuộc tính và phương thức đầy đủ.
                 * //Code chạy ngon nhưng không thích dùng vì nó rườm già quá
                 * //Giữ lại bởi vì để sau này dùng cho việc thêm tùy chọn khi làm phần mềm nâng cao hơn
                 streamWriter.WriteLine("\t\t#region Properties");
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];
                    string parameter = Utility.CreateMethodParameter(column);
                    string type = parameter.Split(' ')[0];
                    string name = parameter.Split(' ')[1];
                    switch (type)
                    {
                        case "int":
                            streamWriter.Write("\t\t private " + type + " ? _" + Utility.FormatPascal(name) + " = 0;");
                            break;
                        case "float":
                            streamWriter.Write("\t\t private " + type + " ? _" + Utility.FormatPascal(name) + " = 0;");
                            break;
                        case "short":
                            streamWriter.Write("\t\t private " + type + " ? _" + Utility.FormatPascal(name) + " = 0;");
                            break;
                        case "DateTime":
                            streamWriter.Write("\t\t private " + type + " ? _" + Utility.FormatPascal(name) + " = null;");
                            break;
                        
                        case "decimal":
                            streamWriter.Write("\t\t private " + type + " ? _" + Utility.FormatPascal(name) + " = 0;");
                            break;
                        default:
                            streamWriter.Write("\t\t private " + type + " _" + Utility.FormatPascal(name) + " = "+'"'+'"'+";");
                            break;
                    }
                 

                    if (i < (table.Columns.Count - 1))
                    {
                        streamWriter.WriteLine();
                    }
                }
                streamWriter.WriteLine();
                streamWriter.WriteLine("\t\t#endregion");

                streamWriter.WriteLine("\t\t#region Method");
                for (int i = 0; i < table.Columns.Count; i++)
				{
					Column column = table.Columns[i];
					string parameter = Utility.CreateMethodParameter(column);
					string type = parameter.Split(' ')[0];
					string name = parameter.Split(' ')[1];
                    //streamWriter.WriteLine("\t\t private " + type + " _" + Utility.FormatPascal(name) + ";");
                    

                    switch (type)
                    {
                        case "int":
                            streamWriter.WriteLine("\t\t public " + type + "? " + Utility.FormatPascal(name));
                            break;
                        case "float":
                            streamWriter.WriteLine("\t\t public " + type + "? " + Utility.FormatPascal(name));
                            break;
                        case "short":
                            streamWriter.WriteLine("\t\t public " + type + "? " + Utility.FormatPascal(name));
                            break;
                        case "DateTime":
                            streamWriter.WriteLine("\t\t public " + type + "? " + Utility.FormatPascal(name));
                            break;

                        case "decimal":
                            streamWriter.WriteLine("\t\t public " + type + "? " + Utility.FormatPascal(name));
                            break;
                        default:
                            streamWriter.WriteLine("\t\t public " + type + " " + Utility.FormatPascal(name));
                            break;
                    }




                    streamWriter.WriteLine("\t\t {");
                    streamWriter.WriteLine("\t\t get");
                    streamWriter.WriteLine("\t\t {");
                    streamWriter.WriteLine("\t\t  return _"+ Utility.FormatPascal(name)+";");
                    streamWriter.WriteLine("\t\t }");
                    streamWriter.WriteLine("\t\t set");
                    streamWriter.WriteLine("\t\t {");
                    streamWriter.WriteLine("\t\t _" + Utility.FormatPascal(name) + " = value;");
                    streamWriter.WriteLine("\t\t }");
                    streamWriter.WriteLine("\t\t }");

                    if (i < (table.Columns.Count - 1))
					{
						streamWriter.WriteLine();
					}
				}
                streamWriter.WriteLine("\t\t#endregion");
                 */

                streamWriter.WriteLine();
				

				// Close out the class and namespace
				streamWriter.WriteLine("\t}");
				streamWriter.WriteLine("}");
			}
		}
		
		/// <summary>
		/// Creates a C# data access class for all of the table's stored procedures.
		/// </summary>
		/// <param name="databaseName">The name of the database.</param>
		/// <param name="table">Instance of the Table class that represents the table this class will be created for.</param>
		/// <param name="targetNamespace">The namespace that the generated C# classes should contained in.</param>
		/// <param name="storedProcedurePrefix">Prefix to be appended to the name of the stored procedure.</param>
		/// <param name="daoSuffix">The suffix to be appended to the data access class.</param>
        /// <param name="dtoSuffix">The suffix to append to the name of each data transfer class.</param>
		/// <param name="path">Path where the class should be created.</param>
		public static void CreateDataAccessClass(string databaseName, Table table, string targetNamespace, string storedProcedurePrefix, string daoSuffix, string dtoSuffix, string path)
		{
			string className = Utility.FormatClassName(table.Name) + daoSuffix;
            path = Path.Combine(path, "DataAccess");

            using (StreamWriter streamWriter = new StreamWriter(Path.Combine(path, className + ".cs")))
			{
				// Create the header for the class
				streamWriter.WriteLine("using System;");
                streamWriter.WriteLine("using System.Linq;");
                
				streamWriter.WriteLine("using System.Collections.Generic;");
				streamWriter.WriteLine("using System.Data;");
				streamWriter.WriteLine("using System.Data.SqlClient;");
                streamWriter.WriteLine("using Entity;");
				streamWriter.WriteLine();

                streamWriter.WriteLine("namespace DataAccess");
				streamWriter.WriteLine("{");

                streamWriter.WriteLine("\tpublic class " + className  + "  : SqlDataProvider");
				streamWriter.WriteLine("\t{");
                // Append the access methods
				streamWriter.WriteLine("\t\t#region Methods");
				streamWriter.WriteLine();
				
				CreateInsertMethod(table, storedProcedurePrefix, dtoSuffix, streamWriter);
				CreateUpdateMethod(table, storedProcedurePrefix, dtoSuffix, streamWriter);
				CreateDeleteMethod(table, storedProcedurePrefix,dtoSuffix, streamWriter);
               
                //Truy vấn lấy dữ liệu sử dụng thủ tục
                CreateSelectMethodByID(table, storedProcedurePrefix, dtoSuffix, streamWriter); //Lấy theo ID
                CreateSelectAllMethod(table, storedProcedurePrefix, dtoSuffix, streamWriter); //Lấy tất cả
                CreateSelectAllJsonMethod(table, storedProcedurePrefix, dtoSuffix, streamWriter); //Select lấy theo ID khóa ngoại

                //Sử dụng truy vấn từ giao dien từ Form
                CreateGetAllMethod(table, storedProcedurePrefix, dtoSuffix, streamWriter);
			    CreateGetAllMethodByQuery(table, storedProcedurePrefix, dtoSuffix, streamWriter);
                CreateGetAllMethodOrderByColumn(table, storedProcedurePrefix, dtoSuffix, streamWriter);
                CreateGetByIDMethod(table, storedProcedurePrefix, dtoSuffix, streamWriter);               
                CreateGetbyLikeColume(table, storedProcedurePrefix, dtoSuffix, streamWriter);
               
                //CreateDeleteAllByMethods(table, storedProcedurePrefix, streamWriter);
                //CreateSelectMethod(table, storedProcedurePrefix, dtoSuffix, streamWriter);
                //CreateSelectJsonMethod(table, storedProcedurePrefix, dtoSuffix, streamWriter);
                //CreateSelectAllMethod(table, storedProcedurePrefix, dtoSuffix, streamWriter);
                // CreateSelectAllJsonMethod(table, storedProcedurePrefix, dtoSuffix, streamWriter);
                //CreateSelectAllByMethods(table, storedProcedurePrefix, dtoSuffix, streamWriter);
                
                //CreateMapMethod(table, dtoSuffix, streamWriter);

				streamWriter.WriteLine();
				streamWriter.WriteLine("\t\t#endregion");

				// Close out the class and namespace
				streamWriter.WriteLine("\t}");
				streamWriter.WriteLine("}");
			}
		}

        public static void CreateBusinessClass(string databaseName, Table table, string targetNamespace, string storedProcedurePrefix, string daoSuffix, string dtoSuffix, string path, string busSuffix)
        {
            string classNameController = Utility.FormatClassName(table.Name) + daoSuffix;
            string variableClassNameController= "c"+Utility.FormatClassName(table.Name) + daoSuffix;
            string className = Utility.FormatClassName(table.Name) + busSuffix;
            string NameEntity = Utility.FormatClassName(table.Name) + dtoSuffix;
            string variableEntity= "obj"+Utility.FormatClassName(table.Name) + dtoSuffix;
            path = Path.Combine(path, "Business");

            using (StreamWriter streamWriter = new StreamWriter(Path.Combine(path, className + ".cs")))
            {
                // Create the header for the class
                streamWriter.WriteLine("using System;");
                streamWriter.WriteLine("using System.Linq;");

                streamWriter.WriteLine("using System.Collections.Generic;");
                streamWriter.WriteLine("using System.Data;");
                streamWriter.WriteLine("using System.Data.SqlClient;");
                streamWriter.WriteLine("using Entity;");
                streamWriter.WriteLine("using DataAccess;");
                streamWriter.WriteLine();

                streamWriter.WriteLine("namespace Business");
                streamWriter.WriteLine("{");

                streamWriter.WriteLine("\tpublic class " + className + "");
                streamWriter.WriteLine("\t{");
                // Append the access methods
                streamWriter.WriteLine("\t\t#region Methods");
               
                streamWriter.WriteLine("\t\t//Get All Table");
                streamWriter.WriteLine("\t\tpublic DataTable GetAll()");
                streamWriter.WriteLine("\t\t{");
                streamWriter.WriteLine("\t\t\t"+classNameController+" "+ variableClassNameController +"= new "+ classNameController+"();");
                streamWriter.WriteLine("\t\t\treturn " + variableClassNameController + ".GetAll();");
                streamWriter.WriteLine("\t\t}");
                streamWriter.WriteLine();

                //streamWriter.WriteLine("\t\t//Get All ToList");
                //streamWriter.WriteLine("\t\tpublic List<"+NameEntity+"> GetAllToList()");
                //streamWriter.WriteLine("\t\t{");
                //streamWriter.WriteLine("\t\t\t" + classNameController +" "+ variableClassNameController + "= new " + classNameController + "();");
                //streamWriter.WriteLine("\t\t\treturn " + variableClassNameController + ".GetAllToList();");
                //streamWriter.WriteLine("\t\t}");
                //streamWriter.WriteLine();

                streamWriter.WriteLine("\t\t//Lấy dữ liệu ra  dữ liệu theo ID khóa chính");
                streamWriter.WriteLine("\t\tpublic DataTable GetAllByID("+ NameEntity+" "+ variableEntity+")");
                streamWriter.WriteLine("\t\t{");
                streamWriter.WriteLine("\t\t\t" + classNameController +" " + variableClassNameController + "= new " + classNameController + "();");
                streamWriter.WriteLine("\t\t\treturn " + variableClassNameController + ".GetAllByID("+ variableEntity + ");");
                streamWriter.WriteLine("\t\t}");
 
                //Lấy dữ liệu của bảng theo ID Khóa Ngoại
                foreach (List<Column> compositeKeyList in table.ForeignKeys.Values)
                {
                    StringBuilder stringBuilder = new StringBuilder(255);
                    stringBuilder.Append("SelectAllBy");
                    for (int i = 0; i < compositeKeyList.Count; i++)
                    {
                        Column column = compositeKeyList[i];

                        if (i > 0)
                        {
                            stringBuilder.Append("_" + Utility.FormatPascal(column.Name));
                        }
                        else
                        {
                            stringBuilder.Append(Utility.FormatPascal(column.Name));
                        }
                    }
                    string methodName = stringBuilder.ToString();
                    //Lay ra ten cua thu tuc
                    string procedureName = storedProcedurePrefix + table.Name + methodName;

                    //Tao ham select theo khoa ngoai trong lop BUS
                    streamWriter.WriteLine("\t\tpublic DataTable " + methodName + "("+NameEntity + " " + variableEntity + ")");
                    streamWriter.WriteLine("\t\t{");
                    streamWriter.WriteLine("\t\t\t" + classNameController + " " + variableClassNameController + "= new " + classNameController + "();");
                    //Lấy ra tên cột
                  
                    for (int i = 0; i < compositeKeyList.Count; i++)
                    {
                        Column column = compositeKeyList[i];
                        streamWriter.WriteLine("\t\t\treturn " + variableClassNameController + "." + methodName + "(" + variableEntity + "." + Utility.FormatPascal(column.Name) + ");");
                        if (i < (compositeKeyList.Count - 1))
                        {
                            streamWriter.Write(",");
                        }

                        streamWriter.WriteLine();
                    }
                    streamWriter.WriteLine("\t\t}");
                }

                streamWriter.WriteLine("\t\t//Insert ");
                streamWriter.WriteLine("\t\tpublic void Insert(" + NameEntity +" "+ variableEntity + ")");
                streamWriter.WriteLine("\t\t{");
                streamWriter.WriteLine("\t\t\t" + classNameController +" "+ variableClassNameController + "= new " + classNameController + "();");
                streamWriter.WriteLine("\t\t\t" + variableClassNameController + ".Insert(" + variableEntity + ");");
                streamWriter.WriteLine("\t\t}");
                streamWriter.WriteLine();

                streamWriter.WriteLine("\t\t//Delete ");
                streamWriter.WriteLine("\t\tpublic void Delete(" + NameEntity + " " + variableEntity + ")");
                streamWriter.WriteLine("\t\t{");
                streamWriter.WriteLine("\t\t\t" + classNameController + " " + variableClassNameController + "= new " + classNameController + "();");
                streamWriter.WriteLine("\t\t\t" + variableClassNameController + ".Delete(" + variableEntity + ");");
                streamWriter.WriteLine("\t\t}");
                streamWriter.WriteLine();

                streamWriter.WriteLine("\t\t//Update ");
                streamWriter.WriteLine("\t\tpublic void Update(" + NameEntity + " " + variableEntity + ")");
                streamWriter.WriteLine("\t\t{");
                streamWriter.WriteLine("\t\t\t" + classNameController + " " + variableClassNameController + "= new " + classNameController + "();");
                streamWriter.WriteLine("\t\t\t" + variableClassNameController + ".Update(" + variableEntity + ");");
                streamWriter.WriteLine("\t\t}");
                streamWriter.WriteLine();

                streamWriter.WriteLine("\t\t//Get table with column by parammert and sort  ");
                streamWriter.WriteLine("\t\tpublic DataTable GetByColumn(string Col, string Pal, string sort)");
                streamWriter.WriteLine("\t\t{");
                streamWriter.WriteLine("\t\t\t" + classNameController + " " + variableClassNameController + "= new " + classNameController + "();");
                streamWriter.WriteLine("\t\t\treturn " + variableClassNameController + ".GetByColumn(Col, Pal, sort);");
                streamWriter.WriteLine("\t\t}");
                streamWriter.WriteLine();

                streamWriter.WriteLine("\t\t//Get table by ID  ");
                streamWriter.WriteLine("\t\tpublic DataTable QueryGetByColumn(string Col, string Pal, string sort)");
                streamWriter.WriteLine("\t\t{");
                streamWriter.WriteLine("\t\t\t" + classNameController + " " + variableClassNameController + "= new " + classNameController + "();");
                streamWriter.WriteLine("\t\t\treturn " + variableClassNameController + ".GetByColumn(Col, Pal, sort);");
                streamWriter.WriteLine("\t\t}");
                streamWriter.WriteLine();





                streamWriter.WriteLine("\t\t#endregion");
                streamWriter.WriteLine("\t}");
                streamWriter.WriteLine("}"); 
                // Close out the class and namespace
            }
        }
        //Tạo lớp SqlDataProvider
        public static void CreateSqlDataProviderClass(string targetNamespace, string path)
        {
            path = Path.Combine(path, "DataAccess");
            using (StreamWriter streamWriter = new StreamWriter(Path.Combine(path, "SqlDataProvider" + ".cs")))
            {
                // Create the header for the class
                streamWriter.WriteLine("using System;");
                streamWriter.WriteLine("using System.Linq;");
                streamWriter.WriteLine("using System.Configuration;");
                streamWriter.WriteLine("using System.Collections.Generic;");
                streamWriter.WriteLine("using System.Data;");
                streamWriter.WriteLine("using System.Data.SqlClient;");
                streamWriter.WriteLine("using Entity;");
                streamWriter.WriteLine();
          
                streamWriter.WriteLine("namespace DataAccess");
                streamWriter.WriteLine("{");

                streamWriter.WriteLine("\t public class SqlDataProvider");
                streamWriter.WriteLine("\t{");
                // Append the access methods
                streamWriter.Write("\t\tpublic static string strConStr = ConfigurationManager.ConnectionStrings[");
                streamWriter.Write('"');
                streamWriter.Write("strConnect");
                streamWriter.Write('"');
                streamWriter.WriteLine("].ConnectionString;");

                streamWriter.WriteLine("\t\tpublic static SqlConnection connection=new SqlConnection(strConStr);");
                streamWriter.WriteLine("\t\tpublic SqlDataProvider()");
                streamWriter.WriteLine("\t\t{");
                streamWriter.WriteLine("\t\t\tif (connection == null) { connection = new SqlConnection(strConStr); }");
                streamWriter.WriteLine("\t\t}");

                streamWriter.WriteLine("\t\t public static SqlConnection GetConnection()");
                streamWriter.WriteLine("\t\t{");
                streamWriter.WriteLine("\t\t\tif (connection.State==ConnectionState.Closed)");
                streamWriter.WriteLine("\t\t\t{");
                streamWriter.WriteLine("\t\t\tconnection.Open();");
                streamWriter.WriteLine("\t\t\treturn connection;");
                streamWriter.WriteLine("\t\t\t}");
                streamWriter.WriteLine("\t\t\telse return connection;");
                streamWriter.WriteLine("\t\t}");

                streamWriter.WriteLine("\t\tprivate static SqlCommand GetCommand(string sql)");
                streamWriter.WriteLine("\t\t{");
                streamWriter.WriteLine("\t\t\tSqlCommand cmd = new SqlCommand(sql, GetConnection());");
                streamWriter.WriteLine("\t\t\tconnection.Close();");
                streamWriter.WriteLine("\t\t\treturn cmd;");
                streamWriter.WriteLine("\t\t}");

                streamWriter.WriteLine("\t\tpublic static DataTable GetData(string sql)"); 
                streamWriter.WriteLine("\t\t{");
                streamWriter.WriteLine("\t\t\treturn GetData(GetCommand(sql));");
                streamWriter.WriteLine("\t\t}");

                streamWriter.WriteLine("\t\tpublic static DataTable GetData(SqlCommand cmd)");
                streamWriter.WriteLine("\t\t{");
                streamWriter.WriteLine("\t\t\ttry"); 
                streamWriter.WriteLine("\t\t\t{");
                streamWriter.WriteLine("\t\t\tif (cmd.Connection == null) { cmd.Connection = GetConnection(); }");
                streamWriter.WriteLine("\t\t\tusing (SqlDataAdapter da = new SqlDataAdapter())");
                streamWriter.WriteLine("\t\t\t\t{");
                streamWriter.WriteLine("\t\t\t\t\tda.SelectCommand = cmd;");
                streamWriter.WriteLine("\t\t\t\t\tDataTable dt=new DataTable();");
                streamWriter.WriteLine("\t\t\t\t\t da.Fill(dt);");
                streamWriter.WriteLine("\t\t\t\t\tGetConnection().Close();");
                streamWriter.WriteLine("\t\t\t\t\treturn dt;");
                streamWriter.WriteLine("\t\t\t\t}");

                streamWriter.WriteLine("\t\t\t}");
                streamWriter.WriteLine("\t\t\tfinally");
                streamWriter.WriteLine("\t\t\t{");
                streamWriter.WriteLine("\t\t\t\tconnection.Close();");
                streamWriter.WriteLine("\t\t\t}");
                streamWriter.WriteLine("\t\t}");
                streamWriter.WriteLine("\t\t");

                streamWriter.WriteLine("\t\tpublic static void ExecuteNonQuery(string sql)");
                streamWriter.WriteLine("\t\t{");
                streamWriter.WriteLine("\t\t\tExecuteNonQuery(GetCommand(sql));");
                streamWriter.WriteLine("\t\t}");
                streamWriter.WriteLine("\t\t");

                streamWriter.WriteLine("\t\tpublic static void ExecuteNonQuery(SqlCommand cmd)");
                streamWriter.WriteLine("\t\t{");
                streamWriter.WriteLine("\t\t\tcmd.Connection = GetConnection();");
                streamWriter.WriteLine("\t\t\tcmd.ExecuteNonQuery();");
                streamWriter.WriteLine("\t\t}");
                streamWriter.WriteLine("\t\t");

                streamWriter.WriteLine("\t\tpublic static object ExecuteScalar(string sql)");
                streamWriter.WriteLine("\t\t{");
                streamWriter.WriteLine("\t\t\treturn ExecuteScalar(GetCommand(sql));");
                streamWriter.WriteLine("\t\t}");
                streamWriter.WriteLine("\t\t");

                streamWriter.WriteLine("\t\tpublic static object ExecuteScalar(SqlCommand cmd)");
                streamWriter.WriteLine("\t\t{");
                streamWriter.WriteLine("\t\t\ttry");
                streamWriter.WriteLine("\t\t\t{");
                streamWriter.WriteLine("\t\t\t\tif (cmd.Connection == null) { cmd.Connection = GetConnection(); }");
                streamWriter.WriteLine("\t\t\t\treturn cmd.ExecuteScalar();");
                streamWriter.WriteLine("\t\t\t}");
                streamWriter.WriteLine("\t\t\tfinally");
                streamWriter.WriteLine("\t\t\t{");
                streamWriter.WriteLine("\t\t\t\tconnection.Close();");
                streamWriter.WriteLine("\t\t\t}");
                streamWriter.WriteLine("\t\t}");
               
                streamWriter.WriteLine();
                // Close out the class and namespace
                streamWriter.WriteLine("\t}");
                streamWriter.WriteLine("}");
            }
        }

        //Tạo chuỗi kết nối
        public static void CreateWebconfigClass(string connectionString,string path)
        {
            using (StreamWriter streamWriter = new StreamWriter(Path.Combine(path, "Web.Config")))
            {
                streamWriter.WriteLine("\t\t<configuration>");
                streamWriter.WriteLine("\t\t\t <connectionStrings>");
                streamWriter.Write("\t\t\t\t<add name=");
                streamWriter.Write('"');
                streamWriter.Write("strConnect");
                streamWriter.Write('"');
                streamWriter.Write(" connectionString=");
                streamWriter.Write('"');
                streamWriter.Write(connectionString);
                streamWriter.WriteLine('"');
                streamWriter.WriteLine("\t\t\t\t\t\t\tproviderName=" + '"' + "System.Data.SqlClient" + '"' + " />");
                streamWriter.WriteLine("\t\t\t</connectionStrings>");
                streamWriter.WriteLine("\t\t</configuration>");
            }
        }
        public static void CreateAppconfigClass(string connectionString, string path)
        {
            using (StreamWriter streamWriter = new StreamWriter(Path.Combine(path, "App.Config")))
            {//"+'"'+"
                streamWriter.WriteLine("<?xml version="+'"'+ "1.0" + '"' + " encoding=" + '"' + "utf-8" + '"' + " ?>");
                streamWriter.WriteLine("\t\t<configuration>");
                streamWriter.WriteLine("\t\t\t <connectionStrings>");
                streamWriter.Write("\t\t\t\t<add name=");
                streamWriter.Write('"');
                streamWriter.Write("strConnect");
                streamWriter.Write('"');
                streamWriter.Write(" connectionString=");
                streamWriter.Write('"');
                streamWriter.Write(connectionString);
                streamWriter.WriteLine('"');
                streamWriter.WriteLine("\t\t\t\t\t\t\tproviderName=" + '"' + "System.Data.SqlClient" + '"' + " />");
                streamWriter.WriteLine("\t\t\t</connectionStrings>");
                streamWriter.WriteLine("\t\t</configuration>");
            }
        }
        ///<summary> Tạo phương thức tìm kiếm
        /// </summary>
        /// <param name="table">bảng thể hiện cho phương thức table .</param>
        /// <param name="storedProcedurePrefix">Thủ tục sẽ được gọi.</param>
        /// <param name="dtoSuffix">Tên của mới lớp entiy.</param>
        /// <param name="streamWriter">The StreamWriter instance that will be used to create the method.</param>

        private static void CreateGetAllMethod(Table table, string storedProcedurePrefix, string dtoSuffix, StreamWriter streamWriter)
        {
            string className = Utility.FormatClassName(table.Name) + dtoSuffix;
            string variableName = Utility.FormatVariableName(table.Name);
            // Append the method header
            streamWriter.WriteLine("\t\t/// <summary>");
            streamWriter.WriteLine("\t\t/// Saves a record to the " + table.Name + " table.");
            streamWriter.WriteLine("\t\t/// </summary>");
            streamWriter.WriteLine("\t\tpublic DataTable QueryGetAll()");
            streamWriter.WriteLine("\t\t{");
            //----create query -------------------------------------
            streamWriter.WriteLine("\n\t\t string str="+'"'+"select * from [" + table.Name + "]"+'"'+";");
            streamWriter.WriteLine("\n\t\t  SqlCommand cmd = new SqlCommand();");
            streamWriter.WriteLine("\n\t\t  cmd.CommandType = CommandType.Text;");
            streamWriter.WriteLine("\n\t\t  cmd.CommandText = str;");
            streamWriter.WriteLine();
            streamWriter.WriteLine("\t\treturn GetData(cmd);");
           
            streamWriter.WriteLine("\t\t }");
            streamWriter.WriteLine();
        }
        //Get All voi query la chuoi truyen vao

        private static void CreateGetAllMethodByQuery(Table table, string storedProcedurePrefix, string dtoSuffix, StreamWriter streamWriter)
        {
            string className = Utility.FormatClassName(table.Name) + dtoSuffix;
            string variableName = Utility.FormatVariableName(table.Name);
            // Append the method header

            streamWriter.WriteLine("\n\t\t public DataTable QueryGetAllByCode(string str)");
            streamWriter.WriteLine("\t\t{");
            //----create query -------------------------------------
            streamWriter.WriteLine("\t\treturn GetData(str);");
            // Append the method footer
            streamWriter.WriteLine("\t\t}");
            streamWriter.WriteLine();
        }
        //
        private static void CreateGetAllMethodOrderByColumn(Table table, string storedProcedurePrefix, string dtoSuffix, StreamWriter streamWriter)
        {
            
            streamWriter.WriteLine("\t//Lấy dữ liệu 1 cột và sắp theo điều kiện.Được truyền vào từ Form ");
            streamWriter.WriteLine("\t\tpublic DataTable QueryGetOrderByColumn(string Col, string sort)");
            streamWriter.WriteLine("\t\t{");
            //----create query -------------------------------------
            streamWriter.WriteLine("\t\t\tstring str = \"select * from ["+table.Name+"]  order by [\" + Col + \"] \" + sort;");
            streamWriter.WriteLine("\t\t\tSqlCommand cmd = new SqlCommand();");
            streamWriter.WriteLine("\t\t\tcmd.CommandType = CommandType.Text; ");
            streamWriter.WriteLine("\t\t\tcmd.CommandText = str; ");
            streamWriter.WriteLine("\t\t\treturn GetData(cmd);");
            streamWriter.WriteLine("\t\t}");

            streamWriter.WriteLine("\t\t//Lấy dữ liệu theo tên cột, giá trị và kiểu sắp xếp. Được truyền vào từ Form");
            streamWriter.WriteLine("\t\tpublic DataTable GetByColumn(string Col, string Pal,string sort)");
            streamWriter.WriteLine("\t\t{");
            streamWriter.WriteLine("\t\t\tstring str = \"select * from [Users] where \" + Col + \"=\" + \"'\" + Pal + \"' Order by [\" + Col + \"] \" + sort;");
            streamWriter.WriteLine("\t\t\tSqlCommand cmd = new SqlCommand();");
            streamWriter.WriteLine("\t\t\tcmd.CommandType = CommandType.Text;");
            streamWriter.WriteLine("\t\t\tcmd.CommandText = str;");
            streamWriter.WriteLine("\t\t\treturn GetData(cmd);");
            streamWriter.WriteLine("\t\t}");
            
            streamWriter.WriteLine();
        }
        private static void CreateGetByIDMethod(Table table, string storedProcedurePrefix, string dtoSuffix, StreamWriter streamWriter)
        {
            string className = Utility.FormatClassName(table.Name) + dtoSuffix;
            string variableName = "obj" + Utility.FormatVariableName(table.Name);
            // Append the method header
            streamWriter.WriteLine("\t\t/// <summary>");
            streamWriter.WriteLine("\t\t/// Saves a record to the " + table.Name + " table.");
            streamWriter.WriteLine("\t\t/// </summary>");
            streamWriter.WriteLine("\t\tpublic DataTable QueryGetById(" + className + " " + variableName + ")");
            streamWriter.WriteLine("\t\t{");
            streamWriter.Write("\t\t\tstring str=" + '"' + "select * from [" + table.Name + "] where ");
            // lấy ra khóa chính của bảng
            for (int i = 0; i < table.PrimaryKeys.Count; i++)
            {
                Column column = table.PrimaryKeys[i];

                if (i == 0)
                {
                    streamWriter.Write(" [" + column.Name + "] = @" + column.Name);
                }
                else
                {
                    streamWriter.Write("\tand [" + column.Name + "] = @" + column.Name);
                }
            }
            streamWriter.Write('"');
            streamWriter.WriteLine(";");
            streamWriter.WriteLine("\t\t\tSqlCommand cmd= new SqlCommand();");
            streamWriter.WriteLine("\t\t\tcmd.CommandType = CommandType.Text;");
            streamWriter.WriteLine("\t\t\tcmd.CommandText = str;");
            
            // Append the parameter declarations
            streamWriter.WriteLine("\t\t\t\tSqlParameter[] parameters = new SqlParameter[]");
            streamWriter.WriteLine("\t\t\t{");
            for (int i = 0; i < table.PrimaryKeys.Count; i++)
            {
                Column column = table.PrimaryKeys[i];

                streamWriter.Write("\t\t\t\t" + Utility.CreateSqlParameter(table, column));
                if (i < (table.PrimaryKeys.Count - 1))
                {
                    streamWriter.Write(",");
                }

                streamWriter.WriteLine();
            }
            streamWriter.WriteLine("\t\t\t};");
            streamWriter.WriteLine();
            streamWriter.WriteLine("\t\t\tfor (int i = 0; i < parameters.Count(); i++)");
            streamWriter.WriteLine("\t\t\t{");
            streamWriter.WriteLine("\t\t\t\tcmd.Parameters.Add(parameters[i]);");
            streamWriter.WriteLine("\t\t\t}");
            streamWriter.WriteLine("\t\t\treturn GetData(cmd);");


            // Append the method footer
            streamWriter.WriteLine("\t\t}");
            streamWriter.WriteLine();
        }
  
       
        public static void CreateGetbyLikeColume(Table table, string storedProcedurePrefix, string dtoSuffix, StreamWriter streamWriter)
        {
            streamWriter.WriteLine();
            streamWriter.WriteLine("\t\tpublic DataTable QueryGetLike(string Col,string Pal)");
            streamWriter.WriteLine("\t\t{");
            //----create query -------------------------------------
            streamWriter.Write("\t\tstring str=" + '"' + "select * from [" + table.Name + "] where ");
            streamWriter.Write('"');
            streamWriter.Write(" + Col + ");
            streamWriter.Write('"');
            streamWriter.Write(" like ");
            streamWriter.Write('"');
            streamWriter.Write("+");
            streamWriter.Write('"');
            streamWriter.Write("'%");
            streamWriter.Write('"');
            streamWriter.Write(" + Pal +");
            streamWriter.Write('"');
            streamWriter.Write("%'");
            streamWriter.Write('"');
            streamWriter.WriteLine(";");


            streamWriter.WriteLine("return GetData(str);");
            // Append the method footer
            streamWriter.WriteLine("\t\t}");
            streamWriter.WriteLine();
        }
        private static void CreateInsertMethod(Table table, string storedProcedurePrefix, string dtoSuffix, StreamWriter streamWriter)
		{
			string className = Utility.FormatClassName(table.Name) + dtoSuffix;
			string variableName = "obj"+Utility.FormatVariableName(table.Name);
            // Append the method header
			streamWriter.WriteLine("\t\t/// <summary>");
			streamWriter.WriteLine("\t\t/// Saves a record to the " + table.Name + " table.");
			streamWriter.WriteLine("\t\t/// </summary>");
			streamWriter.WriteLine("\t\tpublic void Insert(" + className + " " + variableName + ")");
			streamWriter.WriteLine("\t\t{");
			streamWriter.WriteLine("\t\t");
          //  SqlGenerator.CreateInsertStoredProcedure(databaseName, table, grantLoginName, storedProcedurePrefix, sqlPath, createMultipleFiles);
       
             streamWriter.WriteLine("\t\tSqlCommand cmd= new SqlCommand();");
             streamWriter.WriteLine("\t\tcmd.CommandType = CommandType.StoredProcedure;");
             streamWriter.WriteLine("\t\tcmd.CommandText = " + '"'+table.Name+ "Insert"+'"'+";");
             streamWriter.WriteLine("\t\tcmd.Connection = GetConnection();");         
			 streamWriter.WriteLine();
			
			// Append the parameter declarations
			streamWriter.WriteLine("\t\t\tSqlParameter[] parameters = new SqlParameter[]");
			streamWriter.WriteLine("\t\t\t{");
			for (int i = 0; i < table.Columns.Count; i++)
			{
				Column column = table.Columns[i];
				if (column.IsIdentity == false && column.IsRowGuidCol == false)
				{
					streamWriter.Write("\t\t\t\t" + Utility.CreateSqlParameter(table, column));
					if (i < (table.Columns.Count - 1))
					{
						streamWriter.Write(",");
					}
					
					streamWriter.WriteLine();
				}
			}

			streamWriter.WriteLine("\t\t\t};");
			streamWriter.WriteLine();
            streamWriter.WriteLine("\t\tfor (int i = 0; i < parameters.Count(); i++)");
            streamWriter.WriteLine("\t\t\t{");
            streamWriter.WriteLine("\t\t\tcmd.Parameters.Add(parameters[i]);");
            streamWriter.WriteLine("\t\t\t}");
            streamWriter.WriteLine("\t\t\tExecuteNonQuery(cmd);");
           
         
			// Append the method footer
			streamWriter.WriteLine("\t\t}");


			streamWriter.WriteLine();
		}

		/// <summary>
		/// Creates a string that represents the update functionality of the data access class.
		/// </summary>
		/// <param name="table">The Table instance that this method will be created for.</param>
		/// <param name="storedProcedurePrefix">The prefix that is used on the stored procedure that this method will call.</param>
        /// <param name="dtoSuffix">The suffix to append to the name of each data transfer class.</param>
		/// <param name="streamWriter">The StreamWriter instance that will be used to create the method.</param>
		private static void CreateUpdateMethod(Table table, string storedProcedurePrefix, string dtoSuffix, StreamWriter streamWriter)
		{
            string className = Utility.FormatClassName(table.Name) + dtoSuffix;
            string variableName = "obj" + Utility.FormatVariableName(table.Name);
            // Append the method header
            streamWriter.WriteLine("\t\t/// <summary>");
            streamWriter.WriteLine("\t\t/// Saves a record to the " + table.Name + " table.");
            streamWriter.WriteLine("\t\t/// </summary>");
            streamWriter.WriteLine("\t\tpublic void Update(" + className + " " + variableName + ")");
            streamWriter.WriteLine("\t\t{");

            streamWriter.WriteLine("\t\t");
 
            streamWriter.WriteLine("\t\tSqlCommand cmd= new SqlCommand();");
            streamWriter.WriteLine("\t\tcmd.CommandType = CommandType.StoredProcedure;");
            streamWriter.WriteLine("\t\tcmd.CommandText = " + '"' + table.Name + "Update" + '"' + ";");
            streamWriter.WriteLine("\t\tcmd.Connection = GetConnection();");
            streamWriter.WriteLine();

            // Append the parameter declarations
            streamWriter.WriteLine("\t\t\tSqlParameter[] parameters = new SqlParameter[]");
            streamWriter.WriteLine("\t\t\t{");
            for (int i = 0; i < table.Columns.Count; i++)
            {
                Column column = table.Columns[i];
              streamWriter.Write("\t\t\t\t" + Utility.CreateSqlParameter(table, column));
                    if (i < (table.Columns.Count - 1))
                    {
                        streamWriter.Write(",");
                    }

                    streamWriter.WriteLine();
               
            }

            streamWriter.WriteLine("\t\t\t};");
            streamWriter.WriteLine();
            streamWriter.WriteLine("\t\tfor (int i = 0; i < parameters.Count(); i++)");
            streamWriter.WriteLine("\t\t\t{");
            streamWriter.WriteLine("\t\t\tcmd.Parameters.Add(parameters[i]);");
            streamWriter.WriteLine("\t\t\t}");
            streamWriter.WriteLine("\t\t\tExecuteNonQuery(cmd);");


            // Append the method footer
            streamWriter.WriteLine("\t\t}");


            streamWriter.WriteLine();
        }
		/// <summary>
		/// Creates a string that represents the delete functionality of the data access class.
		/// </summary>
		/// <param name="table">The Table instance that this method will be created for.</param>
		/// <param name="storedProcedurePrefix">The prefix that is used on the stored procedure that this method will call.</param>
		/// <param name="streamWriter">The StreamWriter instance that will be used to create the method.</param>
		private static void CreateDeleteMethod(Table table, string storedProcedurePrefix,  string dtoSuffix,StreamWriter streamWriter)
        {
            string className = Utility.FormatClassName(table.Name) + dtoSuffix;
            string variableName = "obj" + Utility.FormatVariableName(table.Name);
            if (table.PrimaryKeys.Count > 0)
			{
				// Append the method header
				streamWriter.WriteLine("\t\t/// <summary>");
				streamWriter.WriteLine("\t\t/// Deletes a record from the " + table.Name + " table by its primary key.");
				streamWriter.WriteLine("\t\t/// </summary>");
                streamWriter.WriteLine("\t\tpublic void Delete(" + className + " " + variableName + ")");

				streamWriter.WriteLine("\t\t{");
               
                streamWriter.WriteLine("\t\tSqlCommand cmd= new SqlCommand();");
                streamWriter.WriteLine("\t\tcmd.CommandType = CommandType.StoredProcedure;");
                streamWriter.WriteLine("\t\tcmd.CommandText = " + '"' + table.Name + "Delete" + '"' + ";");
                streamWriter.WriteLine();
               // Append the parameter declarations
				streamWriter.WriteLine("\t\t\tSqlParameter[] parameters = new SqlParameter[]");
				streamWriter.WriteLine("\t\t\t{");
				for (int i = 0; i < table.PrimaryKeys.Count; i++)
				{
					Column column = table.PrimaryKeys[i];

                    streamWriter.Write("\t\t\t\t" + Utility.CreateSqlParameter(table, column));
                 //   streamWriter.WriteLine("//////////////tesssssssss");
				//	streamWriter.Write("\t\t\t\tnew SqlParameter(\"@" + column.Name + "\", " +table.Name+"."+ Utility.FormatCamel(column.Name) + ")");
					if (i < (table.PrimaryKeys.Count - 1))
					{
						streamWriter.Write(",");
					}
                  
					streamWriter.WriteLine();
				}
                streamWriter.WriteLine("\t\t\t};");
                streamWriter.WriteLine();
                streamWriter.WriteLine("\t\tfor (int i = 0; i < parameters.Count(); i++)");
                streamWriter.WriteLine("\t\t\t{");
                streamWriter.WriteLine("\t\t\tcmd.Parameters.Add(parameters[i]);");
                streamWriter.WriteLine("\t\t\t}");
                streamWriter.WriteLine("\t\t\tExecuteNonQuery(cmd);");


                // Append the method footer
                streamWriter.WriteLine("\t\t}");


                streamWriter.WriteLine();
			}
		}

		/// <summary>
		/// Creates a string that represents the "delete by" functionality of the data access class.
		/// </summary>
		/// <param name="table">The Table instance that this method will be created for.</param>
		/// <param name="storedProcedurePrefix">The prefix that is used on the stored procedure that this method will call.</param>
		/// <param name="streamWriter">The StreamWriter instance that will be used to create the method.</param>
		private static void CreateDeleteAllByMethods(Table table, string storedProcedurePrefix, StreamWriter streamWriter)
		{
			// Create a stored procedure for each foreign key

			foreach (List<Column> compositeKeyList in table.ForeignKeys.Values)
			{
				// Create the stored procedure name
				StringBuilder stringBuilder = new StringBuilder(255);
				stringBuilder.Append("DeleteAllBy");
				for (int i = 0; i < compositeKeyList.Count; i++)
				{
					Column column = compositeKeyList[i];

					if (i > 0)
					{
						stringBuilder.Append("_" + Utility.FormatPascal(column.Name));
					}
					else
					{
						stringBuilder.Append(Utility.FormatPascal(column.Name));
					}

				}
				string methodName = stringBuilder.ToString();
				string procedureName = storedProcedurePrefix + table.Name + methodName;

				// Create the delete function based on keys
				// Append the method header
				streamWriter.WriteLine("\t\t/// <summary>");
				streamWriter.WriteLine("\t\t/// Deletes a record from the " + table.Name + " table by a foreign key.");
				streamWriter.WriteLine("\t\t/// </summary>");

				streamWriter.Write("\t\tpublic void " + methodName + "(");
				for (int i = 0; i < compositeKeyList.Count; i++)
				{
					Column column = compositeKeyList[i];
					streamWriter.Write(Utility.CreateMethodParameter(column));
					if (i < (compositeKeyList.Count - 1))
					{
						streamWriter.Write(", ");
					}
				}
				streamWriter.WriteLine(")");
				streamWriter.WriteLine("\t\t{");

				// Append the parameter declarations
				streamWriter.WriteLine("\t\t\tSqlParameter[] parameters = new SqlParameter[]");
				streamWriter.WriteLine("\t\t\t{");
				for (int i = 0; i < compositeKeyList.Count; i++)
				{
					Column column = compositeKeyList[i];
					streamWriter.Write("\t\t\t\tnew SqlParameter(\"@" + column.Name + "\", " + Utility.FormatCamel(column.Name) + ")");
					if (i < (compositeKeyList.Count - 1))
					{
						streamWriter.Write(",");
					}

					streamWriter.WriteLine();
				}
                streamWriter.WriteLine("\t\t\t};");
                streamWriter.WriteLine();
                streamWriter.WriteLine("\t\tfor (int i = 0; i < parameters.Count(); i++)");
                streamWriter.WriteLine("\t\t\t{;");
                streamWriter.WriteLine("\t\t\tcmd.Parameters.Add(parameters[i]);");
                streamWriter.WriteLine("\t\t\t};");
                streamWriter.WriteLine("\t\t\tExecuteNonQuery(cmd);");


                // Append the method footer
                streamWriter.WriteLine("\t\t}");


                streamWriter.WriteLine();
			}
		}

		/// <summary>
		/// Creates a string that represents the select by primary key functionality of the data access class.
		/// </summary>
		/// <param name="table">The Table instance that this method will be created for.</param>
		/// <param name="storedProcedurePrefix">The prefix that is used on the stored procedure that this method will call.</param>
        /// <param name="dtoSuffix">The suffix to append to the name of each data transfer class.</param>
		/// <param name="streamWriter">The StreamWriter instance that will be used to create the method.</param>
		private static void CreateSelectMethodByID(Table table, string storedProcedurePrefix, string dtoSuffix, StreamWriter streamWriter)
		{
            string className = Utility.FormatClassName(table.Name) + dtoSuffix;
            string variableName = "obj" + Utility.FormatVariableName(table.Name);
            string procedureName = storedProcedurePrefix + table.Name + "Select";
            // Append the method header
            streamWriter.WriteLine("\t\t/// <summary>");
            streamWriter.WriteLine("\t\t/// Saves a record to the " + table.Name + " table.");
            streamWriter.WriteLine("\t\t/// </summary>");
            streamWriter.WriteLine("\t\tpublic DataTable GetAllByID("+ className + " "+ variableName + ")");
            streamWriter.WriteLine("\t\t{");
            //----create query -------------------------------------

            streamWriter.WriteLine("\t\t\tSqlCommand cmd = new SqlCommand();");
            streamWriter.WriteLine("\t\t\tcmd.CommandType = CommandType.StoredProcedure;");
            streamWriter.WriteLine("\t\t\tcmd.CommandText = " + "\"" + procedureName + "\";");
            streamWriter.WriteLine("\t\t\tSqlParameter[] parameters = new SqlParameter[]");
            streamWriter.WriteLine("\t\t{");
            for (int i = 0; i < table.PrimaryKeys.Count; i++)
            {
                Column column = table.PrimaryKeys[i];
                streamWriter.Write("\t\t\tnew SqlParameter(\"@" + column.Name + "\", " + variableName +"."+ Utility.FormatPascal(column.Name) + ")");
                if (i < (table.PrimaryKeys.Count - 1))
                {
                    streamWriter.Write(",");
                }

                streamWriter.WriteLine();
            }

            streamWriter.WriteLine("\t\t};");
            streamWriter.WriteLine("\t\tfor (int i = 0; i < parameters.Count(); i++)");
            streamWriter.WriteLine("\t\t{");
            streamWriter.WriteLine("\t\t\tcmd.Parameters.Add(parameters[i]);");
            streamWriter.WriteLine("\t\t}");

            streamWriter.WriteLine("\t\treturn GetData(cmd);");

            streamWriter.WriteLine("\t\t }");
            streamWriter.WriteLine();
        }

		/// <summary>
		/// Creates a string that represents the select JSON by primary key functionality of the data access class.
		/// </summary>
		/// <param name="table">The Table instance that this method will be created for.</param>
		/// <param name="storedProcedurePrefix">The prefix that is used on the stored procedure that this method will call.</param>
        /// <param name="dtoSuffix">The suffix to append to the name of each data transfer class.</param>
		/// <param name="streamWriter">The StreamWriter instance that will be used to create the method.</param>
		private static void CreateSelectJsonMethod(Table table, string storedProcedurePrefix, string dtoSuffix, StreamWriter streamWriter)
		{
			if (table.PrimaryKeys.Count > 0 && table.Columns.Count != table.ForeignKeys.Count)
			{
				string className = Utility.FormatClassName(table.Name) + dtoSuffix;

				// Append the method header
				streamWriter.WriteLine("\t\t/// <summary>");
				streamWriter.WriteLine("\t\t/// Selects a single record from the " + table.Name + " table.");
				streamWriter.WriteLine("\t\t/// </summary>");

				streamWriter.Write("\t\tpublic string SelectJson(");
				for (int i = 0; i < table.PrimaryKeys.Count; i++)
				{
					Column column = table.PrimaryKeys[i];
					streamWriter.Write(Utility.CreateMethodParameter(column));
					if (i < (table.PrimaryKeys.Count - 1))
					{
						streamWriter.Write(", ");
					}
				}
				streamWriter.WriteLine(")");
				streamWriter.WriteLine("\t\t{");

				// Append the parameter declarations
				streamWriter.WriteLine("\t\t\tSqlParameter[] parameters = new SqlParameter[]");
				streamWriter.WriteLine("\t\t\t{");
				for (int i = 0; i < table.PrimaryKeys.Count; i++)
				{
					Column column = table.PrimaryKeys[i];
					streamWriter.Write("\t\t\t\tnew SqlParameter(\"@" + column.Name + "\", " + Utility.FormatCamel(column.Name) + ")");
					if (i < (table.PrimaryKeys.Count - 1))
					{
						streamWriter.Write(",");
					}

					streamWriter.WriteLine();
				}

				streamWriter.WriteLine("\t\t\t};");
				streamWriter.WriteLine();
				
				// Append the stored procedure execution
				streamWriter.WriteLine("\t\t\treturn SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, \"" + table.Name + "Select\", parameters);");

				// Append the method footer
				streamWriter.WriteLine("\t\t}");
				streamWriter.WriteLine();
			}
		}

		/// <summary>
		/// Creates a string that represents the select functionality of the data access class.
		/// </summary>
		/// <param name="table">The Table instance that this method will be created for.</param>
		/// <param name="storedProcedurePrefix">The prefix that is used on the stored procedure that this method will call.</param>
        /// <param name="dtoSuffix">The suffix to append to the name of each data transfer class.</param>
		/// <param name="streamWriter">The StreamWriter instance that will be used to create the method.</param>
		private static void CreateSelectAllMethod(Table table, string storedProcedurePrefix, string dtoSuffix, StreamWriter streamWriter)
		{
            string className = Utility.FormatClassName(table.Name) + dtoSuffix;
            string variableName = Utility.FormatVariableName(table.Name);
            string procedureName = storedProcedurePrefix + table.Name + "SelectAll";
            // Append the method header
            streamWriter.WriteLine("\t\t/// <summary>");
            streamWriter.WriteLine("\t\t/// Saves a record to the " + table.Name + " table.");
            streamWriter.WriteLine("\t\t/// </summary>");
            streamWriter.WriteLine("\t\tpublic DataTable GetAll()");
            streamWriter.WriteLine("\t\t{");
            //----create query -------------------------------------
            
            streamWriter.WriteLine("\n\t\t  SqlCommand cmd = new SqlCommand();");
            streamWriter.WriteLine("\t\t  cmd.CommandType = CommandType.StoredProcedure;");
            //streamWriter.Write("\t\t\t\tnew SqlParameter(\"@" + column.Name + "\", " + Utility.FormatCamel(column.Name) + ")");
            streamWriter.WriteLine("\t\t  cmd.CommandText = "+"\""+ procedureName + "\";");
            streamWriter.WriteLine();
            streamWriter.WriteLine("\t\treturn GetData(cmd);");

            streamWriter.WriteLine("\t\t }");
            streamWriter.WriteLine();
        }

		/// <summary>
		/// Creates a string that represents the select JSON functionality of the data access class.
		/// </summary>
		/// <param name="table">The Table instance that this method will be created for.</param>
		/// <param name="storedProcedurePrefix">The prefix that is used on the stored procedure that this method will call.</param>
        /// <param name="dtoSuffix">The suffix to append to the name of each data transfer class.</param>
		/// <param name="streamWriter">The StreamWriter instance that will be used to create the method.</param>
		private static void CreateSelectAllJsonMethod(Table table, string storedProcedurePrefix, string dtoSuffix, StreamWriter streamWriter)
		{
            // Create a stored procedure for each foreign key
            foreach (List<Column> compositeKeyList in table.ForeignKeys.Values)
            {
                // Create the stored procedure name
                StringBuilder stringBuilder = new StringBuilder(255);
                stringBuilder.Append("SelectAllBy");
             

                for (int i = 0; i < compositeKeyList.Count; i++)
                {
                    Column column = compositeKeyList[i];

                    if (i > 0)
                    {
                        stringBuilder.Append("_" + Utility.FormatPascal(column.Name));
                    }
                    else
                    {
                        stringBuilder.Append(Utility.FormatPascal(column.Name));
                    }
                }
                string methodName = stringBuilder.ToString();
                string procedureName = storedProcedurePrefix + table.Name + methodName;

                // Create the delete function based on keys
                // Append the method header
                streamWriter.WriteLine("\t\t/// <summary>");
                streamWriter.WriteLine("\t\t/// Select a record from the " + table.Name + " table by a foreign key.");
                streamWriter.WriteLine("\t\t/// </summary>");

                streamWriter.Write("\t\tpublic DataTable " + methodName + "(");
                for (int i = 0; i < compositeKeyList.Count; i++)
                {
                    Column column = compositeKeyList[i];
                    streamWriter.Write(Utility.CreateMethodParameter(column));
                    if (i < (compositeKeyList.Count - 1))
                    {
                        streamWriter.Write(", ");
                    }
                }
                streamWriter.WriteLine(")");
                streamWriter.WriteLine("\t\t{");

                streamWriter.WriteLine("\t\tSqlCommand cmd = new SqlCommand();");
                streamWriter.WriteLine("\t\tcmd.CommandType = CommandType.StoredProcedure;");
                streamWriter.WriteLine("\t\tcmd.CommandText = "+"\""+procedureName+"\";");

                // Append the parameter declarations
                streamWriter.WriteLine("\t\t\tSqlParameter[] parameters = new SqlParameter[]");
                streamWriter.WriteLine("\t\t\t{");
                for (int i = 0; i < compositeKeyList.Count; i++)
                {
                    Column column = compositeKeyList[i];
                    streamWriter.Write("\t\t\t\tnew SqlParameter(\"@" + column.Name + "\", " + Utility.FormatCamel(column.Name) + ")");
                    if (i < (compositeKeyList.Count - 1))
                    {
                        streamWriter.Write(",");
                    }

                    streamWriter.WriteLine();
                }

                streamWriter.WriteLine("\t\t\t};");
                streamWriter.WriteLine("\t\t\tfor (int i = 0; i < parameters.Count(); i++)");
                streamWriter.WriteLine("\t\t\t{");
                streamWriter.WriteLine("\t\t\t\tcmd.Parameters.Add(parameters[i]);");
                streamWriter.WriteLine("\t\t\t}");
                streamWriter.WriteLine("\t\treturn GetData(cmd);");  
                streamWriter.WriteLine("\t\t\t\t");
               
                streamWriter.WriteLine("\t\t}");
                streamWriter.WriteLine();
            }
        }

		/// <summary>
		/// Creates a string that represents the "select by" functionality of the data access class.
		/// </summary>
		/// <param name="table">The Table instance that this method will be created for.</param>
		/// <param name="storedProcedurePrefix">The prefix that is used on the stored procedure that this method will call.</param>
        /// <param name="dtoSuffix">The suffix to append to the name of each data transfer class.</param>
		/// <param name="streamWriter">The StreamWriter instance that will be used to create the method.</param>
		private static void CreateSelectAllByMethods(Table table, string storedProcedurePrefix, string dtoSuffix, StreamWriter streamWriter)
		{
			string className = Utility.FormatClassName(table.Name) + dtoSuffix;
			string dtoVariableName = Utility.FormatCamel(className);

			// Create a stored procedure for each foreign key
			foreach (List<Column> compositeKeyList in table.ForeignKeys.Values)
			{
				// Create the stored procedure name
				StringBuilder stringBuilder = new StringBuilder(255);
				stringBuilder.Append("SelectAllBy");
				for (int i = 0; i < compositeKeyList.Count; i++)
				{
					Column column = compositeKeyList[i];

					if (i > 0)
					{
						stringBuilder.Append("_" + Utility.FormatPascal(column.Name));
					}
					else
					{
						stringBuilder.Append(Utility.FormatPascal(column.Name));
					}
				}
				string methodName = stringBuilder.ToString();
				string procedureName = storedProcedurePrefix + table.Name + methodName;

				// Create the select function based on keys
				// Append the method header
				streamWriter.WriteLine("\t\t/// <summary>");
				streamWriter.WriteLine("\t\t/// Selects all records from the " + table.Name + " table by a foreign key.");
				streamWriter.WriteLine("\t\t/// </summary>");

				streamWriter.Write("\t\tpublic List<" + className + "> " + methodName + "(");
				for (int i = 0; i < compositeKeyList.Count; i++)
				{
					Column column = compositeKeyList[i];
					streamWriter.Write(Utility.CreateMethodParameter(column));
					if (i < (compositeKeyList.Count - 1))
					{
						streamWriter.Write(", ");
					}
				}
				streamWriter.WriteLine(")");
				streamWriter.WriteLine("\t\t{");

				// Append the parameter declarations
				streamWriter.WriteLine("\t\t\tSqlParameter[] parameters = new SqlParameter[]");
				streamWriter.WriteLine("\t\t\t{");
				for (int i = 0; i < compositeKeyList.Count; i++)
				{
					Column column = compositeKeyList[i];
					streamWriter.Write("\t\t\t\tnew SqlParameter(\"@" + column.Name + "\", " + Utility.FormatCamel(column.Name) + ")");
					if (i < (compositeKeyList.Count - 1))
					{
						streamWriter.Write(",");
					}

					streamWriter.WriteLine();
				}

				streamWriter.WriteLine("\t\t\t};");
				streamWriter.WriteLine();
				
				// Append the stored procedure execution
				streamWriter.WriteLine("\t\t\tusing (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, \"" + procedureName + "\", parameters))");
				streamWriter.WriteLine("\t\t\t{");
				streamWriter.WriteLine("\t\t\t\tList<" + className + "> " + dtoVariableName + "List = new List<" + className + ">();");
				streamWriter.WriteLine("\t\t\t\twhile (dataReader.Read())");
				streamWriter.WriteLine("\t\t\t\t{");
				streamWriter.WriteLine("\t\t\t\t\t" + className + " " + dtoVariableName + " = MapDataReader(dataReader);");
				streamWriter.WriteLine("\t\t\t\t\t" + dtoVariableName + "List.Add(" + dtoVariableName + ");");
				streamWriter.WriteLine("\t\t\t\t}");
				streamWriter.WriteLine();
				streamWriter.WriteLine("\t\t\t\treturn " + dtoVariableName + "List;");
				streamWriter.WriteLine("\t\t\t}");

				// Append the method footer
				streamWriter.WriteLine("\t\t}");
				streamWriter.WriteLine();
			}
		}

		/// <summary>
		/// Creates a string that represents the "select by" JSON functionality of the data access class.
		/// </summary>
		/// <param name="table">The Table instance that this method will be created for.</param>
		/// <param name="storedProcedurePrefix">The prefix that is used on the stored procedure that this method will call.</param>
        /// <param name="dtoSuffix">The suffix to append to the name of each data transfer class.</param>
		/// <param name="streamWriter">The StreamWriter instance that will be used to create the method.</param>
		private static void CreateSelectAllByJsonMethods(Table table, string storedProcedurePrefix, string dtoSuffix, StreamWriter streamWriter)
		{
			string className = Utility.FormatClassName(table.Name) + dtoSuffix;
			string dtoVariableName = Utility.FormatCamel(className);

			// Create a stored procedure for each foreign key
			foreach (List<Column> compositeKeyList in table.ForeignKeys.Values)
			{
				// Create the stored procedure name
				StringBuilder stringBuilder = new StringBuilder(255);
				stringBuilder.Append("SelectAllBy");
				for (int i = 0; i < compositeKeyList.Count; i++)
				{
					Column column = compositeKeyList[i];

					if (i > 0)
					{
						stringBuilder.Append("_" + Utility.FormatPascal(column.Name));
					}
					else
					{
						stringBuilder.Append(Utility.FormatPascal(column.Name));
					}
				}

				string methodName = stringBuilder.ToString();
				string procedureName = storedProcedurePrefix + table.Name + methodName;

				// Create the select function based on keys
				// Append the method header
				streamWriter.WriteLine("\t\t/// <summary>");
				streamWriter.WriteLine("\t\t/// Selects all records from the " + table.Name + " table by a foreign key.");
				streamWriter.WriteLine("\t\t/// </summary>");

				streamWriter.Write("\t\tpublic string " + methodName + "Json(");
				for (int i = 0; i < compositeKeyList.Count; i++)
				{
					Column column = compositeKeyList[i];
					streamWriter.Write(Utility.CreateMethodParameter(column));
					if (i < (compositeKeyList.Count - 1))
					{
						streamWriter.Write(", ");
					}
				}
				streamWriter.WriteLine(")");
				streamWriter.WriteLine("\t\t{");

				// Append the parameter declarations
				streamWriter.WriteLine("\t\t\tSqlParameter[] parameters = new SqlParameter[]");
				streamWriter.WriteLine("\t\t\t{");
				for (int i = 0; i < compositeKeyList.Count; i++)
				{
					Column column = compositeKeyList[i];
					streamWriter.Write("\t\t\t\tnew SqlParameter(\"@" + column.Name + "\", " + Utility.FormatCamel(column.Name) + ")");
					if (i < (compositeKeyList.Count - 1))
					{
						streamWriter.Write(",");
					}

					streamWriter.WriteLine();
				}

				streamWriter.WriteLine("\t\t\t};");
				streamWriter.WriteLine();
				
				// Append the stored procedure execution
				streamWriter.WriteLine("\t\t\treturn SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, \"" + procedureName + "\", parameters);");

				// Append the method footer
				streamWriter.WriteLine("\t\t}");
				streamWriter.WriteLine();
			}
		}
		
		/// <summary>
		/// Creates a string that represents the "map" functionality of the data access class.
		/// </summary>
		/// <param name="table">The Table instance that this method will be created for.</param>
        /// <param name="dtoSuffix">The suffix to append to the name of the data transfer class.</param>
		/// <param name="streamWriter">The StreamWriter instance that will be used to create the method.</param>
		private static void CreateMapMethod(Table table, string dtoSuffix, StreamWriter streamWriter)
		{
			string className = Utility.FormatClassName(table.Name) + dtoSuffix;
			string variableName = Utility.FormatVariableName(className);

			streamWriter.WriteLine("\t\t/// <summary>");
			streamWriter.WriteLine("\t\t/// Creates a new instance of the " + className + " class and populates it with data from the specified SqlDataReader.");
			streamWriter.WriteLine("\t\t/// </summary>");
			streamWriter.WriteLine("\t\tprivate " + className + " MapDataReader(SqlDataReader dataReader)");
			streamWriter.WriteLine("\t\t{");
			streamWriter.WriteLine("\t\t\t" + className + " " + variableName + " = new " + className + "();");
			
			foreach (Column column in table.Columns)
			{
				string columnNamePascal = Utility.FormatPascal(column.Name);
				streamWriter.WriteLine("\t\t\t" + variableName + "." + columnNamePascal + " = dataReader." + Utility.GetGetMethod(column) + "(\"" + column.Name + "\", " + Utility.GetDefaultValue(column) + ");");
			}
			
			streamWriter.WriteLine();
			streamWriter.WriteLine("\t\t\treturn " + variableName + ";");
			streamWriter.WriteLine("\t\t}");
		}

	}
}
