﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace App_ChuongTrinhSinhCodeTuDong.CodeWebFormLinQ
{
    public static class CsGeneratorGraphic_CodeWebFormLinQ
    {
        #region  Sửa xóa dữ liệu từ Gridview - TextBox

        /// <summary>
        /// Tạo giao diện phần thêm sửa xóa kiểu Click Gridview tới textbox
        /// </summary>
        /// <param name="databaseName"></param>
        /// <param name="table"></param>
        /// <param name="targetNamespace"></param>
        /// <param name="storedProcedurePrefix"></param>
        /// <param name="daoSuffix"></param>
        /// <param name="dtoSuffix"></param>
        /// <param name="path"></param>
        public static void GridViewToTextBox(string databaseName, Table table, string targetNamespace, string storedProcedurePrefix, string DbContext, string path)
        {

            //string className = Utility.FormatClassName(table.Name) + busSuffix;

            using (StreamWriter streamWriter = new StreamWriter(Path.Combine(path, table.Name + ".aspx")))
            {
                var sb = new StringBuilder();

                sb.Append("\n <%@ Page Title=\"\" Language=\"C#\" MasterPageFile=\"~/Admin/admin.Master\" AutoEventWireup=\"true\"");
                sb.Append("\n CodeBehind=\"" + table.Name + ".aspx.cs\" Inherits=\"" + targetNamespace + "." + table.Name + "\" %>");
                //sb.Append("\n <asp:Content ID=\"Content1\" ContentPlaceHolderID=\"HeadContent\" runat=\"server\">");
                //sb.Append("\n </asp:Content>");
                sb.Append("\n <asp:Content ID=\"Content2\" ContentPlaceHolderID=\"MainContent\" runat=\"server\">");
                sb.Append("\n  <h2>" + table.Name.ToUpper() + "</h2>");
                sb.Append("\n <asp:Panel ID=\"PanelModify\" runat=\"server\">");
                sb.Append("\n  <table>");
                sb.Append("\n ");
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];

                    // Is the current column an identity column?);
                    if (column.IsIdentity == false)
                    {
                        sb.Append("\n <tr>");
                        sb.Append("\n <td>" + column.Name + ":</td>");
                        sb.Append("\n <td> <asp:TextBox ID=\"txt" + column.Name + "\" runat=\"server\" Height=\"26px\" Width=\"200px\"></asp:TextBox></td>");
                        sb.Append("\n </tr>");
                    }
                    else
                    {
                        sb.Append("\n <tr>");
                        sb.Append("\n <td>" + column.Name + ":</td>");
                        sb.Append("\n <td><asp:TextBox ID=\"txt" + column.Name + "\" runat=\"server\" Height=\"26px\" Width=\"200px\"></asp:TextBox></td>");
                        sb.Append("\n </tr>");

                    }
                }
                sb.Append("\n <tr>");
                sb.Append("\n  <td>");
                sb.Append("\n &nbsp;</td>");
                sb.Append("\n <td>");
                sb.Append("\n <asp:Button ID=\"btnSave\" runat=\"server\" onclick=\"btnSave_Click\" Text=\"Save\" />");

                sb.Append("\n <asp:Button ID=\"btnCancel\" runat=\"server\" onclick=\"btnCancel_Click\" Text=\"Cancel\" />");
                sb.Append("\n </td>");
                sb.Append("\n  </tr>");
                sb.Append("\n </table> \n </asp:Panel>");
                sb.Append("\n  <br />");
                sb.Append("\n <asp:Panel ID=\"PanelShow\" runat=\"server\">");
                sb.Append("\n  <asp:Button ID=\"btnThem\" runat=\"server\" onclick=\"btnThem_Click\" Text=\"Add New\" />");
                sb.Append("\n  <br />  <br />  ");
                sb.Append("\n  <asp:GridView ID=\"GridViewShow\" runat=\"server\" AutoGenerateColumns=\"false\" OnRowCommand=\"GridViewShowRowClick\" Width=\"600px\"> ");

                sb.Append("\n  <Columns>");
                sb.Append("\n <asp:TemplateField HeaderText=\" + \">");
                sb.Append("\n <ItemTemplate>");
                sb.Append("\n  <%# Container.DataItemIndex+1 %></ItemTemplate>");
                sb.Append("\n <ItemStyle Width=\"10px\" />");
                sb.Append("\n </asp:TemplateField>");
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];

                    // Is the current column an identity column?);
                    if (column.IsIdentity == false)
                    {
                        sb.Append("\n   <asp:BoundField DataField=\"" + column.Name + "\" HeaderText=\"" + column.Name + "\" />");

                    }
                    else
                    {
                        sb.Append("\n  <asp:BoundField DataField=\"" + column.Name + "\" HeaderText=\"" + column.Name + "\" Visible=\"False\" />");

                        sb.Append("");
                    }
                }
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];
                    if (column.IsIdentity == true)
                    {
                        sb.Append("\n <asp:TemplateField HeaderText=\"Edit\">");
                        sb.Append("\n  <ItemTemplate>");

                        sb.Append("\n <asp:LinkButton ID=\"lbEdit\" runat=\"server\" CommandName='<%#\"S|\"+Eval(\"" + column.Name + "\")%>' Text=\"Edit\"></asp:LinkButton> ");
                        sb.Append("\n </ItemTemplate>");
                        sb.Append("\n </asp:TemplateField>");
                        sb.Append("\n <asp:TemplateField HeaderText=\"Delete\">");
                        sb.Append("\n <ItemTemplate>");

                        sb.Append("\n <asp:LinkButton ID=\"lbDelete\" runat=\"server\" CommandName='<%#\"X|\"+Eval(\"" + column.Name + "\")%>' OnClientClick=\"return confirm('Do you want to delete?')\" Text=\"Delete\"></asp:LinkButton>");
                        sb.Append("\n </ItemTemplate>");
                        sb.Append("\n </asp:TemplateField>");
                    }
                }

                sb.Append("\n </Columns>");
                sb.Append(" <PagerSettings Mode=\"NumericFirstLast\" />");
                sb.Append("</asp:GridView>");

                sb.Append(" </asp:Panel>");

                sb.Append("\n </asp:Content>");

                streamWriter.WriteLine(sb);


            }
        }

        /// <summary>
        ///  /// Tạo Code phần thêm sửa xóa kiểu Click Gridview tới textbox
        /// </summary>
        /// <param name="databaseName"></param>
        /// <param name="table"></param>
        /// <param name="targetNamespace"></param>
        /// <param name="storedProcedurePrefix"></param>
        /// <param name="daoSuffix"></param>
        /// <param name="dtoSuffix"></param>
        /// <param name="path"></param>

        public static void GridViewToTextBox_CS(string databaseName, Table table, string targetNamespace, string storedProcedurePrefix, string DbContext, string path)
        {

            string objTables = table.Name; /*Utility.FormatClassName(table.Name) + "";*/
            string keyColName = "ID";
            for (int i = 0; i < table.Columns.Count; i++)
            {
                Column column = table.Columns[i];
                if (column.IsIdentity == true)
                {
                    keyColName = column.Name;

                }
            }
            //Gridview to textbox
            using (StreamWriter streamWriter = new StreamWriter(Path.Combine(path, table.Name + ".aspx.cs")))
            {
                var sb = new StringBuilder();
                sb.Append("using System;");
                sb.Append("\n using System.Collections.Generic;");
                sb.Append("\n using System.Linq;");
                sb.Append("\n using System.Web;");
                sb.Append("\n using System.Web.UI;");
                sb.Append("\n using System.Web.UI.WebControls;");

                sb.Append("\n using System.Data;");

                sb.Append("\n namespace " + targetNamespace + "");
                sb.Append("\n {");

                sb.Append("\n public partial class " + table.Name + " : System.Web.UI.Page");
                sb.Append("\n  {");
                //==============Hết phần định nghĩa class   
                sb.Append("\n " + DbContext + " db = new " + DbContext + "();");

                sb.Append("\n   protected void Page_Load(object sender, EventArgs e)");
                sb.Append("\n  {");
                sb.Append("\n try");
                sb.Append("\n {");

                sb.Append("\n  if (!Page.IsPostBack)");
                sb.Append("\n  { ");
                sb.Append("\n  Databind();");
                sb.Append("\n  PanelModify.Visible = false; ");
                sb.Append("\n  PanelShow.Visible = true; ");
                sb.Append("\n  ViewState[\"active\"] = \"\"; ");
                sb.Append("\n  } ");
                sb.Append("\n  }");
                sb.Append("\n catch (Exception)");
                sb.Append("\n {");
                sb.Append("\n  clsCommon.Show(\"Some thing went wrong !\");");
                sb.Append("\n }");
                sb.Append("\n  } ");

                //Xóa dữ liệu rỗng
                sb.Append("\n  //================Xóa rỗng các text box==================");
                sb.Append("\n  public void Clear()");
                sb.Append("\n  {");
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];
                    sb.Append("\n txt" + column.Name + ".Text = \"\";");

                }
                sb.Append("\n } ");
                //Load dữ liệu cho Gridview
                sb.Append("\n //====================Load dữ liệu cho Gridview=========================");
                sb.Append("\n  public void Databind()");
                sb.Append("\n  {");
                sb.Append("\n  var p = from p1 in db." + objTables + " select p1;");
                sb.Append("\n GridViewShow.DataSource = p.ToList();");
                sb.Append("\n GridViewShow.DataBind();");
                sb.Append("\n }");
                sb.Append("\n ");

                //Select trong gridview
                sb.Append("\n  //====================================Sự kiện Select trên Gridview===========================");
                sb.Append("\n protected void GridViewShowRowClick(object sender, GridViewCommandEventArgs e)");
                sb.Append("\n {");
                sb.Append("\n try");
                sb.Append("\n {");
                sb.Append("\n string str = e.CommandName;");
                sb.Append("\n if(str.Equals(\"Page\")==false)");
                sb.Append("\n {");
                sb.Append("\n char[] ch = new char[] { '|' };");
                sb.Append("\n string[] s = str.Split(ch);");
                sb.Append("\n  int keyID = int.Parse(s[1].ToString());");
            
                sb.Append("\n   switch (s[0])");
                sb.Append("\n {");
                sb.Append("\n  case \"S\":");
                sb.Append("\n ViewState[\"active\"] = \"edit\";");
                sb.Append("\n var pEdit = from p1 in db." + objTables + " where p1." + keyColName + "==keyID select p1;");
                sb.Append("\n if (pEdit.Count()==1)");
                sb.Append("\n {");
                sb.Append("\n foreach (var item in pEdit)");
                sb.Append("\n {");
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];
                    sb.Append("\n txt" + column.Name + ".Text = item." + column.Name + ".ToString();");
                }
                sb.Append("\n }");
                sb.Append("\n PanelModify.Visible = true;");
                sb.Append("\n PanelShow.Visible = false;");
                sb.Append("\n }");
                sb.Append("\n break;");
                sb.Append("\n  case \"X\":");
                sb.Append("\n  var pDelete = db." + objTables + ".First(p1 => p1." + keyColName + " == keyID);");
                sb.Append("\n  db." + objTables + ".DeleteOnSubmit(pDelete);");
                sb.Append("\n  db.SubmitChanges();");
                sb.Append("\n  Databind();");
                sb.Append("\n  break;");
                sb.Append("\n  }");
                sb.Append("\n }");
                sb.Append("\n else");
                sb.Append("\n { ");
                sb.Append("\n }");
                sb.Append("\n }");
                sb.Append("\n catch (Exception)");
                sb.Append("\n {");
                sb.Append("\n clsCommon.Show(\"Some thing went wrong !\");");
                sb.Append("\n }");
                sb.Append("\n }");

                sb.Append("\n //========================Thêm====================");
                sb.Append("\n protected void btnThem_Click(object sender, EventArgs e)");
                sb.Append("\n { \n PanelModify.Visible = true;");
                sb.Append("\n  PanelShow.Visible = false; ");
                sb.Append("\n Clear();");
                sb.Append("\n ViewState[\"active\"] = \"add\"; ");
                sb.Append("\n }");

                //Khi click vào save để THêm Hoặc SỬa
                sb.Append("\n  //=====================================Khi click vào save để THêm Hoặc SỬa=============================");
                sb.Append("\n   protected void btnSave_Click(object sender, EventArgs e) \n  { ");
                sb.Append("\n  try ");
                sb.Append("\n {");

              
                sb.Append("\n   switch (ViewState[\"active\"].ToString()) \n{");
                sb.Append("\n   case \"add\":  ");
                sb.Append("\n " + objTables + " objAdd = new " + objTables + "();");
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];
                    if (column.IsIdentity == false)
                    {
                        sb.Append(Common.SeparatorLinQ(column, "objAdd"));
                    }
                }
                sb.Append("\n db." + objTables + ".InsertOnSubmit(objAdd);");
                sb.Append("\n db.SubmitChanges();");
                sb.Append("\n   break;");
                sb.Append("\n  case \"edit\":");
                sb.Append("\n  int keyID = int.Parse(txt" + keyColName + ".Text);");

                sb.Append("\n  "+ objTables + " objEdit = db." + objTables + ".First(p => p." + keyColName + " == keyID);");
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];
                    if (column.IsIdentity == true)
                    {
                        sb.Append(Common.SeparatorLinQ(column, "objEdit"));

                    }
                }
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];
                    if (column.IsIdentity == false)
                    {
                        sb.Append(Common.SeparatorLinQ(column, "objEdit"));
                    }
                }
                sb.Append("\n   db.SubmitChanges();");

                sb.Append("\n  break; ");
                sb.Append("\n   } ");
                sb.Append("\n   Databind();");
                sb.Append("\n PanelModify.Visible = false;");
                sb.Append("\n PanelShow.Visible = true;");
                sb.Append("\n }");
                sb.Append("\n catch (Exception ex) ");
                sb.Append("\n {");
                sb.Append("\n clsCommon.Show(\"Cannot Save this item\");");
                sb.Append("\n }");
                sb.Append("\n }");

                //Sự kiện xóa     

                ////Sự kiện Phân Trang    
                //sb.Append("\n  //=====================================Phân Trang============================");
                //sb.Append("\n protected void GridViewShow_PageIndexChanging(object sender, GridViewPageEventArgs e) ");
                //sb.Append("\n { \n GridViewShow.PageIndex = e.NewPageIndex;");
                // sb.Append("\n Databind();");
                //sb.Append("\n }");

                //Nút Cancel    
                sb.Append("\n  //=====================================Nút Cancel============================");
                sb.Append("\n protected void btnCancel_Click(object sender, EventArgs e) ");
                sb.Append("\n { ");
                sb.Append("\n PanelModify.Visible = false; ");
                sb.Append("\n PanelShow.Visible = true;");
                sb.Append("\n }");

                sb.Append("\n }");

                streamWriter.WriteLine(sb);
            }
        }

        /// <summary>
        /// Sinh phan designer cho file cua webform
        /// </summary>
        /// <param name="databaseName"></param>
        /// <param name="table"></param>
        /// <param name="targetNamespace"></param>
        /// <param name="storedProcedurePrefix"></param>
        /// <param name="busSuffix"></param>
        /// <param name="dtoSuffix"></param>
        /// <param name="path"></param>
        public static void GridViewToTextBox_Designer_CS(string databaseName, Table table, string targetNamespace, string storedProcedurePrefix, string DbContext, string path)
        {
            //string className = Utility.FormatClassName(table.Name) + dtoSuffix;
            //string classNameDAO = Utility.FormatClassName(table.Name) + busSuffix;
            //string classNameBusiness = Utility.FormatClassName(table.Name) + busSuffix;
            //string objBUS = "_" + table.Name + busSuffix.ToUpper();

            //Gridview to textbox
            using (StreamWriter streamWriter = new StreamWriter(Path.Combine(path, table.Name + ".aspx.designer.cs")))
            {
                var sb = new StringBuilder();
                sb.Append("\n//------------------------------------------------------------------------------");
                sb.Append("\n// <auto-generated>");
                sb.Append("\n //     This code was generated by a tool.");
                sb.Append("\n //     Changes to this file may cause incorrect behavior and will be lost if");
                sb.Append("\n  //     the code is regenerated. ");
                sb.Append("\n // </auto-generated>");
                sb.Append("\n //------------------------------------------------------------------------------");

                sb.Append("\n namespace " + targetNamespace + "");
                sb.Append("\n {");
                sb.Append("\n public partial class " + table.Name);
                sb.Append("\n{");
                sb.Append("\n protected global::System.Web.UI.WebControls.Panel PanelModify;");
                sb.Append("\n protected global::System.Web.UI.WebControls.Button btnSave;");
                sb.Append("\n protected global::System.Web.UI.WebControls.Button btnDelete;");
                sb.Append("\n protected global::System.Web.UI.WebControls.Button btnCancel;");
                sb.Append("\n protected global::System.Web.UI.WebControls.Button btnThem;");
                sb.Append("\n protected global::System.Web.UI.WebControls.Panel PanelShow;");
                sb.Append("\n protected global::System.Web.UI.WebControls.GridView GridViewShow;");
                sb.Append("\n ");

                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];
                    sb.Append("\n protected global::System.Web.UI.WebControls.TextBox txt" + column.Name + ";");
                }

                sb.Append("\n }");
                sb.Append("\n }");

                streamWriter.WriteLine(sb);
            }
        }

        #endregion

        //Modify in gridview
        #region ===========Thêm Sửa Xóa Trong Gridview==============
        /// <summary>
        /// Phần giao diện aspx
        /// </summary>
        /// <param name="databaseName"></param>
        /// <param name="table"></param>
        /// <param name="targetNamespace"></param>
        /// <param name="storedProcedurePrefix"></param>
        /// <param name="daoSuffix"></param>
        /// <param name="dtoSuffix"></param>
        /// <param name="path"></param>
        public static void CreateModifyGridview(string databaseName, Table table, string targetNamespace, string storedProcedurePrefix, string daoSuffix, string dtoSuffix, string path)
        {

            string className = Utility.FormatClassName(table.Name) + daoSuffix;

            using (StreamWriter streamWriter = new StreamWriter(Path.Combine(path, table.Name + "2.aspx")))
            {
                string Id = "Id";
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];

                    // Is the current column an identity column?);
                    if (column.IsIdentity == true)
                    {
                        Id = column.Name;
                    }


                }

                var sb = new StringBuilder();
                sb.Append("\n <%@ Page Title=\"\" Language=\"C#\" MasterPageFile=\"~/Admin/admin.Master\" AutoEventWireup=\"true\"");
                sb.Append("\n CodeBehind=\"" + table.Name + "2.aspx.cs\" Inherits=\"" + targetNamespace + "2." + table.Name + "2\" %>");
                sb.Append("\n <asp:Content ID=\"Content1\" ContentPlaceHolderID=\"HeadContent\" runat=\"server\">");
                sb.Append("\n </asp:Content>");
                sb.Append("\n <asp:Content ID=\"Content2\" ContentPlaceHolderID=\"MainContent\" runat=\"server\">");
                sb.Append("\n  <h2>" + table.Name.ToUpper() + "</h2>");
                sb.Append("\n <asp:Panel ID=\"PanelModify\" runat=\"server\"");
                sb.Append("\n ScrollBars=\"Vertical\"");
                sb.Append("\n BorderStyle=\"Inset\" BorderWidth=\"1px\" \n style=\"overflow: auto; height: 330px\">");

                sb.Append("\n <asp:GridView ID=\"GridView" + table.Name + "\" runat=\"server\" AutoGenerateColumns=\"False\" ");
                sb.Append("\n AllowPaging=\"True\" DataKeyNames=\"" + Id + "\"");
                sb.Append("\n OnPageIndexChanging=\"GridView" + table.Name + "_PageIndexChanging\"");
                sb.Append("\n OnRowCancelingEdit=\"GridView" + table.Name + "_RowCancelingEdit\"");
                sb.Append("\n OnRowEditing=\"GridView" + table.Name + "_RowEditing\"");
                sb.Append("\n OnRowUpdating=\"GridView" + table.Name + "_RowUpdating\"");
                sb.Append("\n onrowdeleting=\"GridView" + table.Name + "_RowDeleting\"");
                sb.Append("\n ShowFooter=\"True\" Width=\"90%\">");

                sb.Append("\n <Columns>");
                sb.Append("\n <asp:TemplateField HeaderText=\"No.\">");
                sb.Append("\n <ItemTemplate>");
                sb.Append("\n <%# Container.DataItemIndex+1 %>");
                sb.Append("\n</ItemTemplate>");

                sb.Append("\n </asp:TemplateField>");

                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];

                    // Is the current column an identity column?);
                    if (column.IsIdentity == false)
                    {

                        sb.Append("\n <asp:TemplateField HeaderText=\" " + column.Name + " \">");
                        sb.Append("\n <EditItemTemplate>");
                        sb.Append("\n <asp:TextBox ID=\"txt" + column.Name + "\" runat=\"server\" Text='<%# Bind(\"" + column.Name + "\") %>'></asp:TextBox>");
                        sb.Append("\n </EditItemTemplate>");
                        sb.Append("\n <FooterTemplate>");
                        sb.Append("\n <asp:TextBox ID=\"txt" + column.Name + "\" runat=\"server\"></asp:TextBox>");
                        sb.Append("\n </FooterTemplate>");
                        sb.Append("\n <ItemTemplate>");
                        sb.Append("\n <asp:Label ID=\"lbl" + column.Name + "\" runat=\"server\" Text='<%# Bind(\"" + column.Name + "\") %>'></asp:Label>");
                        sb.Append("\n </ItemTemplate>");
                        sb.Append("\n </asp:TemplateField>");

                        sb.Append("");
                    }
                    else
                    {
                    }
                }
                //Tạo Cột xóa
                sb.Append("\n <asp:TemplateField ShowHeader=\"False\" HeaderText=\"Delete\">");
                sb.Append("\n <ItemTemplate>");
                sb.Append("\n <asp:LinkButton ID=\"LinkButtonDelete\" runat=\"server\" CausesValidation=\"False\" CommandName=\"Delete\" ");
                sb.Append("\n Text=\"Delete\" OnClientClick = \"return confirm('Do you want to delete?')\"></asp:LinkButton>");
                sb.Append("\n </ItemTemplate>");
                sb.Append("\n <FooterTemplate>");
                sb.Append("\n <asp:Button ID=\"btnAdd\" runat=\"server\" Text=\"Add\" Width=\"48px\" onclick=\"btnAdd_Click\" />");
                sb.Append("\n </FooterTemplate>");
                sb.Append("\n </asp:TemplateField>");
                //Tạo Cột Sửa
                sb.Append("\n <asp:TemplateField  HeaderText=\"Edit\">");
                sb.Append("\n <ItemTemplate>");
                sb.Append("\n <asp:LinkButton ID=\"LinkButtonEdit\" runat=\"server\" CausesValidation=\"False\" CommandName=\"Edit\" Text=\"Edit\"></asp:LinkButton>");
                sb.Append("\n </ItemTemplate>");
                sb.Append("\n <EditItemTemplate>");
                sb.Append("\n <asp:LinkButton ID=\"LinkButtonUpdate\" runat=\"server\" CausesValidation=\"True\" CommandName=\"Update\" Text=\"Update\"></asp:LinkButton> &nbsp;");
                sb.Append("\n <asp:LinkButton ID=\"LinkButtonCanncel\" runat=\"server\" CausesValidation=\"False\" CommandName=\"Cancel\" Text=\"Cancel\"></asp:LinkButton>");
                sb.Append("\n </EditItemTemplate>");
                sb.Append("\n </asp:TemplateField>");
                //Cột Id
                sb.Append("\n <asp:TemplateField HeaderText=\"" + Id + "\">");
                sb.Append("\n <EditItemTemplate>");
                sb.Append("\n <asp:TextBox ID=\"txtId\" runat=\"server\" Text='<%# Bind(\"" + Id + "\") %>'></asp:TextBox>");
                sb.Append("\n </EditItemTemplate>");
                sb.Append("\n <ItemTemplate>");
                sb.Append("\n <asp:Label ID=\"txtId\" runat=\"server\" Text='<%# Bind(\"" + Id + "\") %>'></asp:Label>");
                sb.Append("\n </ItemTemplate>");
                sb.Append("\n <HeaderStyle CssClass=\"hideGridColumn\" />");
                sb.Append("\n <ItemStyle CssClass=\"hideGridColumn\" />");
                sb.Append("\n <FooterStyle  CssClass=\"hideGridColumn\" />");
                sb.Append("\n </asp:TemplateField>");
                sb.Append("");
                sb.Append("\n </Columns>");
                sb.Append("\n <PagerSettings Mode=\"NumericFirstLast\" />");
                sb.Append("\n </asp:GridView>");

                sb.Append("\n <style type=\"text/css\">.hideGridColumn{display: none;}</style>");
                sb.Append("");
                sb.Append("\n  </asp:Panel>");

                sb.Append("\n </asp:Content>");
                streamWriter.WriteLine(sb);


            }
        }
        /// <summary>
        /// Phần Code sửa xóa trong Gridview
        /// </summary>
        /// <param name="databaseName"></param>
        /// <param name="table"></param>
        /// <param name="targetNamespace"></param>
        /// <param name="storedProcedurePrefix"></param>
        /// <param name="busSuffix"></param>
        /// <param name="dtoSuffix"></param>
        /// <param name="path"></param>
        public static void CreateModifyGridviewCS(string databaseName, Table table, string targetNamespace, string storedProcedurePrefix, string busSuffix, string dtoSuffix, string path)
        {

            string className = Utility.FormatClassName(table.Name) + busSuffix;
            string classNameEntity = Utility.FormatClassName(table.Name) + dtoSuffix;
            string classNameBusiness = Utility.FormatClassName(table.Name) + busSuffix;
            string objBUS = "_" + table.Name + busSuffix.ToUpper();
            string gridviewName = "GridView" + table.Name;
            using (StreamWriter streamWriter = new StreamWriter(Path.Combine(path, table.Name + "2.aspx.cs")))
            {
                var sb = new StringBuilder();
                sb.Append("using System;");
                sb.Append("\n using System.Collections.Generic;");
                sb.Append("\n using System.Linq;");
                sb.Append("\n using System.Web;");
                sb.Append("\n using System.Web.UI;");
                sb.Append("\n using System.Web.UI.WebControls;");
                sb.Append("\n using Business;");
                sb.Append("\n using Entity;");
                sb.Append("\n using System.Data;");
                sb.Append("\n using Common;");
                sb.Append("\n namespace " + targetNamespace + "2");
                sb.Append("\n {");
                sb.Append("\n public partial class " + table.Name + "2 : System.Web.UI.Page");
                sb.Append("\n  {");
                sb.Append("\n " + classNameBusiness + " bus " + " = new " + classNameBusiness + "();");
                sb.Append("\n protected void Page_Load(object sender, EventArgs e)");
                sb.Append("\n {");
                sb.Append("\n if (!IsPostBack)");
                sb.Append("\n {");
                sb.Append("\n Databind();");
                sb.Append("\n }");
                sb.Append("\n }");
                sb.Append("\n ");
                sb.Append("\n //Databin");
                sb.Append("\n public void Databind()");
                sb.Append("\n {");
                sb.Append("\n " + gridviewName + ".DataSource = bus.GetAll();");
                sb.Append("\n " + gridviewName + ".DataBind();");
                sb.Append("\n }");

                //pHAN tRANG
                sb.Append("\n //pHAN tRANG");
                sb.Append("\n protected void " + gridviewName + "_PageIndexChanging(object sender, GridViewPageEventArgs e) ");
                sb.Append("\n { ");
                sb.Append("\n" + gridviewName + ".PageIndex = e.NewPageIndex;");
                sb.Append("\n Databind();");
                sb.Append("\n }");
                //eDIT
                sb.Append("\n //Edit");
                sb.Append("\n protected void  " + gridviewName + "_RowEditing(object sender, GridViewEditEventArgs e)");
                sb.Append("\n { ");
                sb.Append("\n " + gridviewName + ".EditIndex = e.NewEditIndex;");
                sb.Append("\n  Databind();");
                sb.Append("\n }");
                //Update
                sb.Append("\n  //Update trong Gridview");
                sb.Append("\n protected void " + gridviewName + "_RowUpdating(object sender, GridViewUpdateEventArgs e)");
                sb.Append("\n {");
                sb.Append("\n " + classNameEntity + " obj=new " + classNameEntity + "();");
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];
                    sb.Append(Common.GridviewTo_Object(column, gridviewName));
                }
                sb.Append("\n bus.Update(obj);");
                sb.Append("\n " + gridviewName + ".EditIndex = -1; ");
                sb.Append("\n Databind(); ");
                sb.Append("\n}");

                //Canncel
                sb.Append("\n protected void " + gridviewName + "_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)");
                sb.Append("\n { ");
                sb.Append("\n  " + gridviewName + ".EditIndex = -1;");
                sb.Append("Databind();");
                sb.Append("\n }");
                //tHEM
                sb.Append(" //tHEM");
                sb.Append("\n protected void btnAdd_Click(object sender, EventArgs e) ");
                sb.Append("\n { ");
                sb.Append("\n " + classNameEntity + " obj = new " + classNameEntity + "();");
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];
                    if (column.IsIdentity == false)
                    {
                        sb.Append(Common.GridviewTo_Object_Add(column, gridviewName));
                    }
                }
                sb.Append("\n bus.Insert(obj);");
                sb.Append("\n Databind();");
                sb.Append("}");
                //Xoa
                sb.Append("//Xoa");
                sb.Append("\n protected void " + gridviewName + "_RowDeleting(object sender, GridViewDeleteEventArgs e) ");
                sb.Append("\n { ");
                sb.Append("\n " + classNameEntity + " obj=new " + classNameEntity + "();");
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];
                    if (column.IsIdentity == true)
                    {
                        sb.Append(Common.ConvertFor_DeleteInGridview(column, gridviewName));
                    }
                }
                sb.Append("\n bus.Delete(obj);");
                sb.Append("\n Databind();");
                sb.Append("\n  }");
                sb.Append("\n  }");
                sb.Append("\n  }");
                streamWriter.WriteLine(sb);
            }
        }
        /// <summary>
        /// Sinh phan designer cho file cua webform Sửa xóa trong gridview
        /// </summary>
        /// <param name="databaseName"></param>
        /// <param name="table"></param>
        /// <param name="targetNamespace"></param>
        /// <param name="storedProcedurePrefix"></param>
        /// <param name="busSuffix"></param>
        /// <param name="dtoSuffix"></param>
        /// <param name="path"></param>
        public static void CreateModifyGridview_Designer_CS(string databaseName, Table table, string targetNamespace, string storedProcedurePrefix, string busSuffix, string dtoSuffix, string path)
        {
            string className = Utility.FormatClassName(table.Name) + dtoSuffix;
            string classNameDAO = Utility.FormatClassName(table.Name) + busSuffix;
            string classNameBusiness = Utility.FormatClassName(table.Name) + busSuffix;
            string objBUS = "_" + table.Name + busSuffix.ToUpper();

            //Gridview to textbox
            using (StreamWriter streamWriter = new StreamWriter(Path.Combine(path, table.Name + "2.aspx.designer.cs")))
            {
                var sb = new StringBuilder();
                sb.Append("\n//------------------------------------------------------------------------------");
                sb.Append("\n// <auto-generated>");
                sb.Append("\n //     This code was generated by a tool.");
                sb.Append("\n //     Changes to this file may cause incorrect behavior and will be lost if");
                sb.Append("\n  //     the code is regenerated. ");
                sb.Append("\n // </auto-generated>");
                sb.Append("\n //------------------------------------------------------------------------------");

                sb.Append("\n namespace " + targetNamespace + "2");
                sb.Append("\n {");
                sb.Append("\n public partial class " + table.Name + "2");
                sb.Append("\n{");
                sb.Append("\n protected global::System.Web.UI.WebControls.Panel PanelModify;");
                sb.Append("\n protected global::System.Web.UI.WebControls.GridView GridView" + table.Name + ";");
                sb.Append("\n }");
                sb.Append("\n }");

                streamWriter.WriteLine(sb);
            }
        }
        #endregion


        ///Tao Repeater
        #region tao Repeater
        public static void CreateRepeater(string databaseName, Table table, string targetNamespace, string storedProcedurePrefix, string daoSuffix, string dtoSuffix, string path)
        {

            //string className = Utility.FormatClassName(table.Name) + daoSuffix;
            //path = Path.Combine(path, "Presentation");

            //using (StreamWriter streamWriter = new StreamWriter(Path.Combine(path, className + "Repeater.aspx")))
            //{
            //    var sb = new StringBuilder();


            //    streamWriter.WriteLine(sb);
            //}
        }

        #endregion
    }
}

