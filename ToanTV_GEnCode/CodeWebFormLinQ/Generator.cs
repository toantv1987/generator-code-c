﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Windows.Forms;

namespace App_ChuongTrinhSinhCodeTuDong.CodeWebFormLinQ
{
	//internal delegate void CountUpdate(object sender, CountEventArgs e);

	/// <summary>
	/// Generates C# and SQL code for accessing a database.
	/// </summary>
	public static class Generator
	{
	//	public static event CountUpdate DatabaseCounted;
	//	public static event CountUpdate TableCounted;

		/// <summary>
		/// Generates the SQL and C# code for the specified database.
		/// </summary>
		/// <param name="outputDirectory">The directory where the C# and SQL code should be created.</param>
		/// <param name="connectionString">The connection string to be used to connect the to the database.</param>
		/// <param name="grantLoginName">The SQL Server login name that should be granted execute rights on the generated stored procedures.</param>
		/// <param name="storedProcedurePrefix">The prefix that should be used when creating stored procedures.</param>
		/// <param name="createMultipleFiles">A flag indicating if the generated stored procedures should be created in one file or separate files.</param>
        /// <param name="projectName">The name of the project file to be generated.</param>
		/// <param name="targetNamespace">The namespace that the generated C# classes should contained in.</param>
		/// <param name="daoSuffix">The suffix to be applied to all generated DAO classes.</param>
        /// <param name="dtoSuffix">The suffix to be applied to all generated DTO classes.</param>
		public static void Generate(string outputDirectory, string connectionString, string grantLoginName, string storedProcedurePrefix, bool createMultipleFiles, string projectName, string targetNamespace, string DbContext)
		{
            try
            {
                List<Table> tableList = new List<Table>();
                string databaseName;
                string sqlPath;
                string csPath;


                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    databaseName = Utility.FormatPascal(connection.Database);
                    sqlPath = Path.Combine(outputDirectory, "SQL");
                    csPath = Path.Combine(outputDirectory, "CS");

                    connection.Open();

                    // Lấy ra danh sách các bảng trong database
                    DataTable dataTable = new DataTable();
                    string strQuery = "select TABLE_CATALOG,TABLE_SCHEMA,TABLE_NAME,TABLE_TYPE from INFORMATION_SCHEMA.TABLES ";
                    strQuery = strQuery + "where TABLE_TYPE = 'BASE TABLE' and (TABLE_NAME != 'dtProperties' and TABLE_NAME != 'sysdiagrams') ";
                    strQuery = strQuery + "and TABLE_CATALOG = '" + databaseName + "' order by TABLE_NAME";
                    SqlCommand cmd = new SqlCommand(strQuery, connection);
                    SqlDataAdapter dataAdapter = new SqlDataAdapter(cmd);
                    dataAdapter.Fill(dataTable);
                    cmd.Dispose();
                    dataAdapter.Dispose();
                    // Xử lý với từng bảng 
                    foreach (DataRow dataRow in dataTable.Rows)
                    {
                        Table table = new Table();
                        table.Name = (string)dataRow["TABLE_NAME"];
                        QueryTable(connection, table);
                        tableList.Add(table);
                    }
                }

                int count = 0;
                if (tableList.Count > 0)
                {
                    // Tạo đường dẫn Thư mục lưu SQL
                    Utility.CreateSubDirectory(sqlPath, true);

                    // Tạo đường dẫn Thư mục lưu Cshaft
                    Utility.CreateSubDirectory(csPath, true);

                    //Tạo đường dẫn để lưu DataAccess
                    Utility.CreateSubDirectory(Path.Combine(csPath, "DataAccess"), true);
                    //Tạo đường dẫn để lưu Business
                    Utility.CreateSubDirectory(Path.Combine(csPath, "Business"), true);
                    //Tạo đường dẫn để lưu Entity
                    Utility.CreateSubDirectory(Path.Combine(csPath, "Entity"), true);
                    //Tao duong dan Cho webform
                    Utility.CreateSubDirectory(Path.Combine(csPath, projectName), true);
                    //Copy các folder cần thiết vào thư mục Web project vừa tạo
                    string source = Path.Combine(Application.StartupPath, "Template");
                    string target = Path.Combine(csPath, projectName);
                    App_ChuongTrinhSinhCodeTuDong.Common.CopyAll(source, target);
 
                    //Tao folder Chứa file cua dự án winform
                    Utility.CreateSubDirectory(Path.Combine(csPath, "Presentation_WindowsForm"), true);
       
                    // Create the necessary "use [database]" statement
                    SqlGenerator.CreateUseDatabaseStatement(databaseName, sqlPath, createMultipleFiles);
                    // Create the necessary database logins
                   /////////////// SqlGenerator.CreateUserQueries(databaseName, grantLoginName, sqlPath, createMultipleFiles);

                    //Create Admin Master page
                    string pathAdminMasterPage = Path.Combine(csPath, projectName);
                    //pathAdminMasterPage = Path.Combine(pathAdminMasterPage, "Admin");
                    //Phan giao dien admin masterpage
                    csGeneratorAdminMasterPage.createMasterPage_aspx(pathAdminMasterPage, targetNamespace, tableList);
                    //Create .CS for Admin master Pager
                    csGeneratorAdminMasterPage.createMasterPage_cs(pathAdminMasterPage, targetNamespace, tableList);
                    //Create Design.CS for Admin master Pager
                    csGeneratorAdminMasterPage.createMasterPage_designer_cs(pathAdminMasterPage, targetNamespace, tableList);
                    // Create the CRUD stored procedures and data access code for each table
                    foreach (Table table in tableList)
                    {
                    

                        //tạo lớp SqlDataProvider để kết nối với csdl
                        CsGenerator.CreateSqlDataProviderClass(targetNamespace, csPath);
                        string str = connectionString;

                        //Tạo App.config
                        CsGenerator.CreateAppconfigClass(connectionString, csPath);

                        ////Phần giao diện cho Sử dụng thêm sửa ở form khác---Phần giao diện .Designer.cs
                        //CsGeneratorGraphic_ForWindowsForm.WindowsForm_UsingOtherForm_Designer(databaseName, table, targetNamespace,DbContext, csPath);
                        ////Phần Code sử dụng thêm sửa ở form khác--Phần code .cs
                        //CsGeneratorGraphic_ForWindowsForm.WindowsForm_UsingOtherForm_Code(databaseName, table, targetNamespace, DbContext, csPath);
                        ////Phần Code tạo file .resx
                        //CsGeneratorGraphic_ForWindowsForm.WindowsForm_UsingOtherForm_Resx(databaseName, table, targetNamespace, DbContext, csPath);

                        ////Phần Code sử dụng thêm sửa ở form khác-formUpdate--Designer.cs
                        //CsGeneratorGraphic_ForWindowsForm.WindowsFormUpdate_UsingOtherForm_Designer(databaseName, table, targetNamespace, storedProcedurePrefix, daoSuffix, dtoSuffix, csPath);
                        ////Phần Code sử dụng thêm sửa ở form khác-formUpdate--Phần code .cs
                        //CsGeneratorGraphic_ForWindowsForm.WindowsFormUpdate_UsingOtherForm_Code(databaseName, table, targetNamespace, storedProcedurePrefix, daoSuffix, dtoSuffix, busSuffix, csPath);
                        ////Phần Code tạo file .resx
                        //CsGeneratorGraphic_ForWindowsForm.WindowsFormUpdate_UsingOtherForm_Resx(databaseName, table, targetNamespace, storedProcedurePrefix, daoSuffix, dtoSuffix, csPath);

                        /////////////////WEB FORM///////////////////////////////


                        //Tạo thư mục web Admin
                        string foldelWebAdmin = Path.Combine(csPath, target);
                       // foldelWebAdmin = Path.Combine(foldelWebAdmin, "Admin");
                        if (!Directory.Exists(foldelWebAdmin))
                        {
                            Directory.CreateDirectory(foldelWebAdmin);
                        }
                        //Tạo web config
                        CsGenerator.CreateWebconfigClass(connectionString, target);
                        //Tạo bang Them,Sua,Xoa

                        CsGeneratorGraphic_CodeWebFormLinQ.GridViewToTextBox(databaseName, table, targetNamespace, storedProcedurePrefix, DbContext, foldelWebAdmin);
                        //Code them sua xoa
                        CsGeneratorGraphic_CodeWebFormLinQ.GridViewToTextBox_CS(databaseName, table, targetNamespace, storedProcedurePrefix, DbContext, foldelWebAdmin);
                        //phan .designer.cs
                        CsGeneratorGraphic_CodeWebFormLinQ.GridViewToTextBox_Designer_CS(databaseName, table, targetNamespace, storedProcedurePrefix,DbContext, foldelWebAdmin);
                        //Tạo File Gridview


                        //Folder chứa code tham khảo
                        //Tạo thư mục web Admin
                        string codeThamKhao = Path.Combine(csPath, target);
                        codeThamKhao = Path.Combine(codeThamKhao, "Admin2");
                        if (!Directory.Exists(codeThamKhao))
                        {
                            Directory.CreateDirectory(codeThamKhao);
                        }
                        //CsGeneratorGraphic_CodeWebFormLinQ.CreateModifyGridview(databaseName, table, targetNamespace, storedProcedurePrefix, busSuffix, dtoSuffix, codeThamKhao);
                        //CsGeneratorGraphic_CodeWebFormLinQ.CreateModifyGridviewCS(databaseName, table, targetNamespace, storedProcedurePrefix, busSuffix, dtoSuffix, codeThamKhao);
                        //CsGeneratorGraphic_CodeWebFormLinQ.CreateModifyGridview_Designer_CS(databaseName, table, targetNamespace, storedProcedurePrefix, busSuffix, dtoSuffix, codeThamKhao);

                        //count++;
                        //TableCounted(null, new CountEventArgs(count));
                    }

                    //Không liên kết được đến file Resources => bỏ
                    //	CsGenerator.CreateSharpCore(csPath);
                    //	CsGenerator.CreateAssemblyInfo(csPath, databaseName, databaseName);
                    //	CsGenerator.CreateProjectFile(csPath, projectName, tableList, daoSuffix, dtoSuffix);
                }
            }
            catch (Exception)
            {

                throw;
            }
			
		}

		/// <summary>
		/// Retrieves the column, primary key, and foreign key information for the specified table.
		/// </summary>
		/// <param name="connection">The SqlConnection to be used when querying for the table information.</param>
		/// <param name="table">The table instance that information should be retrieved for.</param>
		private static void QueryTable(SqlConnection connection, Table table)
		{
			// Get a list of the entities in the database
			DataTable dataTable = new DataTable();
		    string strQuery = "select INFORMATION_SCHEMA.COLUMNS.*,";
            strQuery = strQuery + "COL_LENGTH('" + table.Name + "', INFORMATION_SCHEMA.COLUMNS.COLUMN_NAME) AS COLUMN_LENGTH,";
            strQuery = strQuery + "COLUMNPROPERTY(OBJECT_ID('" + table.Name + "'), INFORMATION_SCHEMA.COLUMNS.COLUMN_NAME, 'IsComputed') as IS_COMPUTED,";
            strQuery = strQuery + "COLUMNPROPERTY(OBJECT_ID('" + table.Name + "'), INFORMATION_SCHEMA.COLUMNS.COLUMN_NAME, 'IsIdentity') as IS_IDENTITY,";
            strQuery = strQuery + "COLUMNPROPERTY(OBJECT_ID('" + table.Name + "'), INFORMATION_SCHEMA.COLUMNS.COLUMN_NAME, 'IsRowGuidCol') as IS_ROWGUIDCOL ";
		    strQuery = strQuery +"from INFORMATION_SCHEMA.COLUMNS where INFORMATION_SCHEMA.COLUMNS.TABLE_NAME = '"+table.Name+"'";
            SqlCommand cmd = new SqlCommand(strQuery, connection);
 			//SqlDataAdapter dataAdapter = new SqlDataAdapter(Utility.GetColumnQuery(table.Name), connection);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(cmd);
			dataAdapter.Fill(dataTable);
            cmd.Dispose();
            dataAdapter.Dispose();
			foreach (DataRow columnRow in dataTable.Rows)
			{
				Column column = new Column();
				column.Name = columnRow["COLUMN_NAME"].ToString();
				column.Type = columnRow["DATA_TYPE"].ToString();
				column.Precision = columnRow["NUMERIC_PRECISION"].ToString();
				column.Scale = columnRow["NUMERIC_SCALE"].ToString();

				// Determine the column's length
				if (columnRow["CHARACTER_MAXIMUM_LENGTH"] != DBNull.Value)
				{
					column.Length = columnRow["CHARACTER_MAXIMUM_LENGTH"].ToString();
				}
				else
				{
					column.Length = columnRow["COLUMN_LENGTH"].ToString();
				}

				// Is the column a RowGuidCol column?
				if (columnRow["IS_ROWGUIDCOL"].ToString() == "1")
				{
					column.IsRowGuidCol = true;
				}

				// Is the column an Identity column?
				if (columnRow["IS_IDENTITY"].ToString() == "1")
				{
					column.IsIdentity = true;
				}

				// Is columnRow column a computed column?
				if (columnRow["IS_COMPUTED"].ToString() == "1")
				{
					column.IsComputed = true;
				}

				table.Columns.Add(column);
			}

			// Get the list of primary keys
			DataTable primaryKeyTable = Utility.GetPrimaryKeyList(connection, table.Name);
			foreach (DataRow primaryKeyRow in primaryKeyTable.Rows)
			{
				string primaryKeyName = primaryKeyRow["COLUMN_NAME"].ToString();

				foreach (Column column in table.Columns)
				{
					if (column.Name == primaryKeyName)
					{
						table.PrimaryKeys.Add(column);
						break;
					}
				}
			}

			// Get the list of foreign keys
			DataTable foreignKeyTable = Utility.GetForeignKeyList(connection, table.Name);
			foreach (DataRow foreignKeyRow in foreignKeyTable.Rows)
			{
				string name = foreignKeyRow["FK_NAME"].ToString();
				string columnName = foreignKeyRow["FKCOLUMN_NAME"].ToString();

				if (table.ForeignKeys.ContainsKey(name) == false)
				{
					table.ForeignKeys.Add(name, new List<Column>());
				}

				List<Column> foreignKeys = table.ForeignKeys[name];

				foreach (Column column in table.Columns)
				{
					if (column.Name == columnName)
					{
						foreignKeys.Add(column);
						break;
					}
				}
			}
		}
	}
}
