﻿namespace App_ChuongTrinhSinhCodeTuDong.CodeWebFormLinQ
{
    partial class FormCodeWebformLinQ
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormCodeWebformLinQ));
            this.generateButton = new System.Windows.Forms.Button();
            this.txtDbContext = new System.Windows.Forms.TextBox();
            this.daoSuffixLabel = new System.Windows.Forms.Label();
            this.txtNameSpace = new System.Windows.Forms.TextBox();
            this.namespaceLabel = new System.Windows.Forms.Label();
            this.csGroupBox = new System.Windows.Forms.GroupBox();
            this.txtPassword = new System.Windows.Forms.TextBox();
            this.txtUserName = new System.Windows.Forms.TextBox();
            this.passwordLabel = new System.Windows.Forms.Label();
            this.loginNameLabel = new System.Windows.Forms.Label();
            this.databaseLabel = new System.Windows.Forms.Label();
            this.txtDatabaseName = new System.Windows.Forms.TextBox();
            this.serverLabel = new System.Windows.Forms.Label();
            this.txtServerNane = new System.Windows.Forms.TextBox();
            this.txtProject = new System.Windows.Forms.TextBox();
            this.projectLabel = new System.Windows.Forms.Label();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.txtTestConnect = new System.Windows.Forms.Button();
            this.gvTable = new System.Windows.Forms.DataGridView();
            this.MuiltipleTab = new System.Windows.Forms.TabControl();
            this.Code = new System.Windows.Forms.TabPage();
            this.richTextBoxCode = new System.Windows.Forms.RichTextBox();
            this.design = new System.Windows.Forms.TabPage();
            this.richTextBoxDesign = new System.Windows.Forms.RichTextBox();
            this.tabPagePopupCode = new System.Windows.Forms.TabPage();
            this.richTextBoxCodePopup = new System.Windows.Forms.RichTextBox();
            this.tabPagePopupDesign = new System.Windows.Forms.TabPage();
            this.richTextBoxDesignPopup = new System.Windows.Forms.RichTextBox();
            this.tabPageModifyIngridviewCode = new System.Windows.Forms.TabPage();
            this.richTextBoxModifyInGridviewCode = new System.Windows.Forms.RichTextBox();
            this.tabPageModifyInGridview = new System.Windows.Forms.TabPage();
            this.richTextBoxModifyInGridviewDesign = new System.Windows.Forms.RichTextBox();
            this.tabPagetabPage = new System.Windows.Forms.TabPage();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.csGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gvTable)).BeginInit();
            this.MuiltipleTab.SuspendLayout();
            this.Code.SuspendLayout();
            this.design.SuspendLayout();
            this.tabPagePopupCode.SuspendLayout();
            this.tabPagePopupDesign.SuspendLayout();
            this.tabPageModifyIngridviewCode.SuspendLayout();
            this.tabPageModifyInGridview.SuspendLayout();
            this.tabPagetabPage.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.SuspendLayout();
            // 
            // generateButton
            // 
            this.generateButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.generateButton.Location = new System.Drawing.Point(196, 200);
            this.generateButton.Margin = new System.Windows.Forms.Padding(4);
            this.generateButton.Name = "generateButton";
            this.generateButton.Size = new System.Drawing.Size(96, 29);
            this.generateButton.TabIndex = 28;
            this.generateButton.Text = "Generate File";
            this.generateButton.Click += new System.EventHandler(this.generateButton_Click);
            // 
            // txtDbContext
            // 
            this.txtDbContext.Location = new System.Drawing.Point(104, 65);
            this.txtDbContext.Margin = new System.Windows.Forms.Padding(4);
            this.txtDbContext.Name = "txtDbContext";
            this.txtDbContext.Size = new System.Drawing.Size(175, 20);
            this.txtDbContext.TabIndex = 14;
            this.txtDbContext.Text = "dbDataContext";
            // 
            // daoSuffixLabel
            // 
            this.daoSuffixLabel.AutoSize = true;
            this.daoSuffixLabel.Location = new System.Drawing.Point(62, 67);
            this.daoSuffixLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.daoSuffixLabel.Name = "daoSuffixLabel";
            this.daoSuffixLabel.Size = new System.Drawing.Size(32, 13);
            this.daoSuffixLabel.TabIndex = 13;
            this.daoSuffixLabel.Text = "LinQ:";
            // 
            // txtNameSpace
            // 
            this.txtNameSpace.Location = new System.Drawing.Point(104, 40);
            this.txtNameSpace.Margin = new System.Windows.Forms.Padding(4);
            this.txtNameSpace.Name = "txtNameSpace";
            this.txtNameSpace.Size = new System.Drawing.Size(175, 20);
            this.txtNameSpace.TabIndex = 12;
            // 
            // namespaceLabel
            // 
            this.namespaceLabel.AutoSize = true;
            this.namespaceLabel.Location = new System.Drawing.Point(27, 42);
            this.namespaceLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.namespaceLabel.Name = "namespaceLabel";
            this.namespaceLabel.Size = new System.Drawing.Size(67, 13);
            this.namespaceLabel.TabIndex = 11;
            this.namespaceLabel.Text = "Namespace:";
            // 
            // csGroupBox
            // 
            this.csGroupBox.Controls.Add(this.txtPassword);
            this.csGroupBox.Controls.Add(this.txtUserName);
            this.csGroupBox.Controls.Add(this.passwordLabel);
            this.csGroupBox.Controls.Add(this.loginNameLabel);
            this.csGroupBox.Controls.Add(this.databaseLabel);
            this.csGroupBox.Controls.Add(this.txtDatabaseName);
            this.csGroupBox.Controls.Add(this.serverLabel);
            this.csGroupBox.Controls.Add(this.txtServerNane);
            this.csGroupBox.Controls.Add(this.txtProject);
            this.csGroupBox.Controls.Add(this.projectLabel);
            this.csGroupBox.Controls.Add(this.txtDbContext);
            this.csGroupBox.Controls.Add(this.daoSuffixLabel);
            this.csGroupBox.Controls.Add(this.txtNameSpace);
            this.csGroupBox.Controls.Add(this.namespaceLabel);
            this.csGroupBox.Location = new System.Drawing.Point(13, 6);
            this.csGroupBox.Margin = new System.Windows.Forms.Padding(4);
            this.csGroupBox.Name = "csGroupBox";
            this.csGroupBox.Padding = new System.Windows.Forms.Padding(4);
            this.csGroupBox.Size = new System.Drawing.Size(292, 193);
            this.csGroupBox.TabIndex = 29;
            this.csGroupBox.TabStop = false;
            this.csGroupBox.Text = "C#";
            // 
            // txtPassword
            // 
            this.txtPassword.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.txtPassword.Location = new System.Drawing.Point(104, 165);
            this.txtPassword.Margin = new System.Windows.Forms.Padding(4);
            this.txtPassword.Name = "txtPassword";
            this.txtPassword.PasswordChar = '*';
            this.txtPassword.Size = new System.Drawing.Size(175, 20);
            this.txtPassword.TabIndex = 22;
            // 
            // txtUserName
            // 
            this.txtUserName.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.txtUserName.Location = new System.Drawing.Point(104, 140);
            this.txtUserName.Margin = new System.Windows.Forms.Padding(4);
            this.txtUserName.Name = "txtUserName";
            this.txtUserName.Size = new System.Drawing.Size(175, 20);
            this.txtUserName.TabIndex = 21;
            // 
            // passwordLabel
            // 
            this.passwordLabel.AutoSize = true;
            this.passwordLabel.Location = new System.Drawing.Point(38, 167);
            this.passwordLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.passwordLabel.Name = "passwordLabel";
            this.passwordLabel.Size = new System.Drawing.Size(56, 13);
            this.passwordLabel.TabIndex = 20;
            this.passwordLabel.Text = "Password:";
            // 
            // loginNameLabel
            // 
            this.loginNameLabel.AutoSize = true;
            this.loginNameLabel.Location = new System.Drawing.Point(27, 142);
            this.loginNameLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.loginNameLabel.Name = "loginNameLabel";
            this.loginNameLabel.Size = new System.Drawing.Size(67, 13);
            this.loginNameLabel.TabIndex = 19;
            this.loginNameLabel.Text = "Login Name:";
            // 
            // databaseLabel
            // 
            this.databaseLabel.AutoSize = true;
            this.databaseLabel.Location = new System.Drawing.Point(38, 117);
            this.databaseLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.databaseLabel.Name = "databaseLabel";
            this.databaseLabel.Size = new System.Drawing.Size(56, 13);
            this.databaseLabel.TabIndex = 17;
            this.databaseLabel.Text = "Database:";
            // 
            // txtDatabaseName
            // 
            this.txtDatabaseName.Location = new System.Drawing.Point(104, 115);
            this.txtDatabaseName.Margin = new System.Windows.Forms.Padding(4);
            this.txtDatabaseName.Name = "txtDatabaseName";
            this.txtDatabaseName.Size = new System.Drawing.Size(175, 20);
            this.txtDatabaseName.TabIndex = 18;
            // 
            // serverLabel
            // 
            this.serverLabel.AutoSize = true;
            this.serverLabel.Location = new System.Drawing.Point(53, 92);
            this.serverLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.serverLabel.Name = "serverLabel";
            this.serverLabel.Size = new System.Drawing.Size(41, 13);
            this.serverLabel.TabIndex = 16;
            this.serverLabel.Text = "Server:";
            // 
            // txtServerNane
            // 
            this.txtServerNane.Location = new System.Drawing.Point(104, 90);
            this.txtServerNane.Margin = new System.Windows.Forms.Padding(4);
            this.txtServerNane.Name = "txtServerNane";
            this.txtServerNane.Size = new System.Drawing.Size(175, 20);
            this.txtServerNane.TabIndex = 15;
            // 
            // txtProject
            // 
            this.txtProject.Location = new System.Drawing.Point(104, 15);
            this.txtProject.Margin = new System.Windows.Forms.Padding(4);
            this.txtProject.Name = "txtProject";
            this.txtProject.Size = new System.Drawing.Size(175, 20);
            this.txtProject.TabIndex = 10;
            // 
            // projectLabel
            // 
            this.projectLabel.AutoSize = true;
            this.projectLabel.Location = new System.Drawing.Point(51, 17);
            this.projectLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.projectLabel.Name = "projectLabel";
            this.projectLabel.Size = new System.Drawing.Size(43, 13);
            this.projectLabel.TabIndex = 9;
            this.projectLabel.Text = "Project:";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.txtTestConnect);
            this.splitContainer1.Panel1.Controls.Add(this.gvTable);
            this.splitContainer1.Panel1.Controls.Add(this.generateButton);
            this.splitContainer1.Panel1.Controls.Add(this.csGroupBox);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.MuiltipleTab);
            this.splitContainer1.Size = new System.Drawing.Size(1195, 508);
            this.splitContainer1.SplitterDistance = 331;
            this.splitContainer1.TabIndex = 32;
            // 
            // txtTestConnect
            // 
            this.txtTestConnect.Location = new System.Drawing.Point(27, 203);
            this.txtTestConnect.Name = "txtTestConnect";
            this.txtTestConnect.Size = new System.Drawing.Size(91, 23);
            this.txtTestConnect.TabIndex = 32;
            this.txtTestConnect.Text = "Re- Connect";
            this.txtTestConnect.UseVisualStyleBackColor = true;
            this.txtTestConnect.Click += new System.EventHandler(this.txtTestConnect_Click);
            // 
            // gvTable
            // 
            this.gvTable.AllowUserToAddRows = false;
            this.gvTable.AllowUserToDeleteRows = false;
            this.gvTable.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.gvTable.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.gvTable.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.gvTable.BackgroundColor = System.Drawing.Color.White;
            this.gvTable.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gvTable.Location = new System.Drawing.Point(13, 232);
            this.gvTable.Name = "gvTable";
            this.gvTable.ReadOnly = true;
            this.gvTable.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.gvTable.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.gvTable.Size = new System.Drawing.Size(304, 264);
            this.gvTable.TabIndex = 31;
            this.gvTable.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.gvTable_CellClick);
            // 
            // MuiltipleTab
            // 
            this.MuiltipleTab.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.MuiltipleTab.Controls.Add(this.Code);
            this.MuiltipleTab.Controls.Add(this.design);
            this.MuiltipleTab.Controls.Add(this.tabPagePopupCode);
            this.MuiltipleTab.Controls.Add(this.tabPagePopupDesign);
            this.MuiltipleTab.Controls.Add(this.tabPageModifyIngridviewCode);
            this.MuiltipleTab.Controls.Add(this.tabPageModifyInGridview);
            this.MuiltipleTab.Controls.Add(this.tabPagetabPage);
            this.MuiltipleTab.Controls.Add(this.tabPage1);
            this.MuiltipleTab.Location = new System.Drawing.Point(0, 0);
            this.MuiltipleTab.Name = "MuiltipleTab";
            this.MuiltipleTab.SelectedIndex = 0;
            this.MuiltipleTab.Size = new System.Drawing.Size(860, 508);
            this.MuiltipleTab.TabIndex = 0;
            // 
            // Code
            // 
            this.Code.Controls.Add(this.richTextBoxCode);
            this.Code.Location = new System.Drawing.Point(4, 22);
            this.Code.Name = "Code";
            this.Code.Padding = new System.Windows.Forms.Padding(3);
            this.Code.Size = new System.Drawing.Size(852, 482);
            this.Code.TabIndex = 0;
            this.Code.Text = "Code";
            this.Code.UseVisualStyleBackColor = true;
            // 
            // richTextBoxCode
            // 
            this.richTextBoxCode.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBoxCode.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBoxCode.Location = new System.Drawing.Point(3, 3);
            this.richTextBoxCode.Name = "richTextBoxCode";
            this.richTextBoxCode.Size = new System.Drawing.Size(846, 476);
            this.richTextBoxCode.TabIndex = 0;
            this.richTextBoxCode.Text = "";
            // 
            // design
            // 
            this.design.Controls.Add(this.richTextBoxDesign);
            this.design.Location = new System.Drawing.Point(4, 22);
            this.design.Name = "design";
            this.design.Padding = new System.Windows.Forms.Padding(3);
            this.design.Size = new System.Drawing.Size(852, 482);
            this.design.TabIndex = 1;
            this.design.Text = "Giao diện web";
            this.design.UseVisualStyleBackColor = true;
            // 
            // richTextBoxDesign
            // 
            this.richTextBoxDesign.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBoxDesign.Location = new System.Drawing.Point(3, 3);
            this.richTextBoxDesign.Name = "richTextBoxDesign";
            this.richTextBoxDesign.Size = new System.Drawing.Size(846, 476);
            this.richTextBoxDesign.TabIndex = 1;
            this.richTextBoxDesign.Text = "";
            // 
            // tabPagePopupCode
            // 
            this.tabPagePopupCode.Controls.Add(this.richTextBoxCodePopup);
            this.tabPagePopupCode.Location = new System.Drawing.Point(4, 22);
            this.tabPagePopupCode.Name = "tabPagePopupCode";
            this.tabPagePopupCode.Padding = new System.Windows.Forms.Padding(3);
            this.tabPagePopupCode.Size = new System.Drawing.Size(852, 482);
            this.tabPagePopupCode.TabIndex = 2;
            this.tabPagePopupCode.Text = "Using Popup Code";
            this.tabPagePopupCode.UseVisualStyleBackColor = true;
            // 
            // richTextBoxCodePopup
            // 
            this.richTextBoxCodePopup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBoxCodePopup.Location = new System.Drawing.Point(3, 3);
            this.richTextBoxCodePopup.Name = "richTextBoxCodePopup";
            this.richTextBoxCodePopup.Size = new System.Drawing.Size(846, 476);
            this.richTextBoxCodePopup.TabIndex = 1;
            this.richTextBoxCodePopup.Text = "";
            // 
            // tabPagePopupDesign
            // 
            this.tabPagePopupDesign.Controls.Add(this.richTextBoxDesignPopup);
            this.tabPagePopupDesign.Location = new System.Drawing.Point(4, 22);
            this.tabPagePopupDesign.Name = "tabPagePopupDesign";
            this.tabPagePopupDesign.Padding = new System.Windows.Forms.Padding(3);
            this.tabPagePopupDesign.Size = new System.Drawing.Size(852, 482);
            this.tabPagePopupDesign.TabIndex = 3;
            this.tabPagePopupDesign.Text = "Using Popup Design";
            this.tabPagePopupDesign.UseVisualStyleBackColor = true;
            // 
            // richTextBoxDesignPopup
            // 
            this.richTextBoxDesignPopup.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBoxDesignPopup.Location = new System.Drawing.Point(3, 3);
            this.richTextBoxDesignPopup.Name = "richTextBoxDesignPopup";
            this.richTextBoxDesignPopup.Size = new System.Drawing.Size(846, 476);
            this.richTextBoxDesignPopup.TabIndex = 2;
            this.richTextBoxDesignPopup.Text = "";
            // 
            // tabPageModifyIngridviewCode
            // 
            this.tabPageModifyIngridviewCode.Controls.Add(this.richTextBoxModifyInGridviewCode);
            this.tabPageModifyIngridviewCode.Location = new System.Drawing.Point(4, 22);
            this.tabPageModifyIngridviewCode.Name = "tabPageModifyIngridviewCode";
            this.tabPageModifyIngridviewCode.Size = new System.Drawing.Size(852, 482);
            this.tabPageModifyIngridviewCode.TabIndex = 5;
            this.tabPageModifyIngridviewCode.Text = "Modify In Gridview Code";
            this.tabPageModifyIngridviewCode.UseVisualStyleBackColor = true;
            // 
            // richTextBoxModifyInGridviewCode
            // 
            this.richTextBoxModifyInGridviewCode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBoxModifyInGridviewCode.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBoxModifyInGridviewCode.Location = new System.Drawing.Point(0, 0);
            this.richTextBoxModifyInGridviewCode.Name = "richTextBoxModifyInGridviewCode";
            this.richTextBoxModifyInGridviewCode.Size = new System.Drawing.Size(852, 482);
            this.richTextBoxModifyInGridviewCode.TabIndex = 2;
            this.richTextBoxModifyInGridviewCode.Text = "";
            // 
            // tabPageModifyInGridview
            // 
            this.tabPageModifyInGridview.Controls.Add(this.richTextBoxModifyInGridviewDesign);
            this.tabPageModifyInGridview.Location = new System.Drawing.Point(4, 22);
            this.tabPageModifyInGridview.Name = "tabPageModifyInGridview";
            this.tabPageModifyInGridview.Size = new System.Drawing.Size(852, 482);
            this.tabPageModifyInGridview.TabIndex = 4;
            this.tabPageModifyInGridview.Text = "Modify In Gridview Design";
            this.tabPageModifyInGridview.UseVisualStyleBackColor = true;
            // 
            // richTextBoxModifyInGridviewDesign
            // 
            this.richTextBoxModifyInGridviewDesign.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBoxModifyInGridviewDesign.Location = new System.Drawing.Point(0, 0);
            this.richTextBoxModifyInGridviewDesign.Name = "richTextBoxModifyInGridviewDesign";
            this.richTextBoxModifyInGridviewDesign.Size = new System.Drawing.Size(852, 482);
            this.richTextBoxModifyInGridviewDesign.TabIndex = 3;
            this.richTextBoxModifyInGridviewDesign.Text = "";
            // 
            // tabPagetabPage
            // 
            this.tabPagetabPage.Controls.Add(this.richTextBox1);
            this.tabPagetabPage.Location = new System.Drawing.Point(4, 22);
            this.tabPagetabPage.Name = "tabPagetabPage";
            this.tabPagetabPage.Padding = new System.Windows.Forms.Padding(3);
            this.tabPagetabPage.Size = new System.Drawing.Size(852, 482);
            this.tabPagetabPage.TabIndex = 6;
            this.tabPagetabPage.Text = "StylePhanTrang";
            this.tabPagetabPage.UseVisualStyleBackColor = true;
            // 
            // richTextBox1
            // 
            this.richTextBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBox1.Location = new System.Drawing.Point(3, 3);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(846, 476);
            this.richTextBox1.TabIndex = 2;
            this.richTextBox1.Text = resources.GetString("richTextBox1.Text");
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.textBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(852, 482);
            this.tabPage1.TabIndex = 7;
            this.tabPage1.Text = "tabPage1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(3, 6);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(830, 468);
            this.textBox1.TabIndex = 0;
            this.textBox1.Text = resources.GetString("textBox1.Text");
            // 
            // FormCodeWebformLinQ
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1195, 508);
            this.Controls.Add(this.splitContainer1);
            this.Name = "FormCodeWebformLinQ";
            this.Text = "FormCodeWebformLinQ";
            this.Load += new System.EventHandler(this.FormCodeWebformLinQ_Load);
            this.csGroupBox.ResumeLayout(false);
            this.csGroupBox.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gvTable)).EndInit();
            this.MuiltipleTab.ResumeLayout(false);
            this.Code.ResumeLayout(false);
            this.design.ResumeLayout(false);
            this.tabPagePopupCode.ResumeLayout(false);
            this.tabPagePopupDesign.ResumeLayout(false);
            this.tabPageModifyIngridviewCode.ResumeLayout(false);
            this.tabPageModifyInGridview.ResumeLayout(false);
            this.tabPagetabPage.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button generateButton;
        private System.Windows.Forms.TextBox txtDbContext;
        private System.Windows.Forms.Label daoSuffixLabel;
        private System.Windows.Forms.TextBox txtNameSpace;
        private System.Windows.Forms.Label namespaceLabel;
        private System.Windows.Forms.GroupBox csGroupBox;
        private System.Windows.Forms.TextBox txtProject;
        private System.Windows.Forms.Label projectLabel;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.DataGridView gvTable;
        private System.Windows.Forms.TabControl MuiltipleTab;
        private System.Windows.Forms.TabPage Code;
        private System.Windows.Forms.TabPage design;
        private System.Windows.Forms.RichTextBox richTextBoxCode;
        private System.Windows.Forms.RichTextBox richTextBoxDesign;
        private System.Windows.Forms.TabPage tabPagePopupCode;
        private System.Windows.Forms.TabPage tabPagePopupDesign;
        private System.Windows.Forms.RichTextBox richTextBoxCodePopup;
        private System.Windows.Forms.RichTextBox richTextBoxDesignPopup;
        private System.Windows.Forms.TabPage tabPageModifyInGridview;
        private System.Windows.Forms.TabPage tabPageModifyIngridviewCode;
        private System.Windows.Forms.RichTextBox richTextBoxModifyInGridviewCode;
        private System.Windows.Forms.RichTextBox richTextBoxModifyInGridviewDesign;
        private System.Windows.Forms.TabPage tabPagetabPage;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label serverLabel;
        private System.Windows.Forms.TextBox txtServerNane;
        private System.Windows.Forms.Label databaseLabel;
        private System.Windows.Forms.TextBox txtDatabaseName;
        private System.Windows.Forms.TextBox txtPassword;
        private System.Windows.Forms.TextBox txtUserName;
        private System.Windows.Forms.Label passwordLabel;
        private System.Windows.Forms.Label loginNameLabel;
        private System.Windows.Forms.Button txtTestConnect;
    }
}