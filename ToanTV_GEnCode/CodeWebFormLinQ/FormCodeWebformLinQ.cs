﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Win32;
using System.IO;
namespace App_ChuongTrinhSinhCodeTuDong.CodeWebFormLinQ
{
    public partial class FormCodeWebformLinQ : Form
    {
        public FormCodeWebformLinQ()
        {
            InitializeComponent();
        }

      
        public static Table table;
        public static List<Table> tableList;
        private void FormCodeWebformLinQ_Load(object sender, EventArgs e)
        {
            try
            {
                App_ChuongTrinhSinhCodeTuDong.Common.LoadFileIni();
              
                txtServerNane.Text = cConfig.SvName;
                txtDatabaseName.Text = cConfig.DbName;
                txtUserName.Text = cConfig.userName;
                txtPassword.Text = cConfig.passWord;
                txtProject.Text= cConfig.projectName;
                txtNameSpace.Text = cConfig.nameSpace;

                txtProject.Text = cConfig.projectName; ;
                txtNameSpace.Text = cConfig.nameSpace;

                tableList = new List<Table>();
                string databaseName;


                string connectionString = "Server=" + txtServerNane.Text + "; Database=" + txtDatabaseName.Text +
                                        "; User ID=" + txtUserName.Text + "; Password=" + txtPassword.Text + ";";
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    databaseName = Utility.FormatPascal(connection.Database);

                    connection.Open();

                    // Lấy ra danh sách các bảng trong database
                    DataTable dataTable = new DataTable();
                    string strQuery = "select TABLE_NAME from INFORMATION_SCHEMA.TABLES ";
                    strQuery = strQuery + "where TABLE_TYPE = 'BASE TABLE' and (TABLE_NAME != 'dtProperties' and TABLE_NAME != 'sysdiagrams') ";
                    strQuery = strQuery + "and TABLE_CATALOG = '" + databaseName + "' order by TABLE_NAME";
                    SqlCommand cmd = new SqlCommand(strQuery, connection);
                    SqlDataAdapter dataAdapter = new SqlDataAdapter(cmd);
                    dataAdapter.Fill(dataTable);
                    cmd.Dispose();
                    dataAdapter.Dispose();

                    gvTable.DataSource = dataTable;
                    // Xử lý với từng bảng 
                    foreach (DataRow dataRow in dataTable.Rows)
                    {
                        Table table = new Table();
                        table.Name = (string)dataRow["TABLE_NAME"];
                        QueryTable(connection, table);
                        tableList.Add(table);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                
            }
        
        }
      
        /// <summary>
        /// Lấy cột từ tên bảng
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="table"></param>
        private static void QueryTable(SqlConnection connection, Table table)
        {
            // Get a list of the entities in the database
            DataTable dataTable = new DataTable();
            string strQuery = "select INFORMATION_SCHEMA.COLUMNS.*,";
            strQuery = strQuery + "COL_LENGTH('" + table.Name + "', INFORMATION_SCHEMA.COLUMNS.COLUMN_NAME) AS COLUMN_LENGTH,";
            strQuery = strQuery + "COLUMNPROPERTY(OBJECT_ID('" + table.Name + "'), INFORMATION_SCHEMA.COLUMNS.COLUMN_NAME, 'IsComputed') as IS_COMPUTED,";
            strQuery = strQuery + "COLUMNPROPERTY(OBJECT_ID('" + table.Name + "'), INFORMATION_SCHEMA.COLUMNS.COLUMN_NAME, 'IsIdentity') as IS_IDENTITY,";
            strQuery = strQuery + "COLUMNPROPERTY(OBJECT_ID('" + table.Name + "'), INFORMATION_SCHEMA.COLUMNS.COLUMN_NAME, 'IsRowGuidCol') as IS_ROWGUIDCOL ";
            strQuery = strQuery + "from INFORMATION_SCHEMA.COLUMNS where INFORMATION_SCHEMA.COLUMNS.TABLE_NAME = '" + table.Name + "'";
            SqlCommand cmd = new SqlCommand(strQuery, connection);
            //SqlDataAdapter dataAdapter = new SqlDataAdapter(Utility.GetColumnQuery(table.Name), connection);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(cmd);
            dataAdapter.Fill(dataTable);
            cmd.Dispose();
            dataAdapter.Dispose();
            foreach (DataRow columnRow in dataTable.Rows)
            {
                Column column = new Column();
                column.Name = columnRow["COLUMN_NAME"].ToString();
                column.Type = columnRow["DATA_TYPE"].ToString();
                column.Precision = columnRow["NUMERIC_PRECISION"].ToString();
                column.Scale = columnRow["NUMERIC_SCALE"].ToString();

                // Determine the column's length
                if (columnRow["CHARACTER_MAXIMUM_LENGTH"] != DBNull.Value)
                {
                    column.Length = columnRow["CHARACTER_MAXIMUM_LENGTH"].ToString();
                }
                else
                {
                    column.Length = columnRow["COLUMN_LENGTH"].ToString();
                }

                // Is the column a RowGuidCol column?
                if (columnRow["IS_ROWGUIDCOL"].ToString() == "1")
                {
                    column.IsRowGuidCol = true;
                }

                // Is the column an Identity column?
                if (columnRow["IS_IDENTITY"].ToString() == "1")
                {
                    column.IsIdentity = true;
                }

                // Is columnRow column a computed column?
                if (columnRow["IS_COMPUTED"].ToString() == "1")
                {
                    column.IsComputed = true;
                }

                table.Columns.Add(column);
            }

            // Get the list of primary keys
            DataTable primaryKeyTable = Utility.GetPrimaryKeyList(connection, table.Name);
            foreach (DataRow primaryKeyRow in primaryKeyTable.Rows)
            {
                string primaryKeyName = primaryKeyRow["COLUMN_NAME"].ToString();

                foreach (Column column in table.Columns)
                {
                    if (column.Name == primaryKeyName)
                    {
                        table.PrimaryKeys.Add(column);
                        break;
                    }
                }
            }

            // Get the list of foreign keys
            DataTable foreignKeyTable = Utility.GetForeignKeyList(connection, table.Name);
            foreach (DataRow foreignKeyRow in foreignKeyTable.Rows)
            {
                string name = foreignKeyRow["FK_NAME"].ToString();
                string columnName = foreignKeyRow["FKCOLUMN_NAME"].ToString();

                if (table.ForeignKeys.ContainsKey(name) == false)
                {
                    table.ForeignKeys.Add(name, new List<Column>());
                }

                List<Column> foreignKeys = table.ForeignKeys[name];

                foreach (Column column in table.Columns)
                {
                    if (column.Name == columnName)
                    {
                        foreignKeys.Add(column);
                        break;
                    }
                }
            }
        }
      
        /// <summary>
        /// Sinh file 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void generateButton_Click(object sender, EventArgs e)
        {
            
            string connectionString;
            try
            {
                connectionString = "Server=" + txtServerNane.Text + "; Database=" + txtDatabaseName.Text +
                                       "; User ID=" + txtUserName.Text + "; Password=" + txtPassword.Text + ";";

                //Mở lại đường dẫn đã mở trước đó và tạo đường dẫn mới
                string outputDirectory;
                using (
                    RegistryKey registryKey =
                        Registry.CurrentUser.CreateSubKey(@"Software\Adrian Anttila\CSharpDataTier"))
                {
                    outputDirectory = (string)registryKey.GetValue("OutputDirectory", String.Empty);
                }

                using (FolderBrowserDialog dialog = new FolderBrowserDialog())
                {
                    dialog.Description = "Chọn đường dẫn để lưu file";
                    dialog.SelectedPath = outputDirectory;
                    dialog.ShowNewFolderButton = true; //Cho phép tạo folder mới
                    if (dialog.ShowDialog(this) == DialogResult.OK)
                    {
                        outputDirectory = dialog.SelectedPath;
                    }
                    else
                    {
                        return;
                    }
                }
                //Sinh code SQL va C# code
                Generator.Generate(outputDirectory, connectionString, txtUserName.Text,"Sp_", false, txtProject.Text, txtNameSpace.Text, txtDbContext.Text);


                MessageBox.Show("C# classes and stored procedures generated successfully.");

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                
            }
            finally
            {
                
                generateButton.Enabled = true;
            }

        }
       
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void gvTable_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                string tenBang = gvTable.Rows[e.RowIndex].Cells[0].Value.ToString();
                Table table = new Table();
                foreach (Table item in tableList)
                {
                    if (item.Name == tenBang)
                    {
                        table = item;
                    }

                }

                GridViewToTextBox_Design(txtDatabaseName.Text, table, txtNameSpace.Text, "SP_", txtDbContext.Text, "");
                GridViewToTextBox_CS(txtDatabaseName.Text, table, txtNameSpace.Text, "SP_", txtDbContext.Text, "");
               // GridViewToTextBox_Design_Popup(DbName, table, nameSpareStr, "SP_", txtDbContext.Text, "");
                //GridViewToTextBox_CS_Popup(DbName, table, nameSpareStr, "SP_", txtDbContext.Text, "");
                CreateModifyGridview(txtDatabaseName.Text, table, txtNameSpace.Text, "SP_", txtDbContext.Text, "");
                CreateModifyGridviewCS(txtDatabaseName.Text, table, txtNameSpace.Text, "SP_", txtDbContext.Text, "");
            }
            catch (Exception ex)
            {
            
                //throw;
            }
   
        }
      
        #region  Sửa xóa dữ liệu từ Gridview - TextBox

        /// <summary>
        /// Tạo giao diện phần thêm sửa xóa kiểu Click Gridview tới textbox
        /// </summary>
        /// <param name="databaseName"></param>
        /// <param name="table"></param>
        /// <param name="targetNamespace"></param>
        /// <param name="storedProcedurePrefix"></param>
        /// <param name="daoSuffix"></param>
        /// <param name="dtoSuffix"></param>
        /// <param name="path"></param>
        public void GridViewToTextBox_Design(string databaseName, Table table, string targetNamespace, string storedProcedurePrefix, string DbContext, string path)
        {

        
                var sb = new StringBuilder();

                sb.Append("\n <%@ Page Title=\"\" Language=\"C#\" MasterPageFile=\"~/Admin/admin.Master\" AutoEventWireup=\"true\"");
                sb.Append("\n CodeBehind=\"" + table.Name + ".aspx.cs\" Inherits=\"" + targetNamespace + "." + table.Name + "\" %>");
                //sb.Append("\n <asp:Content ID=\"Content1\" ContentPlaceHolderID=\"HeadContent\" runat=\"server\">");
                //sb.Append("\n </asp:Content>");
                sb.Append("\n <asp:Content ID=\"Content2\" ContentPlaceHolderID=\"MainContent\" runat=\"server\">");
                sb.Append("\n  <h2>" + table.Name.ToUpper() + "</h2>");
                sb.Append("\n <asp:Panel ID=\"PanelModify\" runat=\"server\">");
                sb.Append("\n  <table>");
                sb.Append("\n ");
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];

                    // Is the current column an identity column?);
                    if (column.IsIdentity == false)
                    {
                        sb.Append("\n <tr>");
                        sb.Append("\n <td>" + column.Name + ":</td>");
                        sb.Append("\n <td> <asp:TextBox ID=\"txt" + column.Name + "\" runat=\"server\" Height=\"26px\" Width=\"200px\"></asp:TextBox></td>");
                        sb.Append("\n </tr>");
                    }
                    else
                    {
                        sb.Append("\n <tr>");
                        sb.Append("\n <td>" + column.Name + ":</td>");
                        sb.Append("\n <td><asp:TextBox ID=\"txt" + column.Name + "\" runat=\"server\" Height=\"26px\" Width=\"200px\"></asp:TextBox></td>");
                        sb.Append("\n </tr>");

                    }
                }
                sb.Append("\n <tr>");
                sb.Append("\n  <td>");
                sb.Append("\n &nbsp;</td>");
                sb.Append("\n <td>");
                sb.Append("\n <asp:Button ID=\"btnSave\" runat=\"server\" onclick=\"btnSave_Click\" Text=\"Save\" />");

                sb.Append("\n <asp:Button ID=\"btnCancel\" runat=\"server\" onclick=\"btnCancel_Click\" Text=\"Cancel\" />");
                sb.Append("\n </td>");
                sb.Append("\n  </tr>");
                sb.Append("\n </table>");
            sb.Append("\n </asp:Panel>");
            sb.Append("\n  ");
                sb.Append("\n <asp:Panel ID=\"PanelShow\" runat=\"server\">");
            sb.Append("\n  <div class=\"row\">");
            sb.Append("\n <div class=\"col-md-8\">");

            sb.Append("\n  <asp:Button ID=\"btnThem\" runat=\"server\" onclick=\"btnThem_Click\" Text=\"+ Thêm mới\" />");
            sb.Append("\n  </div>");
            sb.Append("\n <div class=\"col-md-4\">");
            sb.Append("\n Số dòng hiển thị:");
            sb.Append("\n  <asp:DropDownList ID=\"ddlSoDong\" runat=\"server\"");
            sb.Append("\n  CssClass=\"dropdown\" AutoPostBack=\"true\"");
            sb.Append("\n  OnSelectedIndexChanged=\"ddlSoDong_SelectedIndexChanged\">");
            sb.Append("\n  <asp:ListItem>10</asp:ListItem>");
            sb.Append("\n  <asp:ListItem>30</asp:ListItem>");
            sb.Append("\n  <asp:ListItem>50</asp:ListItem>");
            sb.Append("\n  <asp:ListItem>100</asp:ListItem>");

            sb.Append("\n  </asp:DropDownList>");
            sb.Append("\n </div>");
            sb.Append("\n </div>");

            sb.Append("\n  <br /> ");
         
                sb.Append("\n  <asp:GridView ID=\"GridViewShow\" runat=\"server\" AutoGenerateColumns=\"false\" AllowPaging=\"True\" OnPageIndexChanging=\"GridViewShow_PageIndexChanging\" " + "EmptyDataText = \"Chưa có dữ liệu\""
                    + "OnRowCommand=\"GridViewShowRowClick\" Width=\"600px\"> ");

                sb.Append("\n  <Columns>");
                sb.Append("\n <asp:TemplateField HeaderText=\" + \">");
                sb.Append("\n <ItemTemplate>");
                sb.Append("\n  <%# Container.DataItemIndex+1 %></ItemTemplate>");
                sb.Append("\n <ItemStyle Width=\"10px\" />");
                sb.Append("\n </asp:TemplateField>");
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];

                    // Is the current column an identity column?);
                    if (column.IsIdentity == false)
                    {
                        sb.Append("\n   <asp:BoundField DataField=\"" + column.Name + "\" HeaderText=\"" + column.Name + "\" />");

                    }
                    else
                    {
                        sb.Append("\n  <asp:BoundField DataField=\"" + column.Name + "\" HeaderText=\"" + column.Name + "\" Visible=\"False\" />");

                        sb.Append("");
                    }
                }
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];
                    if (column.IsIdentity == true)
                    {
                        sb.Append("\n <asp:TemplateField HeaderText=\"Edit\">");
                        sb.Append("\n  <ItemTemplate>");

                        sb.Append("\n <asp:LinkButton ID=\"lbEdit\" runat=\"server\" CommandName='<%#\"S|\"+Eval(\"" + column.Name + "\")%>' Text=\"Edit\"></asp:LinkButton> ");
                        sb.Append("\n </ItemTemplate>");
                        sb.Append("\n </asp:TemplateField>");
                        sb.Append("\n <asp:TemplateField HeaderText=\"Delete\">");
                        sb.Append("\n <ItemTemplate>");

                        sb.Append("\n <asp:LinkButton ID=\"lbDelete\" runat=\"server\" CommandName='<%#\"X|\"+Eval(\"" + column.Name + "\")%>' OnClientClick=\"return confirm('Do you want to delete?')\" Text=\"Delete\"></asp:LinkButton>");
                        sb.Append("\n </ItemTemplate>");
                        sb.Append("\n </asp:TemplateField>");
                    }
                }

                sb.Append("\n </Columns>");
                sb.Append("<PagerSettings Mode=\"NumericFirstLast\" />");
            sb.Append("<PagerStyle CssClass=\"pagination-ys\" HorizontalAlign=\"Center\" />");
                sb.Append("</asp:GridView>");

                sb.Append(" </asp:Panel>");

                sb.Append("\n </asp:Content>");

                richTextBoxDesign.Text = sb.ToString();
               // streamWriter.WriteLine(sb);
                
                
           
        }

        /// <summary>
        /// Tạo giao diện phần thêm sửa xóa kiểu Click Gridview tới textbox
        /// </summary>
        /// <param name="databaseName"></param>
        /// <param name="table"></param>
        /// <param name="targetNamespace"></param>
        /// <param name="storedProcedurePrefix"></param>
        /// <param name="daoSuffix"></param>
        /// <param name="dtoSuffix"></param>
        /// <param name="path"></param>
        public void GridViewToTextBox_Design_Popup(string databaseName, Table table, string targetNamespace, string storedProcedurePrefix, string DbContext, string path)
        {

            //string className = Utility.FormatClassName(table.Name) + busSuffix;

            using (StreamWriter streamWriter = new StreamWriter(Path.Combine(path, table.Name + ".aspx")))
            {
                var sb = new StringBuilder();

                sb.Append("\n <%@ Page Title=\"\" Language=\"C#\" MasterPageFile=\"~/Admin/admin.Master\" AutoEventWireup=\"true\"");
                sb.Append("\n CodeBehind=\"" + table.Name + ".aspx.cs\" Inherits=\"" + targetNamespace + "." + table.Name + "\" %>");
                //sb.Append("\n <asp:Content ID=\"Content1\" ContentPlaceHolderID=\"HeadContent\" runat=\"server\">");
                //sb.Append("\n </asp:Content>");
                sb.Append("\n <asp:Content ID=\"Content2\" ContentPlaceHolderID=\"MainContent\" runat=\"server\">");
                sb.Append("\n  <h2>" + table.Name.ToUpper() + "</h2>");
                sb.Append("\n <asp:Panel ID=\"PanelModify\" runat=\"server\">");
                sb.Append("\n  <table>");
                sb.Append("\n ");
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];

                    // Is the current column an identity column?);
                    if (column.IsIdentity == false)
                    {
                        sb.Append("\n <tr>");
                        sb.Append("\n <td>" + column.Name + ":</td>");
                        sb.Append("\n <td> <asp:TextBox ID=\"txt" + column.Name + "\" runat=\"server\" Height=\"26px\" Width=\"200px\"></asp:TextBox></td>");
                        sb.Append("\n </tr>");
                    }
                    else
                    {
                        sb.Append("\n <tr>");
                        sb.Append("\n <td>" + column.Name + ":</td>");
                        sb.Append("\n <td><asp:TextBox ID=\"txt" + column.Name + "\" runat=\"server\" Height=\"26px\" Width=\"200px\"></asp:TextBox></td>");
                        sb.Append("\n </tr>");

                    }
                }
                sb.Append("\n <tr>");
                sb.Append("\n  <td>");
                sb.Append("\n &nbsp;</td>");
                sb.Append("\n <td>");
                sb.Append("\n <asp:Button ID=\"btnSave\" runat=\"server\" onclick=\"btnSave_Click\" Text=\"Save\" />");

                sb.Append("\n <asp:Button ID=\"btnCancel\" runat=\"server\" onclick=\"btnCancel_Click\" Text=\"Cancel\" />");
                sb.Append("\n </td>");
                sb.Append("\n  </tr>");
                sb.Append("\n </table> \n");
                sb.Append("\n </asp:Panel>");
                sb.Append("\n ");
 
                sb.Append("\n  <br /> ");
                sb.Append("\n <asp:Panel ID=\"PanelShow\" runat=\"server\">");
                sb.Append("\n  <div class=\"row\">");
                sb.Append("\n <div class=\"col - md - 8\">");

                sb.Append("\n  <asp:Button ID=\"btnThem\" runat=\"server\" onclick=\"btnThem_Click\" Text=\"Add New\" />");
                sb.Append("\n  </div>");
                sb.Append("\n <div class=\"col - md - 4\">");
                sb.Append("\n Số dòng hiển thị:");
                sb.Append("\n  <asp:DropDownList ID=\"ddlSoDong\" runat=\"server\"");
                sb.Append("\n  CssClass=\"dropdown\" AutoPostBack=\"true\"");
                sb.Append("\n  OnSelectedIndexChanged=\"ddlSoDong_SelectedIndexChanged\">");
                sb.Append("\n  <asp:ListItem>10</asp:ListItem>");
                sb.Append("\n  <asp:ListItem>30</asp:ListItem>");
                sb.Append("\n  <asp:ListItem>50</asp:ListItem>");
                sb.Append("\n  <asp:ListItem>100</asp:ListItem>");

                sb.Append("\n  </asp:DropDownList>");
                sb.Append("\n </div>");
                sb.Append("\n </div>");

                sb.Append("\n  <br /> ");
                sb.Append("\n  <asp:GridView ID=\"GridViewShow\" runat=\"server\" AutoGenerateColumns=\"false\" OnRowCommand=\"GridViewShowRowClick\" Width=\"600px\"> ");

                sb.Append("\n  <Columns>");
                sb.Append("\n <asp:TemplateField HeaderText=\" + \">");
                sb.Append("\n <ItemTemplate>");
                sb.Append("\n  <%# Container.DataItemIndex+1 %></ItemTemplate>");
                sb.Append("\n <ItemStyle Width=\"10px\" />");
                sb.Append("\n </asp:TemplateField>");
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];

                    // Is the current column an identity column?);
                    if (column.IsIdentity == false)
                    {
                        sb.Append("\n   <asp:BoundField DataField=\"" + column.Name + "\" HeaderText=\"" + column.Name + "\" />");

                    }
                    else
                    {
                        sb.Append("\n  <asp:BoundField DataField=\"" + column.Name + "\" HeaderText=\"" + column.Name + "\" Visible=\"False\" />");

                        sb.Append("");
                    }
                }
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];
                    if (column.IsIdentity == true)
                    {
                        sb.Append("\n <asp:TemplateField HeaderText=\"Edit\">");
                        sb.Append("\n  <ItemTemplate>");

                        sb.Append("\n <asp:LinkButton ID=\"lbEdit\" runat=\"server\" CommandName='<%#\"S|\"+Eval(\"" + column.Name + "\")%>' Text=\"Edit\"></asp:LinkButton> ");
                        sb.Append("\n </ItemTemplate>");
                        sb.Append("\n </asp:TemplateField>");
                        sb.Append("\n <asp:TemplateField HeaderText=\"Delete\">");
                        sb.Append("\n <ItemTemplate>");

                        sb.Append("\n <asp:LinkButton ID=\"lbDelete\" runat=\"server\" CommandName='<%#\"X|\"+Eval(\"" + column.Name + "\")%>' OnClientClick=\"return confirm('Do you want to delete?')\" Text=\"Delete\"></asp:LinkButton>");
                        sb.Append("\n </ItemTemplate>");
                        sb.Append("\n </asp:TemplateField>");
                    }
                }

                sb.Append("\n </Columns>");
                sb.Append(" <PagerSettings Mode=\"NumericFirstLast\" />");
                sb.Append("</asp:GridView>");

                sb.Append(" </asp:Panel>");

                sb.Append("\n </asp:Content>");

                richTextBoxDesignPopup.Text = sb.ToString();
                // streamWriter.WriteLine(sb);


            }
        }


        /// <summary>
        ///  /// Tạo Code phần thêm sửa xóa kiểu Click Gridview tới textbox
        /// </summary>
        /// <param name="databaseName"></param>
        /// <param name="table"></param>
        /// <param name="targetNamespace"></param>
        /// <param name="storedProcedurePrefix"></param>
        /// <param name="daoSuffix"></param>
        /// <param name="dtoSuffix"></param>
        /// <param name="path"></param>

        public void GridViewToTextBox_CS(string databaseName, Table table, string targetNamespace, string storedProcedurePrefix, string DbContext, string path)
        {
          
            string objTables = table.Name; /*Utility.FormatClassName(table.Name) + "";*/
            string keyColName = "ID";
            for (int i = 0; i < table.Columns.Count; i++)
            {
                Column column = table.Columns[i];
                if (column.IsIdentity == true)
                {
                    keyColName = column.Name;

                }
            }
            //Gridview to textbox
            using (StreamWriter streamWriter = new StreamWriter(Path.Combine(path, table.Name + ".aspx.cs")))
            {
                var sb = new StringBuilder();
                sb.Append("using System;");
                sb.Append("\n using System.Collections.Generic;");
                sb.Append("\n using System.Linq;");
                sb.Append("\n using System.Web;");
                sb.Append("\n using System.Web.UI;");
                sb.Append("\n using System.Web.UI.WebControls;");

                sb.Append("\n using System.Data;");

                sb.Append("\n namespace " + targetNamespace + "");
                sb.Append("\n {");

                sb.Append("\n public partial class " + table.Name + " : System.Web.UI.Page");
                sb.Append("\n  {");
                //==============Hết phần định nghĩa class   
                sb.Append("\n " + DbContext + " db = new " + DbContext + "();");

                sb.Append("\n   protected void Page_Load(object sender, EventArgs e)");
                sb.Append("\n  {");
                sb.Append("\n try");
                sb.Append("\n {");

                sb.Append("\n  if (!Page.IsPostBack)");
                sb.Append("\n  { ");
                sb.Append("\n  Databind();");
                sb.Append("\n  PanelModify.Visible = false; ");
                sb.Append("\n  PanelShow.Visible = true; ");
                sb.Append("\n  ViewState[\"active\"] = \"\"; ");
                sb.Append("\n  } ");
                sb.Append("\n  }");
                sb.Append("\n catch (Exception)");
                sb.Append("\n {");
                sb.Append("\n  ClassThongbao.Show(\"Some thing went wrong !\");");
                sb.Append("\n }");
                sb.Append("\n  } ");

                //Xóa dữ liệu rỗng
                sb.Append("\n  //================Xóa rỗng các text box==================");
                sb.Append("\n  public void Clear()");
                sb.Append("\n  {");
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];
                    sb.Append("\n txt" + column.Name + ".Text = \"\";");

                }
                sb.Append("\n } ");
                //Load dữ liệu cho Gridview
                sb.Append("\n //====================Load dữ liệu cho Gridview=========================");
                sb.Append("\n  public void Databind()");
                sb.Append("\n  {");
                sb.Append("\n  var p = from p1 in db." + objTables + " select p1;");
                sb.Append("\n GridViewShow.DataSource = p.ToList();");
                sb.Append("\n GridViewShow.DataBind();");
                sb.Append("\n }");
                sb.Append("\n ");

                //Select trong gridview
                sb.Append("\n  //====================================Sự kiện Select trên Gridview===========================");
                sb.Append("\n protected void GridViewShowRowClick(object sender, GridViewCommandEventArgs e)");
                sb.Append("\n {");
                sb.Append("\n try");
                sb.Append("\n {");
                sb.Append("\n string str = e.CommandName;");
                sb.Append("\n if(str.Equals(\"Page\")==false)");
                sb.Append("\n {");
                sb.Append("\n char[] ch = new char[] { '|' };");
                sb.Append("\n string[] s = str.Split(ch);");
                sb.Append("\n  int keyID = int.Parse(s[1].ToString());");

                sb.Append("\n   switch (s[0])");
                sb.Append("\n {");
                sb.Append("\n  case \"S\":");
                sb.Append("\n ViewState[\"active\"] = \"edit\";");
                sb.Append("\n var pEdit = from p1 in db." + objTables + " where p1." + keyColName + "==keyID select p1;");
                sb.Append("\n if (pEdit.Count()==1)");
                sb.Append("\n {");
                sb.Append("\n foreach (var item in pEdit)");
                sb.Append("\n {");
                for (int i = 0; i < table.Columns.Count; i++)
                {
                  
                    Column column = table.Columns[i];
                    
                    string a = column.Type;
                    switch (a)
                    {
                        case "int":
                            sb.Append("\n txt" + column.Name + ".Text = string.IsNullOrEmpty(item." + column.Name + ".ToString())==false?item." + column.Name + ".ToString():\"0\";");
                            break;
                        case "float":
                            sb.Append("\n txt" + column.Name + ".Text = string.IsNullOrEmpty(item." + column.Name + ".ToString())==false?item." + column.Name + ".ToString():\"0\";");
                            break;
                        case "nvarchar":
                            sb.Append("\n txt" + column.Name + ".Text = string.IsNullOrEmpty(item." + column.Name + ")==false?item." + column.Name + ".ToString():\"\";");
                            break;
                        case "nchar":
                            sb.Append("\n txt" + column.Name + ".Text = string.IsNullOrEmpty(item." + column.Name + ")==false?item." + column.Name + ".ToString():\"\";");
                            break;
                        case "bit":
                            sb.Append("\n txt" + column.Name + ".Text = string.IsNullOrEmpty(item." + column.Name + ".ToString())==false?item." + column.Name + ".ToString():\"\";");
                            break;
                        case "money":
                            sb.Append("\n txt" + column.Name + ".Text = string.IsNullOrEmpty(item." + column.Name + ".ToString())==false?item." + column.Name + ".ToString():\"0\";");
                            break;
                        default:
                            sb.Append("\n txt" + column.Name + ".Text = string.IsNullOrEmpty(item." + column.Name + ".ToString())==false?item." + column.Name + ".ToString():\"\";");
                            break;
                    }
                  
                    // txtCurrency.Text = String.IsNullOrEmpty(item.Currency.ToString())==true? item.Currency.ToString():"VND";
                }
                sb.Append("\n }");
                sb.Append("\n PanelModify.Visible = true;");
                sb.Append("\n PanelShow.Visible = false;");
                sb.Append("\n }");
                sb.Append("\n break;");
                sb.Append("\n  case \"X\":");
                sb.Append("\n  var pDelete = db." + objTables + ".First(p1 => p1." + keyColName + " == keyID);");
                sb.Append("\n  db." + objTables + ".DeleteOnSubmit(pDelete);");
                sb.Append("\n  db.SubmitChanges();");
                sb.Append("\n  Databind();");
                sb.Append("\n  break;");
                sb.Append("\n  }");
                sb.Append("\n }");
                sb.Append("\n else");
                sb.Append("\n { ");
                sb.Append("\n }");
                sb.Append("\n }");
                sb.Append("\n catch (Exception)");
                sb.Append("\n {");
                sb.Append("\n ClassThongbao.Show(\"Some thing went wrong !\");");
                sb.Append("\n }");
                sb.Append("\n }");

                sb.Append("\n //========================Thêm====================");
                sb.Append("\n protected void btnThem_Click(object sender, EventArgs e)");
                sb.Append("\n { \n PanelModify.Visible = true;");
                sb.Append("\n  PanelShow.Visible = false; ");
                sb.Append("\n Clear();");
                sb.Append("\n ViewState[\"active\"] = \"add\"; ");
                sb.Append("\n }");

                //Khi click vào save để THêm Hoặc SỬa
                sb.Append("\n  //=====================================Khi click vào save để THêm Hoặc SỬa=============================");
                sb.Append("\n   protected void btnSave_Click(object sender, EventArgs e) \n  { ");
                sb.Append("\n  try ");
                sb.Append("\n {");


                sb.Append("\n   switch (ViewState[\"active\"].ToString()) \n{");
                sb.Append("\n   case \"add\":  ");
                sb.Append("\n " + objTables + " objAdd = new " + objTables + "();");
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];
                    if (column.IsIdentity == false)
                    {
                        sb.Append(Common.SeparatorLinQ(column, "objAdd"));
                    }
                }
                sb.Append("\n db." + objTables + ".InsertOnSubmit(objAdd);");
                sb.Append("\n db.SubmitChanges();");
                sb.Append("\n   break;");
                sb.Append("\n  case \"edit\":");
                sb.Append("\n  int keyID = int.Parse(txt" + keyColName + ".Text);");

                sb.Append("\n  " + objTables + " objEdit = db." + objTables + ".First(p => p." + keyColName + " == keyID);");
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];
                    if (column.IsIdentity == true)
                    {
                        sb.Append(Common.SeparatorLinQ(column, "objEdit"));

                    }
                }
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];
                    if (column.IsIdentity == false)
                    {
                        sb.Append(Common.SeparatorLinQ(column, "objEdit"));
                    }
                }
                sb.Append("\n   db.SubmitChanges();");

                sb.Append("\n  break; ");
                sb.Append("\n   } ");
                sb.Append("\n   Databind();");
                sb.Append("\n PanelModify.Visible = false;");
                sb.Append("\n PanelShow.Visible = true;");
                sb.Append("\n }");
                sb.Append("\n catch (Exception ex) ");
                sb.Append("\n {");
                sb.Append("\n ClassThongbao.Show(\"Cannot Save this item\");");
                sb.Append("\n }");
                sb.Append("\n }");

                /// <summary>
                /// Số dòng sẽ hiển thị trong Gridview
                /// </summary>
                /// <param name="sender"></param>
                /// <param name="e"></param>
                sb.Append("\n  protected void ddlSoDong_SelectedIndexChanged(object sender, EventArgs e)");
                sb.Append("\n {");
                sb.Append("\n  try");
                sb.Append("\n {");
                sb.Append("\n switch (ddlSoDong.SelectedValue.ToString())");
                sb.Append("\n  {");
                sb.Append("\n case \"10\":");
                sb.Append("\n GridViewShow.PageSize = 10;");
                sb.Append("\n  Databind();");
                sb.Append("\n break;");
                sb.Append("\n case \"30\":");
                sb.Append("\n GridViewShow.PageSize = 30;");
                sb.Append("\n  Databind();");
                sb.Append("\n break;");
                sb.Append("\n case \"50\":");
                sb.Append("\n GridViewShow.PageSize = 50;");
                sb.Append("\n  Databind();");
                sb.Append("\n break;");
                sb.Append("\n case \"100\":");
                sb.Append("\n GridViewShow.PageSize = 100;");
                sb.Append("\n  Databind();");
                sb.Append("\n break;");
             
                sb.Append("\n  default:");
                sb.Append("\n GridViewShow.PageSize = 10;");
                sb.Append("\n  Databind();");
                sb.Append("\n break;");
                sb.Append("\n }");
                sb.Append("\n }");
                sb.Append("\n catch (Exception)");
                sb.Append("\n {");
                sb.Append("\n GridViewShow.PageSize = 10;");
                sb.Append("\n  Databind();");
                sb.Append("\n }");
                sb.Append("\n }");

                ////Sự kiện Phân Trang    
                //sb.Append("\n  //=====================================Phân Trang============================");
                //sb.Append("\n protected void GridViewShow_PageIndexChanging(object sender, GridViewPageEventArgs e) ");
                //sb.Append("\n { \n GridViewShow.PageIndex = e.NewPageIndex;");
                // sb.Append("\n Databind();");
                //sb.Append("\n }");

                //Nút Cancel    
                sb.Append("\n  //=====================================Nút Cancel============================");
                sb.Append("\n protected void btnCancel_Click(object sender, EventArgs e) ");
                sb.Append("\n { ");
                sb.Append("\n PanelModify.Visible = false; ");
                sb.Append("\n PanelShow.Visible = true;");
                sb.Append("\n }");

                //Phân trang
                sb.Append("\n  protected void GridViewShow_PageIndexChanging(object sender, GridViewPageEventArgs e)");
                sb.Append("\n {");
                sb.Append("\n  GridViewShow.PageIndex = e.NewPageIndex;");
                sb.Append("\n Databind();");
                sb.Append("\n }");
                sb.Append("\n }");
                sb.Append("\n }");
                sb.Append("\n ");

                richTextBoxCode.Text = sb.ToString();
               // streamWriter.WriteLine(sb);
            }
        }

        /// <summary>
        ///  /// Tạo Code phần thêm sửa xóa kiểu Click Gridview tới textbox
        /// </summary>
        /// <param name="databaseName"></param>
        /// <param name="table"></param>
        /// <param name="targetNamespace"></param>
        /// <param name="storedProcedurePrefix"></param>
        /// <param name="daoSuffix"></param>
        /// <param name="dtoSuffix"></param>
        /// <param name="path"></param>

        public void GridViewToTextBox_CS_Popup(string databaseName, Table table, string targetNamespace, string storedProcedurePrefix, string DbContext, string path)
        {

            string objTables = table.Name; /*Utility.FormatClassName(table.Name) + "";*/
            string keyColName = "ID";
            for (int i = 0; i < table.Columns.Count; i++)
            {
                Column column = table.Columns[i];
                if (column.IsIdentity == true)
                {
                    keyColName = column.Name;

                }
            }
        
                var sb = new StringBuilder();
                sb.Append("using System;");
                sb.Append("\n using System.Collections.Generic;");
                sb.Append("\n using System.Linq;");
                sb.Append("\n using System.Web;");
                sb.Append("\n using System.Web.UI;");
                sb.Append("\n using System.Web.UI.WebControls;");

                sb.Append("\n using System.Data;");

                sb.Append("\n namespace " + targetNamespace + "");
                sb.Append("\n {");

                sb.Append("\n public partial class " + table.Name + " : System.Web.UI.Page");
                sb.Append("\n  {");
                //==============Hết phần định nghĩa class   
                sb.Append("\n " + DbContext + " db = new " + DbContext + "();");

                sb.Append("\n   protected void Page_Load(object sender, EventArgs e)");
                sb.Append("\n  {");
                sb.Append("\n try");
                sb.Append("\n {");

                sb.Append("\n  if (!Page.IsPostBack)");
                sb.Append("\n  { ");
                sb.Append("\n  Databind();");
                sb.Append("\n  PanelModify.Visible = false; ");
                sb.Append("\n  PanelShow.Visible = true; ");
                sb.Append("\n  ViewState[\"active\"] = \"\"; ");
                sb.Append("\n  } ");
                sb.Append("\n  }");
                sb.Append("\n catch (Exception)");
                sb.Append("\n {");
                sb.Append("\n  ClassThongbao.Show(\"Some thing went wrong !\");");
                sb.Append("\n }");
                sb.Append("\n  } ");

                //Xóa dữ liệu rỗng
                sb.Append("\n  //================Xóa rỗng các text box==================");
                sb.Append("\n  public void Clear()");
                sb.Append("\n  {");
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];
                    sb.Append("\n txt" + column.Name + ".Text = \"\";");

                }
                sb.Append("\n } ");
                //Load dữ liệu cho Gridview
                sb.Append("\n //====================Load dữ liệu cho Gridview=========================");
                sb.Append("\n  public void Databind()");
                sb.Append("\n  {");
                sb.Append("\n  var p = from p1 in db." + objTables + " select p1;");
                sb.Append("\n GridViewShow.DataSource = p.ToList();");
                sb.Append("\n GridViewShow.DataBind();");
                sb.Append("\n }");
                sb.Append("\n ");

                //Select trong gridview
                sb.Append("\n  //====================================Sự kiện Select trên Gridview===========================");
                sb.Append("\n protected void GridViewShowRowClick(object sender, GridViewCommandEventArgs e)");
                sb.Append("\n {");
                sb.Append("\n try");
                sb.Append("\n {");
                sb.Append("\n string str = e.CommandName;");
                sb.Append("\n if(str.Equals(\"Page\")==false)");
                sb.Append("\n {");
                sb.Append("\n char[] ch = new char[] { '|' };");
                sb.Append("\n string[] s = str.Split(ch);");
                sb.Append("\n  int keyID = int.Parse(s[1].ToString());");

                sb.Append("\n   switch (s[0])");
                sb.Append("\n {");
                sb.Append("\n  case \"S\":");
                sb.Append("\n ViewState[\"active\"] = \"edit\";");
                sb.Append("\n var pEdit = from p1 in db." + objTables + " where p1." + keyColName + "==keyID select p1;");
                sb.Append("\n if (pEdit.Count()==1)");
                sb.Append("\n {");
                sb.Append("\n foreach (var item in pEdit)");
                sb.Append("\n {");
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];
                    sb.Append("\n txt" + column.Name + ".Text = item." + column.Name + ".ToString();");
                }
                sb.Append("\n }");
                sb.Append("\n PanelModify.Visible = true;");
                sb.Append("\n PanelShow.Visible = false;");
                sb.Append("\n }");
                sb.Append("\n break;");
                sb.Append("\n  case \"X\":");
                sb.Append("\n  var pDelete = db." + objTables + ".First(p1 => p1." + keyColName + " == keyID);");
                sb.Append("\n  db." + objTables + ".DeleteOnSubmit(pDelete);");
                sb.Append("\n  db.SubmitChanges();");
                sb.Append("\n  Databind();");
                sb.Append("\n  break;");
                sb.Append("\n  }");
                sb.Append("\n }");
                sb.Append("\n else");
                sb.Append("\n { ");
                sb.Append("\n }");
                sb.Append("\n }");
                sb.Append("\n catch (Exception)");
                sb.Append("\n {");
                sb.Append("\n ClassThongbao.Show(\"Some thing went wrong !\");");
                sb.Append("\n }");
                sb.Append("\n }");

                sb.Append("\n //========================Thêm====================");
                sb.Append("\n protected void btnThem_Click(object sender, EventArgs e)");
                sb.Append("\n { \n PanelModify.Visible = true;");
                sb.Append("\n  PanelShow.Visible = false; ");
                sb.Append("\n Clear();");
                sb.Append("\n ViewState[\"active\"] = \"add\"; ");
                sb.Append("\n }");

                //Khi click vào save để THêm Hoặc SỬa
                sb.Append("\n  //=====================================Khi click vào save để THêm Hoặc SỬa=============================");
                sb.Append("\n   protected void btnSave_Click(object sender, EventArgs e) \n  { ");
                sb.Append("\n  try ");
                sb.Append("\n {");


                sb.Append("\n   switch (ViewState[\"active\"].ToString()) \n{");
                sb.Append("\n   case \"add\":  ");
                sb.Append("\n " + objTables + " objAdd = new " + objTables + "();");
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];
                    if (column.IsIdentity == false)
                    {
                        sb.Append(Common.SeparatorLinQ(column, "objAdd"));
                    }
                }
                sb.Append("\n db." + objTables + ".InsertOnSubmit(objAdd);");
                sb.Append("\n db.SubmitChanges();");
                sb.Append("\n   break;");
                sb.Append("\n  case \"edit\":");
                sb.Append("\n  int keyID = int.Parse(txt" + keyColName + ".Text);");

                sb.Append("\n  " + objTables + " objEdit = db." + objTables + ".First(p => p." + keyColName + " == keyID);");
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];
                    if (column.IsIdentity == true)
                    {
                        sb.Append(Common.SeparatorLinQ(column, "objEdit"));

                    }
                }
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];
                    if (column.IsIdentity == false)
                    {
                        sb.Append(Common.SeparatorLinQ(column, "objEdit"));
                    }
                }
                sb.Append("\n   db.SubmitChanges();");

                sb.Append("\n  break; ");
                sb.Append("\n   } ");
                sb.Append("\n   Databind();");
                sb.Append("\n PanelModify.Visible = false;");
                sb.Append("\n PanelShow.Visible = true;");
                sb.Append("\n }");
                sb.Append("\n catch (Exception ex) ");
                sb.Append("\n {");
                sb.Append("\n ClassThongbao.Show(\"Cannot Save this item\");");
                sb.Append("\n }");
                sb.Append("\n }");

                /// <summary>
                /// Số dòng sẽ hiển thị trong Gridview
                /// </summary>
                /// <param name="sender"></param>
                /// <param name="e"></param>
                sb.Append("\n  protected void ddlSoDong_SelectedIndexChanged(object sender, EventArgs e)");
                sb.Append("\n {");
                sb.Append("\n  try");
                sb.Append("\n {");
                sb.Append("\n switch (ddlSoDong.SelectedValue.ToString())");
                sb.Append("\n  {");
                sb.Append("\n case \"10\":");
                sb.Append("\n GridViewShow.PageSize = 10;");
                sb.Append("\n  Databind();");
                sb.Append("\n break;");
                sb.Append("\n case \"30\":");
                sb.Append("\n GridViewShow.PageSize = 30;");
                sb.Append("\n  Databind();");
                sb.Append("\n break;");
                sb.Append("\n case \"50\":");
                sb.Append("\n GridViewShow.PageSize = 50;");
                sb.Append("\n  Databind();");
                sb.Append("\n break;");
                sb.Append("\n case \"100\":");
                sb.Append("\n GridViewShow.PageSize = 100;");
                sb.Append("\n  Databind();");
                sb.Append("\n break;");

                sb.Append("\n  default:");
                sb.Append("\n GridViewShow.PageSize = 10;");
                sb.Append("\n  Databind();");
                sb.Append("\n break;");
                sb.Append("\n }");
                sb.Append("\n }");
                sb.Append("\n catch (Exception)");
                sb.Append("\n {");
                sb.Append("\n GridViewShow.PageSize = 10;");
                sb.Append("\n  Databind();");
                sb.Append("\n }");
                sb.Append("\n }");

                ////Sự kiện Phân Trang    
                //sb.Append("\n  //=====================================Phân Trang============================");
                //sb.Append("\n protected void GridViewShow_PageIndexChanging(object sender, GridViewPageEventArgs e) ");
                //sb.Append("\n { \n GridViewShow.PageIndex = e.NewPageIndex;");
                // sb.Append("\n Databind();");
                //sb.Append("\n }");

                //Nút Cancel    
                sb.Append("\n  //=====================================Nút Cancel============================");
                sb.Append("\n protected void btnCancel_Click(object sender, EventArgs e) ");
                sb.Append("\n { ");
                sb.Append("\n PanelModify.Visible = false; ");
                sb.Append("\n PanelShow.Visible = true;");
                sb.Append("\n }");

                sb.Append("\n }");
                richTextBoxCode.Text = sb.ToString();
                // streamWriter.WriteLine(sb);
            
        }
        /// <summary>
        /// Sinh phan designer cho file cua webform
        /// </summary>
        /// <param name="databaseName"></param>
        /// <param name="table"></param>
        /// <param name="targetNamespace"></param>
        /// <param name="storedProcedurePrefix"></param>
        /// <param name="busSuffix"></param>
        /// <param name="dtoSuffix"></param>
        /// <param name="path"></param>
        public static void GridViewToTextBox_Designer_CS(string databaseName, Table table, string targetNamespace, string storedProcedurePrefix, string DbContext, string path)
        {
            //string className = Utility.FormatClassName(table.Name) + dtoSuffix;
            //string classNameDAO = Utility.FormatClassName(table.Name) + busSuffix;
            //string classNameBusiness = Utility.FormatClassName(table.Name) + busSuffix;
            //string objBUS = "_" + table.Name + busSuffix.ToUpper();

            //Gridview to textbox
            using (StreamWriter streamWriter = new StreamWriter(Path.Combine(path, table.Name + ".aspx.designer.cs")))
            {
                var sb = new StringBuilder();
                sb.Append("\n//------------------------------------------------------------------------------");
                sb.Append("\n// <auto-generated>");
                sb.Append("\n //     This code was generated by a tool.");
                sb.Append("\n //     Changes to this file may cause incorrect behavior and will be lost if");
                sb.Append("\n  //     the code is regenerated. ");
                sb.Append("\n // </auto-generated>");
                sb.Append("\n //------------------------------------------------------------------------------");

                sb.Append("\n namespace " + targetNamespace + "");
                sb.Append("\n {");
                sb.Append("\n public partial class " + table.Name);
                sb.Append("\n{");
                sb.Append("\n protected global::System.Web.UI.WebControls.Panel PanelModify;");
                sb.Append("\n protected global::System.Web.UI.WebControls.Button btnSave;");
                sb.Append("\n protected global::System.Web.UI.WebControls.Button btnDelete;");
                sb.Append("\n protected global::System.Web.UI.WebControls.Button btnCancel;");
                sb.Append("\n protected global::System.Web.UI.WebControls.Button btnThem;");
                sb.Append("\n protected global::System.Web.UI.WebControls.Panel PanelShow;");
                sb.Append("\n protected global::System.Web.UI.WebControls.GridView GridViewShow;");
                sb.Append("\n ");

                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];
                    sb.Append("\n protected global::System.Web.UI.WebControls.TextBox txt" + column.Name + ";");
                }

                sb.Append("\n }");
                sb.Append("\n }");

                streamWriter.WriteLine(sb);
            }
        }

        #endregion

        //Modify in gridview
        #region ===========Thêm Sửa Xóa Trong Gridview==============
        /// <summary>
        /// Phần giao diện aspx
        /// </summary>
        /// <param name="databaseName"></param>
        /// <param name="table"></param>
        /// <param name="targetNamespace"></param>
        /// <param name="storedProcedurePrefix"></param>
        /// <param name="daoSuffix"></param>
        /// <param name="dtoSuffix"></param>
        /// <param name="path"></param>
        public void CreateModifyGridview(string databaseName, Table table, string targetNamespace, string storedProcedurePrefix, string daoSuffix, string dtoSuffix)
        {

            string className = Utility.FormatClassName(table.Name) + daoSuffix;

           

           
                string Id = "Id";
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];

                    // Is the current column an identity column?);
                    if (column.IsIdentity == true)
                    {
                        Id = column.Name;
                    }


                }

                StringBuilder sb = new StringBuilder();
                sb.Append("\n <%@ Page Title=\"\" Language=\"C#\" MasterPageFile=\"~/Admin/admin.Master\" AutoEventWireup=\"true\"");
                sb.Append("\n CodeBehind=\"" + table.Name + "2.aspx.cs\" Inherits=\"" + targetNamespace + "2." + table.Name + "2\" %>");
                sb.Append("\n <asp:Content ID=\"Content1\" ContentPlaceHolderID=\"HeadContent\" runat=\"server\">");
                sb.Append("\n </asp:Content>");
                sb.Append("\n <asp:Content ID=\"Content2\" ContentPlaceHolderID=\"MainContent\" runat=\"server\">");
                sb.Append("\n  <h2>" + table.Name.ToUpper() + "</h2>");
                sb.Append("\n <asp:Panel ID=\"PanelModify\" runat=\"server\"");
                sb.Append("\n ScrollBars=\"Vertical\"");
                sb.Append("\n BorderStyle=\"Inset\" BorderWidth=\"1px\" \n style=\"overflow: auto; height: 330px\">");

                sb.Append("\n <asp:GridView ID=\"GridView" + table.Name + "\" runat=\"server\" AutoGenerateColumns=\"False\" ");
                sb.Append("\n AllowPaging=\"True\" DataKeyNames=\"" + Id + "\"");
                sb.Append("\n OnPageIndexChanging=\"GridView" + table.Name + "_PageIndexChanging\"");
                sb.Append("\n OnRowCancelingEdit=\"GridView" + table.Name + "_RowCancelingEdit\"");
                sb.Append("\n OnRowEditing=\"GridView" + table.Name + "_RowEditing\"");
                sb.Append("\n OnRowUpdating=\"GridView" + table.Name + "_RowUpdating\"");
                sb.Append("\n onrowdeleting=\"GridView" + table.Name + "_RowDeleting\"");
                sb.Append("\n ShowFooter=\"True\" Width=\"90%\">");

                sb.Append("\n <Columns>");
                sb.Append("\n <asp:TemplateField HeaderText=\"No.\">");
                sb.Append("\n <ItemTemplate>");
                sb.Append("\n <%# Container.DataItemIndex+1 %>");
                sb.Append("\n</ItemTemplate>");

                sb.Append("\n </asp:TemplateField>");

                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];

                    // Is the current column an identity column?);
                    if (column.IsIdentity == false)
                    {

                        sb.Append("\n <asp:TemplateField HeaderText=\" " + column.Name + " \">");
                        sb.Append("\n <EditItemTemplate>");
                        sb.Append("\n <asp:TextBox ID=\"txt" + column.Name + "\" runat=\"server\" Text='<%# Bind(\"" + column.Name + "\") %>'></asp:TextBox>");
                        sb.Append("\n </EditItemTemplate>");
                        sb.Append("\n <FooterTemplate>");
                        sb.Append("\n <asp:TextBox ID=\"txt" + column.Name + "\" runat=\"server\"></asp:TextBox>");
                        sb.Append("\n </FooterTemplate>");
                        sb.Append("\n <ItemTemplate>");
                        sb.Append("\n <asp:Label ID=\"lbl" + column.Name + "\" runat=\"server\" Text='<%# Bind(\"" + column.Name + "\") %>'></asp:Label>");
                        sb.Append("\n </ItemTemplate>");
                        sb.Append("\n </asp:TemplateField>");

                        sb.Append("");
                    }
                    else
                    {
                    }
                }
                //Tạo Cột xóa
                sb.Append("\n <asp:TemplateField ShowHeader=\"False\" HeaderText=\"Delete\">");
                sb.Append("\n <ItemTemplate>");
                sb.Append("\n <asp:LinkButton ID=\"LinkButtonDelete\" runat=\"server\" CausesValidation=\"False\" CommandName=\"Delete\" ");
                sb.Append("\n Text=\"Delete\" OnClientClick = \"return confirm('Do you want to delete?')\"></asp:LinkButton>");
                sb.Append("\n </ItemTemplate>");
                sb.Append("\n <FooterTemplate>");
                sb.Append("\n <asp:Button ID=\"btnAdd\" runat=\"server\" Text=\"Add\" Width=\"48px\" onclick=\"btnAdd_Click\" />");
                sb.Append("\n </FooterTemplate>");
                sb.Append("\n </asp:TemplateField>");
                //Tạo Cột Sửa
                sb.Append("\n <asp:TemplateField  HeaderText=\"Edit\">");
                sb.Append("\n <ItemTemplate>");
                sb.Append("\n <asp:LinkButton ID=\"LinkButtonEdit\" runat=\"server\" CausesValidation=\"False\" CommandName=\"Edit\" Text=\"Edit\"></asp:LinkButton>");
                sb.Append("\n </ItemTemplate>");
                sb.Append("\n <EditItemTemplate>");
                sb.Append("\n <asp:LinkButton ID=\"LinkButtonUpdate\" runat=\"server\" CausesValidation=\"True\" CommandName=\"Update\" Text=\"Update\"></asp:LinkButton> &nbsp;");
                sb.Append("\n <asp:LinkButton ID=\"LinkButtonCanncel\" runat=\"server\" CausesValidation=\"False\" CommandName=\"Cancel\" Text=\"Cancel\"></asp:LinkButton>");
                sb.Append("\n </EditItemTemplate>");
                sb.Append("\n </asp:TemplateField>");
                //Cột Id
                sb.Append("\n <asp:TemplateField HeaderText=\"" + Id + "\">");
                sb.Append("\n <EditItemTemplate>");
                sb.Append("\n <asp:TextBox ID=\"txtId\" runat=\"server\" Text='<%# Bind(\"" + Id + "\") %>'></asp:TextBox>");
                sb.Append("\n </EditItemTemplate>");
                sb.Append("\n <ItemTemplate>");
                sb.Append("\n <asp:Label ID=\"txtId\" runat=\"server\" Text='<%# Bind(\"" + Id + "\") %>'></asp:Label>");
                sb.Append("\n </ItemTemplate>");
                sb.Append("\n <HeaderStyle CssClass=\"hideGridColumn\" />");
                sb.Append("\n <ItemStyle CssClass=\"hideGridColumn\" />");
                sb.Append("\n <FooterStyle  CssClass=\"hideGridColumn\" />");
                sb.Append("\n </asp:TemplateField>");
                sb.Append("");
                sb.Append("\n </Columns>");
                sb.Append("\n <PagerSettings Mode=\"NumericFirstLast\" />");
                sb.Append("\n </asp:GridView>");

                sb.Append("\n <style type=\"text/css\">.hideGridColumn{display: none;}</style>");
                sb.Append("");
                sb.Append("\n  </asp:Panel>");

                sb.Append("\n </asp:Content>");
                richTextBoxModifyInGridviewDesign.Text = sb.ToString();

 
        }
        /// <summary>
        /// Phần Code sửa xóa trong Gridview
        /// </summary>
        /// <param name="databaseName"></param>
        /// <param name="table"></param>
        /// <param name="targetNamespace"></param>
        /// <param name="storedProcedurePrefix"></param>
        /// <param name="busSuffix"></param>
        /// <param name="dtoSuffix"></param>
        /// <param name="path"></param>
        public void CreateModifyGridviewCS(string databaseName, Table table, string targetNamespace, string storedProcedurePrefix, string busSuffix, string dtoSuffix)
        {

            string className = Utility.FormatClassName(table.Name) + busSuffix;
            string classNameEntity = Utility.FormatClassName(table.Name) + dtoSuffix;
            string classNameBusiness = Utility.FormatClassName(table.Name) + busSuffix;
            string objBUS = "_" + table.Name + busSuffix.ToUpper();
            string gridviewName = "GridView" + table.Name;
          
                var sb = new StringBuilder();
                sb.Append("using System;");
                sb.Append("\n using System.Collections.Generic;");
                sb.Append("\n using System.Linq;");
                sb.Append("\n using System.Web;");
                sb.Append("\n using System.Web.UI;");
                sb.Append("\n using System.Web.UI.WebControls;");
                sb.Append("\n using Business;");
                sb.Append("\n using Entity;");
                sb.Append("\n using System.Data;");
                sb.Append("\n using Common;");
                sb.Append("\n namespace " + targetNamespace + "2");
                sb.Append("\n {");
                sb.Append("\n public partial class " + table.Name + "2 : System.Web.UI.Page");
                sb.Append("\n  {");
                sb.Append("\n " + classNameBusiness + " bus " + " = new " + classNameBusiness + "();");
                sb.Append("\n protected void Page_Load(object sender, EventArgs e)");
                sb.Append("\n {");
                sb.Append("\n if (!IsPostBack)");
                sb.Append("\n {");
                sb.Append("\n Databind();");
                sb.Append("\n }");
                sb.Append("\n }");
                sb.Append("\n ");
                sb.Append("\n //Databin");
                sb.Append("\n public void Databind()");
                sb.Append("\n {");
                sb.Append("\n " + gridviewName + ".DataSource = bus.GetAll();");
                sb.Append("\n " + gridviewName + ".DataBind();");
                sb.Append("\n }");

                //pHAN tRANG
                sb.Append("\n //pHAN tRANG");
                sb.Append("\n protected void " + gridviewName + "_PageIndexChanging(object sender, GridViewPageEventArgs e) ");
                sb.Append("\n { ");
                sb.Append("\n" + gridviewName + ".PageIndex = e.NewPageIndex;");
                sb.Append("\n Databind();");
                sb.Append("\n }");
                //eDIT
                sb.Append("\n //Edit");
                sb.Append("\n protected void  " + gridviewName + "_RowEditing(object sender, GridViewEditEventArgs e)");
                sb.Append("\n { ");
                sb.Append("\n " + gridviewName + ".EditIndex = e.NewEditIndex;");
                sb.Append("\n  Databind();");
                sb.Append("\n }");
                //Update
                sb.Append("\n  //Update trong Gridview");
                sb.Append("\n protected void " + gridviewName + "_RowUpdating(object sender, GridViewUpdateEventArgs e)");
                sb.Append("\n {");
                sb.Append("\n " + classNameEntity + " obj=new " + classNameEntity + "();");
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];
                    sb.Append(Common.GridviewTo_Object(column, gridviewName));
                }
                sb.Append("\n bus.Update(obj);");
                sb.Append("\n " + gridviewName + ".EditIndex = -1; ");
                sb.Append("\n Databind(); ");
                sb.Append("\n}");

                //Canncel
                sb.Append("\n protected void " + gridviewName + "_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)");
                sb.Append("\n { ");
                sb.Append("\n  " + gridviewName + ".EditIndex = -1;");
                sb.Append("Databind();");
                sb.Append("\n }");
                //tHEM
                sb.Append(" //tHEM");
                sb.Append("\n protected void btnAdd_Click(object sender, EventArgs e) ");
                sb.Append("\n { ");
                sb.Append("\n " + classNameEntity + " obj = new " + classNameEntity + "();");
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];
                    if (column.IsIdentity == false)
                    {
                        sb.Append(Common.GridviewTo_Object_Add(column, gridviewName));
                    }
                }
                sb.Append("\n bus.Insert(obj);");
                sb.Append("\n Databind();");
                sb.Append("}");
                //Xoa
                sb.Append("//Xoa");
                sb.Append("\n protected void " + gridviewName + "_RowDeleting(object sender, GridViewDeleteEventArgs e) ");
                sb.Append("\n { ");
                sb.Append("\n " + classNameEntity + " obj=new " + classNameEntity + "();");
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];
                    if (column.IsIdentity == true)
                    {
                        sb.Append(Common.ConvertFor_DeleteInGridview(column, gridviewName));
                    }
                }
                sb.Append("\n bus.Delete(obj);");
                sb.Append("\n Databind();");
                sb.Append("\n  }");
                sb.Append("\n  }");
                sb.Append("\n  }");
                richTextBoxModifyInGridviewCode.Text = sb.ToString();
             
        }
        /// <summary>
        /// Sinh phan designer cho file cua webform Sửa xóa trong gridview
        /// </summary>
        /// <param name="databaseName"></param>
        /// <param name="table"></param>
        /// <param name="targetNamespace"></param>
        /// <param name="storedProcedurePrefix"></param>
        /// <param name="busSuffix"></param>
        /// <param name="dtoSuffix"></param>
        /// <param name="path"></param>
        public static void CreateModifyGridview_Designer_CS(string databaseName, Table table, string targetNamespace, string storedProcedurePrefix, string busSuffix, string dtoSuffix, string path)
        {
            string className = Utility.FormatClassName(table.Name) + dtoSuffix;
            string classNameDAO = Utility.FormatClassName(table.Name) + busSuffix;
            string classNameBusiness = Utility.FormatClassName(table.Name) + busSuffix;
            string objBUS = "_" + table.Name + busSuffix.ToUpper();

            //Gridview to textbox
            using (StreamWriter streamWriter = new StreamWriter(Path.Combine(path, table.Name + "2.aspx.designer.cs")))
            {
                var sb = new StringBuilder();
                sb.Append("\n//------------------------------------------------------------------------------");
                sb.Append("\n// <auto-generated>");
                sb.Append("\n //     This code was generated by a tool.");
                sb.Append("\n //     Changes to this file may cause incorrect behavior and will be lost if");
                sb.Append("\n  //     the code is regenerated. ");
                sb.Append("\n // </auto-generated>");
                sb.Append("\n //------------------------------------------------------------------------------");

                sb.Append("\n namespace " + targetNamespace + "2");
                sb.Append("\n {");
                sb.Append("\n public partial class " + table.Name + "2");
                sb.Append("\n{");
                sb.Append("\n protected global::System.Web.UI.WebControls.Panel PanelModify;");
                sb.Append("\n protected global::System.Web.UI.WebControls.GridView GridView" + table.Name + ";");
                sb.Append("\n }");
                sb.Append("\n }");

                streamWriter.WriteLine(sb);
            }
        }

        #endregion

        private void txtTestConnect_Click(object sender, EventArgs e)
        {
            try
            {
               
                tableList = new List<Table>();
                string databaseName;


                string connectionString = "Server=" + txtServerNane.Text + "; Database=" + txtDatabaseName.Text +
                                        "; User ID=" + txtUserName.Text + "; Password=" + txtPassword.Text + ";";
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    databaseName = Utility.FormatPascal(connection.Database);

                    connection.Open();

                    // Lấy ra danh sách các bảng trong database
                    DataTable dataTable = new DataTable();
                    string strQuery = "select TABLE_NAME from INFORMATION_SCHEMA.TABLES ";
                    strQuery = strQuery + "where TABLE_TYPE = 'BASE TABLE' and (TABLE_NAME != 'dtProperties' and TABLE_NAME != 'sysdiagrams') ";
                    strQuery = strQuery + "and TABLE_CATALOG = '" + databaseName + "' order by TABLE_NAME";
                    SqlCommand cmd = new SqlCommand(strQuery, connection);
                    SqlDataAdapter dataAdapter = new SqlDataAdapter(cmd);
                    dataAdapter.Fill(dataTable);
                    cmd.Dispose();
                    dataAdapter.Dispose();

                    gvTable.DataSource = dataTable;
                    // Xử lý với từng bảng 
                    foreach (DataRow dataRow in dataTable.Rows)
                    {
                        Table table = new Table();
                        table.Name = (string)dataRow["TABLE_NAME"];
                        QueryTable(connection, table);
                        tableList.Add(table);
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());

            }
        }
    }
}
