﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("Auto gen code  - Toantv")]
[assembly: AssemblyDescription("Phần mềm sinh code tự động: Linq to sql, Query in form, query in Store, CRUD in Winform and ASP.NET")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Asahi 2017")]
[assembly: AssemblyProduct("Auto gen code  - Toantv")]
[assembly: AssemblyCopyright("Toàn Đao Kiếm Copyright ©  2017")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(true)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("666b1008-6c47-464e-8cfd-beca8e89ee62")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("1.0.0.2")]
[assembly: AssemblyFileVersion("1.0.0.3")]
[assembly: AssemblyDelaySign(false)]