﻿namespace App_ChuongTrinhSinhCodeTuDong.CodeInForm
{
    partial class FormCodeInForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormCodeInForm));
            this.projectTextBox = new System.Windows.Forms.TextBox();
            this.projectLabel = new System.Windows.Forms.Label();
            this.csGroupBox = new System.Windows.Forms.GroupBox();
            this.busSuffixTextBox = new System.Windows.Forms.TextBox();
            this.dtoSuffixTextBox = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.dtoSuffixLabel = new System.Windows.Forms.Label();
            this.daoSuffixTextBox = new System.Windows.Forms.TextBox();
            this.daoSuffixLabel = new System.Windows.Forms.Label();
            this.namespaceTextBox = new System.Windows.Forms.TextBox();
            this.namespaceLabel = new System.Windows.Forms.Label();
            this.storedProcedurePrefixTextBox = new System.Windows.Forms.TextBox();
            this.serverLabel = new System.Windows.Forms.Label();
            this.sqlGroupBox = new System.Windows.Forms.GroupBox();
            this.storedProcedurePrefixLabel = new System.Windows.Forms.Label();
            this.grantUserTextBox = new System.Windows.Forms.TextBox();
            this.grantUserLabel = new System.Windows.Forms.Label();
            this.multipleFilesCheckBox = new System.Windows.Forms.CheckBox();
            this.passwordTextBox = new System.Windows.Forms.TextBox();
            this.loginNameTextBox = new System.Windows.Forms.TextBox();
            this.generateButton = new System.Windows.Forms.Button();
            this.passwordLabel = new System.Windows.Forms.Label();
            this.loginNameLabel = new System.Windows.Forms.Label();
            this.sqlServerAuthenticationRadioButton = new System.Windows.Forms.RadioButton();
            this.authenticationGroupBox = new System.Windows.Forms.GroupBox();
            this.windowsAuthenticationRadioButton = new System.Windows.Forms.RadioButton();
            this.databaseLabel = new System.Windows.Forms.Label();
            this.databaseTextBox = new System.Windows.Forms.TextBox();
            this.serverTextBox = new System.Windows.Forms.TextBox();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.label1 = new System.Windows.Forms.Label();
            this.csGroupBox.SuspendLayout();
            this.sqlGroupBox.SuspendLayout();
            this.authenticationGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // projectTextBox
            // 
            this.projectTextBox.Location = new System.Drawing.Point(104, 25);
            this.projectTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.projectTextBox.Name = "projectTextBox";
            this.projectTextBox.Size = new System.Drawing.Size(271, 23);
            this.projectTextBox.TabIndex = 10;
            this.projectTextBox.Text = "YourNameProject";
            // 
            // projectLabel
            // 
            this.projectLabel.Location = new System.Drawing.Point(16, 25);
            this.projectLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.projectLabel.Name = "projectLabel";
            this.projectLabel.Size = new System.Drawing.Size(96, 28);
            this.projectLabel.TabIndex = 9;
            this.projectLabel.Text = "Project:";
            // 
            // csGroupBox
            // 
            this.csGroupBox.Controls.Add(this.projectTextBox);
            this.csGroupBox.Controls.Add(this.projectLabel);
            this.csGroupBox.Controls.Add(this.busSuffixTextBox);
            this.csGroupBox.Controls.Add(this.dtoSuffixTextBox);
            this.csGroupBox.Controls.Add(this.label2);
            this.csGroupBox.Controls.Add(this.dtoSuffixLabel);
            this.csGroupBox.Controls.Add(this.daoSuffixTextBox);
            this.csGroupBox.Controls.Add(this.daoSuffixLabel);
            this.csGroupBox.Controls.Add(this.namespaceTextBox);
            this.csGroupBox.Controls.Add(this.namespaceLabel);
            this.csGroupBox.Location = new System.Drawing.Point(364, 200);
            this.csGroupBox.Margin = new System.Windows.Forms.Padding(4);
            this.csGroupBox.Name = "csGroupBox";
            this.csGroupBox.Padding = new System.Windows.Forms.Padding(4);
            this.csGroupBox.Size = new System.Drawing.Size(384, 192);
            this.csGroupBox.TabIndex = 19;
            this.csGroupBox.TabStop = false;
            this.csGroupBox.Text = "C#";
            // 
            // busSuffixTextBox
            // 
            this.busSuffixTextBox.Location = new System.Drawing.Point(103, 161);
            this.busSuffixTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.busSuffixTextBox.Name = "busSuffixTextBox";
            this.busSuffixTextBox.Size = new System.Drawing.Size(271, 23);
            this.busSuffixTextBox.TabIndex = 16;
            this.busSuffixTextBox.Text = "BUS";
            // 
            // dtoSuffixTextBox
            // 
            this.dtoSuffixTextBox.Location = new System.Drawing.Point(103, 130);
            this.dtoSuffixTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.dtoSuffixTextBox.Name = "dtoSuffixTextBox";
            this.dtoSuffixTextBox.Size = new System.Drawing.Size(271, 23);
            this.dtoSuffixTextBox.TabIndex = 16;
            this.dtoSuffixTextBox.Text = "Info";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(15, 156);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 28);
            this.label2.TabIndex = 15;
            this.label2.Text = "BUS Suffix:";
            // 
            // dtoSuffixLabel
            // 
            this.dtoSuffixLabel.Location = new System.Drawing.Point(15, 130);
            this.dtoSuffixLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.dtoSuffixLabel.Name = "dtoSuffixLabel";
            this.dtoSuffixLabel.Size = new System.Drawing.Size(96, 28);
            this.dtoSuffixLabel.TabIndex = 15;
            this.dtoSuffixLabel.Text = "DTO Suffix:";
            // 
            // daoSuffixTextBox
            // 
            this.daoSuffixTextBox.Location = new System.Drawing.Point(104, 95);
            this.daoSuffixTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.daoSuffixTextBox.Name = "daoSuffixTextBox";
            this.daoSuffixTextBox.Size = new System.Drawing.Size(271, 23);
            this.daoSuffixTextBox.TabIndex = 14;
            this.daoSuffixTextBox.Text = "DAL";
            // 
            // daoSuffixLabel
            // 
            this.daoSuffixLabel.Location = new System.Drawing.Point(16, 95);
            this.daoSuffixLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.daoSuffixLabel.Name = "daoSuffixLabel";
            this.daoSuffixLabel.Size = new System.Drawing.Size(96, 28);
            this.daoSuffixLabel.TabIndex = 13;
            this.daoSuffixLabel.Text = "DAO Suffix:";
            // 
            // namespaceTextBox
            // 
            this.namespaceTextBox.Location = new System.Drawing.Point(104, 58);
            this.namespaceTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.namespaceTextBox.Name = "namespaceTextBox";
            this.namespaceTextBox.Size = new System.Drawing.Size(271, 23);
            this.namespaceTextBox.TabIndex = 12;
            this.namespaceTextBox.Text = "YourNameSpace";
            // 
            // namespaceLabel
            // 
            this.namespaceLabel.Location = new System.Drawing.Point(16, 58);
            this.namespaceLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.namespaceLabel.Name = "namespaceLabel";
            this.namespaceLabel.Size = new System.Drawing.Size(96, 28);
            this.namespaceLabel.TabIndex = 11;
            this.namespaceLabel.Text = "Namespace:";
            // 
            // storedProcedurePrefixTextBox
            // 
            this.storedProcedurePrefixTextBox.Location = new System.Drawing.Point(104, 66);
            this.storedProcedurePrefixTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.storedProcedurePrefixTextBox.Name = "storedProcedurePrefixTextBox";
            this.storedProcedurePrefixTextBox.Size = new System.Drawing.Size(271, 23);
            this.storedProcedurePrefixTextBox.TabIndex = 7;
            // 
            // serverLabel
            // 
            this.serverLabel.Location = new System.Drawing.Point(28, 59);
            this.serverLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.serverLabel.Name = "serverLabel";
            this.serverLabel.Size = new System.Drawing.Size(80, 28);
            this.serverLabel.TabIndex = 13;
            this.serverLabel.Text = "Server:";
            // 
            // sqlGroupBox
            // 
            this.sqlGroupBox.Controls.Add(this.storedProcedurePrefixTextBox);
            this.sqlGroupBox.Controls.Add(this.storedProcedurePrefixLabel);
            this.sqlGroupBox.Controls.Add(this.grantUserTextBox);
            this.sqlGroupBox.Controls.Add(this.grantUserLabel);
            this.sqlGroupBox.Controls.Add(this.multipleFilesCheckBox);
            this.sqlGroupBox.Location = new System.Drawing.Point(364, 52);
            this.sqlGroupBox.Margin = new System.Windows.Forms.Padding(4);
            this.sqlGroupBox.Name = "sqlGroupBox";
            this.sqlGroupBox.Padding = new System.Windows.Forms.Padding(4);
            this.sqlGroupBox.Size = new System.Drawing.Size(384, 133);
            this.sqlGroupBox.TabIndex = 17;
            this.sqlGroupBox.TabStop = false;
            this.sqlGroupBox.Text = "SQL";
            // 
            // storedProcedurePrefixLabel
            // 
            this.storedProcedurePrefixLabel.Location = new System.Drawing.Point(16, 66);
            this.storedProcedurePrefixLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.storedProcedurePrefixLabel.Name = "storedProcedurePrefixLabel";
            this.storedProcedurePrefixLabel.Size = new System.Drawing.Size(88, 28);
            this.storedProcedurePrefixLabel.TabIndex = 10;
            this.storedProcedurePrefixLabel.Text = "Prefix:";
            // 
            // grantUserTextBox
            // 
            this.grantUserTextBox.Location = new System.Drawing.Point(104, 30);
            this.grantUserTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.grantUserTextBox.Name = "grantUserTextBox";
            this.grantUserTextBox.Size = new System.Drawing.Size(271, 23);
            this.grantUserTextBox.TabIndex = 6;
            // 
            // grantUserLabel
            // 
            this.grantUserLabel.Location = new System.Drawing.Point(16, 30);
            this.grantUserLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.grantUserLabel.Name = "grantUserLabel";
            this.grantUserLabel.Size = new System.Drawing.Size(88, 28);
            this.grantUserLabel.TabIndex = 8;
            this.grantUserLabel.Text = "Grant User:";
            // 
            // multipleFilesCheckBox
            // 
            this.multipleFilesCheckBox.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.multipleFilesCheckBox.Location = new System.Drawing.Point(16, 96);
            this.multipleFilesCheckBox.Margin = new System.Windows.Forms.Padding(4);
            this.multipleFilesCheckBox.Name = "multipleFilesCheckBox";
            this.multipleFilesCheckBox.Size = new System.Drawing.Size(288, 30);
            this.multipleFilesCheckBox.TabIndex = 8;
            this.multipleFilesCheckBox.Text = "Create multiple files for stored procedures";
            // 
            // passwordTextBox
            // 
            this.passwordTextBox.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.passwordTextBox.Enabled = false;
            this.passwordTextBox.Location = new System.Drawing.Point(136, 133);
            this.passwordTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.passwordTextBox.Name = "passwordTextBox";
            this.passwordTextBox.PasswordChar = '*';
            this.passwordTextBox.Size = new System.Drawing.Size(175, 23);
            this.passwordTextBox.TabIndex = 5;
            this.passwordTextBox.Text = "admin";
            // 
            // loginNameTextBox
            // 
            this.loginNameTextBox.BackColor = System.Drawing.SystemColors.InactiveBorder;
            this.loginNameTextBox.Enabled = false;
            this.loginNameTextBox.Location = new System.Drawing.Point(136, 96);
            this.loginNameTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.loginNameTextBox.Name = "loginNameTextBox";
            this.loginNameTextBox.Size = new System.Drawing.Size(175, 23);
            this.loginNameTextBox.TabIndex = 4;
            this.loginNameTextBox.Text = "admin";
            // 
            // generateButton
            // 
            this.generateButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.generateButton.Location = new System.Drawing.Point(652, 400);
            this.generateButton.Margin = new System.Windows.Forms.Padding(4);
            this.generateButton.Name = "generateButton";
            this.generateButton.Size = new System.Drawing.Size(96, 30);
            this.generateButton.TabIndex = 18;
            this.generateButton.Text = "Generate";
            this.generateButton.Click += new System.EventHandler(this.generateButton_Click);
            // 
            // passwordLabel
            // 
            this.passwordLabel.Enabled = false;
            this.passwordLabel.Location = new System.Drawing.Point(40, 133);
            this.passwordLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.passwordLabel.Name = "passwordLabel";
            this.passwordLabel.Size = new System.Drawing.Size(96, 28);
            this.passwordLabel.TabIndex = 3;
            this.passwordLabel.Text = "Password:";
            // 
            // loginNameLabel
            // 
            this.loginNameLabel.Enabled = false;
            this.loginNameLabel.Location = new System.Drawing.Point(40, 96);
            this.loginNameLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.loginNameLabel.Name = "loginNameLabel";
            this.loginNameLabel.Size = new System.Drawing.Size(96, 28);
            this.loginNameLabel.TabIndex = 2;
            this.loginNameLabel.Text = "Login Name:";
            // 
            // sqlServerAuthenticationRadioButton
            // 
            this.sqlServerAuthenticationRadioButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.sqlServerAuthenticationRadioButton.Location = new System.Drawing.Point(16, 59);
            this.sqlServerAuthenticationRadioButton.Margin = new System.Windows.Forms.Padding(4);
            this.sqlServerAuthenticationRadioButton.Name = "sqlServerAuthenticationRadioButton";
            this.sqlServerAuthenticationRadioButton.Size = new System.Drawing.Size(208, 30);
            this.sqlServerAuthenticationRadioButton.TabIndex = 3;
            this.sqlServerAuthenticationRadioButton.Text = "SQL Server Authentication";
            this.sqlServerAuthenticationRadioButton.CheckedChanged += new System.EventHandler(this.sqlServerAuthenticationRadioButton_CheckedChanged);
            // 
            // authenticationGroupBox
            // 
            this.authenticationGroupBox.Controls.Add(this.passwordTextBox);
            this.authenticationGroupBox.Controls.Add(this.loginNameTextBox);
            this.authenticationGroupBox.Controls.Add(this.passwordLabel);
            this.authenticationGroupBox.Controls.Add(this.loginNameLabel);
            this.authenticationGroupBox.Controls.Add(this.sqlServerAuthenticationRadioButton);
            this.authenticationGroupBox.Controls.Add(this.windowsAuthenticationRadioButton);
            this.authenticationGroupBox.Location = new System.Drawing.Point(28, 133);
            this.authenticationGroupBox.Margin = new System.Windows.Forms.Padding(4);
            this.authenticationGroupBox.Name = "authenticationGroupBox";
            this.authenticationGroupBox.Padding = new System.Windows.Forms.Padding(4);
            this.authenticationGroupBox.Size = new System.Drawing.Size(320, 170);
            this.authenticationGroupBox.TabIndex = 16;
            this.authenticationGroupBox.TabStop = false;
            this.authenticationGroupBox.Text = "Authentication";
            // 
            // windowsAuthenticationRadioButton
            // 
            this.windowsAuthenticationRadioButton.Checked = true;
            this.windowsAuthenticationRadioButton.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.windowsAuthenticationRadioButton.Location = new System.Drawing.Point(16, 30);
            this.windowsAuthenticationRadioButton.Margin = new System.Windows.Forms.Padding(4);
            this.windowsAuthenticationRadioButton.Name = "windowsAuthenticationRadioButton";
            this.windowsAuthenticationRadioButton.Size = new System.Drawing.Size(208, 30);
            this.windowsAuthenticationRadioButton.TabIndex = 2;
            this.windowsAuthenticationRadioButton.TabStop = true;
            this.windowsAuthenticationRadioButton.Text = "Windows Authentication";
            // 
            // databaseLabel
            // 
            this.databaseLabel.Location = new System.Drawing.Point(28, 96);
            this.databaseLabel.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.databaseLabel.Name = "databaseLabel";
            this.databaseLabel.Size = new System.Drawing.Size(80, 28);
            this.databaseLabel.TabIndex = 14;
            this.databaseLabel.Text = "Database:";
            // 
            // databaseTextBox
            // 
            this.databaseTextBox.Location = new System.Drawing.Point(108, 96);
            this.databaseTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.databaseTextBox.Name = "databaseTextBox";
            this.databaseTextBox.Size = new System.Drawing.Size(239, 23);
            this.databaseTextBox.TabIndex = 15;
            this.databaseTextBox.Text = "FR-HN44_G1";
            // 
            // serverTextBox
            // 
            this.serverTextBox.Location = new System.Drawing.Point(108, 59);
            this.serverTextBox.Margin = new System.Windows.Forms.Padding(4);
            this.serverTextBox.Name = "serverTextBox";
            this.serverTextBox.Size = new System.Drawing.Size(239, 23);
            this.serverTextBox.TabIndex = 12;
            this.serverTextBox.Text = "TOANTV-PC\\TOANTV3";
            // 
            // progressBar
            // 
            this.progressBar.Location = new System.Drawing.Point(28, 400);
            this.progressBar.Margin = new System.Windows.Forms.Padding(4);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(616, 28);
            this.progressBar.TabIndex = 20;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(26, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(375, 29);
            this.label1.TabIndex = 21;
            this.label1.Text = "General code Simple Not Story";
            // 
            // FormCodeInForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(774, 442);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.csGroupBox);
            this.Controls.Add(this.serverLabel);
            this.Controls.Add(this.sqlGroupBox);
            this.Controls.Add(this.generateButton);
            this.Controls.Add(this.authenticationGroupBox);
            this.Controls.Add(this.databaseLabel);
            this.Controls.Add(this.databaseTextBox);
            this.Controls.Add(this.serverTextBox);
            this.Controls.Add(this.progressBar);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "FormCodeInForm";
            this.Text = "General Code";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.csGroupBox.ResumeLayout(false);
            this.csGroupBox.PerformLayout();
            this.sqlGroupBox.ResumeLayout(false);
            this.sqlGroupBox.PerformLayout();
            this.authenticationGroupBox.ResumeLayout(false);
            this.authenticationGroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox projectTextBox;
        private System.Windows.Forms.Label projectLabel;
        private System.Windows.Forms.GroupBox csGroupBox;
        private System.Windows.Forms.TextBox dtoSuffixTextBox;
        private System.Windows.Forms.Label dtoSuffixLabel;
        private System.Windows.Forms.TextBox daoSuffixTextBox;
        private System.Windows.Forms.Label daoSuffixLabel;
        private System.Windows.Forms.TextBox namespaceTextBox;
        private System.Windows.Forms.Label namespaceLabel;
        private System.Windows.Forms.TextBox storedProcedurePrefixTextBox;
        private System.Windows.Forms.Label serverLabel;
        private System.Windows.Forms.GroupBox sqlGroupBox;
        private System.Windows.Forms.Label storedProcedurePrefixLabel;
        private System.Windows.Forms.TextBox grantUserTextBox;
        private System.Windows.Forms.Label grantUserLabel;
        private System.Windows.Forms.CheckBox multipleFilesCheckBox;
        private System.Windows.Forms.TextBox passwordTextBox;
        private System.Windows.Forms.TextBox loginNameTextBox;
        private System.Windows.Forms.Button generateButton;
        private System.Windows.Forms.Label passwordLabel;
        private System.Windows.Forms.Label loginNameLabel;
        private System.Windows.Forms.RadioButton sqlServerAuthenticationRadioButton;
        private System.Windows.Forms.GroupBox authenticationGroupBox;
        private System.Windows.Forms.RadioButton windowsAuthenticationRadioButton;
        private System.Windows.Forms.Label databaseLabel;
        private System.Windows.Forms.TextBox databaseTextBox;
        private System.Windows.Forms.TextBox serverTextBox;
        private System.Windows.Forms.ProgressBar progressBar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox busSuffixTextBox;
    }
}