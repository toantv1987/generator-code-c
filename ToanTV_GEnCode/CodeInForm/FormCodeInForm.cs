﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Win32;

namespace App_ChuongTrinhSinhCodeTuDong.CodeInForm
{
    public partial class FormCodeInForm : Form
    {
        public FormCodeInForm()
        {
            InitializeComponent();
     
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            serverTextBox.Text = cConfig.SvName;
            databaseTextBox.Text = cConfig.DbName;
            loginNameTextBox.Text = cConfig.userName;
            passwordTextBox.Text = cConfig.passWord;
            projectTextBox.Text = databaseTextBox.Text;
            namespaceTextBox.Text = databaseTextBox.Text;
        }
        private void ReadIniFile()
        {
            try
            {
                string FilePath = "Config.ini";
                string text = File.ReadAllText(FilePath);
               // IniFile.Text = text;
            }
            catch (Exception)
            {
            }
        }
        private void generateButton_Click(object sender, EventArgs e)
        {
            string connectionString;
            try
            {
              //  generateButton.Enabled = false;
                //Tạo chuỗi kết nối
                if (windowsAuthenticationRadioButton.Checked)
                {
                    connectionString = "Server=" + serverTextBox.Text + ";Database=" + databaseTextBox.Text +
                                       "; Trusted_Connection=True;";
                    //Integrated Security=True => cũng được
                }
                else
                {
                    connectionString = "Server=" + serverTextBox.Text + "; Database=" + databaseTextBox.Text +
                                       "; User ID=" + loginNameTextBox.Text + "; Password=" + passwordTextBox.Text + ";";
                }
                
                //Mở lại đường dẫn đã mở trước đó và tạo đường dẫn mới
                string outputDirectory;
                using (
                    RegistryKey registryKey =
                        Registry.CurrentUser.CreateSubKey(@"Software\Adrian Anttila\CSharpDataTier"))
                {
                    outputDirectory = (string) registryKey.GetValue("OutputDirectory", String.Empty);
                }

                using(FolderBrowserDialog dialog=new FolderBrowserDialog())
                {
                    dialog.Description = "Chọn đường dẫn để lưu file";
                    dialog.SelectedPath = outputDirectory;
                    dialog.ShowNewFolderButton = true; //Cho phép tạo folder mới
                    if (dialog.ShowDialog(this)==DialogResult.OK)
                    {
                        outputDirectory = dialog.SelectedPath;
                    }
                    else
                    {
                        return;
                    }
                }
                //Sinh code SQL va C# code
                Generator.Generate(outputDirectory, connectionString, grantUserTextBox.Text, storedProcedurePrefixTextBox.Text, multipleFilesCheckBox.Checked, projectTextBox.Text, namespaceTextBox.Text, daoSuffixTextBox.Text, dtoSuffixTextBox.Text, busSuffixTextBox.Text);


                MessageBox.Show("C# classes and stored procedures generated successfully.");

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                //throw;
            }
            finally
            {
                progressBar.Value = 0;
                generateButton.Enabled = true;
            }

        }

        private void sqlServerAuthenticationRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            loginNameLabel.Enabled = true;
            loginNameTextBox.Enabled = true;
            loginNameTextBox.BackColor = SystemColors.Window;

            passwordLabel.Enabled = true;
            passwordTextBox.Enabled = true;
            passwordTextBox.BackColor = SystemColors.Window;

          //  EnableGenerateButton();
        }
        private void EnableGenerateButton()
        {
            if (serverTextBox.Text.Length == 0)
            {
                generateButton.Enabled = false;
                return;
            }

            if (databaseTextBox.Text.Length == 0)
            {
                generateButton.Enabled = false;
                return;
            }

            if (sqlServerAuthenticationRadioButton.Checked)
            {
                if (loginNameTextBox.Text.Length == 0)
                {
                    generateButton.Enabled = false;
                    return;
                }
            }

            if (namespaceTextBox.Text.Length == 0)
            {
                generateButton.Enabled = false;
                return;
            }

            generateButton.Enabled = true;
        }
       

      
    }
}
