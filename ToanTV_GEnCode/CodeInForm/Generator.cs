﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;

namespace App_ChuongTrinhSinhCodeTuDong.CodeInForm
{
	//internal delegate void CountUpdate(object sender, CountEventArgs e);

	/// <summary>
	/// Generates C# and SQL code for accessing a database.
	/// </summary>
	internal static class Generator
	{
	//	public static event CountUpdate DatabaseCounted;
	//	public static event CountUpdate TableCounted;

		/// <summary>
		/// Generates the SQL and C# code for the specified database.
		/// </summary>
		/// <param name="outputDirectory">The directory where the C# and SQL code should be created.</param>
		/// <param name="connectionString">The connection string to be used to connect the to the database.</param>
		/// <param name="grantLoginName">The SQL Server login name that should be granted execute rights on the generated stored procedures.</param>
		/// <param name="storedProcedurePrefix">The prefix that should be used when creating stored procedures.</param>
		/// <param name="createMultipleFiles">A flag indicating if the generated stored procedures should be created in one file or separate files.</param>
        /// <param name="projectName">The name of the project file to be generated.</param>
		/// <param name="targetNamespace">The namespace that the generated C# classes should contained in.</param>
		/// <param name="daoSuffix">The suffix to be applied to all generated DAO classes.</param>
        /// <param name="dtoSuffix">The suffix to be applied to all generated DTO classes.</param>
        public static void Generate(string outputDirectory, string connectionString, string grantLoginName, string storedProcedurePrefix, bool createMultipleFiles, string projectName, string targetNamespace, string daoSuffix, string dtoSuffix, string busSuffix)
        {
			List<Table> tableList = new List<Table>();
			string databaseName;
			string sqlPath;
			string csPath;
            

			using (SqlConnection connection = new SqlConnection(connectionString))
			{
				databaseName = Utility.FormatPascal(connection.Database);
				sqlPath = Path.Combine(outputDirectory, "SQL");
				csPath = Path.Combine(outputDirectory, "CS");

				connection.Open();

                // Lấy ra danh sách các bảng trong database
				DataTable dataTable = new DataTable();
			    string strQuery = "select TABLE_CATALOG,TABLE_SCHEMA,TABLE_NAME,TABLE_TYPE from INFORMATION_SCHEMA.TABLES ";
			    strQuery = strQuery +"where TABLE_TYPE = 'BASE TABLE' and (TABLE_NAME != 'dtProperties' and TABLE_NAME != 'sysdiagrams') ";
                strQuery = strQuery + "and TABLE_CATALOG = '" + databaseName + "' order by TABLE_NAME";
                SqlCommand cmd=new SqlCommand(strQuery,connection);
				SqlDataAdapter dataAdapter=new SqlDataAdapter(cmd);
				dataAdapter.Fill(dataTable);
                cmd.Dispose();
                dataAdapter.Dispose();
                // Xử lý với từng bảng 
				foreach (DataRow dataRow in dataTable.Rows)
				{
					Table table = new Table();
					table.Name = (string) dataRow["TABLE_NAME"];
					QueryTable(connection, table);
					tableList.Add(table);
				}
			}

		    int count = 0;
			if (tableList.Count > 0)
			{
                // Tạo đường dẫn Thư mục lưu SQL
                Utility.CreateSubDirectory(sqlPath, true);

                // Tạo đường dẫn Thư mục lưu Cshaft
                Utility.CreateSubDirectory(csPath, true);

                //Tạo đường dẫn để lưu DataAccess
                Utility.CreateSubDirectory(Path.Combine(csPath, "DataAccess"), true);
                //Tạo đường dẫn để lưu Business
                Utility.CreateSubDirectory(Path.Combine(csPath, "Business"), true);
                //Tạo đường dẫn để lưu Entity
                Utility.CreateSubDirectory(Path.Combine(csPath, "Entity"), true);
                //Tao duong dan Cho webform
                Utility.CreateSubDirectory(Path.Combine(csPath, "Presentation"), true);
                Utility.CreateSubDirectory(Path.Combine(csPath, "Presentation_WindowsForm"), true);
                //Copy file .csproj tới thư mục DataAccess
                // System.IO.File.Copy("DataAccess.csproj", System.IO.Path.Combine(Path.Combine(csPath, "DataAccess\\DataAccess.csproj")), true);

                CsGenerator.CreateProjectFile_DAL(System.IO.Path.Combine(Path.Combine(csPath, "DataAccess")), "ProjectDAL.xml", tableList, daoSuffix, dtoSuffix);
                //Tạo File  "AssemblyInfo.cs" trong DataAccess
                //CsGenerator.CreateAssemblyInfo(csPath, databaseName, databaseName);
                CsGenerator.CreateAssemblyInfo(Path.Combine(csPath, "DataAccess"), "DataAccess", databaseName);

                //Copy file .csproj tới thư mục Business
                CsGenerator.CreateProjectFile_BUS(System.IO.Path.Combine(Path.Combine(csPath, "Business")), "ProjectBUS.xml", tableList, busSuffix, dtoSuffix);
                //Tạo File  "AssemblyInfo.cs" trong Bussiness
                CsGenerator.CreateAssemblyInfo(Path.Combine(csPath, "Business"), "Business", databaseName);

                //Copy file .csproj tới thư mục Entity
                CsGenerator.CreateProjectFileEntity(System.IO.Path.Combine(Path.Combine(csPath, "Entity")), "ProjectEntity.xml", tableList, daoSuffix, dtoSuffix);

                //Tạo File  "AssemblyInfo.cs" trong Entity
                CsGenerator.CreateAssemblyInfo(Path.Combine(csPath, "Entity"), "Entity", databaseName);

                // Create the necessary "use [database]" statement
                SqlGenerator.CreateUseDatabaseStatement(databaseName, sqlPath, createMultipleFiles);
                // Create the necessary database logins
                SqlGenerator.CreateUserQueries(databaseName, grantLoginName, sqlPath, createMultipleFiles);

                // Create the CRUD stored procedures and data access code for each table
                foreach (Table table in tableList)
				{
                    //Tạo thủ tục inser
                    SqlGenerator.CreateInsertStoredProcedure(databaseName, table, grantLoginName, storedProcedurePrefix, sqlPath, createMultipleFiles);
                    //Tạo thủ tục Update
                    SqlGenerator.CreateUpdateStoredProcedure(databaseName, table, grantLoginName, storedProcedurePrefix, sqlPath, createMultipleFiles);
                    //Tạo thủ tục Delete
                    SqlGenerator.CreateDeleteStoredProcedure(databaseName, table, grantLoginName, storedProcedurePrefix, sqlPath, createMultipleFiles);
                    ////Tạo thủ tục Delete By All
                    SqlGenerator.CreateDeleteAllByStoredProcedures(databaseName, table, grantLoginName, storedProcedurePrefix, sqlPath, createMultipleFiles);
                    ////Tạo thủ tục Select
                    SqlGenerator.CreateSelectStoredProcedure(databaseName, table, grantLoginName, storedProcedurePrefix, sqlPath, createMultipleFiles);
                    //Tạo thủ tục Select
                    SqlGenerator.CreateSelectAllStoredProcedure(databaseName, table, grantLoginName, storedProcedurePrefix, sqlPath, createMultipleFiles);
                    //Tạo thủ tục Select tất cả
                    SqlGenerator.CreateSelectAllByStoredProcedures(databaseName, table, grantLoginName, storedProcedurePrefix, sqlPath, createMultipleFiles);
                    //Tạo lớp entity
					CsGenerator.CreateDataTransferClass(table, targetNamespace, dtoSuffix, csPath);
                    //Tạo lớp Business
                    CsGenerator.CreateBusinessClass(databaseName, table, targetNamespace, storedProcedurePrefix, daoSuffix, dtoSuffix, csPath);
                    //Tạo lớp làm việc với csdl
					CsGenerator.CreateDataAccessClass(databaseName, table, targetNamespace, storedProcedurePrefix, daoSuffix, dtoSuffix, csPath);
                    //tạo lớp SqlDataProvider để kết nối với csdl
				    CsGenerator.CreateSqlDataProviderClass(targetNamespace, csPath);
				    string str = connectionString;
				    //Tạo web config
                    CsGenerator.CreateWebconfigClass(connectionString,csPath);
                    //Tạo bang Them,Sua,Xoa
                    CsGeneratorGraphic_ForWebForm.GridViewToTextBox(databaseName, table, targetNamespace, storedProcedurePrefix, daoSuffix, dtoSuffix, csPath);
                    //Code them sua xoa
                    CsGeneratorGraphic_ForWebForm.GridViewToTextBox_CS(databaseName, table, targetNamespace, storedProcedurePrefix, daoSuffix, dtoSuffix, csPath);
                    //Tạo File Gridview
                   
                    //KHong dung den : Cần thêm phần lấy ID và select ra bang
                  ///// CsGeneratorGraphic_ForWebForm.CreateFormGridview(databaseName, table, targetNamespace, storedProcedurePrefix, daoSuffix, dtoSuffix, csPath);
                    CsGeneratorGraphic_ForWebForm.GetTextById(databaseName, table, targetNamespace, storedProcedurePrefix, daoSuffix, dtoSuffix, csPath);
                   //=======================
                    CsGeneratorGraphic_ForWebForm.CreateModifyGridview(databaseName, table, targetNamespace, storedProcedurePrefix, daoSuffix, dtoSuffix, csPath);
                    CsGeneratorGraphic_ForWebForm.CreateModifyGridviewCS(databaseName, table, targetNamespace, storedProcedurePrefix, daoSuffix, dtoSuffix, csPath);
                    CsGeneratorGraphic_ForWebForm.GridViewToTextBox_Designers(databaseName, table, targetNamespace, storedProcedurePrefix, daoSuffix, dtoSuffix, csPath);
                  
                    //count++;
				    //TableCounted(null, new CountEventArgs(count));
				}

                //Không liên kết được đến file Resources => bỏ
			//	CsGenerator.CreateSharpCore(csPath);
		 
			}
		}

		/// <summary>
		/// Retrieves the column, primary key, and foreign key information for the specified table.
		/// </summary>
		/// <param name="connection">The SqlConnection to be used when querying for the table information.</param>
		/// <param name="table">The table instance that information should be retrieved for.</param>
		private static void QueryTable(SqlConnection connection, Table table)
		{
			// Get a list of the entities in the database
			DataTable dataTable = new DataTable();
		    string strQuery = "select INFORMATION_SCHEMA.COLUMNS.*,";
            strQuery = strQuery + "COL_LENGTH('" + table.Name + "', INFORMATION_SCHEMA.COLUMNS.COLUMN_NAME) AS COLUMN_LENGTH,";
            strQuery = strQuery + "COLUMNPROPERTY(OBJECT_ID('" + table.Name + "'), INFORMATION_SCHEMA.COLUMNS.COLUMN_NAME, 'IsComputed') as IS_COMPUTED,";
            strQuery = strQuery + "COLUMNPROPERTY(OBJECT_ID('" + table.Name + "'), INFORMATION_SCHEMA.COLUMNS.COLUMN_NAME, 'IsIdentity') as IS_IDENTITY,";
            strQuery = strQuery + "COLUMNPROPERTY(OBJECT_ID('" + table.Name + "'), INFORMATION_SCHEMA.COLUMNS.COLUMN_NAME, 'IsRowGuidCol') as IS_ROWGUIDCOL ";
		    strQuery = strQuery +"from INFORMATION_SCHEMA.COLUMNS where INFORMATION_SCHEMA.COLUMNS.TABLE_NAME = '"+table.Name+"'";
            SqlCommand cmd = new SqlCommand(strQuery, connection);
 			//SqlDataAdapter dataAdapter = new SqlDataAdapter(Utility.GetColumnQuery(table.Name), connection);
            SqlDataAdapter dataAdapter = new SqlDataAdapter(cmd);
			dataAdapter.Fill(dataTable);
            cmd.Dispose();
            dataAdapter.Dispose();
			foreach (DataRow columnRow in dataTable.Rows)
			{
				Column column = new Column();
				column.Name = columnRow["COLUMN_NAME"].ToString();
				column.Type = columnRow["DATA_TYPE"].ToString();
				column.Precision = columnRow["NUMERIC_PRECISION"].ToString();
				column.Scale = columnRow["NUMERIC_SCALE"].ToString();

				// Determine the column's length
				if (columnRow["CHARACTER_MAXIMUM_LENGTH"] != DBNull.Value)
				{
					column.Length = columnRow["CHARACTER_MAXIMUM_LENGTH"].ToString();
				}
				else
				{
					column.Length = columnRow["COLUMN_LENGTH"].ToString();
				}

				// Is the column a RowGuidCol column?
				if (columnRow["IS_ROWGUIDCOL"].ToString() == "1")
				{
					column.IsRowGuidCol = true;
				}

				// Is the column an Identity column?
				if (columnRow["IS_IDENTITY"].ToString() == "1")
				{
					column.IsIdentity = true;
				}

				// Is columnRow column a computed column?
				if (columnRow["IS_COMPUTED"].ToString() == "1")
				{
					column.IsComputed = true;
				}

				table.Columns.Add(column);
			}

			// Get the list of primary keys
			DataTable primaryKeyTable = Utility.GetPrimaryKeyList(connection, table.Name);
			foreach (DataRow primaryKeyRow in primaryKeyTable.Rows)
			{
				string primaryKeyName = primaryKeyRow["COLUMN_NAME"].ToString();

				foreach (Column column in table.Columns)
				{
					if (column.Name == primaryKeyName)
					{
						table.PrimaryKeys.Add(column);
						break;
					}
				}
			}

			// Get the list of foreign keys
			DataTable foreignKeyTable = Utility.GetForeignKeyList(connection, table.Name);
			foreach (DataRow foreignKeyRow in foreignKeyTable.Rows)
			{
				string name = foreignKeyRow["FK_NAME"].ToString();
				string columnName = foreignKeyRow["FKCOLUMN_NAME"].ToString();

				if (table.ForeignKeys.ContainsKey(name) == false)
				{
					table.ForeignKeys.Add(name, new List<Column>());
				}

				List<Column> foreignKeys = table.ForeignKeys[name];

				foreach (Column column in table.Columns)
				{
					if (column.Name == columnName)
					{
						foreignKeys.Add(column);
						break;
					}
				}
			}
		}
	}
}
