﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using App_ChuongTrinhSinhCodeTuDong.CodeInForm;
namespace App_ChuongTrinhSinhCodeTuDong
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
             
            Application.Run(new Main());
        }
        static void LoadFileIni()
        {
            string Section = "ConnectSQL";
            string ServerName = "Server";
            string DatabaseName = "Database";
            string FilePath = "Config.ini";
            string _uName = "userName";
            string _password = "passWord";
            string projectName = "projectName";
            string nameSpace = "nameSpace";
            cConfig.SvName = IniFileHelper.ReadValue(Section, ServerName, System.IO.Path.GetFullPath(FilePath));
            cConfig.DbName = IniFileHelper.ReadValue(Section, DatabaseName, System.IO.Path.GetFullPath(FilePath));
            cConfig.userName = IniFileHelper.ReadValue(Section, _uName, System.IO.Path.GetFullPath(FilePath));
            cConfig.passWord = IniFileHelper.ReadValue(Section, _password, System.IO.Path.GetFullPath(FilePath));
            cConfig.projectName = IniFileHelper.ReadValue(Section, projectName, System.IO.Path.GetFullPath(FilePath));
            cConfig.nameSpace = IniFileHelper.ReadValue(Section, nameSpace, System.IO.Path.GetFullPath(FilePath));
        }
    }
}

/* Sử dụng thêm sửa xóa .ini
 public MainWindow()
		{
			InitializeComponent();

			FilePath.Text = "test.ini";
			Section.Text = "section2";
			Key.Text = "s2k2";
			Value.Text = "4";
			ReadIniFile();
		}

		private void ReadValue_Click(object sender, RoutedEventArgs e)
		{
			string value = IniFileHelper.ReadValue(Section.Text, Key.Text, System.IO.Path.GetFullPath(FilePath.Text));
			IniFile.Text = value;
		}

		private void ReadSections_Click(object sender, RoutedEventArgs e)
		{
			string[] values = IniFileHelper.ReadSections(System.IO.Path.GetFullPath(FilePath.Text));
			if (values != null)
			{
				string value = string.Join(Environment.NewLine, values);
				IniFile.Text = value;
			}
			else
			{
				IniFile.Text = "Reading sections failed.";
			}
		}

		private void ReadKeys_Click(object sender, RoutedEventArgs e)
		{
			string[] values = IniFileHelper.ReadKeys(Section.Text, System.IO.Path.GetFullPath(FilePath.Text));
			if (values != null)
			{
				string value = string.Join(Environment.NewLine, values);
				IniFile.Text = value;
			}
			else
			{
				IniFile.Text = "Reading keys failed.";
			}
		}

		private void ReadKeyValuePairs_Click(object sender, RoutedEventArgs e)
		{
			string[] values = IniFileHelper.ReadKeyValuePairs(Section.Text, System.IO.Path.GetFullPath(FilePath.Text));
			if (values != null)
			{
				string value = string.Join(Environment.NewLine, values);
				IniFile.Text = value;
			}
			else
			{
				IniFile.Text = "Reading key value pairs failed.";
			}
		}

		private void ReadFile_Click(object sender, RoutedEventArgs e)
		{
			ReadIniFile();
		}

		private void WriteValue_Click(object sender, RoutedEventArgs e)
		{
			bool result = IniFileHelper.WriteValue(Section.Text, Key.Text, Value.Text, System.IO.Path.GetFullPath(FilePath.Text));
			ReadIniFile();
		}

		private void DeleteSection_Click(object sender, RoutedEventArgs e)
		{
			bool result = IniFileHelper.DeleteSection(Section.Text, System.IO.Path.GetFullPath(FilePath.Text));
			ReadIniFile();
		}

		private void DeleteKey_Click(object sender, RoutedEventArgs e)
		{
			bool result = IniFileHelper.DeleteKey(Section.Text, Key.Text, System.IO.Path.GetFullPath(FilePath.Text));
			ReadIniFile();
		}

		private void ReadIniFile()
		{
			try
			{
				string text = File.ReadAllText(FilePath.Text);
				IniFile.Text = text;
			}
			catch (Exception)
			{
			}
		}
     
     */
