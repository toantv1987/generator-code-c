﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Web.UI;
namespace Common
{
    public static class Alert
    {
        public static void Show(string message)
        {
            string cleanMessage = message.Replace("'", "\'");
            string script = "<script type=\"text/javascript\"> alert('" + cleanMessage + "');</script>";
            //Get the executing web page
            Page page = HttpContext.Current.CurrentHandler as Page;
            //Check if the handler is a Page and that the script isn't allready on the Page
            if (page != null && !page.ClientScript.IsClientScriptBlockRegistered("alert"))
            {
                page.ClientScript.RegisterClientScriptBlock(typeof(Alert), "alert", script);
            }
        }
    }
}