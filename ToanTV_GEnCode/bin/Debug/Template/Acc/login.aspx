﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="LoveProgrammingWeb.Acc.login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
      <link href="csslogin.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div class="page">
            <div class="imgcontainer">
                <img src="../Images/man.png" alt="Avatar" class="avatar">
            </div>

            <div class="container">
                <label><b>Username</b></label>

                <asp:TextBox ID="txtUname" runat="server" placeholder="Enter Username"></asp:TextBox>
                <label><b>Password</b></label>
                <asp:TextBox ID="txtPsw" runat="server" placeholder="Enter Password"></asp:TextBox>

                <asp:Button ID="btnLogin" runat="server" Text="Login" CssClass="button" type="submit" />

                <label>
                    <asp:CheckBox ID="CheckBox1" runat="server" Checked="true" Text="Remember me" />


                </label>
            </div>

            <div class="container" style="background-color: #f1f1f1">
                <button type="button" class="cancelbtn">Cancel</button>
                <span class="psw">Forgot <a href="#">password?</a></span>
            </div>

       
        </div>
    </form>
</body>
</html>
