﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace App_ChuongTrinhSinhCodeTuDong.CodeInform2
{
    internal static class CsGeneratorGraphic_ForWebForm
    {
        #region  Sửa xóa dữ liệu từ Gridview - TextBox
        
      
        //
        public static void GridViewToTextBox(string databaseName, Table table, string targetNamespace, string storedProcedurePrefix, string daoSuffix, string dtoSuffix, string path)
        {

            string className = Utility.FormatClassName(table.Name) + daoSuffix;
            path = Path.Combine(path, "Presentation");

            using (StreamWriter streamWriter = new StreamWriter(Path.Combine(path, table.Name + ".aspx")))
            {
                var sb = new StringBuilder();

                sb.Append("\n <%@ Page Title=\"\" Language=\"C#\" MasterPageFile=\"~/Site.Master\" AutoEventWireup=\"true\"");
                sb.Append("\n CodeBehind=\""+table.Name+".aspx.cs\" Inherits=\"Gridview_Toantv."+table.Name+"\" %>");
                sb.Append("\n <asp:Content ID=\"Content1\" ContentPlaceHolderID=\"HeadContent\" runat=\"server\">");
                sb.Append("\n </asp:Content>");
                sb.Append("\n <asp:Content ID=\"Content2\" ContentPlaceHolderID=\"MainContent\" runat=\"server\">");
                sb.Append("\n  <h3>"+table.Name+"</h3>");
                sb.Append("\n <asp:Panel ID=\"PanelModify\" runat=\"server\">");
                sb.Append("\n  <table>");
                sb.Append("\n ");
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];

                    // Is the current column an identity column?);
                    if (column.IsIdentity == false)
                    {
                        sb.Append("\n <tr>");
                        sb.Append("\n <td align=\"right\">" + column.Name + ":</td>");
                        sb.Append("\n <td><asp:TextBox ID=\"txt"+column.Name+"\" runat=\"server\" Height=\"26px\" Width=\"200px\"></asp:TextBox></td>");
                        sb.Append("\n </tr>");
                    }
                    else
                    {
                        sb.Append("\n <tr>");
                        sb.Append("\n <td align=\"right\">" + column.Name + ":</td>");
                        sb.Append("\n <td><asp:TextBox ID=\"txt"+column.Name+"\" runat=\"server\" Height=\"26px\" Width=\"200px\"></asp:TextBox></td>");
                        sb.Append("\n </tr>");
                      
                    }
                }
                sb.Append("\n <tr>");
                sb.Append("\n  <td align=\"left\">");
                sb.Append("\n &nbsp;</td>");
                sb.Append("\n <td>");
                sb.Append("\n <asp:Button ID=\"btnSave\" runat=\"server\" onclick=\"btnSave_Click\" Text=\"Save\" />");
                sb.Append("\n <asp:Button ID=\"btnDelete\" runat=\"server\" onclick=\"btnDelete_Click\" Text=\"Delete\" Visible=\"False\" />");
                sb.Append("\n  <asp:Button ID=\"btnCancel\" runat=\"server\" onclick=\"btnCancel_Click\" Text=\"Cancel\" />");
                sb.Append("\n </td>");
                sb.Append("\n  </tr>");             
                sb.Append("\n </table> \n \t</asp:Panel>");
                sb.Append("\n  <br />");
                sb.Append("\n <asp:Panel ID=\"PanelShow\" runat=\"server\">");
                sb.Append("\n  <asp:Button ID=\"btnThem\" runat=\"server\" onclick=\"btnThem_Click\" Text=\"Add New\" />");
                sb.Append("\n   <asp:GridView ID=\"GridViewShow\" runat=\"server\" AutoGenerateColumns=\"false\" OnRowCommand=\"GridViewShowRowClick\" Width=\"600px\"> ");
               
                sb.Append("\n  <Columns>");
                sb.Append("\n <asp:TemplateField HeaderText=\" + \">");
                sb.Append("\n <ItemTemplate>");
                sb.Append("\n  <%# Container.DataItemIndex+1 %></ItemTemplate>");
                sb.Append("\n <ItemStyle Width=\"10px\" />");
                sb.Append("\n </asp:TemplateField>");
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];

                    // Is the current column an identity column?);
                    if (column.IsIdentity == false)
                    {
                        sb.Append("\n \t  <asp:BoundField DataField=\"" + column.Name + "\" HeaderText=\"" + column.Name + "\" />");

                    }
                    else
                    {
                        sb.Append("\n \t <asp:BoundField DataField=\"" + column.Name + "\" HeaderText=\"" + column.Name + "\" Visible=\"False\" />");

                        sb.Append("");
                    }
                }
                 for (int i = 0; i < table.Columns.Count; i++)
                 {
                     Column column = table.Columns[i];
                     if (column.IsIdentity==true)
                     {
                          sb.Append("\n <asp:TemplateField HeaderText=\"Edit\">");
                            sb.Append("\n  <ItemTemplate>");

                            sb.Append("\n <asp:LinkButton ID=\"lbEdit\" runat=\"server\" CommandName='<%#\"S|\"+Eval(\"" + column.Name + "\")%>' Text=\"Edit\"></asp:LinkButton> ");
                            sb.Append("\n </ItemTemplate>");
                            sb.Append("\n </asp:TemplateField>");
                            sb.Append("\n <asp:TemplateField HeaderText=\"Delete\">");
                            sb.Append("\n <ItemTemplate>");

                            sb.Append("\n <asp:LinkButton ID=\"lbDelete\" runat=\"server\" CommandName='<%#\"X|\"+Eval(\"" + column.Name + "\")%>' OnClientClick=\"return confirm('Do you want to delete?')\" Text=\"Delete\"></asp:LinkButton>");
                            sb.Append("\n </ItemTemplate>");
                            sb.Append("\n </asp:TemplateField>");
                     }
                 }
            
                sb.Append("\n </Columns>");
                sb.Append(" <PagerSettings Mode=\"NumericFirstLast\" />");
                sb.Append("</asp:GridView>");
                
                sb.Append(" </asp:Panel>");
             
                sb.Append("\n </asp:Content>");
                  
                streamWriter.WriteLine(sb);
                
                            
             }
        }

        //=====================================Phần Code them sua xoa. ===========================
        public static void GridViewToTextBox_CS(string databaseName, Table table, string targetNamespace, string storedProcedurePrefix, string daoSuffix, string dtoSuffix, string path)
        {
            string className = Utility.FormatClassName(table.Name) + dtoSuffix;
            string classNameDAO = Utility.FormatClassName(table.Name) + daoSuffix;
            string classNameBusiness = "Business."+Utility.FormatClassName(table.Name) + "Business";

            path = Path.Combine(path, "Presentation");
            //Gridview to textbox
            using (StreamWriter streamWriter = new StreamWriter(Path.Combine(path, table.Name + ".aspx.cs")))
            {              
                var sb = new StringBuilder();
                sb.Append("\n using System;");
                sb.Append("\n using System.Collections.Generic;");
                sb.Append("\n using System.Linq;");
                sb.Append("\n using System.Web;");
                sb.Append("\n using System.Web.UI;");
                sb.Append("\n using System.Web.UI.WebControls;");
                sb.Append("\n using DataAccess;");
                sb.Append("\n using Entity;");
                sb.Append("\n using System.Data;");
                sb.Append("\n namespace "+targetNamespace);
                sb.Append("\n {");
                sb.Append("\n public partial class "+table.Name+" : System.Web.UI.Page");
                sb.Append("\n\t {");
                //==============Hết phần định nghĩa class
                sb.Append("\n  protected void Page_Load(object sender, EventArgs e)");
                sb.Append("{");
                sb.Append("\n    if (!Page.IsPostBack)");
                sb.Append("\n  { ");
                sb.Append("\n  Databind();");
                sb.Append("\n  PanelModify.Visible = false; ");
                sb.Append("\n  PanelShow.Visible = true; ");
                sb.Append("\n  ViewState[\"active\"] = \"\"; ");
               
                sb.Append("\n } ");
                sb.Append("\n} ");

                //Xóa dữ liệu rỗng
                sb.Append("\n//================Xóa rỗng các text box==================");
                sb.Append("\n public void Clear() \n{");
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];
                    sb.Append("\n  txt"+column.Name+".Text = \"\";");

                }
                sb.Append("\n} ");
                //Load dữ liệu cho Gridview
                sb.Append("\n //====================Load dữ liệu cho Gridview=========================");
                sb.Append("\n public void Databind() \n {");
                sb.Append("\n\t  GridViewShow.DataSource =" + classNameBusiness + ".GetAll();");
                sb.Append("\n\t GridViewShow.DataBind();");
                sb.Append("\n\t }");
                sb.Append("\n\t ");
               
                //Select trong gridview
                sb.Append("\n  //====================================Sự kiện Select trên Gridview===========================");
                sb.Append("\n protected void GridViewShowRowClick(object sender, GridViewCommandEventArgs e)");
                sb.Append("\n\t\t {");
                sb.Append("\n\t\t string str = e.CommandName;");
                sb.Append("\n\t\t char[] ch = new char[] { '|' };");
                sb.Append("\n\t\t  string[] s = str.Split(ch);");
                sb.Append("\n\t\t  " + className + " obj=new " + className + "();");
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];
                    if (column.IsIdentity==true)
                    {
                        sb.Append("\n\t\t  obj."+column.Name+" = int.Parse(s[1].ToString());");
                    }
                    
                }
               
                sb.Append("\n\t\t   switch (s[0])");
                sb.Append("\n\t\t\t {");
                sb.Append("\n\t\t\t\t  case \"S\":");
                sb.Append("\n\t\t ViewState[\"active\"] = \"edit\";");
                sb.Append("\n\t\t DataTable dt= " + classNameBusiness + ".GetByID(obj);");
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];
                    sb.Append("\n txt" + column.Name + ".Text =dt.Rows[0][\""+column.Name+"\"].ToString();");
                }
              
                sb.Append("\n\t\t ");
                sb.Append("\n\t\t PanelModify.Visible = true;");
                sb.Append("\n\t\t PanelShow.Visible = false;");
                sb.Append("\n\t\t break;");
                sb.Append("\n\t\t  case \"X\":");
                sb.Append("\n\t\t  "+classNameBusiness+".Delete(obj);");
                sb.Append("\n\t\t   Databind();");
                sb.Append("\n\t\t  break;");
                sb.Append("\n\t\t   }");
                sb.Append("\n\t\t }");
                
         
                sb.Append("\n //========================Thêm====================");
                sb.Append("\n protected void btnThem_Click(object sender, EventArgs e)");
                sb.Append("\n { \n\t PanelModify.Visible = true;");
                sb.Append("\n\t  PanelShow.Visible = false; ");
                sb.Append("\n\t Clear();");
                sb.Append("\n\t ViewState[\"active\"] = \"add\"; ");
                sb.Append("\n }");

                //Khi click vào save để THêm Hoặc SỬa
                sb.Append("\n  //=====================================Khi click vào save để THêm Hoặc SỬa=============================");
                sb.Append("\n   protected void btnSave_Click(object sender, EventArgs e) \n\t  { ");
                sb.Append("\n " + className + " obj = new " + className + "();");
              
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];
                    if (column.IsIdentity == false)
                    {
                        switch (column.Type)
                        { 
                            case "int":
                                sb.Append("\n\t obj." + column.Name + "  = int.Parse(txt" + column.Name + ".Text); ");
                                break;
                            case "bit":
                                 sb.Append("\n\t obj." + column.Name + "  = bool.Parse(txt" + column.Name + ".Text); ");
                                break;
                            case "datetime":
                                sb.Append("\n\t obj." + column.Name + "  = Convert.ToDateTime(txt" + column.Name + ".Text); ");
                                break;
                            default:
                                sb.Append("\n\t obj." + column.Name + "  =  txt" + column.Name + ".Text; ");
                                break;
                        }
                                                

                    }
                    
                }

                sb.Append("\n   switch (ViewState[\"active\"].ToString()) \n{");
                sb.Append("\n   case \"add\":  ");
                sb.Append("\n\t " + classNameBusiness + ".Insert(obj);");
              
                sb.Append("\n\t   break;");
                sb.Append("\n  case \"edit\":");
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];
                    if (column.IsIdentity == true)
                    {
                        sb.Append("\n\t obj."+column.Name+" = int.Parse(txt"+column.Name+".Text); ");

                    }
                }
               
                sb.Append("\n\t " + classNameBusiness + ".Update(obj);");
               
                sb.Append("\n\t  break; ");
                sb.Append("\n   } ");
                sb.Append("\n   Databind();");
                sb.Append("\n  PanelModify.Visible = false;");
                sb.Append("\n  PanelShow.Visible = true;}");

                //Sự kiện xóa trên Gridview    
                sb.Append("\n  //=====================================Sự kiện xóa  =============================");
                sb.Append("\n  protected void btnDelete_Click(object sender, EventArgs e) \n\t {");
                sb.Append("\n " + className + " obj = new " + className + "();");
                

                 for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];
                    if (column.IsIdentity == true)
                    {
                        //sb.Append("\n\t obj."+column.Name+" = int.Parse(txt"+column.Name+".Text); ");
                        sb.Append("\n obj."+column.Name+" = int.Parse(txt"+column.Name+".Text);");
                    }
                }

                sb.Append("\n " + classNameBusiness + ".Delete(obj);");
                sb.Append("\n  Databind();");
                sb.Append(" Common.WebMsgBox.Show(\"Delete data success !!!\");");
                sb.Append("\n PanelModify.Visible = false;");
                sb.Append("PanelShow.Visible = true; }");
                ////Sự kiện Phân Trang    
                //sb.Append("\n  //=====================================Phân Trang============================");
                //sb.Append("\n protected void GridViewShow_PageIndexChanging(object sender, GridViewPageEventArgs e) ");
                //sb.Append("\n { \n\t GridViewShow.PageIndex = e.NewPageIndex;");
                // sb.Append("\n Databind(); \n }");

                //Nút Cancel    
                sb.Append("\n  //=====================================Nút Cancel============================");
                sb.Append("\n  protected void btnCancel_Click(object sender, EventArgs e) ");
                sb.Append("\n\t { ");
                sb.Append("\n \t PanelModify.Visible = false; \n\t  PanelShow.Visible = true; \n\t\t}");
                sb.Append("\n\n }");
                sb.Append("\n }");
                streamWriter.WriteLine(sb);
            }
        }
        //PHần design code
        public static void GridViewToTextBox_Designers(string databaseName, Table table, string targetNamespace, string storedProcedurePrefix, string daoSuffix, string dtoSuffix, string path)
        {
            string className = Utility.FormatClassName(table.Name) + dtoSuffix;
            string classNameDAO = Utility.FormatClassName(table.Name) + daoSuffix;
            path = Path.Combine(path, "Presentation");
            //Gridview to textbox
            using (StreamWriter streamWriter = new StreamWriter(Path.Combine(path, table.Name + ".aspx.designer.cs")))
            {
                var sb = new StringBuilder();
                sb.Append("\n namespace "+targetNamespace+" {");
                sb.Append("\n  public partial class "+table.Name+"");
                sb.Append("{");
                sb.Append("\n  protected global::System.Web.UI.WebControls.Panel PanelModify;");
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];
                    sb.Append("\n protected global::System.Web.UI.WebControls.TextBox txt"+column.Name+";");
                }


                sb.Append("\n  protected global::System.Web.UI.WebControls.Button btnSave;");
                sb.Append("\n protected global::System.Web.UI.WebControls.Button btnDelete;");
                sb.Append("\n  protected global::System.Web.UI.WebControls.Button btnCancel;");
                sb.Append("\n  protected global::System.Web.UI.WebControls.Panel PanelShow;");
                sb.Append("\n protected global::System.Web.UI.WebControls.Button btnThem;");
                sb.Append("\n protected global::System.Web.UI.WebControls.GridView GridViewShow;");
                sb.Append("\n }");
                sb.Append("\n }");
               
                sb.Append("\n ");
                streamWriter.WriteLine(sb);
            }
        }

        #endregion
       
        //


        //Modify in gridview
        #region ===========Thêm Sửa Xóa Trong Gridview==============

        public static void CreateModifyGridview(string databaseName, Table table, string targetNamespace, string storedProcedurePrefix, string daoSuffix, string dtoSuffix, string path)
        {

            string className = Utility.FormatClassName(table.Name) + daoSuffix;
            path = Path.Combine(path, "Presentation");

            using (StreamWriter streamWriter = new StreamWriter(Path.Combine(path, table.Name + "IG.aspx")))
            {
                string Id = "Id";
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];

                    // Is the current column an identity column?);
                    if (column.IsIdentity == true)
                    {
                        Id = column.Name;
                    }


                }

                var sb = new StringBuilder();
                sb.Append(" <asp:GridView ID=\"GridView" + table.Name + "\" runat=\"server\" AutoGenerateColumns=\"False\" ");
                sb.Append(" AllowPaging=\"True\" DataKeyNames=\"Id\"");
                sb.Append(" OnPageIndexChanging=\"GridView"+table.Name+"_PageIndexChanging\"");
                sb.Append("  OnRowCancelingEdit=\"GridView" + table.Name + "_RowCancelingEdit\"");
                sb.Append(" OnRowEditing=\"GridView" + table.Name + "_RowEditing\"");
                sb.Append(" OnRowUpdating=\"GridView" + table.Name + "_RowUpdating\"");
                sb.Append(" onrowdeleting=\"GridView" + table.Name + "_RowDeleting\"");
                sb.Append("  ShowFooter=\"True\" Width=\"90%\">");
               
                sb.Append(" <Columns>");
                sb.Append(" <asp:TemplateField HeaderText=\"No.\">");
                sb.Append("  <ItemTemplate>");
                sb.Append(" <%# Container.DataItemIndex+1 %>");
                sb.Append("</ItemTemplate>");
                
                sb.Append(" </asp:TemplateField>");

                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];

                    // Is the current column an identity column?);
                    if (column.IsIdentity == false)
                    {
                       
                        sb.Append("<asp:TemplateField HeaderText=\" "+column.Name+" \">");
                        sb.Append("<EditItemTemplate>");
                        sb.Append("<asp:TextBox ID=\"txt" + column.Name + "\" runat=\"server\" Text='<%# Bind(\"" + column.Name + "\") %>'></asp:TextBox>");
                        sb.Append(" </EditItemTemplate>");
                        sb.Append("<FooterTemplate>");
                        sb.Append(" <asp:TextBox ID=\"txt"+column.Name+"\" runat=\"server\"></asp:TextBox>");
                        sb.Append("</FooterTemplate>");
                        sb.Append(" <ItemTemplate>"); 
                        sb.Append("<asp:Label ID=\"lbl"+column.Name+"\" runat=\"server\" Text='<%# Bind(\""+column.Name+"\") %>'></asp:Label>");
                        sb.Append(" </ItemTemplate>");
                        sb.Append(" </asp:TemplateField>"); 
                       
                        sb.Append("");
                    }
                    else
                    {                                           
                    }
                }
                //Tạo Cột xóa
                sb.Append(" <asp:TemplateField ShowHeader=\"False\" HeaderText=\"Delete\">");
                sb.Append("  <ItemTemplate>");
                sb.Append(" <asp:LinkButton ID=\"LinkButtonDelete\" runat=\"server\" CausesValidation=\"False\" CommandName=\"Delete\" ");
                sb.Append("Text=\"Delete\" OnClientClick = \"return confirm('Do you want to delete?')\"></asp:LinkButton>");
                sb.Append("</ItemTemplate>");
                sb.Append("<FooterTemplate>");
                sb.Append("<asp:Button ID=\"btnAdd\" runat=\"server\" Text=\"Add\" Width=\"48px\" onclick=\"btnAdd_Click\" />");
                sb.Append(" </FooterTemplate>");
                sb.Append(" </asp:TemplateField>");
                //Tạo Cột Sửa
                sb.Append("  <asp:TemplateField  HeaderText=\"Edit\">");
                sb.Append(" <ItemTemplate>");
                sb.Append("<asp:LinkButton ID=\"LinkButtonEdit\" runat=\"server\" CausesValidation=\"False\" CommandName=\"Edit\" Text=\"Edit\"></asp:LinkButton>");
                sb.Append("</ItemTemplate>");
                sb.Append("<EditItemTemplate>");
                sb.Append("<asp:LinkButton ID=\"LinkButtonUpdate\" runat=\"server\" CausesValidation=\"True\" CommandName=\"Update\" Text=\"Update\"></asp:LinkButton> &nbsp;");
                sb.Append("<asp:LinkButton ID=\"LinkButtonCanncel\" runat=\"server\" CausesValidation=\"False\" CommandName=\"Cancel\" Text=\"Cancel\"></asp:LinkButton>");
                sb.Append("</EditItemTemplate>");
                sb.Append("</asp:TemplateField>");
                //Cột Id
                sb.Append("<asp:TemplateField HeaderText=\"Id\">");
                sb.Append("<EditItemTemplate>");
                sb.Append("<asp:TextBox ID=\"txtId\" runat=\"server\" Text='<%# Bind(\"Id\") %>'></asp:TextBox>");
                sb.Append("</EditItemTemplate>");
                sb.Append("<ItemTemplate>");
                sb.Append("<asp:Label ID=\"txtId\" runat=\"server\" Text='<%# Bind(\"Id\") %>'></asp:Label>");
                sb.Append("</ItemTemplate>");
                sb.Append("<HeaderStyle CssClass=\"hideGridColumn\" />");
                sb.Append("<ItemStyle CssClass=\"hideGridColumn\" />");
                sb.Append("<FooterStyle  CssClass=\"hideGridColumn\" />");
                sb.Append("</asp:TemplateField>");
                sb.Append("");
                sb.Append(" </Columns>"); 
                sb.Append(" <PagerSettings Mode=\"NumericFirstLast\" />");
                sb.Append("</asp:GridView>");
               
                sb.Append("  <style type=\"text/css\">.hideGridColumn{display: none;}</style>");
                sb.Append("");
                streamWriter.WriteLine(sb);


            }
        }
      

        public static void CreateModifyGridviewCS(string databaseName, Table table, string targetNamespace, string storedProcedurePrefix, string daoSuffix, string dtoSuffix, string path)
        {

            string className = Utility.FormatClassName(table.Name) + daoSuffix;
            string classNameEntity = Utility.FormatClassName(table.Name) + dtoSuffix;
            path = Path.Combine(path, "Presentation");

            using (StreamWriter streamWriter = new StreamWriter(Path.Combine(path, table.Name + "IG.cs")))
            {

                var sb = new StringBuilder();
                //lOAD
                sb.Append("\n  protected void Page_Load(object sender, EventArgs e) ");
                sb.Append("\n  { if (!IsPostBack) { Databind();} } ");
                //bIND dATA
                sb.Append("\n   public void Databind() { GridView" + table.Name + ".DataSource =" + className + ".GetAll();GridView" + table.Name + ".DataBind();}");
                //pHAN tRANG
                sb.Append("\n   protected void GridView" + table.Name + "_PageIndexChanging(object sender, GridViewPageEventArgs e) ");
                sb.Append("\n  {GridView" + table.Name + ".PageIndex = e.NewPageIndex;Databind();}");
                //eDIT
                sb.Append("\n   protected void GridView" + table.Name + "_RowEditing(object sender, GridViewEditEventArgs e) { GridView" + table.Name + ".EditIndex = e.NewEditIndex; Databind(); }");
               //Update
                sb.Append("\n protected void GridView" + table.Name + "_RowUpdating(object sender, GridViewUpdateEventArgs e)   {");
                sb.Append("\n " + classNameEntity + " obj=new " + classNameEntity + "();");
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];
                    if (column.IsIdentity==false)
                    {
                        sb.Append("\n obj."+column.Name+" = ((TextBox)GridView"+table.Name+".Rows[e.RowIndex].FindControl(\"txt"+column.Name+"\")).Text;");
                    }
                    else
                    {
                        sb.Append("\n obj.Id = int.Parse(((TextBox)GridView" + table.Name + ".Rows[e.RowIndex].FindControl(\"txtId\")).Text.Trim());");
                    }

                }


                sb.Append("\n " + className + ".Update(obj);");
                sb.Append("\n GridView" + table.Name + ".EditIndex = -1; ");
                sb.Append("\n Databind(); }");

                //cANCEL
                sb.Append("\n protected void GridView" + table.Name + "_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)  ");
                sb.Append("\n  { GridView" + table.Name + ".EditIndex = -1; Databind();} ");
                //tHEM
                sb.Append("\n  protected void btnAdd_Click(object sender, EventArgs e) ");
                sb.Append("\n    { " + classNameEntity + " obj = new " + classNameEntity + "();");
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];
                    if (column.IsIdentity==false)
                    {
                        sb.Append("\n  obj." + column.Name + " = ((TextBox) GridView" + table.Name + ".FooterRow.FindControl(\"txt" + column.Name + "\")).Text;");
                    }
                    

                }

                sb.Append("\n " + className + ".Insert(obj);");
                sb.Append("\n Databind(); }");
                //Xoa
                sb.Append("\n  protected void GridView" + table.Name + "_RowDeleting(object sender, GridViewDeleteEventArgs e) { ");
                sb.Append("\n  " + classNameEntity + " obj=new " + classNameEntity + "();");
                sb.Append("\n   obj.Id = int.Parse(GridView" + table.Name + ".DataKeys[e.RowIndex].Values[0].ToString());");
                sb.Append("\n  " + className + ".Delete(obj);");
                sb.Append("\n  Databind(); }");
                sb.Append("\n  ");
                sb.Append("\n  ");
                streamWriter.WriteLine(sb);


            }
        }

        #endregion

        #region Tạo gridview Sua xoa o 1 trang khac
        ///Tao gridview Sua xoa o 1 trang khac
        public static void CreateFormGridview(string databaseName, Table table, string targetNamespace, string storedProcedurePrefix, string daoSuffix, string dtoSuffix, string path)
        {

            string className = Utility.FormatClassName(table.Name) + daoSuffix;
            path = Path.Combine(path, "Presentation");

            using (StreamWriter streamWriter = new StreamWriter(Path.Combine(path, className + "Gridview.aspx")))
            {
                string Id = "Id";
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];

                    // Is the current column an identity column?);
                    if (column.IsIdentity == true)
                    {
                        Id = column.Name;
                    }

                }

                var sb = new StringBuilder();
                sb.Append("<asp:GridView ID=\"grvShow\" runat=\"server\" AutoGenerateColumns=\"False\" CssClass=\"grid\"");
                sb.Append(" Width=\"100%\" AllowPaging=\"True\" OnPageIndexChanging=\"grvShow_PageIndexChanging\"");
                sb.Append(" AllowSorting=\"True\" OnSorting=\"grvShow_Sorting\" DataKeyNames=" + Id + " OnRowDeleting=\"grvShow_RowDeleting\">");
                sb.Append(" <Columns>");
                sb.Append(" <asp:TemplateField HeaderText=\"No.\">");
                sb.Append("  <ItemTemplate>");
                sb.Append(" <%# Container.DataItemIndex+1 %>");
                sb.Append("</ItemTemplate>");
                sb.Append(" <ItemStyle Width=\"10px\" />");
                sb.Append(" </asp:TemplateField>");

                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];

                    // Is the current column an identity column?);
                    if (column.IsIdentity == false)
                    {
                        sb.Append(" <asp:BoundField DataField=" + column.Name + " HeaderText=" + column.Name + " SortExpression=" + column.Name + " />");
                    }
                    else
                    {
                        sb.Append("<asp:TemplateField HeaderText=\"Option\">");
                        sb.Append(" <ItemTemplate>");
                        sb.Append("  <asp:HyperLink ID=\"HyperLink1\" runat=\"server\" CommandName=\"Edit\" ");

                        sb.Append(" NavigateUrl='<%# String.Format(\"~/ModelAct.aspx?act={0}&id={1}\",\"edit\",Eval(\"" + column.Name + "\")) %>'>Edit</asp:HyperLink>");

                        sb.Append("<asp:LinkButton ID=\"LinkButton1\" runat=\"server\" CausesValidation=\"False\" CommandName=\"Delete\"");
                        sb.Append(" Text=\"Delete\" OnClientClick='return confirm(\"Are you sure you want to Delete this item ?\")'></asp:LinkButton>");

                        sb.Append("</ItemTemplate>");
                        sb.Append("</asp:TemplateField>");
                    }
                }
                sb.Append("</Columns>");
                sb.Append(" <PagerSettings Mode=\"NumericFirstLast\" />");
                sb.Append("</asp:GridView>");



                sb.Append("");
                sb.Append("");
                streamWriter.WriteLine(sb);


            }
        }
        public static void GetTextById(string databaseName, Table table, string targetNamespace, string storedProcedurePrefix, string daoSuffix, string dtoSuffix, string path)
        {

            string className = Utility.FormatClassName(table.Name) + daoSuffix;
            string classNameEntity = Utility.FormatClassName(table.Name) + dtoSuffix;
            path = Path.Combine(path, "Presentation");

            using (StreamWriter streamWriter = new StreamWriter(Path.Combine(path, className + "_GetTextById.cs")))
            {

                var sb = new StringBuilder();
                sb.Append("\n  protected void Page_Load(object sender, EventArgs e)");
                sb.Append("\n   {");
                sb.Append("\n  if (!IsPostBack)");
                sb.Append("\n  {");
                sb.Append("\n    GetTextById(Id);");
                sb.Append("\n   }");
                sb.Append("\n   }");
                sb.Append("\n   #region Lam Viec Voi Gridview");
                sb.Append("\n   public void GetTextById(int Id)");
                sb.Append("\n  { ");
                sb.Append("\n   " + classNameEntity + " obj= new " + classNameEntity + "();");
                sb.Append("\n  obj.Id = Id;");
                sb.Append("\n   DataTable dt = "+className+".GetById(obj);");
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];
                   
                    sb.Append("\n  txt"+column.Name+".Text = dt.Rows[0][\""+column.Name+"\"].ToString();");
                }
                sb.Append("\n   ");
                sb.Append("\n   }");
               

                streamWriter.WriteLine(sb);


            }
        }
        #endregion
        ///Tao Repeater
        #region tao Repeater
        public static void CreateRepeater(string databaseName, Table table, string targetNamespace, string storedProcedurePrefix, string daoSuffix, string dtoSuffix, string path)
        {

            //string className = Utility.FormatClassName(table.Name) + daoSuffix;
            //path = Path.Combine(path, "Presentation");

            //using (StreamWriter streamWriter = new StreamWriter(Path.Combine(path, className + "Repeater.aspx")))
            //{
            //    var sb = new StringBuilder();


            //    streamWriter.WriteLine(sb);
            //}
        }

     #endregion
    }
}

