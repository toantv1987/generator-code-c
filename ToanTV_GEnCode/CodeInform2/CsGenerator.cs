﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Xml;
namespace App_ChuongTrinhSinhCodeTuDong.CodeInform2
{
	/// <summary>
	/// Generates C# data access and data transfer classes.
	/// </summary>
	internal static class CsGenerator
	{
		/// <summary>
		/// Creates a project file that references each generated C# code file for data access.
		/// </summary>
		/// <param name="path">The path where the project file should be created.</param>
		/// <param name="projectName">The name of the project.</param>
		/// <param name="tableList">The list of tables code files were created for.</param>
		/// <param name="daoSuffix">The suffix to append to the name of each data access class.</param>
        /// <param name="dtoSuffix">The suffix to append to the name of each data transfer class.</param>
		public static void CreateProjectFile(string path, string projectName, List<Table> tableList, string daoSuffix, string dtoSuffix)
		{
			string projectXml = Utility.GetResource("DataTierGenerator.Resources.Project.xml");
			XmlDocument document = new XmlDocument();
			document.LoadXml(projectXml);

			XmlNamespaceManager namespaceManager = new XmlNamespaceManager(document.NameTable);
			namespaceManager.AddNamespace(String.Empty, "http://schemas.microsoft.com/developer/msbuild/2003");
			namespaceManager.AddNamespace("msbuild", "http://schemas.microsoft.com/developer/msbuild/2003");

			document.SelectSingleNode("/msbuild:Project/msbuild:PropertyGroup/msbuild:ProjectGuid", namespaceManager).InnerText = "{" + Guid.NewGuid().ToString() + "}";
			document.SelectSingleNode("/msbuild:Project/msbuild:PropertyGroup/msbuild:RootNamespace", namespaceManager).InnerText = projectName;
			document.SelectSingleNode("/msbuild:Project/msbuild:PropertyGroup/msbuild:AssemblyName", namespaceManager).InnerText = projectName;

			XmlNode itemGroupNode = document.SelectSingleNode("/msbuild:Project/msbuild:ItemGroup[msbuild:Compile]", namespaceManager);
			foreach (Table table in tableList)
			{
				string className = Utility.FormatClassName(table.Name);
				
				XmlNode dtoCompileNode = document.CreateElement("Compile", "http://schemas.microsoft.com/developer/msbuild/2003");
				XmlAttribute dtoAttribute = document.CreateAttribute("Include");
				dtoAttribute.Value = className + dtoSuffix + ".cs";
				dtoCompileNode.Attributes.Append(dtoAttribute);
				itemGroupNode.AppendChild(dtoCompileNode);
				
				XmlNode dataCompileNode = document.CreateElement("Compile", "http://schemas.microsoft.com/developer/msbuild/2003");
				XmlAttribute dataAttribute = document.CreateAttribute("Include");
                dataAttribute.Value = Path.Combine("DataAccess", Utility.FormatClassName(table.Name) + daoSuffix + ".cs");
				dataCompileNode.Attributes.Append(dataAttribute);
				itemGroupNode.AppendChild(dataCompileNode);
			}
			
			document.Save(Path.Combine(path, projectName + ".csproj"));
		}

		/// <summary>
		/// Creates the AssemblyInfo.cs file for the project.
		/// </summary>
		/// <param name="path">The root path of the project.</param>
		/// <param name="assemblyTitle">The title of the assembly.</param>
		/// <param name="databaseName">The name of the database the assembly provides access to.</param>
		public static void CreateAssemblyInfo(string path, string assemblyTitle, string databaseName)
		{
			string assemblyInfo = Utility.GetResource("DataTierGenerator.Resources.AssemblyInfo.txt");
			assemblyInfo.Replace("#AssemblyTitle", assemblyTitle);
			assemblyInfo.Replace("#DatabaseName", databaseName);

			string propertiesDirectory = Path.Combine(path, "Properties");
			if (Directory.Exists(propertiesDirectory) == false)
			{
				Directory.CreateDirectory(propertiesDirectory);
			}

			File.WriteAllText(Path.Combine(propertiesDirectory, "AssemblyInfo.cs"), assemblyInfo);
		}

		/// <summary>
		/// Creates the SharpCore DLLs required by the generated code.
		/// </summary>
		/// <param name="path">The root path of the project</param>
		public static void CreateSharpCore(string path)
		{
			string sharpCoreDirectory = Path.Combine(Path.Combine(path, "Lib"), "SharpCore");
			if (Directory.Exists(sharpCoreDirectory) == false)
			{
				Directory.CreateDirectory(sharpCoreDirectory);
			}

			Utility.WriteResourceToFile("DataTierGenerator.Resources.SharpCore.SharpCore.Data.dll", Path.Combine(sharpCoreDirectory, "SharpCore.Data.dll"));
			Utility.WriteResourceToFile("DataTierGenerator.Resources.SharpCore.SharpCore.Data.pdb", Path.Combine(sharpCoreDirectory, "SharpCore.Data.pdb"));
			Utility.WriteResourceToFile("DataTierGenerator.Resources.SharpCore.SharpCore.Extensions.dll", Path.Combine(sharpCoreDirectory, "SharpCore.Extensions.dll"));
			Utility.WriteResourceToFile("DataTierGenerator.Resources.SharpCore.SharpCore.Extensions.pdb", Path.Combine(sharpCoreDirectory, "SharpCore.Extensions.pdb"));
			Utility.WriteResourceToFile("DataTierGenerator.Resources.SharpCore.SharpCore.Utilities.dll", Path.Combine(sharpCoreDirectory, "SharpCore.Utilities.dll"));
			Utility.WriteResourceToFile("DataTierGenerator.Resources.SharpCore.SharpCore.Utilities.pdb", Path.Combine(sharpCoreDirectory, "SharpCore.Utilities.pdb"));
		}

		/// <summary>
		/// Creates a C# class for all of the table's stored procedures.
		/// </summary>
		/// <param name="table">Instance of the Table class that represents the table this class will be created for.</param>
		/// <param name="targetNamespace">The namespace that the generated C# classes should contained in.</param>
		/// <param name="daoSuffix">The suffix to be appended to the data access class.</param>
		/// <param name="path">Path where the class should be created.</param>
		public static void CreateDataTransferClass(Table table, string targetNamespace, string dtoSuffix, string path)
		{
			string className = Utility.FormatClassName(table.Name) + dtoSuffix;
            path = Path.Combine(path, "Entity");
            using (StreamWriter streamWriter = new StreamWriter(Path.Combine(path, className + ".cs")))
			{
				// Create the header for the class
				streamWriter.WriteLine("using System;");
				streamWriter.WriteLine();
				//streamWriter.WriteLine("namespace " + targetNamespace);
                streamWriter.WriteLine("namespace Entity");
                streamWriter.WriteLine("{");

				streamWriter.WriteLine("\tpublic class " + className);
				streamWriter.WriteLine("\t{");
                //Append the public properties and Method
				streamWriter.WriteLine("\t\t#region Properties");
				for (int i = 0; i < table.Columns.Count; i++)
				{
					Column column = table.Columns[i];
					string parameter = Utility.CreateMethodParameter(column);
					string type = parameter.Split(' ')[0];
					string name = parameter.Split(' ')[1];
                    //streamWriter.WriteLine("\t\tprivate " + type + " _" + Utility.FormatPascal(name) + ";");
                    streamWriter.WriteLine("\t\tpublic " + type + " " + Utility.FormatPascal(name) + " { get; set; }");

					if (i < (table.Columns.Count - 1))
					{
						streamWriter.WriteLine();
					}
				}
				 
				streamWriter.WriteLine();
				streamWriter.WriteLine("\t\t#endregion");

				// Close out the class and namespace
				streamWriter.WriteLine("\t}");
				streamWriter.WriteLine("}");
			}
		}
		
		/// <summary>
		/// Creates a C# data access class for all of the table's stored procedures.
		/// </summary>
		/// <param name="databaseName">The name of the database.</param>
		/// <param name="table">Instance of the Table class that represents the table this class will be created for.</param>
		/// <param name="targetNamespace">The namespace that the generated C# classes should contained in.</param>
		/// <param name="storedProcedurePrefix">Prefix to be appended to the name of the stored procedure.</param>
		/// <param name="daoSuffix">The suffix to be appended to the data access class.</param>
        /// <param name="dtoSuffix">The suffix to append to the name of each data transfer class.</param>
		/// <param name="path">Path where the class should be created.</param>
		public static void CreateDataAccessClass(string databaseName, Table table, string targetNamespace, string storedProcedurePrefix, string daoSuffix, string dtoSuffix, string path)
		{
			string className = Utility.FormatClassName(table.Name) + daoSuffix;
            path = Path.Combine(path, "DataAccess");

            using (StreamWriter streamWriter = new StreamWriter(Path.Combine(path, className + ".cs")))
			{
				// Create the header for the class
				streamWriter.WriteLine("using System;");
                streamWriter.WriteLine("using System.Linq;");
                
				streamWriter.WriteLine("using System.Collections.Generic;");
				streamWriter.WriteLine("using System.Data;");
				streamWriter.WriteLine("using System.Data.SqlClient;");
                streamWriter.WriteLine("using Entity;");
				streamWriter.WriteLine();

                streamWriter.WriteLine("namespace DataAccess");
				streamWriter.WriteLine("{");

                streamWriter.WriteLine("\tpublic class " + className  + "  : SqlDataProvider");
				streamWriter.WriteLine("\t{");
                // Append the access methods
				streamWriter.WriteLine("\t\t#region Methods");
				streamWriter.WriteLine();
				
				CreateInsertMethod(table, storedProcedurePrefix, dtoSuffix, streamWriter);
				CreateUpdateMethod(table, storedProcedurePrefix, dtoSuffix, streamWriter);
				CreateDeleteMethod(table, storedProcedurePrefix,dtoSuffix, streamWriter);
                CreateGetAllMethod(table, storedProcedurePrefix, dtoSuffix, streamWriter);
			    CreateGetAllMethodByQuery(table, storedProcedurePrefix, dtoSuffix, streamWriter);
                CreateGetAllMethodOrderByColumn(table, storedProcedurePrefix, dtoSuffix, streamWriter);
                CreateGetByIDMethod(table, storedProcedurePrefix, dtoSuffix, streamWriter);               
               
                CreateGetbyLikeColume(table, storedProcedurePrefix, dtoSuffix, streamWriter);
               
                //CreateDeleteAllByMethods(table, storedProcedurePrefix, streamWriter);
                //CreateSelectMethod(table, storedProcedurePrefix, dtoSuffix, streamWriter);
                //CreateSelectJsonMethod(table, storedProcedurePrefix, dtoSuffix, streamWriter);
                //CreateSelectAllMethod(table, storedProcedurePrefix, dtoSuffix, streamWriter);
                //CreateSelectAllJsonMethod(table, storedProcedurePrefix, dtoSuffix, streamWriter);
                //CreateSelectAllByMethods(table, storedProcedurePrefix, dtoSuffix, streamWriter);
                //CreateSelectAllByJsonMethods(table, storedProcedurePrefix, dtoSuffix, streamWriter);
                //CreateMapMethod(table, dtoSuffix, streamWriter);

				streamWriter.WriteLine();
				streamWriter.WriteLine("\t\t#endregion");

				// Close out the class and namespace
				streamWriter.WriteLine("\t}");
				streamWriter.WriteLine("}");
			}
		}

        public static void CreateBusinessClass(string databaseName, Table table, string targetNamespace, string storedProcedurePrefix, string daoSuffix, string dtoSuffix, string path)
        {
            string classNameController = Utility.FormatClassName(table.Name) + daoSuffix;
            string className = Utility.FormatClassName(table.Name) + "Business";
            string NameEntity = Utility.FormatClassName(table.Name) + dtoSuffix;
            path = Path.Combine(path, "Business");

            using (StreamWriter streamWriter = new StreamWriter(Path.Combine(path, className + ".cs")))
            {
                // Create the header for the class
                streamWriter.WriteLine("using System;");
                streamWriter.WriteLine("using System.Linq;");

                streamWriter.WriteLine("using System.Collections.Generic;");
                streamWriter.WriteLine("using System.Data;");
                streamWriter.WriteLine("using System.Data.SqlClient;");
                streamWriter.WriteLine("using Entity;");
                streamWriter.WriteLine();

                streamWriter.WriteLine("namespace Business");
                streamWriter.WriteLine("{");

                streamWriter.WriteLine("\tpublic class " + className + "");
                streamWriter.WriteLine("\t {");
                // Append the access methods
                streamWriter.WriteLine("\t\t#region Methods");
                string strOjectControler = "C" + classNameController;
                streamWriter.WriteLine("    public static DataAccess." + classNameController + " " + strOjectControler + " = new DataAccess." + classNameController + "();");
                streamWriter.WriteLine("    //Get All Table");
                streamWriter.WriteLine("   public static DataTable GetAll() \n {\n return " + strOjectControler + ".GetAll(); \n }");
                streamWriter.WriteLine(" ");
                streamWriter.WriteLine("  //InSert \n public static void Insert(" + NameEntity + " Object) \n{ \n " + strOjectControler + ".Insert(Object); \n}");
                streamWriter.WriteLine(" ");
                streamWriter.WriteLine("   //Delete \n public static void Delete(" + NameEntity + " Object) \n { \n " + strOjectControler + ".Delete(Object); \n }");
                streamWriter.WriteLine(" ");
                streamWriter.WriteLine("  //Update \n public static void Update(" + NameEntity + " Object) \n { \n " + strOjectControler + ".Update(Object); \n }");
                streamWriter.WriteLine("  //Get table with column by parammert and sort \n public static DataTable GetByColumn(string Col, string Pal,string sort) \n { \n return " + strOjectControler + ".GetByColumn(Col, Pal,sort); \n }");
                streamWriter.WriteLine(" ");
                streamWriter.WriteLine(" //Get table by ID \n public static DataTable GetByID(" + NameEntity + " obj) \n { \n return " + strOjectControler + ".GetById(obj); \n }");
                streamWriter.WriteLine(" ");
                streamWriter.WriteLine(" //Get LIKE \n public static DataTable GetLike(string Col, string Pal) \n { \n return " + strOjectControler + ".GetLike(Col, Pal); \n }");
                streamWriter.WriteLine();
                streamWriter.WriteLine("\t\t#endregion");
                streamWriter.WriteLine("\t}");
                streamWriter.WriteLine("}"); 
                // Close out the class and namespace
            }
        }
        //Tạo lớp SqlDataProvider
        public static void CreateSqlDataProviderClass(string targetNamespace, string path)
        {
            path = Path.Combine(path, "DataAccess");
            using (StreamWriter streamWriter = new StreamWriter(Path.Combine(path, "SqlDataProvider" + ".cs")))
            {
                // Create the header for the class
                streamWriter.WriteLine("using System;");
                streamWriter.WriteLine("using System.Linq;");
                streamWriter.WriteLine("using System.Configuration;");
                streamWriter.WriteLine("using System.Collections.Generic;");
                streamWriter.WriteLine("using System.Data;");
                streamWriter.WriteLine("using System.Data.SqlClient;");
                streamWriter.WriteLine("using Entity;");
                streamWriter.WriteLine();
          
                streamWriter.WriteLine("namespace DataAccess");
                streamWriter.WriteLine("{");

                streamWriter.WriteLine("\t public class SqlDataProvider");
                streamWriter.WriteLine("\t{");
                // Append the access methods
                streamWriter.Write("\t\tpublic static string strConStr = ConfigurationManager.ConnectionStrings[");
                streamWriter.Write('"');
                streamWriter.Write("strConnect");
                streamWriter.Write('"');
                streamWriter.WriteLine("].ConnectionString;");

                streamWriter.WriteLine("\t\tpublic static SqlConnection connection=new SqlConnection(strConStr);");
                streamWriter.WriteLine("\t\tpublic SqlDataProvider()");
                streamWriter.WriteLine("\t\t{");
                streamWriter.WriteLine("\t\t\tif (connection == null) { connection = new SqlConnection(strConStr); }");
                streamWriter.WriteLine("\t\t}");

                streamWriter.WriteLine("\t\t public static SqlConnection GetConnection()");
                streamWriter.WriteLine("\t\t{");
                streamWriter.WriteLine("\t\t\tif (connection.State==ConnectionState.Closed)");
                streamWriter.WriteLine("\t\t\t{");
                streamWriter.WriteLine("\t\t\tconnection.Open();");
                streamWriter.WriteLine("\t\t\treturn connection;");
                streamWriter.WriteLine("\t\t\t}");
                streamWriter.WriteLine("\t\t\telse return connection;");
                streamWriter.WriteLine("\t\t}");

                streamWriter.WriteLine("\t\tprivate static SqlCommand GetCommand(string sql)");
                streamWriter.WriteLine("\t\t{");
                streamWriter.WriteLine("\t\t\tSqlCommand cmd = new SqlCommand(sql, GetConnection());");
                streamWriter.WriteLine("\t\t\tconnection.Close();");
                streamWriter.WriteLine("\t\t\treturn cmd;");
                streamWriter.WriteLine("\t\t}");

                streamWriter.WriteLine("\t\tpublic static DataTable GetData(string sql)"); 
                streamWriter.WriteLine("\t\t{");
                streamWriter.WriteLine("\t\t\treturn GetData(GetCommand(sql));");
                streamWriter.WriteLine("\t\t}");

                streamWriter.WriteLine("\t\tpublic static DataTable GetData(SqlCommand cmd)");
                streamWriter.WriteLine("\t\t{");
                streamWriter.WriteLine("\t\t\ttry"); 
                streamWriter.WriteLine("\t\t\t{");
                streamWriter.WriteLine("\t\t\tif (cmd.Connection == null) { cmd.Connection = GetConnection(); }");
                streamWriter.WriteLine("\t\t\tusing (SqlDataAdapter da = new SqlDataAdapter())");
                streamWriter.WriteLine("\t\t\t\t{");
                streamWriter.WriteLine("\t\t\t\t\tda.SelectCommand = cmd;");
                streamWriter.WriteLine("\t\t\t\t\tDataTable dt=new DataTable();");
                streamWriter.WriteLine("\t\t\t\t\t da.Fill(dt);");
                streamWriter.WriteLine("\t\t\t\t\tGetConnection().Close();");
                streamWriter.WriteLine("\t\t\t\t\treturn dt;");
                streamWriter.WriteLine("\t\t\t\t}");

                streamWriter.WriteLine("\t\t\t}");
                streamWriter.WriteLine("\t\t\tfinally");
                streamWriter.WriteLine("\t\t\t{");
                streamWriter.WriteLine("\t\t\t\tconnection.Close();");
                streamWriter.WriteLine("\t\t\t}");
                streamWriter.WriteLine("\t\t}");
                streamWriter.WriteLine("\t\t");

                streamWriter.WriteLine("\t\tpublic static void ExecuteNonQuery(string sql)");
                streamWriter.WriteLine("\t\t{");
                streamWriter.WriteLine("\t\t\tExecuteNonQuery(GetCommand(sql));");
                streamWriter.WriteLine("\t\t}");
                streamWriter.WriteLine("\t\t");

                streamWriter.WriteLine("\t\tpublic static void ExecuteNonQuery(SqlCommand cmd)");
                streamWriter.WriteLine("\t\t{");
                streamWriter.WriteLine("\t\t\tcmd.Connection = GetConnection();");
                streamWriter.WriteLine("\t\t\tcmd.ExecuteNonQuery();");
                streamWriter.WriteLine("\t\t}");
                streamWriter.WriteLine("\t\t");

                streamWriter.WriteLine("\t\tpublic static object ExecuteScalar(string sql)");
                streamWriter.WriteLine("\t\t{");
                streamWriter.WriteLine("\t\t\treturn ExecuteScalar(GetCommand(sql));");
                streamWriter.WriteLine("\t\t}");
                streamWriter.WriteLine("\t\t");

                streamWriter.WriteLine("\t\tpublic static object ExecuteScalar(SqlCommand cmd)");
                streamWriter.WriteLine("\t\t{");
                streamWriter.WriteLine("\t\t\ttry");
                streamWriter.WriteLine("\t\t\t{");
                streamWriter.WriteLine("\t\t\t\tif (cmd.Connection == null) { cmd.Connection = GetConnection(); }");
                streamWriter.WriteLine("\t\t\t\treturn cmd.ExecuteScalar();");
                streamWriter.WriteLine("\t\t\t}");
                streamWriter.WriteLine("\t\t\tfinally");
                streamWriter.WriteLine("\t\t\t{");
                streamWriter.WriteLine("\t\t\t\tconnection.Close();");
                streamWriter.WriteLine("\t\t\t}");
                streamWriter.WriteLine("\t\t}");
               
                streamWriter.WriteLine();
                // Close out the class and namespace
                streamWriter.WriteLine("\t}");
                streamWriter.WriteLine("}");
            }
        }

        //Tạo chuỗi kết nối
        public static void CreateWebconfigClass(string connectionString,string path)
        {
            using (StreamWriter streamWriter = new StreamWriter(Path.Combine(path, "Web.Config")))
            {
                streamWriter.WriteLine("\t\t<configuration>");
                streamWriter.WriteLine("\t\t\t <connectionStrings>");
                streamWriter.Write("\t\t\t\t<add name=");
                streamWriter.Write('"');
                streamWriter.Write("strConnect");
                streamWriter.Write('"');
                streamWriter.Write(" connectionString=");
                streamWriter.Write('"');
                streamWriter.Write(connectionString);
                streamWriter.WriteLine('"');
                streamWriter.WriteLine("\t\t\t\t\t\t\tproviderName=" + '"' + "System.Data.SqlClient" + '"' + " />");
                streamWriter.WriteLine("\t\t\t</connectionStrings>");
                streamWriter.WriteLine("\t\t</configuration>");
            }
        }
     
        ///<summary> Tạo phương thức tìm kiếm
        /// </summary>
        /// <param name="table">bảng thể hiện cho phương thức table .</param>
        /// <param name="storedProcedurePrefix">Thủ tục sẽ được gọi.</param>
        /// <param name="dtoSuffix">Tên của mới lớp entiy.</param>
		/// <param name="streamWriter">The StreamWriter instance that will be used to create the method.</param>
        
        private static void CreateGetAllMethod(Table table, string storedProcedurePrefix, string dtoSuffix, StreamWriter streamWriter)
        {
            string className = Utility.FormatClassName(table.Name) + dtoSuffix;
            string variableName = Utility.FormatVariableName(table.Name);
            // Append the method header
            streamWriter.WriteLine("\t\t/// <summary>");
            streamWriter.WriteLine("\t\t/// Saves a record to the " + table.Name + " table.");
            streamWriter.WriteLine("\t\t/// </summary>");
            streamWriter.WriteLine("\t\tpublic DataTable GetAll()");
            streamWriter.WriteLine("\t\t{");
            //----create query -------------------------------------
            streamWriter.WriteLine("\n\t\t string str="+'"'+"select * from [" + table.Name + "]"+'"'+";");
            streamWriter.WriteLine("\n\t\t  SqlCommand cmd = new SqlCommand();");
            streamWriter.WriteLine("\n\t\t  cmd.CommandType = CommandType.Text;");
            streamWriter.WriteLine("\n\t\t  cmd.CommandText = str;");
            streamWriter.WriteLine();
            streamWriter.WriteLine("\t\treturn GetData(cmd);");
           
            streamWriter.WriteLine("\t\t }");
            streamWriter.WriteLine();
        }
        //Get All voi query la chuoi truyen vao

        private static void CreateGetAllMethodByQuery(Table table, string storedProcedurePrefix, string dtoSuffix, StreamWriter streamWriter)
        {
            string className = Utility.FormatClassName(table.Name) + dtoSuffix;
            string variableName = Utility.FormatVariableName(table.Name);
            // Append the method header

            streamWriter.WriteLine("\n\t\t public DataTable GetAll(string str)");
            streamWriter.WriteLine("\t\t{");
            //----create query -------------------------------------
            streamWriter.WriteLine("\t\treturn GetData(str);");
            // Append the method footer
            streamWriter.WriteLine("\t\t}");
            streamWriter.WriteLine();
        }
        //
        private static void CreateGetAllMethodOrderByColumn(Table table, string storedProcedurePrefix, string dtoSuffix, StreamWriter streamWriter)
        {
            
            streamWriter.WriteLine("\n //Get column with order");
            streamWriter.WriteLine("\n\t\t   public DataTable GetOrderByColumn(string Col, string sort)");
            streamWriter.WriteLine("\t\t{");
            //----create query -------------------------------------
            streamWriter.Write("\n\t\t  string str = \"select * from ["+table.Name+"]  order by [\" + Col + \"] \" + sort;");

            streamWriter.Write("\n SqlCommand cmd = new SqlCommand();");
            streamWriter.Write("\n cmd.CommandType = CommandType.Text; ");
            streamWriter.Write("\n cmd.CommandText = str; ");
             streamWriter.Write("\n return GetData(cmd);");
            streamWriter.WriteLine("\t }");
            streamWriter.WriteLine("\t //Get by Column");
            streamWriter.WriteLine("\t public DataTable GetByColumn(string Col, string Pal,string sort)");
            streamWriter.WriteLine("\t {");
            streamWriter.WriteLine("\t string str = \"select * from [Users] where \" + Col + \"=\" + \"'\" + Pal + \"' Order by [\" + Col + \"] \" + sort;");
            streamWriter.WriteLine("\t  SqlCommand cmd = new SqlCommand();");
            streamWriter.WriteLine("\t cmd.CommandType = CommandType.Text;");
            streamWriter.WriteLine("\t cmd.CommandText = str;");
            streamWriter.WriteLine("\t return GetData(cmd);");
            streamWriter.WriteLine("\t }");
            
            streamWriter.WriteLine();
        }
        private static void CreateGetByIDMethod(Table table, string storedProcedurePrefix, string dtoSuffix, StreamWriter streamWriter)
        {
            string className = Utility.FormatClassName(table.Name) + dtoSuffix;
            string variableName = Utility.FormatVariableName(table.Name);
            // Append the method header
            streamWriter.WriteLine("\t\t/// <summary>");
            streamWriter.WriteLine("\t\t/// Saves a record to the " + table.Name + " table.");
            streamWriter.WriteLine("\t\t/// </summary>");
            streamWriter.WriteLine("\t\tpublic DataTable GetById(" + className + " " + variableName + ")");
            streamWriter.WriteLine("\t\t{");

            streamWriter.WriteLine("\t\t");

            streamWriter.Write("\t\tstring str=" + '"' + "select * from [" + table.Name + "] where ");
            // lấy ra khóa chính của bảng
            for (int i = 0; i < table.PrimaryKeys.Count; i++)
            {
                Column column = table.PrimaryKeys[i];

                if (i == 0)
                {
                    streamWriter.Write(" [" + column.Name + "] = @" + column.Name);
                }
                else
                {
                    streamWriter.Write("\tand [" + column.Name + "] = @" + column.Name);
                }
            }
            streamWriter.Write('"');
            streamWriter.WriteLine(";");
            streamWriter.WriteLine("\t\tSqlCommand cmd= new SqlCommand();");
            streamWriter.WriteLine("\t\tcmd.CommandType = CommandType.Text;");
            streamWriter.WriteLine("\t\tcmd.CommandText = str;");
            streamWriter.WriteLine();
            // Append the parameter declarations
            streamWriter.WriteLine("\t\t\tSqlParameter[] parameters = new SqlParameter[]");
            streamWriter.WriteLine("\t\t\t{");
            for (int i = 0; i < table.PrimaryKeys.Count; i++)
            {
                Column column = table.PrimaryKeys[i];

                streamWriter.Write("\t\t\t\t" + Utility.CreateSqlParameter(table, column));
                if (i < (table.PrimaryKeys.Count - 1))
                {
                    streamWriter.Write(",");
                }

                streamWriter.WriteLine();
            }
            streamWriter.WriteLine("\t\t\t};");
            streamWriter.WriteLine();
            streamWriter.WriteLine("\t\tfor (int i = 0; i < parameters.Count(); i++)");
            streamWriter.WriteLine("\t\t\t{");
            streamWriter.WriteLine("\t\t\tcmd.Parameters.Add(parameters[i]);");
            streamWriter.WriteLine("\t\t\t}");
            streamWriter.WriteLine("\t\t\treturn GetData(cmd);");


            // Append the method footer
            streamWriter.WriteLine("\t\t}");


            streamWriter.WriteLine();
        }

        //Get by columm
        /*
           public static DataTable GetByColumn(string Col, string Pal)
        {
            //    int Param = int.Parse(Pal);
         string str = "Select * from tbPartList where " + Col + "= " + "'" + Pal + "'";
            return GetData(str);

        }
         */
       
        public static void CreateGetbyLikeColume(Table table, string storedProcedurePrefix, string dtoSuffix, StreamWriter streamWriter)
        {
            streamWriter.WriteLine();
            streamWriter.WriteLine("\t\tpublic  DataTable GetLike(string Col,string Pal)");
            streamWriter.WriteLine("\t\t{");
            //----create query -------------------------------------
            streamWriter.Write("\t\tstring str=" + '"' + "select * from [" + table.Name + "] where ");
            streamWriter.Write('"');
            streamWriter.Write(" + Col + ");
            streamWriter.Write('"');
            streamWriter.Write(" like ");
            streamWriter.Write('"');
            streamWriter.Write("+");
            streamWriter.Write('"');
            streamWriter.Write("'%");
            streamWriter.Write('"');
            streamWriter.Write(" + Pal +");
            streamWriter.Write('"');
            streamWriter.Write("%'");
            streamWriter.Write('"');
            streamWriter.WriteLine(";");


            streamWriter.WriteLine("return GetData(str);");
            // Append the method footer
            streamWriter.WriteLine("\t\t}");
            streamWriter.WriteLine();
        }
        private static void CreateInsertMethod(Table table, string storedProcedurePrefix, string dtoSuffix, StreamWriter streamWriter)
		{
			string className = Utility.FormatClassName(table.Name) + dtoSuffix;
			string variableName = Utility.FormatVariableName(table.Name);
            // Append the method header
			streamWriter.WriteLine("\t\t/// <summary>");
			streamWriter.WriteLine("\t\t/// Saves a record to the " + table.Name + " table.");
			streamWriter.WriteLine("\t\t/// </summary>");
			streamWriter.WriteLine("\t\tpublic  void Insert(" + className + " " + variableName + ")");
			streamWriter.WriteLine("\t\t{");
			streamWriter.WriteLine("\t\t");
          //  SqlGenerator.CreateInsertStoredProcedure(databaseName, table, grantLoginName, storedProcedurePrefix, sqlPath, createMultipleFiles);
            
            //----create query -------------------------------------
            streamWriter.Write("\t\tstring str="+'"'+"insert into [" + table.Name + "]");
            streamWriter.Write("(");

            // Create the parameter list
            for (int i = 0; i < table.Columns.Count; i++)
            {
                Column column = table.Columns[i];
              // Ignore any identity columns
                if (column.IsIdentity == false)
                {
                    // Append the column name as a parameter of the insert statement
                    if (i < (table.Columns.Count - 1))
                    {
                        streamWriter.Write("[" + column.Name + "],");
                    }
                    else
                    {
                        streamWriter.Write("[" + column.Name + "]");
                    }
                }
            }

            streamWriter.Write(")");
            streamWriter.Write(" values");
            streamWriter.Write("(");

            // Create the values list
            for (int i = 0; i < table.Columns.Count; i++)
            {
                Column column = table.Columns[i];

                // Is the current column an identity column?
                if (column.IsIdentity == false)
                {
                    // Append the necessary line breaks and commas
                    if (i < (table.Columns.Count - 1))
                    {
                        streamWriter.Write("@" + column.Name + ",");
                    }
                    else
                    {
                        streamWriter.Write("@" + column.Name);
                    }
                }
            }

            streamWriter.Write(")");
            streamWriter.Write('"');
            streamWriter.WriteLine(";");

             // string str = "insert into  tbPartInput(PartId,UserId,Quantity,TimeDate) values(@PartId,@UserId,@Quantity,@TimeDate)";
             streamWriter.WriteLine("\t\tSqlCommand cmd= new SqlCommand();");
             streamWriter.WriteLine("\t\tcmd.CommandType = CommandType.Text;");
             streamWriter.WriteLine("\t\tcmd.CommandText = str;");
             streamWriter.WriteLine("\t\tcmd.Connection = GetConnection();");         
			 streamWriter.WriteLine();
			
			// Append the parameter declarations
			streamWriter.WriteLine("\t\t\tSqlParameter[] parameters = new SqlParameter[]");
			streamWriter.WriteLine("\t\t\t{");
			for (int i = 0; i < table.Columns.Count; i++)
			{
				Column column = table.Columns[i];
				if (column.IsIdentity == false && column.IsRowGuidCol == false)
				{
					streamWriter.Write("\t\t\t\t" + Utility.CreateSqlParameter(table, column));
					if (i < (table.Columns.Count - 1))
					{
						streamWriter.Write(",");
					}
					
					streamWriter.WriteLine();
				}
			}

			streamWriter.WriteLine("\t\t\t};");
			streamWriter.WriteLine();
            streamWriter.WriteLine("\t\tfor (int i = 0; i < parameters.Count(); i++)");
            streamWriter.WriteLine("\t\t\t{");
            streamWriter.WriteLine("\t\t\tcmd.Parameters.Add(parameters[i]);");
            streamWriter.WriteLine("\t\t\t}");
            streamWriter.WriteLine("\t\t\tExecuteNonQuery(cmd);");
           
         
			// Append the method footer
			streamWriter.WriteLine("\t\t}");


			streamWriter.WriteLine();
		}

		/// <summary>
		/// Creates a string that represents the update functionality of the data access class.
		/// </summary>
		/// <param name="table">The Table instance that this method will be created for.</param>
		/// <param name="storedProcedurePrefix">The prefix that is used on the stored procedure that this method will call.</param>
        /// <param name="dtoSuffix">The suffix to append to the name of each data transfer class.</param>
		/// <param name="streamWriter">The StreamWriter instance that will be used to create the method.</param>
		private static void CreateUpdateMethod(Table table, string storedProcedurePrefix, string dtoSuffix, StreamWriter streamWriter)
		{
            string className = Utility.FormatClassName(table.Name) + dtoSuffix;
            string variableName = Utility.FormatVariableName(table.Name);
            // Append the method header
            streamWriter.WriteLine("\t\t/// <summary>");
            streamWriter.WriteLine("\t\t/// Saves a record to the " + table.Name + " table.");
            streamWriter.WriteLine("\t\t/// </summary>");
            streamWriter.WriteLine("\t\tpublic  void Update(" + className + " " + variableName + ")");
            streamWriter.WriteLine("\t\t{");

            streamWriter.WriteLine("\t\t");
         
            //----create query -------------------------------------
            streamWriter.Write("\t\tstring str=" + '"' + "Update [" + table.Name + "] set ");
            // Create the set statement
            bool firstLine = true;
            for (int i = 0; i < table.Columns.Count; i++)
            {
                Column column = (Column)table.Columns[i];

                // Ignore Identity and RowGuidCol columns
                if (table.PrimaryKeys.Contains(column) == false)
                {
                    if (firstLine)
                    {
                        streamWriter.Write(" ");
                        firstLine = false;
                    }
                    else
                    {
                        streamWriter.Write("\t");
                    }

                    streamWriter.Write("[" + column.Name + "] = @" + column.Name);

                    if (i < (table.Columns.Count - 1))
                    {
                        streamWriter.Write(",");
                    }

                    streamWriter.Write("");
                }
            }

            streamWriter.Write(" where");

            // Create the where clause
            for (int i = 0; i < table.PrimaryKeys.Count; i++)
            {
                Column column = table.PrimaryKeys[i];

                if (i == 0)
                {
                    streamWriter.Write(" [" + column.Name + "] = @" + column.Name);
                }
                else
                {
                    streamWriter.Write("\tand [" + column.Name + "] = @" + column.Name);
                }
            }
        
            streamWriter.Write('"');
            streamWriter.WriteLine(";");

          
            streamWriter.WriteLine("\t\tSqlCommand cmd= new SqlCommand();");
            streamWriter.WriteLine("\t\tcmd.CommandType = CommandType.Text;");
            streamWriter.WriteLine("\t\tcmd.CommandText = str;");
            streamWriter.WriteLine("\t\tcmd.Connection = GetConnection();");
            streamWriter.WriteLine();

            // Append the parameter declarations
            streamWriter.WriteLine("\t\t\tSqlParameter[] parameters = new SqlParameter[]");
            streamWriter.WriteLine("\t\t\t{");
            for (int i = 0; i < table.Columns.Count; i++)
            {
                Column column = table.Columns[i];
              streamWriter.Write("\t\t\t\t" + Utility.CreateSqlParameter(table, column));
                    if (i < (table.Columns.Count - 1))
                    {
                        streamWriter.Write(",");
                    }

                    streamWriter.WriteLine();
               
            }

            streamWriter.WriteLine("\t\t\t};");
            streamWriter.WriteLine();
            streamWriter.WriteLine("\t\tfor (int i = 0; i < parameters.Count(); i++)");
            streamWriter.WriteLine("\t\t\t{");
            streamWriter.WriteLine("\t\t\tcmd.Parameters.Add(parameters[i]);");
            streamWriter.WriteLine("\t\t\t}");
            streamWriter.WriteLine("\t\t\tExecuteNonQuery(cmd);");


            // Append the method footer
            streamWriter.WriteLine("\t\t}");


            streamWriter.WriteLine();
        }
		/// <summary>
		/// Creates a string that represents the delete functionality of the data access class.
		/// </summary>
		/// <param name="table">The Table instance that this method will be created for.</param>
		/// <param name="storedProcedurePrefix">The prefix that is used on the stored procedure that this method will call.</param>
		/// <param name="streamWriter">The StreamWriter instance that will be used to create the method.</param>
		private static void CreateDeleteMethod(Table table, string storedProcedurePrefix,  string dtoSuffix,StreamWriter streamWriter)
        {
            string className = Utility.FormatClassName(table.Name) + dtoSuffix;
            string variableName = Utility.FormatVariableName(table.Name);
			if (table.PrimaryKeys.Count > 0)
			{
				// Append the method header
				streamWriter.WriteLine("\t\t/// <summary>");
				streamWriter.WriteLine("\t\t/// Deletes a record from the " + table.Name + " table by its primary key.");
				streamWriter.WriteLine("\t\t/// </summary>");
                streamWriter.WriteLine("\t\tpublic  void Delete(" + className + " " + variableName + ")");

				streamWriter.WriteLine("\t\t{");
                //
                streamWriter.Write("\t\tstring str=" + '"' + "Delete from [" + table.Name + "] where ");
                // Create the where clause
                for (int i = 0; i < table.PrimaryKeys.Count; i++)
                {
                    Column column = table.PrimaryKeys[i];

                    if (i == 0)
                    {
                        streamWriter.Write(" [" + column.Name + "] = @" + column.Name);
                    }
                    else
                    {
                        streamWriter.Write("\tand [" + column.Name + "] = @" + column.Name);
                    }
                }
                streamWriter.Write('"');
                streamWriter.WriteLine(";");
                streamWriter.WriteLine("\t\tSqlCommand cmd= new SqlCommand();");
                streamWriter.WriteLine("\t\tcmd.CommandType = CommandType.Text;");
                streamWriter.WriteLine("\t\tcmd.CommandText = str;");
                streamWriter.WriteLine();
               // Append the parameter declarations
				streamWriter.WriteLine("\t\t\tSqlParameter[] parameters = new SqlParameter[]");
				streamWriter.WriteLine("\t\t\t{");
				for (int i = 0; i < table.PrimaryKeys.Count; i++)
				{
					Column column = table.PrimaryKeys[i];

                    streamWriter.Write("\t\t\t\t" + Utility.CreateSqlParameter(table, column));
                 //   streamWriter.WriteLine("//////////////tesssssssss");
				//	streamWriter.Write("\t\t\t\tnew SqlParameter(\"@" + column.Name + "\", " +table.Name+"."+ Utility.FormatCamel(column.Name) + ")");
					if (i < (table.PrimaryKeys.Count - 1))
					{
						streamWriter.Write(",");
					}
                  
					streamWriter.WriteLine();
				}
                streamWriter.WriteLine("\t\t\t};");
                streamWriter.WriteLine();
                streamWriter.WriteLine("\t\tfor (int i = 0; i < parameters.Count(); i++)");
                streamWriter.WriteLine("\t\t\t{");
                streamWriter.WriteLine("\t\t\tcmd.Parameters.Add(parameters[i]);");
                streamWriter.WriteLine("\t\t\t}");
                streamWriter.WriteLine("\t\t\tExecuteNonQuery(cmd);");


                // Append the method footer
                streamWriter.WriteLine("\t\t}");


                streamWriter.WriteLine();
			}
		}

		/// <summary>
		/// Creates a string that represents the "delete by" functionality of the data access class.
		/// </summary>
		/// <param name="table">The Table instance that this method will be created for.</param>
		/// <param name="storedProcedurePrefix">The prefix that is used on the stored procedure that this method will call.</param>
		/// <param name="streamWriter">The StreamWriter instance that will be used to create the method.</param>
		private static void CreateDeleteAllByMethods(Table table, string storedProcedurePrefix, StreamWriter streamWriter)
		{
			// Create a stored procedure for each foreign key

			foreach (List<Column> compositeKeyList in table.ForeignKeys.Values)
			{
				// Create the stored procedure name
				StringBuilder stringBuilder = new StringBuilder(255);
				stringBuilder.Append("DeleteAllBy");
				for (int i = 0; i < compositeKeyList.Count; i++)
				{
					Column column = compositeKeyList[i];

					if (i > 0)
					{
						stringBuilder.Append("_" + Utility.FormatPascal(column.Name));
					}
					else
					{
						stringBuilder.Append(Utility.FormatPascal(column.Name));
					}

				}
				string methodName = stringBuilder.ToString();
				string procedureName = storedProcedurePrefix + table.Name + methodName;

				// Create the delete function based on keys
				// Append the method header
				streamWriter.WriteLine("\t\t/// <summary>");
				streamWriter.WriteLine("\t\t/// Deletes a record from the " + table.Name + " table by a foreign key.");
				streamWriter.WriteLine("\t\t/// </summary>");

				streamWriter.Write("\t\tpublic void " + methodName + "(");
				for (int i = 0; i < compositeKeyList.Count; i++)
				{
					Column column = compositeKeyList[i];
					streamWriter.Write(Utility.CreateMethodParameter(column));
					if (i < (compositeKeyList.Count - 1))
					{
						streamWriter.Write(", ");
					}
				}
				streamWriter.WriteLine(")");
				streamWriter.WriteLine("\t\t{");

				// Append the parameter declarations
				streamWriter.WriteLine("\t\t\tSqlParameter[] parameters = new SqlParameter[]");
				streamWriter.WriteLine("\t\t\t{");
				for (int i = 0; i < compositeKeyList.Count; i++)
				{
					Column column = compositeKeyList[i];
					streamWriter.Write("\t\t\t\tnew SqlParameter(\"@" + column.Name + "\", " + Utility.FormatCamel(column.Name) + ")");
					if (i < (compositeKeyList.Count - 1))
					{
						streamWriter.Write(",");
					}

					streamWriter.WriteLine();
				}
                streamWriter.WriteLine("\t\t\t};");
                streamWriter.WriteLine();
                streamWriter.WriteLine("\t\tfor (int i = 0; i < parameters.Count(); i++)");
                streamWriter.WriteLine("\t\t\t{;");
                streamWriter.WriteLine("\t\t\tcmd.Parameters.Add(parameters[i]);");
                streamWriter.WriteLine("\t\t\t};");
                streamWriter.WriteLine("\t\t\tExecuteNonQuery(cmd);");


                // Append the method footer
                streamWriter.WriteLine("\t\t}");


                streamWriter.WriteLine();
			}
		}

		/// <summary>
		/// Creates a string that represents the select by primary key functionality of the data access class.
		/// </summary>
		/// <param name="table">The Table instance that this method will be created for.</param>
		/// <param name="storedProcedurePrefix">The prefix that is used on the stored procedure that this method will call.</param>
        /// <param name="dtoSuffix">The suffix to append to the name of each data transfer class.</param>
		/// <param name="streamWriter">The StreamWriter instance that will be used to create the method.</param>
		private static void CreateSelectMethod(Table table, string storedProcedurePrefix, string dtoSuffix, StreamWriter streamWriter)
		{
			if (table.PrimaryKeys.Count > 0 && table.Columns.Count != table.ForeignKeys.Count)
			{
				string className = Utility.FormatClassName(table.Name) + dtoSuffix;

				// Append the method header
				streamWriter.WriteLine("\t\t/// <summary>");
				streamWriter.WriteLine("\t\t/// Selects a single record from the " + table.Name + " table.");
				streamWriter.WriteLine("\t\t/// </summary>");

				streamWriter.Write("\t\tpublic " + className + " Select(");
				for (int i = 0; i < table.PrimaryKeys.Count; i++)
				{
					Column column = table.PrimaryKeys[i];
					streamWriter.Write(Utility.CreateMethodParameter(column));
					if (i < (table.PrimaryKeys.Count - 1))
					{
						streamWriter.Write(", ");
					}
				}
				streamWriter.WriteLine(")");
				streamWriter.WriteLine("\t\t{");

				// Append the parameter declarations
				streamWriter.WriteLine("\t\t\tSqlParameter[] parameters = new SqlParameter[]");
				streamWriter.WriteLine("\t\t\t{");
				for (int i = 0; i < table.PrimaryKeys.Count; i++)
				{
					Column column = table.PrimaryKeys[i];
					streamWriter.Write("\t\t\t\tnew SqlParameter(\"@" + column.Name + "\", " + Utility.FormatCamel(column.Name) + ")");
					if (i < (table.PrimaryKeys.Count - 1))
					{
						streamWriter.Write(",");
					}

					streamWriter.WriteLine();
				}

				streamWriter.WriteLine("\t\t\t};");
				streamWriter.WriteLine();
				
				// Append the stored procedure execution
				streamWriter.WriteLine("\t\t\tusing (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, \"" + table.Name + "Select\", parameters))");
				streamWriter.WriteLine("\t\t\t{");
				streamWriter.WriteLine("\t\t\t\tif (dataReader.Read())");
				streamWriter.WriteLine("\t\t\t\t{");
				streamWriter.WriteLine("\t\t\t\t\treturn MapDataReader(dataReader);");
				streamWriter.WriteLine("\t\t\t\t}");
				streamWriter.WriteLine("\t\t\t\telse");
				streamWriter.WriteLine("\t\t\t\t{");
				streamWriter.WriteLine("\t\t\t\t\treturn null;");
				streamWriter.WriteLine("\t\t\t\t}");
				streamWriter.WriteLine("\t\t\t}");

				// Append the method footer
				streamWriter.WriteLine("\t\t}");
				streamWriter.WriteLine();
			}
		}

		/// <summary>
		/// Creates a string that represents the select JSON by primary key functionality of the data access class.
		/// </summary>
		/// <param name="table">The Table instance that this method will be created for.</param>
		/// <param name="storedProcedurePrefix">The prefix that is used on the stored procedure that this method will call.</param>
        /// <param name="dtoSuffix">The suffix to append to the name of each data transfer class.</param>
		/// <param name="streamWriter">The StreamWriter instance that will be used to create the method.</param>
		private static void CreateSelectJsonMethod(Table table, string storedProcedurePrefix, string dtoSuffix, StreamWriter streamWriter)
		{
			if (table.PrimaryKeys.Count > 0 && table.Columns.Count != table.ForeignKeys.Count)
			{
				string className = Utility.FormatClassName(table.Name) + dtoSuffix;

				// Append the method header
				streamWriter.WriteLine("\t\t/// <summary>");
				streamWriter.WriteLine("\t\t/// Selects a single record from the " + table.Name + " table.");
				streamWriter.WriteLine("\t\t/// </summary>");

				streamWriter.Write("\t\tpublic string SelectJson(");
				for (int i = 0; i < table.PrimaryKeys.Count; i++)
				{
					Column column = table.PrimaryKeys[i];
					streamWriter.Write(Utility.CreateMethodParameter(column));
					if (i < (table.PrimaryKeys.Count - 1))
					{
						streamWriter.Write(", ");
					}
				}
				streamWriter.WriteLine(")");
				streamWriter.WriteLine("\t\t{");

				// Append the parameter declarations
				streamWriter.WriteLine("\t\t\tSqlParameter[] parameters = new SqlParameter[]");
				streamWriter.WriteLine("\t\t\t{");
				for (int i = 0; i < table.PrimaryKeys.Count; i++)
				{
					Column column = table.PrimaryKeys[i];
					streamWriter.Write("\t\t\t\tnew SqlParameter(\"@" + column.Name + "\", " + Utility.FormatCamel(column.Name) + ")");
					if (i < (table.PrimaryKeys.Count - 1))
					{
						streamWriter.Write(",");
					}

					streamWriter.WriteLine();
				}

				streamWriter.WriteLine("\t\t\t};");
				streamWriter.WriteLine();
				
				// Append the stored procedure execution
				streamWriter.WriteLine("\t\t\treturn SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, \"" + table.Name + "Select\", parameters);");

				// Append the method footer
				streamWriter.WriteLine("\t\t}");
				streamWriter.WriteLine();
			}
		}

		/// <summary>
		/// Creates a string that represents the select functionality of the data access class.
		/// </summary>
		/// <param name="table">The Table instance that this method will be created for.</param>
		/// <param name="storedProcedurePrefix">The prefix that is used on the stored procedure that this method will call.</param>
        /// <param name="dtoSuffix">The suffix to append to the name of each data transfer class.</param>
		/// <param name="streamWriter">The StreamWriter instance that will be used to create the method.</param>
		private static void CreateSelectAllMethod(Table table, string storedProcedurePrefix, string dtoSuffix, StreamWriter streamWriter)
		{
			if (table.Columns.Count != table.PrimaryKeys.Count && table.Columns.Count != table.ForeignKeys.Count)
			{
				string className = Utility.FormatClassName(table.Name) + dtoSuffix;
				string dtoVariableName = Utility.FormatCamel(className);

				// Append the method header
				streamWriter.WriteLine("\t\t/// <summary>");
				streamWriter.WriteLine("\t\t/// Selects all records from the " + table.Name + " table.");
				streamWriter.WriteLine("\t\t/// </summary>");
				streamWriter.WriteLine("\t\tpublic List<" + className + "> SelectAll()");
				streamWriter.WriteLine("\t\t{");

				// Append the stored procedure execution
				streamWriter.WriteLine("\t\t\tusing (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, \"" + table.Name + "SelectAll\"))");
				streamWriter.WriteLine("\t\t\t{");
				streamWriter.WriteLine("\t\t\t\tList<" + className + "> " + dtoVariableName + "List = new List<" + className + ">();");
				streamWriter.WriteLine("\t\t\t\twhile (dataReader.Read())");
				streamWriter.WriteLine("\t\t\t\t{");
				streamWriter.WriteLine("\t\t\t\t\t" + className + " " + dtoVariableName + " = MapDataReader(dataReader);");
				streamWriter.WriteLine("\t\t\t\t\t" + dtoVariableName + "List.Add(" + dtoVariableName + ");");
				streamWriter.WriteLine("\t\t\t\t}");
				streamWriter.WriteLine();
				streamWriter.WriteLine("\t\t\t\treturn " + dtoVariableName + "List;");
				streamWriter.WriteLine("\t\t\t}");

				// Append the method footer
				streamWriter.WriteLine("\t\t}");
				streamWriter.WriteLine();
			}
		}

		/// <summary>
		/// Creates a string that represents the select JSON functionality of the data access class.
		/// </summary>
		/// <param name="table">The Table instance that this method will be created for.</param>
		/// <param name="storedProcedurePrefix">The prefix that is used on the stored procedure that this method will call.</param>
        /// <param name="dtoSuffix">The suffix to append to the name of each data transfer class.</param>
		/// <param name="streamWriter">The StreamWriter instance that will be used to create the method.</param>
		private static void CreateSelectAllJsonMethod(Table table, string storedProcedurePrefix, string dtoSuffix, StreamWriter streamWriter)
		{
			if (table.Columns.Count != table.PrimaryKeys.Count && table.Columns.Count != table.ForeignKeys.Count)
			{
				string className = Utility.FormatClassName(table.Name) + dtoSuffix;
				string dtoVariableName = Utility.FormatCamel(className);

				// Append the method header
				streamWriter.WriteLine("\t\t/// <summary>");
				streamWriter.WriteLine("\t\t/// Selects all records from the " + table.Name + " table.");
				streamWriter.WriteLine("\t\t/// </summary>");
				streamWriter.WriteLine("\t\tpublic string SelectAllJson()");
				streamWriter.WriteLine("\t\t{");

				// Append the stored procedure execution
				streamWriter.WriteLine("\t\t\treturn SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, \"" + table.Name + "SelectAll\");");

				// Append the method footer
				streamWriter.WriteLine("\t\t}");
				streamWriter.WriteLine();
			}
		}

		/// <summary>
		/// Creates a string that represents the "select by" functionality of the data access class.
		/// </summary>
		/// <param name="table">The Table instance that this method will be created for.</param>
		/// <param name="storedProcedurePrefix">The prefix that is used on the stored procedure that this method will call.</param>
        /// <param name="dtoSuffix">The suffix to append to the name of each data transfer class.</param>
		/// <param name="streamWriter">The StreamWriter instance that will be used to create the method.</param>
		private static void CreateSelectAllByMethods(Table table, string storedProcedurePrefix, string dtoSuffix, StreamWriter streamWriter)
		{
			string className = Utility.FormatClassName(table.Name) + dtoSuffix;
			string dtoVariableName = Utility.FormatCamel(className);

			// Create a stored procedure for each foreign key
			foreach (List<Column> compositeKeyList in table.ForeignKeys.Values)
			{
				// Create the stored procedure name
				StringBuilder stringBuilder = new StringBuilder(255);
				stringBuilder.Append("SelectAllBy");
				for (int i = 0; i < compositeKeyList.Count; i++)
				{
					Column column = compositeKeyList[i];

					if (i > 0)
					{
						stringBuilder.Append("_" + Utility.FormatPascal(column.Name));
					}
					else
					{
						stringBuilder.Append(Utility.FormatPascal(column.Name));
					}
				}
				string methodName = stringBuilder.ToString();
				string procedureName = storedProcedurePrefix + table.Name + methodName;

				// Create the select function based on keys
				// Append the method header
				streamWriter.WriteLine("\t\t/// <summary>");
				streamWriter.WriteLine("\t\t/// Selects all records from the " + table.Name + " table by a foreign key.");
				streamWriter.WriteLine("\t\t/// </summary>");

				streamWriter.Write("\t\tpublic List<" + className + "> " + methodName + "(");
				for (int i = 0; i < compositeKeyList.Count; i++)
				{
					Column column = compositeKeyList[i];
					streamWriter.Write(Utility.CreateMethodParameter(column));
					if (i < (compositeKeyList.Count - 1))
					{
						streamWriter.Write(", ");
					}
				}
				streamWriter.WriteLine(")");
				streamWriter.WriteLine("\t\t{");

				// Append the parameter declarations
				streamWriter.WriteLine("\t\t\tSqlParameter[] parameters = new SqlParameter[]");
				streamWriter.WriteLine("\t\t\t{");
				for (int i = 0; i < compositeKeyList.Count; i++)
				{
					Column column = compositeKeyList[i];
					streamWriter.Write("\t\t\t\tnew SqlParameter(\"@" + column.Name + "\", " + Utility.FormatCamel(column.Name) + ")");
					if (i < (compositeKeyList.Count - 1))
					{
						streamWriter.Write(",");
					}

					streamWriter.WriteLine();
				}

				streamWriter.WriteLine("\t\t\t};");
				streamWriter.WriteLine();
				
				// Append the stored procedure execution
				streamWriter.WriteLine("\t\t\tusing (SqlDataReader dataReader = SqlClientUtility.ExecuteReader(connectionStringName, CommandType.StoredProcedure, \"" + procedureName + "\", parameters))");
				streamWriter.WriteLine("\t\t\t{");
				streamWriter.WriteLine("\t\t\t\tList<" + className + "> " + dtoVariableName + "List = new List<" + className + ">();");
				streamWriter.WriteLine("\t\t\t\twhile (dataReader.Read())");
				streamWriter.WriteLine("\t\t\t\t{");
				streamWriter.WriteLine("\t\t\t\t\t" + className + " " + dtoVariableName + " = MapDataReader(dataReader);");
				streamWriter.WriteLine("\t\t\t\t\t" + dtoVariableName + "List.Add(" + dtoVariableName + ");");
				streamWriter.WriteLine("\t\t\t\t}");
				streamWriter.WriteLine();
				streamWriter.WriteLine("\t\t\t\treturn " + dtoVariableName + "List;");
				streamWriter.WriteLine("\t\t\t}");

				// Append the method footer
				streamWriter.WriteLine("\t\t}");
				streamWriter.WriteLine();
			}
		}

		/// <summary>
		/// Creates a string that represents the "select by" JSON functionality of the data access class.
		/// </summary>
		/// <param name="table">The Table instance that this method will be created for.</param>
		/// <param name="storedProcedurePrefix">The prefix that is used on the stored procedure that this method will call.</param>
        /// <param name="dtoSuffix">The suffix to append to the name of each data transfer class.</param>
		/// <param name="streamWriter">The StreamWriter instance that will be used to create the method.</param>
		private static void CreateSelectAllByJsonMethods(Table table, string storedProcedurePrefix, string dtoSuffix, StreamWriter streamWriter)
		{
			string className = Utility.FormatClassName(table.Name) + dtoSuffix;
			string dtoVariableName = Utility.FormatCamel(className);

			// Create a stored procedure for each foreign key
			foreach (List<Column> compositeKeyList in table.ForeignKeys.Values)
			{
				// Create the stored procedure name
				StringBuilder stringBuilder = new StringBuilder(255);
				stringBuilder.Append("SelectAllBy");
				for (int i = 0; i < compositeKeyList.Count; i++)
				{
					Column column = compositeKeyList[i];

					if (i > 0)
					{
						stringBuilder.Append("_" + Utility.FormatPascal(column.Name));
					}
					else
					{
						stringBuilder.Append(Utility.FormatPascal(column.Name));
					}
				}

				string methodName = stringBuilder.ToString();
				string procedureName = storedProcedurePrefix + table.Name + methodName;

				// Create the select function based on keys
				// Append the method header
				streamWriter.WriteLine("\t\t/// <summary>");
				streamWriter.WriteLine("\t\t/// Selects all records from the " + table.Name + " table by a foreign key.");
				streamWriter.WriteLine("\t\t/// </summary>");

				streamWriter.Write("\t\tpublic string " + methodName + "Json(");
				for (int i = 0; i < compositeKeyList.Count; i++)
				{
					Column column = compositeKeyList[i];
					streamWriter.Write(Utility.CreateMethodParameter(column));
					if (i < (compositeKeyList.Count - 1))
					{
						streamWriter.Write(", ");
					}
				}
				streamWriter.WriteLine(")");
				streamWriter.WriteLine("\t\t{");

				// Append the parameter declarations
				streamWriter.WriteLine("\t\t\tSqlParameter[] parameters = new SqlParameter[]");
				streamWriter.WriteLine("\t\t\t{");
				for (int i = 0; i < compositeKeyList.Count; i++)
				{
					Column column = compositeKeyList[i];
					streamWriter.Write("\t\t\t\tnew SqlParameter(\"@" + column.Name + "\", " + Utility.FormatCamel(column.Name) + ")");
					if (i < (compositeKeyList.Count - 1))
					{
						streamWriter.Write(",");
					}

					streamWriter.WriteLine();
				}

				streamWriter.WriteLine("\t\t\t};");
				streamWriter.WriteLine();
				
				// Append the stored procedure execution
				streamWriter.WriteLine("\t\t\treturn SqlClientUtility.ExecuteJson(connectionStringName, CommandType.StoredProcedure, \"" + procedureName + "\", parameters);");

				// Append the method footer
				streamWriter.WriteLine("\t\t}");
				streamWriter.WriteLine();
			}
		}
		
		/// <summary>
		/// Creates a string that represents the "map" functionality of the data access class.
		/// </summary>
		/// <param name="table">The Table instance that this method will be created for.</param>
        /// <param name="dtoSuffix">The suffix to append to the name of the data transfer class.</param>
		/// <param name="streamWriter">The StreamWriter instance that will be used to create the method.</param>
		private static void CreateMapMethod(Table table, string dtoSuffix, StreamWriter streamWriter)
		{
			string className = Utility.FormatClassName(table.Name) + dtoSuffix;
			string variableName = Utility.FormatVariableName(className);

			streamWriter.WriteLine("\t\t/// <summary>");
			streamWriter.WriteLine("\t\t/// Creates a new instance of the " + className + " class and populates it with data from the specified SqlDataReader.");
			streamWriter.WriteLine("\t\t/// </summary>");
			streamWriter.WriteLine("\t\tprivate " + className + " MapDataReader(SqlDataReader dataReader)");
			streamWriter.WriteLine("\t\t{");
			streamWriter.WriteLine("\t\t\t" + className + " " + variableName + " = new " + className + "();");
			
			foreach (Column column in table.Columns)
			{
				string columnNamePascal = Utility.FormatPascal(column.Name);
				streamWriter.WriteLine("\t\t\t" + variableName + "." + columnNamePascal + " = dataReader." + Utility.GetGetMethod(column) + "(\"" + column.Name + "\", " + Utility.GetDefaultValue(column) + ");");
			}
			
			streamWriter.WriteLine();
			streamWriter.WriteLine("\t\t\treturn " + variableName + ";");
			streamWriter.WriteLine("\t\t}");
		}

	}
}
