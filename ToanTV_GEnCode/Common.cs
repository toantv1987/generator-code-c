﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
namespace App_ChuongTrinhSinhCodeTuDong
{
   public class Common
    {
       public static void LoadFileIni()
        {
            string Section = "ConnectSQL";
            string ServerName = "Server";
            string DatabaseName = "Database";
            string FilePath = "Config.ini";
            string _uName = "userName";
            string _password = "passWord";
            string projectName = "projectName";
            string nameSpace = "nameSpace";
            cConfig.SvName = IniFileHelper.ReadValue(Section, ServerName, System.IO.Path.GetFullPath(FilePath));
            cConfig.DbName = IniFileHelper.ReadValue(Section, DatabaseName, System.IO.Path.GetFullPath(FilePath));
            cConfig.userName = IniFileHelper.ReadValue(Section, _uName, System.IO.Path.GetFullPath(FilePath));
            cConfig.passWord = IniFileHelper.ReadValue(Section, _password, System.IO.Path.GetFullPath(FilePath));
            cConfig.projectName = IniFileHelper.ReadValue(Section, projectName, System.IO.Path.GetFullPath(FilePath));
            cConfig.nameSpace = IniFileHelper.ReadValue(Section, nameSpace, System.IO.Path.GetFullPath(FilePath));
           
        }

        #region Copy file and folder
        //Copy file trong 1 duong dan
        public static void Copy(string sourceDirectory, string targetDirectory)
        {
            DirectoryInfo diSource = new DirectoryInfo(sourceDirectory);
            DirectoryInfo diTarget = new DirectoryInfo(targetDirectory);

            CopyAll(diSource, diTarget);
        }    
        //Using method Recursively(phương pháp đệ quy)
        public static void CopyAll(DirectoryInfo source, DirectoryInfo target)
        {
            //Tạo folder cho thư mục đích
            Directory.CreateDirectory(target.FullName);

            // Copy each file into the new directory.
            foreach (FileInfo file in source.GetFiles())
            {
                string str=  "Copying " + target.FullName + " to "+ file.Name;
                //  MessageBox.Show(str);
                file.CopyTo(Path.Combine(target.FullName, file.Name), true);
            }
            
            // Copy each subdirectory using recursion.
            foreach (DirectoryInfo diSourceSubDir in source.GetDirectories())
            {
                DirectoryInfo nextTargetSubDir = target.CreateSubdirectory(diSourceSubDir.Name);
                CopyAll(diSourceSubDir, nextTargetSubDir);
            }
        }
        public static void CopyAll(string pathSource, string pathDestination)
        {
            DirectoryInfo source = new DirectoryInfo(pathSource);
            DirectoryInfo target = new DirectoryInfo(pathDestination);
            //Tạo folder cho thư mục đích
            Directory.CreateDirectory(target.FullName);

            // Copy each file into the new directory.
            foreach (FileInfo file in source.GetFiles())
            {
                string str = "Copying " + target.FullName + " to " + file.Name;
                //  MessageBox.Show(str);
                file.CopyTo(Path.Combine(target.FullName, file.Name), true);
            }

            // Copy each subdirectory using recursion.
            foreach (DirectoryInfo diSourceSubDir in source.GetDirectories())
            {
                DirectoryInfo nextTargetSubDir = target.CreateSubdirectory(diSourceSubDir.Name);
                CopyAll(diSourceSubDir, nextTargetSubDir);
            }
        }


        #endregion
    }
}
