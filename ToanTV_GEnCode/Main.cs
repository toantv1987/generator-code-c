﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
namespace App_ChuongTrinhSinhCodeTuDong
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
        }

        
        //Sinh mã dùng cho thủ tục
        private void btnSP_Click(object sender, EventArgs e)
        {
            FormUserSP f = new FormUserSP();
            f.Show();
        }
        //Sinh mã dùng cho windows form
        private void btnCodeInForm_Click(object sender, EventArgs e)
        {
            CodeInForm.FormCodeInForm f = new CodeInForm.FormCodeInForm();
            f.Show();
        }
        //Sinh mã dùng cho windows form
        private void btnCodeInform2_Click(object sender, EventArgs e)
        {
            CodeInform2.Form1 f = new CodeInform2.Form1();
            f.Show();
        }
        private void btnGenCodeWebformLinQ_Click(object sender, EventArgs e)
        {
            CodeWebFormLinQ.FormCodeWebformLinQ f = new CodeWebFormLinQ.FormCodeWebformLinQ();
            f.Show();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                string FilePath = "Config.ini";
                string Section = "ConnectSQL";
                string ServerName = "Server";
                string DatabaseName = "Database";
                string _uName = "userName";
                string _password = "passWord";
                string projectName = "projectName";
                string nameSpace = "nameSpace";
                IniFileHelper.WriteValue(Section, ServerName, serverTextBox.Text, System.IO.Path.GetFullPath(FilePath));
                IniFileHelper.WriteValue(Section, DatabaseName, databaseTextBox.Text, System.IO.Path.GetFullPath(FilePath));
                IniFileHelper.WriteValue(Section, _uName, loginNameTextBox.Text, System.IO.Path.GetFullPath(FilePath));
                IniFileHelper.WriteValue(Section, _password, passwordTextBox.Text, System.IO.Path.GetFullPath(FilePath));
                IniFileHelper.WriteValue(Section, projectName, projectTextBox.Text, System.IO.Path.GetFullPath(FilePath));
                IniFileHelper.WriteValue(Section, nameSpace, namespaceTextBox.Text, System.IO.Path.GetFullPath(FilePath));
                MessageBox.Show("Cập nhật thành công");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Something went wrong");
                //throw;
            }
            
        }

        private void Main_Load(object sender, EventArgs e)
        {
            Common.LoadFileIni();
            serverTextBox.Text = cConfig.SvName;
            databaseTextBox.Text = cConfig.DbName;
            loginNameTextBox.Text = cConfig.userName;
            passwordTextBox.Text = cConfig.passWord;
            projectTextBox.Text = cConfig.projectName;
            namespaceTextBox.Text = cConfig.nameSpace;

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string sourcePath = @"C:\Users\eng63\Desktop\Nguon";
            string targetPath = @"C:\Users\eng63\Desktop\Dich";
            DirectoryInfo s = new DirectoryInfo(sourcePath);
            DirectoryInfo t = new DirectoryInfo(targetPath);
           Common.CopyAll(s, t);

        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AboutBox1 about = new AboutBox1();
            about.ShowDialog();
        }




        //static void LoadFileIni()
        //{
        //    string Section = "ConnectSQL";
        //    string ServerName = "Server";
        //    string DatabaseName = "Database";
        //    string FilePath = "Config.ini";
        //    string _uName = "userName";
        //    string _password = "passWord";
        //    string projectName = "projectName";
        //    string nameSpace = "nameSpace";
        //    cConfig.SvName = IniFileHelper.ReadValue(Section, ServerName, System.IO.Path.GetFullPath(FilePath));
        //    cConfig.DbName = IniFileHelper.ReadValue(Section, DatabaseName, System.IO.Path.GetFullPath(FilePath));
        //    cConfig.userName = IniFileHelper.ReadValue(Section, _uName, System.IO.Path.GetFullPath(FilePath));
        //    cConfig.passWord = IniFileHelper.ReadValue(Section, _password, System.IO.Path.GetFullPath(FilePath));
        //    cConfig.projectName = IniFileHelper.ReadValue(Section, projectName, System.IO.Path.GetFullPath(FilePath));
        //    cConfig.nameSpace = IniFileHelper.ReadValue(Section, nameSpace, System.IO.Path.GetFullPath(FilePath));
        //}
    }
}
