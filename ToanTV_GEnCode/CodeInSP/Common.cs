﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace App_ChuongTrinhSinhCodeTuDong.CodeInSP
{
    public static class Common
    {
        public static string SeparatorText(Column column)
        {
            string Result = "";
            switch (column.Type)
            {
                case "int":
                    Result= "\n\t\t\t obj." + column.Name + "  = int.Parse(txt" + column.Name + ".Text); ";
                    break;
                case "smallint":
                    Result= "\n\t\t\t obj." + column.Name + "  = short.Parse(txt" + column.Name + ".Text); ";
                    break;
                case "bit":
                    Result="\n\t\t\t obj." + column.Name + "  = bool.Parse(txt" + column.Name + ".Text); ";
                    break;
                case "datetime":
                    Result= "\n\t\t\t obj." + column.Name + "  = Convert.ToDateTime(txt" + column.Name + ".Text); ";
                    break;
                case "date":
                    Result= "\n\t\t\t obj." + column.Name + "  = Convert.ToDateTime(txt" + column.Name + ".Text); ";
                    break;
                default:
                    Result = "\n\t\t\t obj." + column.Name + "  =  txt" + column.Name + ".Text; ";
                    break;
            }
            return Result;
        }

        public static string SeparatorText_ForGridview(Column column)
        {
            string Result = "";
            switch (column.Type)
            {
                case "int":
                    
                    Result = "\n\t\t\t obj." + column.Name + "  = int.Parse(s[1].ToString()); ";
                    break;
                case "smallint":
                    Result = "\n\t\t\t obj." + column.Name + "  = short.Parse(s[1].ToString()); ";
                    break;
                case "bit":
                    Result = "\n\t\t\t obj." + column.Name + "  = bool.Parse(s[1].ToString()); ";
                    break;
                case "datetime":
                    Result = "\n\t\t\t obj." + column.Name + "  = Convert.ToDateTime(s[1].ToString()); ";
                    break;
                case "date":
                    Result = "\n\t\t\t obj." + column.Name + "  = Convert.ToDateTime(s[1].ToString()); ";
                    break;
                default:
                    Result = "\n\t\t\t obj." + column.Name + "  =  s[1].ToString(); ";
                    break;
            }
            return Result;
        }
      
 
        /// <summary>
        /// Gán dữ liệu từ grid cho text
        /// </summary>
        /// <param name="column"></param>
        /// <param name="gridview"></param>
        /// <returns></returns>
        public static string GridviewTo_Object(Column column, string gridview)
        {
            string Result = "";
            switch (column.Type)
            {
                case "int":

                    Result = "\n\t\t\t obj." + column.Name + " =  int.Parse(((TextBox)" + gridview + ".Rows[e.RowIndex].FindControl(\"txt" + column.Name + "\")).Text);";
                    break;
                case "smallint":

                    Result = "\n\t\t\t obj." + column.Name + " =  short.Parse(((TextBox)" + gridview + ".Rows[e.RowIndex].FindControl(\"txt" + column.Name + "\")).Text);";
                    break;
                case "bit":

                    Result = "\n\t\t\t obj." + column.Name + " =  bool.Parse(((TextBox)" + gridview + ".Rows[e.RowIndex].FindControl(\"txt" + column.Name + "\")).Text);";
                    break;
                case "datetime":
                    Result = "\n\t\t\t obj." + column.Name + " =  Convert.ToDateTime(((TextBox)" + gridview + ".Rows[e.RowIndex].FindControl(\"txt" + column.Name + "\")).Text);";

                    break;
                case "date":
                    Result = "\n\t\t\t obj." + column.Name + " =  Convert.ToDateTime(((TextBox)" + gridview + ".Rows[e.RowIndex].FindControl(\"txt" + column.Name + "\")).Text);";

                    break;
                default:

                    Result = "\n\t\t\t obj." + column.Name + " =  ((TextBox)" + gridview + ".Rows[e.RowIndex].FindControl(\"txt" + column.Name + "\")).Text;";
                    break;
            }
            return Result;
        }

        /// <summary>
        /// Gán dữ liệu từ grid cho text truong hop Add
        /// </summary>
        /// <param name="column"></param>
        /// <param name="gridviewName"></param>
        /// <returns></returns>
        public static string GridviewTo_Object_Add(Column column, string gridviewName)
        {
            string Result = "";
            switch (column.Type)
            {
                case "int":
                    Result = "\n\t\t obj." + column.Name + " = int.Parse(((TextBox)" + gridviewName + ".FooterRow.FindControl(\"txt" + column.Name + "\")).Text);";
                    break;
                case "smallint":
                    Result = "\n\t\t obj." + column.Name + " = short.Parse(((TextBox)" + gridviewName + ".FooterRow.FindControl(\"txt" + column.Name + "\")).Text);";
                   
                    break;
                case "bit":
                    Result = "\n\t\t obj." + column.Name + " = bool.Parse(((TextBox)" + gridviewName + ".FooterRow.FindControl(\"txt" + column.Name + "\")).Text);";
                  
                    break;
                case "datetime":
                     
                    Result = "\n\t\t obj." + column.Name + " = Convert.ToDateTime(((TextBox)" + gridviewName + ".FooterRow.FindControl(\"txt" + column.Name + "\")).Text);";
                    break;
                case "date":
                    Result = "\n\t\t obj." + column.Name + " = Convert.ToDateTime(((TextBox)" + gridviewName + ".FooterRow.FindControl(\"txt" + column.Name + "\")).Text);";
                    break;
                default:
                    Result = "\n\t\t obj." + column.Name + " =  ((TextBox)" + gridviewName + ".FooterRow.FindControl(\"txt" + column.Name + "\")).Text;";
                    
                    break;
            }
            return Result;
        }
        /// <summary>
        /// Delete trong gridview
        /// </summary>
        /// <param name="column"></param>
        /// <param name="gridview"></param>
        /// <returns></returns>
        public static string ConvertFor_DeleteInGridview(Column column, string gridview)
        {
            string Result = "";
            switch (column.Type)
            {
                case "int":
                    Result = "\n\t\t  obj." + column.Name + " = int.Parse(" + gridview + ".DataKeys[e.RowIndex].Values[0].ToString());";
                     
                    break;
                case "smallint":
                    
                    Result = "\n\t\t  obj." + column.Name + " = short.Parse(" + gridview + ".DataKeys[e.RowIndex].Values[0].ToString());";
                    break;
                case "bit":
                   
                    Result = "\n\t\t  obj." + column.Name + " = bool.Parse(" + gridview + ".DataKeys[e.RowIndex].Values[0].ToString());";
                    break;
                case "datetime":
                    Result = "\n\t\t  obj." + column.Name + " = Convert.ToDateTime(" + gridview + ".DataKeys[e.RowIndex].Values[0].ToString());";
                  
                    break;
                case "date":
                    Result = "\n\t\t  obj." + column.Name + " = Convert.ToDateTime(" + gridview + ".DataKeys[e.RowIndex].Values[0].ToString());";
                    break;
                default:
                    
                    Result = "\n\t\t  obj." + column.Name + " = " + gridview + ".DataKeys[e.RowIndex].Values[0].ToString();";
                    break;
            }
            return Result;
        }
    }
}
