﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace App_ChuongTrinhSinhCodeTuDong.CodeInSP
{
    internal static class CsGeneratorGraphic_ForWindowsForm
    {
        #region Trang Hiển thị
        ///Tao gridview Sua xoa o 1 trang khac
        public static void WindowsForm_UsingOtherForm_Designer(string databaseName, Table table, string targetNamespace, string storedProcedurePrefix, string daoSuffix, string dtoSuffix, string path)
        {
 
            string tableName = Utility.FormatClassName(table.Name);
            path = Path.Combine(path, "Presentation_WindowsForm");
            string FileName = "Form" + tableName + ".Designer.cs";
            string FormName = "Form" + tableName;
            //Path.Combine(csPath, "DataAccess")
            using (StreamWriter streamWriter = new StreamWriter(Path.Combine(path, FileName)))
            {
                

                var sb = new StringBuilder();
                sb.Append("namespace "+ targetNamespace);
                sb.Append("\n {");
                sb.Append("\n partial class "+ FormName);
                sb.Append("\n {");
                sb.Append("\n ");
               
                sb.Append("\n /// <summary>");
                sb.Append("\n  /// Required designer variable.");
                sb.Append("\n  /// </summary>");
                sb.Append("\n   private System.ComponentModel.IContainer components = null;");
                sb.Append("\n  protected override void Dispose(bool disposing)");
                sb.Append("\n  {");
                sb.Append("\n  if (disposing && (components != null))");
                sb.Append("\n  {");
                sb.Append("\n  components.Dispose();");
                sb.Append("\n  }");
                sb.Append("\n  base.Dispose(disposing);");
                sb.Append("\n  }");
                sb.Append("\n  #region Windows Form Designer generated code by Toàn Đẹp Trai");
               
                sb.Append("\n  private void InitializeComponent()");
                sb.Append("\n  {");
                sb.Append("\n  this.dataGridView1 = new System.Windows.Forms.DataGridView();");
                sb.Append("\n  this.btnAddNew = new System.Windows.Forms.Button();");
                sb.Append("\n  this.btnEdit = new System.Windows.Forms.Button();");
                sb.Append("\n  this.btnDelete = new System.Windows.Forms.Button();");
 
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];
                    sb.Append("\n\t\tthis."+column.Name+ " =  new System.Windows.Forms.DataGridViewTextBoxColumn();");
                }
                sb.Append("\n  ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();");
                sb.Append("\n  this.SuspendLayout();");
                sb.Append("\n");
                sb.Append("\n  //Datagriview1");
                sb.Append("\n  this.dataGridView1.AllowUserToAddRows = false;");
                sb.Append("\n  this.dataGridView1.AllowUserToDeleteRows = false;");
                sb.Append("\n  this.dataGridView1.ReadOnly = true;");
                sb.Append("\n  this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;");
                sb.Append("\n  this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;");
                sb.Append("\n  this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {");
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];
                    sb.Append("\n  this." +column.Name + ",");
                }
                
                sb.Append("\n  });");
                sb.Append("\n  this.dataGridView1.Location = new System.Drawing.Point(12, 57);");
                sb.Append("\n  this.dataGridView1.Name = "+'"'+"dataGridView1"+'"'+";");
                sb.Append("\n  this.dataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;"); 
                sb.Append("\n  this.dataGridView1.Size = new System.Drawing.Size(554, 343);");
                sb.Append("\n  this.dataGridView1.TabIndex = 0;");               
                sb.Append("\n ");
                sb.Append("\n  // btnAddNew");
                sb.Append("\n  this.btnAddNew.Location = new System.Drawing.Point(324, 21);");
                sb.Append("\n  this.btnAddNew.Name ="+'"'+ "btnAddNew"+'"' + ";"); // this.btnAddNew.Name = "btnAddNew";
                sb.Append("\n  this.btnAddNew.Size = new System.Drawing.Size(80, 30);");
                sb.Append("\n  this.btnAddNew.TabIndex = 1;");
                sb.Append("\n  this.btnAddNew.Text =" + '"' + "Add New" + '"' + ";");
                sb.Append("\n  this.btnAddNew.UseVisualStyleBackColor = true;");
                sb.Append("\n  this.btnAddNew.Click += new System.EventHandler(this.btnAddNew_Click);");
                sb.Append("\n ");
                sb.Append("\n //btnEdit");
                sb.Append("\n  this.btnEdit.Location = new System.Drawing.Point(405, 21);");
                sb.Append("\n  this.btnEdit.Name  =" + '"' + "btnEdit" + '"' + ";");
                sb.Append("\n  this.btnEdit.Size = new System.Drawing.Size(80, 30);");
                sb.Append("\n  this.btnEdit.TabIndex = 2;");
                sb.Append("\n  this.btnEdit.Text  =" + '"' + "Edit" + '"' + ";");
                sb.Append("\n  this.btnEdit.UseVisualStyleBackColor = true;");
                sb.Append("\n  this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);");
                sb.Append("\n ");
                sb.Append("\n  // btnDelete");
                sb.Append("\n  this.btnDelete.Location = new System.Drawing.Point(486, 21);");
                sb.Append("\n  this.btnDelete.Name  =" + '"' + "btnDelete" + '"' + ";");
                sb.Append("\n  this.btnDelete.Size = new System.Drawing.Size(80, 30);");
                sb.Append("\n  this.btnDelete.TabIndex = 2;");
                sb.Append("\n  this.btnDelete.Text  =" + '"' + "Delete" + '"'+";");
                sb.Append("\n  this.btnDelete.UseVisualStyleBackColor = true;");
                sb.Append("\n  this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);");
                sb.Append("\n ");

                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];

                    // Is the current column an identity column?);
                    if (column.IsIdentity == true)
                    {
                        sb.Append("\n//");
                        sb.Append("\n//" + column.Name);
                        sb.Append("\n//");
                        sb.Append("\n\t\t this."+column.Name+".DataPropertyName = " + '"' + column.Name + '"' + ";");
                        sb.Append("\n\t\t this."+ column.Name +".HeaderText = "+'"' + column.Name +'"'+";");
                        sb.Append("\n\t\t this."+column.Name+".Name = " + '"' + column.Name + '"' + ";");
                        sb.Append("\n\t\t this."+ column.Name +".Visible = false;");
                    }
                    else
                    {
                        sb.Append("\n//");
                        sb.Append("\n//" + column.Name);
                        sb.Append("\n//");
                        sb.Append("\n\t\t this." + column.Name + ".DataPropertyName = " + '"' + column.Name + '"' + ";");
                        sb.Append("\n\t\t this." + column.Name + ".HeaderText = " + '"' + column.Name + '"' + ";");
                        sb.Append("\n\t\t this." + column.Name + ".Name = " + '"' + column.Name + '"' + ";");
                        sb.Append("\n\t\t this." + column.Name + ".Visible = true;");
                    }

                }
                sb.Append("\n  // Form"+ tableName);
                sb.Append("\n ");
                sb.Append("\n    this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);");
                sb.Append("\n  this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;");
                sb.Append("\n  this.ClientSize = new System.Drawing.Size(598, 438);");
                sb.Append("\n   this.Controls.Add(this.btnDelete);");
                sb.Append("\n  this.Controls.Add(this.btnEdit);");
                sb.Append("\n  this.Controls.Add(this.btnAddNew);");
                sb.Append("\n  this.Controls.Add(this.dataGridView1);");
                sb.Append("\n  this.Name ="+'"'+ FormName + '"'+";");  
                sb.Append("\n  this.Text  =" + '"' + tableName + '"' + ";");
                sb.Append("\n   this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;");
                sb.Append("\n  this.Load += new System.EventHandler(this.Form"+tableName+"_Load);");
                sb.Append("\n   ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();");
                sb.Append("\n  this.ResumeLayout(false);");
                sb.Append("\n  }");
                sb.Append("\n  #endregion");
                sb.Append("\n  private System.Windows.Forms.DataGridView dataGridView1;");
                sb.Append("\n  private System.Windows.Forms.Button btnAddNew;");
                sb.Append("\n  private System.Windows.Forms.Button btnEdit;");
                sb.Append("\n  private System.Windows.Forms.Button btnDelete;");
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];
                    sb.Append("\n\t\t private System.Windows.Forms.DataGridViewTextBoxColumn "+column.Name + ";");
                }
                sb.Append("\n }");
                sb.Append("\n }");
                sb.Append("\n ");
                sb.Append("\n ");
             
          
                streamWriter.WriteLine(sb);


            }
        }
       
        public static void WindowsForm_UsingOtherForm_Code(string databaseName, Table table, string targetNamespace, string storedProcedurePrefix, string daoSuffix, string dtoSuffix,string busSuffix, string path)
        {
            string tableName = Utility.FormatClassName(table.Name);
            string classNameInfo = tableName + dtoSuffix;          
            string classNameDAO = tableName + daoSuffix;
            string classNameBusiness = tableName + busSuffix;
            string FileName = "Form" + tableName + ".cs";
            string FormName = "Form" + tableName;
    
            path = Path.Combine(path, "Presentation_WindowsForm");

            string Id = "Id";
            for (int i = 0; i < table.Columns.Count; i++)
            {
                Column column = table.Columns[i];

                // Is the current column an identity column?);
                if (column.IsIdentity == true)
                {
                    Id = Utility.FormatPascal(column.Name);
                }

            }
            //Gridview to textbox
            using (StreamWriter streamWriter = new StreamWriter(Path.Combine(path, FileName)))
            {
                var sb = new StringBuilder();
                sb.Append("\nusing System;");
                sb.Append("\nusing System.Collections.Generic;");
                sb.Append("\nusing System.ComponentModel;");
                sb.Append("\nusing System.Drawing;");
                sb.Append("\nusing System.Linq;");
                sb.Append("\nusing System.Text;");
                sb.Append("\nusing System.Windows.Forms;");
                sb.Append("\nusing Business;");
                sb.Append("\nusing Entity;");
                sb.Append("\nusing System.Data;");
                sb.Append("\nnamespace " + targetNamespace);
                sb.Append("\n{");
                sb.Append("\n\tpublic partial class Form" + tableName + " : Form");
                sb.Append("\n\t{");

                sb.Append("\n\t\tpublic Form" + tableName + "()");
                sb.Append("\n\t\t{");
                sb.Append("\n\t\t\tInitializeComponent();");
                sb.Append("\n\t\t} ");
                sb.Append("\n\t\t" + classNameBusiness + " bus = new " + classNameBusiness + "();");
                sb.Append("\n\t\tint Id = 0; ");
                sb.Append("\n\t\tprivate void Form"+tableName+"_Load(object sender, EventArgs e) ");
                sb.Append("\n\t\t{ ");
                sb.Append("\n\t\t\t\tloadData();");
                sb.Append("\n\t\t}");
 
                //Load dữ liệu cho Gridview 
                sb.Append("\n\t //====================Load dữ liệu cho Datagridview=========================");
                sb.Append("\n\t\tpublic void loadData() \n");
                sb.Append("\n\t\t{ ");
                sb.Append("\n\t\t\ttry");
                sb.Append("\n\t\t\t{");
                sb.Append("\n\t\t\t\tdataGridView1.DataSource = bus.GetAll();");
                sb.Append("\n\t\t\t\tdataGridView1.Columns[0].Visible = false;");
                sb.Append("\n\t\t\t}");
                sb.Append("\n\t\t\tcatch (Exception ex)");
                sb.Append("\n\t\t\t{");
                sb.Append("\n\t\t\t\tMessageBox.Show(ex.ToString());");
                sb.Append("\n\t\t\t}");

                sb.Append("\n\n\t\t}");
                sb.Append("\n\t\t//Them");
                sb.Append("\n");
                sb.Append("\n\t\tprivate void btnAddNew_Click(object sender, EventArgs e)");
                sb.Append("\n\t\t{");
                sb.Append("\n\t\t\tForm" + tableName + "Update f = new Form" + tableName + "Update();");
                sb.Append("\n\t\t\tf.ShowDialog();");
                sb.Append("\n\t\t\tloadData();");
                sb.Append("\n\t\t}");
                sb.Append("\n\t");

                sb.Append("\n\t\t//Sua");
                sb.Append("\n\t\tprivate void btnEdit_Click(object sender, EventArgs e)");
                sb.Append("\n\t\t{");
                sb.Append("\n\t\t\ttry");
                sb.Append("\n\t\t\t{");
                sb.Append("\n\t\t\t\tint Id = (int)dataGridView1.SelectedRows[0].Cells[" + '"' + Id + '"' + "].Value;");
                sb.Append("\n\t\t\t\tif (Id > 0)");
                sb.Append("\n\t\t\t\t{");
                sb.Append("\n\t\t\t\t\tForm" + tableName + "Update f = new Form" + tableName + "Update(Id);");
                sb.Append("\n\t\t\t\t\tf.ShowDialog(); ");
                sb.Append("\n\t\t\t\t\tloadData(); ");
                sb.Append("\n\t\t\t\t} ");
                sb.Append("\n\t\t\t\telse");
                sb.Append("\n\t\t\t\t{");
                sb.Append("\n\t\t\t\t\tstring str = " + '"' + "You must choose a row on table data" + '"' + "; ");
                sb.Append("\n\t\t\t\t\tMessageBox.Show(str); ");
                sb.Append("\n\t\t\t\t} ");
                sb.Append("\n\t\t\t}");
                sb.Append("\n\t\t\tcatch (Exception) ");
                sb.Append("\n\t\t\t{ ");
                sb.Append("\n\t\t\t\tstring str = " + '"' + "You must choose a row on table data" + '"' + "; ");
                sb.Append("\n\t\t\t\tMessageBox.Show(str);");
                sb.Append("\n\t\t\t}");
                sb.Append("\n\t\t}");
                sb.Append("\n\t  ");

                sb.Append("\n\t\t//Xoa ");
                sb.Append("\n\t\tprivate void btnDelete_Click(object sender, EventArgs e) ");
                sb.Append("\n\t\t{");
                sb.Append("\n\t\t\ttry");
                sb.Append("\n\t\t\t{");
                sb.Append("\n\t\t\t\tint Id = (int)dataGridView1.SelectedRows[0].Cells[" + '"' + Id + '"' + "].Value; ");
                sb.Append("\n\t\t\t\tif (Id > 0)");
                sb.Append("\n\t\t\t\t{");
                sb.Append("\n\t\t\t\t\tif (MessageBox.Show(" + '"' + "Are you sure delete this record ? " + '"' + "," + '"' + "Alert" + '"' + ", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == System.Windows.Forms.DialogResult.Yes)");
                sb.Append("\n\t\t\t\t\t{");
                sb.Append("\n\t\t\t\t\t\t" + classNameInfo + " obj = new " + classNameInfo + "(); ");
                sb.Append("\n\t\t\t\t\t\tobj." + Utility.FormatPascal(Id) + " = Id;");
                sb.Append("\n\t\t\t\t\t\tbus.Delete(obj);");
                sb.Append("\n\t\t\t\t\t\tloadData();");
                sb.Append("\n\t\t\t\t\t\tdataGridView1.Rows[0].Selected = false;");
                sb.Append("\n\t\t\t\t\t} ");
                sb.Append("\n\t\t\t\t}");
                sb.Append("\n\t\t\t\telse");
                sb.Append("\n\t\t\t\t{");
                sb.Append("\n\t\t\t\t\tMessageBox.Show(" + '"' + "You must choose a row on table data !" + '"' + ", " + '"' + "Alert" + '"' + "); ");
                sb.Append("\n\t\t\t\t}");
                sb.Append("\n\t\t\t}");
                sb.Append("\n\t\t\tcatch (Exception) ");
                sb.Append("\n\t\t\t{");
                sb.Append("\n\t\t\t\tMessageBox.Show(" + '"' + "You must choose a row on table data !" + '"' + ", " + '"' + "Alert" + '"' + "); ");
                sb.Append("\n\t\t\t}");
                sb.Append("\n\t\t}");
                sb.Append("\n\t}");
                sb.Append("\n }");

                streamWriter.WriteLine(sb);
            }
        }
        /// <summary>
        /// Resx
        /// </summary>
        public static void WindowsForm_UsingOtherForm_Resx(string databaseName, Table table, string targetNamespace, string storedProcedurePrefix, string daoSuffix, string dtoSuffix, string path)
        {
            string tableName = Utility.FormatClassName(table.Name);
            string classNameInfo = tableName + dtoSuffix;
            string classNameDAO = tableName + daoSuffix;
            string classNameBusiness = tableName + "Business";
            string FileName = "Form" + tableName + ".resx";
            string FormName = "Form" + tableName;

            path = Path.Combine(path, "Presentation_WindowsForm");
            string strFullPath = Path.Combine(path, FileName);          
            string FiLeNguon = System.Windows.Forms.Application.StartupPath + "\\FormMeasuring.resx";            
            File.Copy(FiLeNguon, strFullPath, true);


         
        }
        #endregion

        #region Tạo form Update cho bảng tương ứng
        public static void WindowsFormUpdate_UsingOtherForm_Designer(string databaseName, Table table, string targetNamespace, string storedProcedurePrefix, string daoSuffix, string dtoSuffix, string path)
        {

            string tableName = Utility.FormatClassName(table.Name);
            path = Path.Combine(path, "Presentation_WindowsForm");
            string FileName = "Form" + tableName + "Update.Designer.cs";
            string FormName = "Form" + tableName+ "Update";
            using (StreamWriter streamWriter = new StreamWriter(Path.Combine(path, FileName)))
            {
                string Id = "Id";
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];

                    // Is the current column an identity column?);
                    if (column.IsIdentity == true)
                    {
                        Id = column.Name;
                    }

                }

                var sb = new StringBuilder();
                sb.Append("namespace " + targetNamespace);
                sb.Append("\n{");
                sb.Append("\n\tpartial class " + FormName);
                sb.Append("\n\t{");
                sb.Append("\n\t\t\t");

                sb.Append("\n\t\t/// <summary>");
                sb.Append("\n\t\t /// Required designer variable.");
                sb.Append("\n\t\t /// </summary>");
                sb.Append("\n\t\tprivate System.ComponentModel.IContainer components = null;");
                sb.Append("\n\t\tprotected override void Dispose(bool disposing)");
                sb.Append("\n\t\t{");
                sb.Append("\n\t\t\tif (disposing && (components != null))");
                sb.Append("\n\t\t\t{");
                sb.Append("\n\t\t\t\tcomponents.Dispose();");
                sb.Append("\n\t\t\t}");
                sb.Append("\n\t\t\tbase.Dispose(disposing);");
                sb.Append("\n\t\t}"); 
                sb.Append("\n\t\tprivate void InitializeComponent()");
                sb.Append("\n\t\t{");
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i]; 
                    if (column.IsIdentity == false)
                    {                         
                        sb.Append("\n\t\t\tthis.label" + column.Name+ " = new System.Windows.Forms.Label();");                        
                        sb.Append("\n\t\t\tthis.txt" + column.Name + "= new System.Windows.Forms.TextBox();");
                    }
                }

                sb.Append("\n\t\t\tthis.btnClose = new System.Windows.Forms.Button();");
                sb.Append("\n\t\t\tthis.btnSave = new System.Windows.Forms.Button();");

                sb.Append("\n\t\t\tthis.SuspendLayout(); ");

                //Lable
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];
                    if (column.IsIdentity == false)
                    {
                        if (i<15)
                        {
                            int p = 50;
                            p = p + (i * 25);
                            sb.Append("\n\t\t\tthis.label" + column.Name + ".AutoSize = true;");
                            sb.Append("\n\t\t\tthis.label" + column.Name + ".Location = new System.Drawing.Point(11, " + p + ");");
                            sb.Append("\n\t\t\tthis.label" + column.Name + ".Name = " + '"' + "label" + column.Name + '"' + ";");
                            sb.Append("\n\t\t\tthis.label" + column.Name + ".Size = new System.Drawing.Size(79, 13);");
                            sb.Append("\n\t\t\tthis.label" + column.Name + ".TabIndex = 4;");
                            sb.Append("\n\t\t\tthis.label" + column.Name + ".Text = " + '"' + column.Name + '"' + ";");
                        }
                        else
                        {
                            int p = 50;
                            p = p + ((i-14) * 25);
                            sb.Append("\n\t\t\tthis.label" + column.Name + ".AutoSize = true;");
                            sb.Append("\n\t\t\tthis.label" + column.Name + ".Location = new System.Drawing.Point(390, " + p + ");");
                            sb.Append("\n\t\t\tthis.label" + column.Name + ".Name = " + '"' + "label" + column.Name + '"' + ";");
                            sb.Append("\n\t\t\tthis.label" + column.Name + ".Size = new System.Drawing.Size(79, 13);");
                            sb.Append("\n\t\t\tthis.label" + column.Name + ".TabIndex = 4;");
                            sb.Append("\n\t\t\tthis.label" + column.Name + ".Text = " + '"' + column.Name + '"' + ";");
                        }
                        
                    }
                }
                //Texbox
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];
                    if (column.IsIdentity == false)
                    {
                        if (i<15)
                        {
                            int p = 50;
                            p = p + (i * 25);
                            sb.Append("\n\t\t\t this.txt" + column.Name + ".Name= " + '"' + "txt" + column.Name + '"' + ";");
                            sb.Append("\n\t\t\t this.txt" + column.Name + ".Location=new System.Drawing.Point(105, " + p + ");");
                            sb.Append("\n\t\t\t this.txt" + column.Name + ".Size = new System.Drawing.Size(158, 20);");
                            sb.Append("\n\t\t\t this.txt" + column.Name + ".TabIndex = 1;");
                        }
                        else
                        {
                            int p = 50;
                            p = p + ((i - 14) * 25);
                            sb.Append("\n\t\t\t this.txt" + column.Name + ".Name= " + '"' + "txt" + column.Name + '"' + ";");
                            sb.Append("\n\t\t\t this.txt" + column.Name + ".Location=new System.Drawing.Point(480, " + p + ");");
                            sb.Append("\n\t\t\t this.txt" + column.Name + ".Size = new System.Drawing.Size(158, 20);");
                            sb.Append("\n\t\t\t this.txt" + column.Name + ".TabIndex = 1;");
                        }
                     
                    }
                }
               

                //    "+'"'+"
                sb.Append("\n\t\t\tthis.btnSave.Location = new System.Drawing.Point(105, 12); ");
                sb.Append("\n\t\t\t this.btnSave.Name = " + '"' + "btnSave" + '"' + "; ");
                sb.Append("\n\t\t\tthis.btnSave.Size = new System.Drawing.Size(95, 32); ");
                sb.Append("\n\t\t\t this.btnSave.TabIndex = 9;");
                sb.Append("\n\t\t\t this.btnSave.Text = "+'"'+ "Save" + '"' + ";");
                sb.Append("\n\t\t\t this.btnSave.UseVisualStyleBackColor = true;");
                sb.Append("\n\t\t\t this.btnSave.Click += new System.EventHandler(this.btnSave_Click);");
                //
                sb.Append("\n\t\t\t this.btnClose.Location = new System.Drawing.Point(206, 12);");
                sb.Append("\n\t\t\t this.btnClose.Name = " + '"' + "btnClose" + '"' + "; ");
                sb.Append("\n\t\t\t this.btnClose.Size = new System.Drawing.Size(75, 32);");
                sb.Append("\n\t\t\t this.btnClose.TabIndex = 15;");
                sb.Append("\n\t\t\t this.btnClose.Text = " + '"' + "Close" + '"' + "; ");
                sb.Append("\n\t\t\t this.btnClose.UseVisualStyleBackColor = true; ");
                sb.Append("\n\t\t\t this.btnClose.Click += new System.EventHandler(this.btnClose_Click);");
                sb.Append("\n\t\t\t ");
                sb.Append("\n\t\t\t ");
                sb.Append("\n\t\t\t ");
                // 
                // FormMeasuringUpdate
                // 
                sb.Append("\n\t\t\t // FormMeasuringUpdate ");
                sb.Append("\n\t\t\tthis.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F); ");             
                sb.Append("\n\t\t\tthis.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font; ");
                sb.Append("\n\t\t\tthis.ClientSize = new System.Drawing.Size(706, 543);");
                sb.Append("\n\t\t\tthis.Controls.Add(this.btnClose);");
                sb.Append("\n\t\t\tthis.Controls.Add(this.btnSave); ");

                //  this.Controls.Add(this.btnClose);
                             
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];
                    if (column.IsIdentity == false)
                    {
                        sb.Append("\n\t\t\t  this.Controls.Add(this.txt" + column.Name + ");");
                         
                    }
                }
                //Label
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];
                    if (column.IsIdentity == false)
                    {
                        sb.Append("\n\t\t\t this.Controls.Add(this.label" + column.Name + ");");
                        
                    }
                }
                //    "+'"'+"
         
                sb.Append("\n\t\t\t this.Name =  " + '"' + FormName + '"' + "; ");
                sb.Append("\n\t\t\t this.Text =  " + '"' + FormName + '"' + "; ");
                sb.Append("\n\t\t\t this.ResumeLayout(false);");
                sb.Append("\n\t\t\t this.PerformLayout();");
                sb.Append("\n\t\t\t  }");
                //
                sb.Append("\n\t\t\t ");
                sb.Append("\n\t\t private System.Windows.Forms.Button btnClose;");
                sb.Append("\n\t\t private System.Windows.Forms.Button btnSave; ");                
                // 
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];
                    if (column.IsIdentity == false)
                    {
                        sb.Append("\n\t\tprivate System.Windows.Forms.TextBox txt" + column.Name + ";");
                        sb.Append("\n\t\tprivate System.Windows.Forms.Label label" + column.Name + ";");
                    }
                }
 
                sb.Append("\n\t\t\t ");
                sb.Append("\n\t\t\t}");
                sb.Append("\n }");
             
                streamWriter.WriteLine(sb);


            }
        }
        //Sinh code cho form cập nhật dữ liệu 
        public static void WindowsFormUpdate_UsingOtherForm_Code(string databaseName, Table table, string targetNamespace, string storedProcedurePrefix, string daoSuffix, string dtoSuffix,string busSuffix, string path)
        {
            string tableName = Utility.FormatClassName(table.Name);
            string classNameInfo = tableName + dtoSuffix;
            string classNameDAO = tableName + daoSuffix;
            string classNameBusiness = tableName + busSuffix;
            string FileName = "Form" + tableName + "Update.cs";
            string FormName = "Form" + tableName + "Update";

            path = Path.Combine(path, "Presentation_WindowsForm");
            //tìm Khóa của bảng
            string Id = "Id";
            for (int i = 0; i < table.Columns.Count; i++)
            {
                Column column = table.Columns[i];

                // Is the current column an identity column?);
                if (column.IsIdentity == true)
                {
                    Id = column.Name;
                   // return;
                }

            }
            //Gridview to textbox
            using (StreamWriter streamWriter = new StreamWriter(Path.Combine(path, FileName)))
            {
                var sb = new StringBuilder();
                sb.Append("\n using System;");
                sb.Append("\n using System.Collections.Generic;");
                sb.Append("\n using System.ComponentModel;");
                sb.Append("\n using System.Drawing;");
                sb.Append("\n using System.Linq;");
                sb.Append("\n using System.Text;");
                sb.Append("\n using System.Windows.Forms;");
                sb.Append("\n using Business;");
                sb.Append("\n using Entity;");
                sb.Append("\n using System.Data;");
                sb.Append("\n namespace " + targetNamespace);
                sb.Append("\n {");
                sb.Append("\n\t public partial class " + FormName + " : Form");
                sb.Append("\n\t {");

                sb.Append("\n\t\tprivate int _Id = 0;");
                sb.Append("\n\t\t" + classNameBusiness + " bus = new " + classNameBusiness + "();");
                sb.Append("\n\t\tpublic "+ FormName + "()");
                sb.Append("\n\t\t{");
                sb.Append("\n\t\t\tInitializeComponent();");
                sb.Append("\n\t\t} ");

                sb.Append("\n\t\t  public "+FormName+"(int Id)");
                sb.Append("\n\t\t{");
                sb.Append("\n\t\t\t InitializeComponent();");
                sb.Append("\n\t\t\t try");
                sb.Append("\n\t\t\t {");
                sb.Append("\n\t\t\t\t this._Id = Id;");
                sb.Append("\n\t\t\t\t" + classNameInfo + " obj = new " + classNameInfo + "();");
                //" + classNameInfo +"
                sb.Append("\n\t\t\t\t obj." + Utility.FormatPascal(Id) + " = Id;");
                sb.Append("\n\t\t\t\t DataTable dt = bus.GetAllByID(obj);");
               

                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];
                    if (column.IsIdentity == false)
                    {
                        sb.Append("\n\t\t\t\ttxt"+column.Name+".Text = dt.Rows[0]["+'"'+column.Name+'"'+"].ToString();");                      
                    }
                }
                sb.Append("\n\t\t\t }");
                sb.Append("\n\t\t\t catch (Exception ex)");
                sb.Append("\n\t\t\t {");
                sb.Append("\n\t\t\t\t  MessageBox.Show(" + '"' + "Something went wrong" + '"' + ", " + '"' + "Alert" + '"' + "); ");

                sb.Append("\n\t\t\t }");
                sb.Append("\n\t\t } ");
                
                sb.Append("\n\t ");
                
                // Click SAVE
                sb.Append("\n\t\t  private void btnSave_Click(object sender, EventArgs e)");
                sb.Append("\n\t\t  {");
                sb.Append("\n\t\t\t  try");
                sb.Append("\n\t\t\t {");
                sb.Append("\n\t\t\t\t  "+classNameInfo+" obj = new "+classNameInfo+"();");
                sb.Append("\n\t\t\t\t if (_Id == 0)");
                sb.Append("\n\t\t\t\t {");

                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];
                    if (column.IsIdentity == false)
                    {

                        switch (column.Type)
                        {
                            case "int":
                                sb.Append("\n\t\t\t\t\tobj." + Utility.FormatPascal(column.Name) + "  =  (txt" + column.Name + ".Text!=" + '"' + '"' + ") ? int.Parse(txt" + column.Name + ".Text): 0;");
                                break;
                            case "datetime":
                                sb.Append("\n\t\t\t\t\tobj." + Utility.FormatPascal(column.Name) + "  = Convert.ToDateTime(txt" + column.Name + ".Text); ");
                                break;
                            case "date":
                                sb.Append("\n\t\t\t\t\tobj." + Utility.FormatPascal(column.Name) + "  = Convert.ToDateTime(txt" + column.Name + ".Text); ");
                                break;
                            case "bit":                                
                                sb.Append("\n\t\t\t\t\tobj." + Utility.FormatPascal(column.Name) + "  = bool.Parse(txt" + column.Name + ".Text); ");
                                break;
                            case "numeric":
                                sb.Append("\n\t\t\t\t\tobj." + Utility.FormatPascal(column.Name) + "  =  (txt" + column.Name + ".Text!=" + '"' + '"' + " )? decimal.Parse(txt" + column.Name + ".Text): 0;");
                                break;
                            default:                               
                                sb.Append("\n\t\t\t\t\tobj." + Utility.FormatPascal(column.Name) + "  =  (txt" + column.Name + ".Text!="+'"'+'"'+ ") ? txt" + column.Name + ".Text :" + '"' + '"' + ";");
                                break;
                        }

                    }
                }
                sb.Append("\n\t\t\t\t\t bus.Insert(obj);");
                sb.Append("\n\t\t\t\t\t MessageBox.Show(" + '"' + "Insert data success !" + '"' + ", " + '"' + "Information" + '"' + "); ");
                sb.Append("\n\t\t\t\t\t this.Close();");
                sb.Append("\n\t\t\t\t}");
                sb.Append("\n\t\t\t\telse");
                sb.Append("\n\t\t\t\t{");
                sb.Append("\n\t\t\t\t\tobj."+ Utility.FormatPascal(Id)+" = _Id;");
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];
                    if (column.IsIdentity == false)
                    {

                        switch (column.Type)
                        {
                            case "int":
                                sb.Append("\n\t\t\t\t\tobj." + Utility.FormatPascal(column.Name) + "  =  (txt" + column.Name + ".Text!=" + '"' + '"' + ") ? int.Parse(txt" + column.Name + ".Text): 0;");
                                break;
                            case "datetime":
                                sb.Append("\n\t\t\t\t\tobj." + Utility.FormatPascal(column.Name) + "  = Convert.ToDateTime(txt" + column.Name + ".Text); ");
                                break;
                            case "date":
                                sb.Append("\n\t\t\t\t\tobj." + Utility.FormatPascal(column.Name) + "  = Convert.ToDateTime(txt" + column.Name + ".Text); ");
                                break;
                            case "bit":
                                sb.Append("\n\t\t\t\t\tobj." + Utility.FormatPascal(column.Name) + "  = bool.Parse(txt" + column.Name + ".Text); ");
                                break;
                            case "numeric":
                                sb.Append("\n\t\t\t\t\tobj." + Utility.FormatPascal(column.Name) + "  =  (txt" + column.Name + ".Text!=" + '"' + '"' + " )? decimal.Parse(txt" + column.Name + ".Text): 0;");
                                break;
                            default:
                                sb.Append("\n\t\t\t\t\tobj." + Utility.FormatPascal(column.Name) + "  =  (txt" + column.Name + ".Text!=" + '"' + '"' + ") ? txt" + column.Name + ".Text :" + '"' + '"' + ";");
                                break;
                        }

                    }
                }

               
                sb.Append("\n\t\t\t\t\t bus.Update(obj);");
                sb.Append("\n\t\t\t\t\t MessageBox.Show("+'"'+ "Update data success !" + '"' + ", " + '"' + "Information" + '"' + ");");
                sb.Append("\n\t\t\t\t\t  this.Close();");
                sb.Append("\n\t\t\t\t }");
                sb.Append("\n\t\t\t }");
                sb.Append("\n\t\t\t catch (Exception ex)");
                sb.Append("\n\t\t\t {");
                sb.Append("\n\t\t\t MessageBox.Show(ex.ToString());");
                sb.Append("\n\t\t\t }");
                sb.Append("\n\t\t}");
               

                sb.Append("\n\t\t private void btnClose_Click(object sender, EventArgs e)");
                sb.Append("\n\t\t {");
                sb.Append("\n\t\t\t this.Close();");
                sb.Append("\n\t\t }");
              
                sb.Append("\n\t ");
                sb.Append("\n\t ");
                sb.Append("\n\t }");
                sb.Append("\n }");
              
                streamWriter.WriteLine(sb);
            }
        }      
        public static void WindowsFormUpdate_UsingOtherForm_Resx(string databaseName, Table table, string targetNamespace, string storedProcedurePrefix, string daoSuffix, string dtoSuffix, string path)
        {
            string tableName = Utility.FormatClassName(table.Name);
            string classNameInfo = tableName + dtoSuffix;
            string classNameDAO = tableName + daoSuffix;
            string classNameBusiness = tableName + "Business";
            string FileName = "Form" + tableName + "Update.resx";
            string FormName = "Form" + tableName;

            path = Path.Combine(path, "Presentation_WindowsForm");
            string strFullPath = Path.Combine(path, FileName);
            string FiLeNguon = System.Windows.Forms.Application.StartupPath + "\\FormMeasuring.resx";

            File.Copy(FiLeNguon, strFullPath, true);
 
        }

    
        #endregion
    }
}
