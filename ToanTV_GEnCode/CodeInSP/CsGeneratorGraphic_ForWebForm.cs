﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace App_ChuongTrinhSinhCodeTuDong.CodeInSP
{
    internal static class CsGeneratorGraphic_ForWebForm
    {
        #region  Sửa xóa dữ liệu từ Gridview - TextBox

        /// <summary>
        /// Tạo giao diện phần thêm sửa xóa kiểu Click Gridview tới textbox
        /// </summary>
        /// <param name="databaseName"></param>
        /// <param name="table"></param>
        /// <param name="targetNamespace"></param>
        /// <param name="storedProcedurePrefix"></param>
        /// <param name="daoSuffix"></param>
        /// <param name="dtoSuffix"></param>
        /// <param name="path"></param>
        public static void GridViewToTextBox(string databaseName, Table table, string targetNamespace, string storedProcedurePrefix, string busSuffix, string dtoSuffix, string path)
        {

            string className = Utility.FormatClassName(table.Name) + busSuffix;
          
            using (StreamWriter streamWriter = new StreamWriter(Path.Combine(path, table.Name + ".aspx")))
            {
                var sb = new StringBuilder();

                sb.Append("\n <%@ Page Title=\"\" Language=\"C#\" MasterPageFile=\"~/Admin/admin.Master\" AutoEventWireup=\"true\"");
                sb.Append("\n CodeBehind=\""+table.Name+ ".aspx.cs\" Inherits=\""+targetNamespace+ ".Admin." + table.Name+"\" %>");
                sb.Append("\n <asp:Content ID=\"Content1\" ContentPlaceHolderID=\"HeadContent\" runat=\"server\">");
                sb.Append("\n </asp:Content>");
                sb.Append("\n <asp:Content ID=\"Content2\" ContentPlaceHolderID=\"MainContent\" runat=\"server\">");
                sb.Append("\n  <h2>"+table.Name.ToUpper()+"</h2>");
                sb.Append("\n <asp:Panel ID=\"PanelModify\" runat=\"server\">");
                sb.Append("\n  <table>");
                sb.Append("\n ");
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];

                    // Is the current column an identity column?);
                    if (column.IsIdentity == false)
                    {
                        sb.Append("\n <tr>");
                        sb.Append("\n <td align=\"right\">" + column.Name + ":</td>");
                        sb.Append("\n <td><asp:TextBox ID=\"txt"+column.Name+"\" runat=\"server\" Height=\"26px\" Width=\"200px\"></asp:TextBox></td>");
                        sb.Append("\n </tr>");
                    }
                    else
                    {
                        sb.Append("\n <tr>");
                        sb.Append("\n <td align=\"right\">" + column.Name + ":</td>");
                        sb.Append("\n <td><asp:TextBox ID=\"txt"+column.Name+"\" runat=\"server\" Height=\"26px\" Width=\"200px\"></asp:TextBox></td>");
                        sb.Append("\n </tr>");
                      
                    }
                }
                sb.Append("\n <tr>");
                sb.Append("\n  <td align=\"left\">");
                sb.Append("\n &nbsp;</td>");
                sb.Append("\n <td>");
                sb.Append("\n <asp:Button ID=\"btnSave\" runat=\"server\" onclick=\"btnSave_Click\" Text=\"Save\" />");
                sb.Append("\n <asp:Button ID=\"btnDelete\" runat=\"server\" onclick=\"btnDelete_Click\" Text=\"Delete\" Visible=\"False\" />");
                sb.Append("\n <asp:Button ID=\"btnCancel\" runat=\"server\" onclick=\"btnCancel_Click\" Text=\"Cancel\" />");
                sb.Append("\n </td>");
                sb.Append("\n  </tr>");             
                sb.Append("\n </table> \n \t</asp:Panel>");
                sb.Append("\n  <br />");
                sb.Append("\n <asp:Panel ID=\"PanelShow\" runat=\"server\">");
                sb.Append("\n  <asp:Button ID=\"btnThem\" runat=\"server\" onclick=\"btnThem_Click\" Text=\"Add New\" />");
                sb.Append("\n  <br />  <br />  ");
                sb.Append("\n  <asp:GridView ID=\"GridViewShow\" runat=\"server\" AutoGenerateColumns=\"false\" OnRowCommand=\"GridViewShowRowClick\" Width=\"600px\"> ");
               
                sb.Append("\n  <Columns>");
                sb.Append("\n <asp:TemplateField HeaderText=\" + \">");
                sb.Append("\n <ItemTemplate>");
                sb.Append("\n  <%# Container.DataItemIndex+1 %></ItemTemplate>");
                sb.Append("\n <ItemStyle Width=\"10px\" />");
                sb.Append("\n </asp:TemplateField>");
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];

                    // Is the current column an identity column?);
                    if (column.IsIdentity == false)
                    {
                        sb.Append("\n \t  <asp:BoundField DataField=\"" + column.Name + "\" HeaderText=\"" + column.Name + "\" />");

                    }
                    else
                    {
                        sb.Append("\n \t <asp:BoundField DataField=\"" + column.Name + "\" HeaderText=\"" + column.Name + "\" Visible=\"False\" />");

                        sb.Append("");
                    }
                }
                 for (int i = 0; i < table.Columns.Count; i++)
                 {
                     Column column = table.Columns[i];
                     if (column.IsIdentity==true)
                     {
                          sb.Append("\n <asp:TemplateField HeaderText=\"Edit\">");
                            sb.Append("\n  <ItemTemplate>");

                            sb.Append("\n <asp:LinkButton ID=\"lbEdit\" runat=\"server\" CommandName='<%#\"S|\"+Eval(\"" + column.Name + "\")%>' Text=\"Edit\"></asp:LinkButton> ");
                            sb.Append("\n </ItemTemplate>");
                            sb.Append("\n </asp:TemplateField>");
                            sb.Append("\n <asp:TemplateField HeaderText=\"Delete\">");
                            sb.Append("\n <ItemTemplate>");

                            sb.Append("\n <asp:LinkButton ID=\"lbDelete\" runat=\"server\" CommandName='<%#\"X|\"+Eval(\"" + column.Name + "\")%>' OnClientClick=\"return confirm('Do you want to delete?')\" Text=\"Delete\"></asp:LinkButton>");
                            sb.Append("\n </ItemTemplate>");
                            sb.Append("\n </asp:TemplateField>");
                     }
                 }
            
                sb.Append("\n </Columns>");
                sb.Append(" <PagerSettings Mode=\"NumericFirstLast\" />");
                sb.Append("</asp:GridView>");
                
                sb.Append(" </asp:Panel>");
             
                sb.Append("\n </asp:Content>");
                  
                streamWriter.WriteLine(sb);
                
                            
             }
        }

        /// <summary>
        ///  /// Tạo Code phần thêm sửa xóa kiểu Click Gridview tới textbox
        /// </summary>
        /// <param name="databaseName"></param>
        /// <param name="table"></param>
        /// <param name="targetNamespace"></param>
        /// <param name="storedProcedurePrefix"></param>
        /// <param name="daoSuffix"></param>
        /// <param name="dtoSuffix"></param>
        /// <param name="path"></param>
       
        public static void GridViewToTextBox_CS(string databaseName, Table table, string targetNamespace, string storedProcedurePrefix, string busSuffix, string dtoSuffix, string path)
        {
            string className = Utility.FormatClassName(table.Name) + dtoSuffix;
            string classNameDAO = Utility.FormatClassName(table.Name) + busSuffix;
            string classNameBusiness = Utility.FormatClassName(table.Name) + busSuffix;
            string objBUS = "_" + table.Name + busSuffix.ToUpper();
           
            //Gridview to textbox
            using (StreamWriter streamWriter = new StreamWriter(Path.Combine(path, table.Name + ".aspx.cs")))
            {
                var sb = new StringBuilder();
                sb.Append("using System;");
                sb.Append("\n using System.Collections.Generic;");
                sb.Append("\n using System.Linq;");
                sb.Append("\n using System.Web;");
                sb.Append("\n using System.Web.UI;");
                sb.Append("\n using System.Web.UI.WebControls;");
                sb.Append("\n using Business;");
                sb.Append("\n using Entity;");
                sb.Append("\n using System.Data;");
                sb.Append("\n using Common;");
                sb.Append("\n namespace " + targetNamespace+ ".Admin");
                sb.Append("\n {");
                sb.Append("\n public partial class " + table.Name + " : System.Web.UI.Page");
                sb.Append("\n \t {");
                //==============Hết phần định nghĩa class
                sb.Append("\n \t\t" + classNameBusiness + " " + objBUS + " = new " + classNameBusiness + "();");

                sb.Append("\n \t\t  protected void Page_Load(object sender, EventArgs e)");
                sb.Append("\n \t\t {");
                sb.Append("\n\t\t try");
                sb.Append("\n\t\t {");
               
                sb.Append("\n \t\t\t if (!Page.IsPostBack)");
                sb.Append("\n \t\t\t { ");
                sb.Append("\n \t\t\t\t Databind();");
                sb.Append("\n \t\t\t\t PanelModify.Visible = false; ");
                sb.Append("\n \t\t\t\t PanelShow.Visible = true; ");
                sb.Append("\n \t\t\t\t ViewState[\"active\"] = \"\"; ");
                sb.Append("\n \t\t\t } ");
                sb.Append("\n \t\t }");
                sb.Append("\n\t\t\t catch (Exception)");
                sb.Append("\n\t\t\t {");
                sb.Append("\n\t\t\t\t  Alert.Show(\"Some thing went wrong !\");");
                sb.Append("\n\t\t\t }");
                sb.Append("\n \t\t } ");

                //Xóa dữ liệu rỗng
                sb.Append("\n \t\t //================Xóa rỗng các text box==================");
                sb.Append("\n \t\t public void Clear()");
                sb.Append("\n \t\t {");
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];
                    sb.Append("\n\t\t\t txt" + column.Name + ".Text = \"\";");

                }
                sb.Append("\n \t\t} ");
                //Load dữ liệu cho Gridview
                sb.Append("\n //====================Load dữ liệu cho Gridview=========================");
                sb.Append("\n \t\t public void Databind()");
                sb.Append("\n \t\t {");
                sb.Append("\n\t\t\t GridViewShow.DataSource =" + objBUS + ".GetAll();");
                sb.Append("\n\t\t\t GridViewShow.DataBind();");
                sb.Append("\n\t\t }");
                sb.Append("\n\t ");

                //Select trong gridview
                sb.Append("\n  //====================================Sự kiện Select trên Gridview===========================");
                sb.Append("\n\t\t protected void GridViewShowRowClick(object sender, GridViewCommandEventArgs e)");
                sb.Append("\n\t\t {");
                sb.Append("\n\t\t\t try");
                sb.Append("\n\t\t {");
                sb.Append("\n\t\t string str = e.CommandName;");
                sb.Append("\n\t\t if(str.Equals(\"Page\")==false)");
                sb.Append("\n\t\t {");
                sb.Append("\n\t\t char[] ch = new char[] { '|' };");
                sb.Append("\n\t\t string[] s = str.Split(ch);");
                sb.Append("\n\t\t " + className + " obj=new " + className + "();");
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];
                    if (column.IsIdentity == true)
                    {
                        sb.Append(App_ChuongTrinhSinhCodeTuDong.CodeInSP.Common.SeparatorText_ForGridview(column));
                    }

                }

                sb.Append("\n\t\t   switch (s[0])");
                sb.Append("\n\t\t\t {");
                sb.Append("\n\t\t\t\t  case \"S\":");
                sb.Append("\n\t\t ViewState[\"active\"] = \"edit\";");
                sb.Append("\n\t\t DataTable dt= " + objBUS + ".GetAllByID(obj);");
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];
                    sb.Append("\n\t\t\t txt" + column.Name + ".Text =dt.Rows[0][\"" + column.Name + "\"].ToString();");
                }

                sb.Append("\n\t\t ");
                sb.Append("\n\t\t PanelModify.Visible = true;");
                sb.Append("\n\t\t PanelShow.Visible = false;");
                sb.Append("\n\t\t break;");
                sb.Append("\n\t\t  case \"X\":");
                sb.Append("\n\t\t  " + objBUS + ".Delete(obj);");
                sb.Append("\n\t\t   Databind();");
                sb.Append("\n\t\t  break;");
                sb.Append("\n\t\t   }");
                sb.Append("\n\t\t\t }");
                sb.Append("\t\t\t else");
                sb.Append("\t\t\t\t { ");
                sb.Append("\t\t\t\t }");
                sb.Append("\n\t\t }");
                sb.Append("\n\t\t catch (Exception)");
                sb.Append("\n\t\t {");
                sb.Append("\n\t\t Alert.Show(\"Some thing went wrong !\");");
                sb.Append("\n\t\t\t }");
                sb.Append("\n\t\t }");

                sb.Append("\n //========================Thêm====================");
                sb.Append("\n protected void btnThem_Click(object sender, EventArgs e)");
                sb.Append("\n { \n\t PanelModify.Visible = true;");
                sb.Append("\n\t  PanelShow.Visible = false; ");
                sb.Append("\n\t Clear();");
                sb.Append("\n\t ViewState[\"active\"] = \"add\"; ");
                sb.Append("\n }");

                //Khi click vào save để THêm Hoặc SỬa
                sb.Append("\n  //=====================================Khi click vào save để THêm Hoặc SỬa=============================");
                sb.Append("\n   protected void btnSave_Click(object sender, EventArgs e) \n\t  { ");
                sb.Append("\n \t\t try ");
                sb.Append("\n\t\t\t {");
               
                sb.Append("\n " + className + " obj = new " + className + "();");

                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];
                    if (column.IsIdentity == false)
                    {
                        sb.Append(App_ChuongTrinhSinhCodeTuDong.CodeInSP.Common.SeparatorText(column));
                        //switch (column.Type)
                        //{
                        //    case "int":
                        //        sb.Append("\n\t obj." + column.Name + "  = int.Parse(txt" + column.Name + ".Text); ");
                        //        break;
                        //    case "smallint":
                        //        sb.Append("\n\t obj." + column.Name + "  = short.Parse(txt" + column.Name + ".Text); ");
                        //        break;
                        //    case "bit":
                        //        sb.Append("\n\t obj." + column.Name + "  = bool.Parse(txt" + column.Name + ".Text); ");
                        //        break;
                        //    case "datetime":
                        //        sb.Append("\n\t obj." + column.Name + "  = Convert.ToDateTime(txt" + column.Name + ".Text); ");
                        //        break;
                        //    case "date":
                        //        sb.Append("\n\t obj." + column.Name + "  = Convert.ToDateTime(txt" + column.Name + ".Text); ");
                        //        break;
                        //    default:
                        //        sb.Append("\n\t obj." + column.Name + "  =  txt" + column.Name + ".Text; ");
                        //        break;
                        //}


                    }

                }

                sb.Append("\n   switch (ViewState[\"active\"].ToString()) \n{");
                sb.Append("\n   case \"add\":  ");
                sb.Append("\n\t " + objBUS + ".Insert(obj);");

                sb.Append("\n\t   break;");
                sb.Append("\n  case \"edit\":");
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];
                    if (column.IsIdentity == true)
                    {
                        sb.Append(App_ChuongTrinhSinhCodeTuDong.CodeInSP.Common.SeparatorText(column));
                        
                    }
                }

                sb.Append("\n\t\t\t " + objBUS + ".Update(obj);");

                sb.Append("\n\t\t\t  break; ");
                sb.Append("\n\t\t   } ");
                sb.Append("\n\t\t   Databind();");
                sb.Append("\n\t\t PanelModify.Visible = false;");
                sb.Append("\n\t\t PanelShow.Visible = true;");
                sb.Append("\n\t\t }");
                sb.Append("\n\t\t\t catch (Exception ex) ");
                sb.Append("\n\t\t\t {");
                sb.Append("\n\t\t\t Alert.Show(\"Cannot Save this item\");");
                sb.Append("\n\t\t\t }");
                sb.Append("\n\t\t }");
                
                //Sự kiện xóa     
                sb.Append("\n  //=====================================Sự kiện xóa =============================");
                sb.Append("\n\t\t  protected void btnDelete_Click(object sender, EventArgs e)");
                sb.Append("\n\t\t {");
                sb.Append("\n\t\t try");
                sb.Append("\n\t\t\t {");
               
                sb.Append("\n\t\t\t " + className + " obj = new " + className + "();");
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];
                    if (column.IsIdentity == true)
                    {
                        sb.Append(App_ChuongTrinhSinhCodeTuDong.CodeInSP.Common.SeparatorText(column));
                         
                    }
                }

                sb.Append("\n\t\t\t " + objBUS + ".Delete(obj);");
                sb.Append("\n\t\t\t  Databind();");
                sb.Append("\n\t\t\t Alert.Show(\"Item deleted !\");");
                sb.Append("\n\t\t\t PanelModify.Visible = false;");
                sb.Append("\n\t\t\t PanelShow.Visible = true;");
                sb.Append("\n\t\t }");
                sb.Append("\n\t\t catch (Exception)");
                sb.Append("\n\t\t\t {");
                sb.Append("\n\t\t\t Alert.Show(\"Cannot delete this item\");");
                sb.Append("\n\t\t\t }");
                sb.Append("\n\t\t\t }");
                sb.Append("\n\t\t\t ");
                ////Sự kiện Phân Trang    
                //sb.Append("\n  //=====================================Phân Trang============================");
                //sb.Append("\n\t\t protected void GridViewShow_PageIndexChanging(object sender, GridViewPageEventArgs e) ");
                //sb.Append("\n\t\t { \n\t GridViewShow.PageIndex = e.NewPageIndex;");
                // sb.Append("\n\t\t\t Databind();");
                //sb.Append("\n\t\t\t }");

                //Nút Cancel    
                sb.Append("\n  //=====================================Nút Cancel============================");
                sb.Append("\n\t\t protected void btnCancel_Click(object sender, EventArgs e) ");
                sb.Append("\n\t\t { ");
                sb.Append("\n\t\t\t PanelModify.Visible = false; ");
                sb.Append("\n\t\t\t PanelShow.Visible = true;");
                sb.Append("\n\t\t\t }");

                sb.Append("\n\t\t }");
                sb.Append("\n }");

                streamWriter.WriteLine(sb);
            }
        }

        /// <summary>
        /// Sinh phan designer cho file cua webform
        /// </summary>
        /// <param name="databaseName"></param>
        /// <param name="table"></param>
        /// <param name="targetNamespace"></param>
        /// <param name="storedProcedurePrefix"></param>
        /// <param name="busSuffix"></param>
        /// <param name="dtoSuffix"></param>
        /// <param name="path"></param>
        public static void GridViewToTextBox_Designer_CS(string databaseName, Table table, string targetNamespace, string storedProcedurePrefix, string busSuffix, string dtoSuffix, string path)
        {
            string className = Utility.FormatClassName(table.Name) + dtoSuffix;
            string classNameDAO = Utility.FormatClassName(table.Name) + busSuffix;
            string classNameBusiness = Utility.FormatClassName(table.Name) + busSuffix;
            string objBUS = "_" + table.Name + busSuffix.ToUpper();
           
            //Gridview to textbox
            using (StreamWriter streamWriter = new StreamWriter(Path.Combine(path, table.Name + ".aspx.designer.cs")))
            {
                var sb = new StringBuilder();
                sb.Append("\n//------------------------------------------------------------------------------");
                sb.Append("\n// <auto-generated>");
                sb.Append("\n //     This code was generated by a tool.");
                sb.Append("\n //     Changes to this file may cause incorrect behavior and will be lost if");
                sb.Append("\n  //     the code is regenerated. ");
                sb.Append("\n // </auto-generated>");
                sb.Append("\n //------------------------------------------------------------------------------");

                sb.Append("\n namespace " + targetNamespace + ".Admin");
                sb.Append("\n {");
                sb.Append("\n\t public partial class " + table.Name);
                sb.Append("\n\t{");
                sb.Append("\n\t protected global::System.Web.UI.WebControls.Panel PanelModify;");
                sb.Append("\n\t protected global::System.Web.UI.WebControls.Button btnSave;");
                sb.Append("\n\t protected global::System.Web.UI.WebControls.Button btnDelete;");
                sb.Append("\n\t protected global::System.Web.UI.WebControls.Button btnCancel;");
                sb.Append("\n\t protected global::System.Web.UI.WebControls.Button btnThem;");
                sb.Append("\n\t protected global::System.Web.UI.WebControls.Panel PanelShow;");
                sb.Append("\n\t protected global::System.Web.UI.WebControls.GridView GridViewShow;");
                sb.Append("\n ");
                 
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];
                    sb.Append("\n\t protected global::System.Web.UI.WebControls.TextBox txt" + column.Name + ";");
                }

                sb.Append("\n\t }");
                sb.Append("\n }");

                streamWriter.WriteLine(sb);
            }
        }
        
        #endregion

        //


        //Modify in gridview
        #region ===========Thêm Sửa Xóa Trong Gridview==============
            /// <summary>
            /// Phần giao diện aspx
            /// </summary>
            /// <param name="databaseName"></param>
            /// <param name="table"></param>
            /// <param name="targetNamespace"></param>
            /// <param name="storedProcedurePrefix"></param>
            /// <param name="daoSuffix"></param>
            /// <param name="dtoSuffix"></param>
            /// <param name="path"></param>
        public static void CreateModifyGridview(string databaseName, Table table, string targetNamespace, string storedProcedurePrefix, string daoSuffix, string dtoSuffix, string path)
        {

            string className = Utility.FormatClassName(table.Name) + daoSuffix;
           
            using (StreamWriter streamWriter = new StreamWriter(Path.Combine(path, table.Name + "2.aspx")))
            {
                string Id = "Id";
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];

                    // Is the current column an identity column?);
                    if (column.IsIdentity == true)
                    {
                        Id = column.Name;
                    }


                }

                var sb = new StringBuilder();
                sb.Append("\n <%@ Page Title=\"\" Language=\"C#\" MasterPageFile=\"~/Admin/admin.Master\" AutoEventWireup=\"true\"");
                sb.Append("\n CodeBehind=\"" + table.Name + "2.aspx.cs\" Inherits=\"" + targetNamespace + ".Admin2." + table.Name + "2\" %>");
                sb.Append("\n <asp:Content ID=\"Content1\" ContentPlaceHolderID=\"HeadContent\" runat=\"server\">");
                sb.Append("\n </asp:Content>");
                sb.Append("\n <asp:Content ID=\"Content2\" ContentPlaceHolderID=\"MainContent\" runat=\"server\">");
                sb.Append("\n  <h2>" + table.Name.ToUpper() + "</h2>");
                sb.Append("\n <asp:Panel ID=\"PanelModify\" runat=\"server\"");
                sb.Append("\n ScrollBars=\"Vertical\"");
                sb.Append("\n BorderStyle=\"Inset\" BorderWidth=\"1px\" \n style=\"overflow: auto; height: 330px\">");

                sb.Append("\n <asp:GridView ID=\"GridView" + table.Name + "\" runat=\"server\" AutoGenerateColumns=\"False\" ");
                sb.Append("\n AllowPaging=\"True\" DataKeyNames=\""+Id+"\"");
                sb.Append("\n OnPageIndexChanging=\"GridView"+table.Name+"_PageIndexChanging\"");
                sb.Append("\n OnRowCancelingEdit=\"GridView" + table.Name + "_RowCancelingEdit\"");
                sb.Append("\n OnRowEditing=\"GridView" + table.Name + "_RowEditing\"");
                sb.Append("\n OnRowUpdating=\"GridView" + table.Name + "_RowUpdating\"");
                sb.Append("\n onrowdeleting=\"GridView" + table.Name + "_RowDeleting\"");
                sb.Append("\n ShowFooter=\"True\" Width=\"90%\">");
               
                sb.Append("\n\t <Columns>");
                sb.Append("\n\t <asp:TemplateField HeaderText=\"No.\">");
                sb.Append("\n\t <ItemTemplate>");
                sb.Append("\n\t <%# Container.DataItemIndex+1 %>");
                sb.Append("\n\t</ItemTemplate>");
                
                sb.Append("\n\t </asp:TemplateField>");

                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];

                    // Is the current column an identity column?);
                    if (column.IsIdentity == false)
                    {
                       
                        sb.Append("\n\t <asp:TemplateField HeaderText=\" "+column.Name+" \">");
                        sb.Append("\n\t <EditItemTemplate>");
                        sb.Append("\n\t <asp:TextBox ID=\"txt" + column.Name + "\" runat=\"server\" Text='<%# Bind(\"" + column.Name + "\") %>'></asp:TextBox>");
                        sb.Append("\n\t </EditItemTemplate>");
                        sb.Append("\n\t <FooterTemplate>");
                        sb.Append("\n\t <asp:TextBox ID=\"txt"+column.Name+"\" runat=\"server\"></asp:TextBox>");
                        sb.Append("\n\t </FooterTemplate>");
                        sb.Append("\n\t <ItemTemplate>"); 
                        sb.Append("\n\t <asp:Label ID=\"lbl"+column.Name+"\" runat=\"server\" Text='<%# Bind(\""+column.Name+"\") %>'></asp:Label>");
                        sb.Append("\n\t </ItemTemplate>");
                        sb.Append("\n\t </asp:TemplateField>"); 
                       
                        sb.Append("");
                    }
                    else
                    {                                           
                    }
                }
                //Tạo Cột xóa
                sb.Append("\n\t <asp:TemplateField ShowHeader=\"False\" HeaderText=\"Delete\">");
                sb.Append("\n\t <ItemTemplate>");
                sb.Append("\n\t <asp:LinkButton ID=\"LinkButtonDelete\" runat=\"server\" CausesValidation=\"False\" CommandName=\"Delete\" ");
                sb.Append("\n\t Text=\"Delete\" OnClientClick = \"return confirm('Do you want to delete?')\"></asp:LinkButton>");
                sb.Append("\n\t </ItemTemplate>");
                sb.Append("\n\t <FooterTemplate>");
                sb.Append("\n\t <asp:Button ID=\"btnAdd\" runat=\"server\" Text=\"Add\" Width=\"48px\" onclick=\"btnAdd_Click\" />");
                sb.Append("\n\t </FooterTemplate>");
                sb.Append("\n\t </asp:TemplateField>");
                //Tạo Cột Sửa
                sb.Append("\n\t <asp:TemplateField  HeaderText=\"Edit\">");
                sb.Append("\n\t <ItemTemplate>");
                sb.Append("\n\t <asp:LinkButton ID=\"LinkButtonEdit\" runat=\"server\" CausesValidation=\"False\" CommandName=\"Edit\" Text=\"Edit\"></asp:LinkButton>");
                sb.Append("\n\t </ItemTemplate>");
                sb.Append("\n\t <EditItemTemplate>");
                sb.Append("\n\t <asp:LinkButton ID=\"LinkButtonUpdate\" runat=\"server\" CausesValidation=\"True\" CommandName=\"Update\" Text=\"Update\"></asp:LinkButton> &nbsp;");
                sb.Append("\n\t <asp:LinkButton ID=\"LinkButtonCanncel\" runat=\"server\" CausesValidation=\"False\" CommandName=\"Cancel\" Text=\"Cancel\"></asp:LinkButton>");
                sb.Append("\n\t </EditItemTemplate>");
                sb.Append("\n\t </asp:TemplateField>");
                //Cột Id
                sb.Append("\n\t <asp:TemplateField HeaderText=\"" + Id + "\">");
                sb.Append("\n\t <EditItemTemplate>");
                sb.Append("\n\t <asp:TextBox ID=\"txtId\" runat=\"server\" Text='<%# Bind(\"" + Id+"\") %>'></asp:TextBox>");
                sb.Append("\n\t </EditItemTemplate>");
                sb.Append("\n\t <ItemTemplate>");
                sb.Append("\n\t <asp:Label ID=\"txtId\" runat=\"server\" Text='<%# Bind(\"" + Id+"\") %>'></asp:Label>");
                sb.Append("\n\t </ItemTemplate>");
                sb.Append("\n\t <HeaderStyle CssClass=\"hideGridColumn\" />");
                sb.Append("\n\t <ItemStyle CssClass=\"hideGridColumn\" />");
                sb.Append("\n\t <FooterStyle  CssClass=\"hideGridColumn\" />");
                sb.Append("\n\t </asp:TemplateField>");
                sb.Append("");
                sb.Append("\n\t </Columns>"); 
                sb.Append("\n\t <PagerSettings Mode=\"NumericFirstLast\" />");
                sb.Append("\n\t </asp:GridView>");
               
                sb.Append("\n\t <style type=\"text/css\">.hideGridColumn{display: none;}</style>");
                sb.Append("");
                sb.Append("\n\t  </asp:Panel>");

                sb.Append("\n\t </asp:Content>");
                streamWriter.WriteLine(sb);


            }
        }
        /// <summary>
        /// Phần Code sửa xóa trong Gridview
        /// </summary>
        /// <param name="databaseName"></param>
        /// <param name="table"></param>
        /// <param name="targetNamespace"></param>
        /// <param name="storedProcedurePrefix"></param>
        /// <param name="busSuffix"></param>
        /// <param name="dtoSuffix"></param>
        /// <param name="path"></param>
        public static void CreateModifyGridviewCS(string databaseName, Table table, string targetNamespace, string storedProcedurePrefix, string busSuffix, string dtoSuffix, string path)
        {

            string className = Utility.FormatClassName(table.Name) + busSuffix;
            string classNameEntity = Utility.FormatClassName(table.Name) + dtoSuffix;
            string classNameBusiness = Utility.FormatClassName(table.Name) + busSuffix;
            string objBUS = "_" + table.Name + busSuffix.ToUpper();
            string gridviewName = "GridView" + table.Name;
            using (StreamWriter streamWriter = new StreamWriter(Path.Combine(path, table.Name + "2.aspx.cs")))
            {
                var sb = new StringBuilder();
                sb.Append("using System;");
                sb.Append("\n using System.Collections.Generic;");
                sb.Append("\n using System.Linq;");
                sb.Append("\n using System.Web;");
                sb.Append("\n using System.Web.UI;");
                sb.Append("\n using System.Web.UI.WebControls;");
                sb.Append("\n using Business;");
                sb.Append("\n using Entity;");
                sb.Append("\n using System.Data;");
                sb.Append("\n using Common;");
                sb.Append("\n namespace " + targetNamespace + ".Admin2");
                sb.Append("\n {");
                sb.Append("\n public partial class " + table.Name + "2 : System.Web.UI.Page");
                sb.Append("\n \t {");
                sb.Append("\n \t\t" + classNameBusiness + " bus " + " = new " + classNameBusiness + "();");
                sb.Append("\n\t protected void Page_Load(object sender, EventArgs e)");
                sb.Append("\n\t {");
                sb.Append("\n\t if (!IsPostBack)");
                sb.Append("\n\t\t {");
                sb.Append("\n\t\t Databind();");
                sb.Append("\n\t\t }");
                sb.Append("\n\t }");
                sb.Append("\n ");
                sb.Append("\n //Databin");
                sb.Append("\n public void Databind()");
                sb.Append("\n {");
                sb.Append("\n\t "+ gridviewName + ".DataSource = bus.GetAll();");
                sb.Append("\n\t " + gridviewName + ".DataBind();");
                sb.Append("\n }");
               
                //pHAN tRANG
                sb.Append("\n //pHAN tRANG");
                sb.Append("\n protected void " + gridviewName + "_PageIndexChanging(object sender, GridViewPageEventArgs e) ");
                sb.Append("\n { ");
                sb.Append("\n\t" + gridviewName + ".PageIndex = e.NewPageIndex;");
                sb.Append("\n\t Databind();");
                sb.Append("\n }");
                //eDIT
                sb.Append("\n //Edit");
                sb.Append("\n protected void  " + gridviewName + "_RowEditing(object sender, GridViewEditEventArgs e)");
                sb.Append("\n { ");
                sb.Append("\n\t " + gridviewName + ".EditIndex = e.NewEditIndex;");
                sb.Append("\n \t Databind();");
                sb.Append("\n }");
                //Update
                sb.Append("\n  //Update trong Gridview");
                sb.Append("\n protected void " + gridviewName + "_RowUpdating(object sender, GridViewUpdateEventArgs e)");
                sb.Append("\n {");
                sb.Append("\n " + classNameEntity + " obj=new " + classNameEntity + "();");
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];
                    sb.Append(App_ChuongTrinhSinhCodeTuDong.CodeInSP.Common.GridviewTo_Object(column,gridviewName));
                }
                sb.Append("\n\t bus.Update(obj);");
                sb.Append("\n\t " + gridviewName + ".EditIndex = -1; ");
                sb.Append("\n Databind(); ");
                sb.Append("\n}");

                //Canncel
                sb.Append("\n protected void " + gridviewName + "_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)");
                sb.Append("\n\t { ");
                sb.Append("\n\t  " + gridviewName + ".EditIndex = -1;");
                sb.Append("Databind();");
                sb.Append("\n\t }");
                //tHEM
                sb.Append(" //tHEM");
                sb.Append("\n\t protected void btnAdd_Click(object sender, EventArgs e) ");
                sb.Append("\n\t { ");
                sb.Append("\n "+ classNameEntity + " obj = new " + classNameEntity + "();");
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];
                    if (column.IsIdentity==false)
                    {
                        sb.Append(App_ChuongTrinhSinhCodeTuDong.CodeInSP.Common.GridviewTo_Object_Add(column, gridviewName));
                    }
                }
                sb.Append("\n bus.Insert(obj);");
                sb.Append("\n Databind();");
                sb.Append("}");
                //Xoa
                sb.Append("//Xoa");
                sb.Append("\n protected void " + gridviewName + "_RowDeleting(object sender, GridViewDeleteEventArgs e) ");
                sb.Append("\n { ");
                sb.Append("\n " + classNameEntity + " obj=new " + classNameEntity + "();");
                for (int i = 0; i < table.Columns.Count; i++)
                {
                    Column column = table.Columns[i];
                    if (column.IsIdentity == true)
                    {
                        sb.Append(App_ChuongTrinhSinhCodeTuDong.CodeInSP.Common.ConvertFor_DeleteInGridview(column, gridviewName));
                    }
                }
                sb.Append("\n bus.Delete(obj);");
                sb.Append("\n Databind();");
                sb.Append("\n\t  }");
                sb.Append("\n\t  }");
                sb.Append("\n  }");
                streamWriter.WriteLine(sb);
            }
        }
        /// <summary>
        /// Sinh phan designer cho file cua webform Sửa xóa trong gridview
        /// </summary>
        /// <param name="databaseName"></param>
        /// <param name="table"></param>
        /// <param name="targetNamespace"></param>
        /// <param name="storedProcedurePrefix"></param>
        /// <param name="busSuffix"></param>
        /// <param name="dtoSuffix"></param>
        /// <param name="path"></param>
        public static void CreateModifyGridview_Designer_CS(string databaseName, Table table, string targetNamespace, string storedProcedurePrefix, string busSuffix, string dtoSuffix, string path)
        {
            string className = Utility.FormatClassName(table.Name) + dtoSuffix;
            string classNameDAO = Utility.FormatClassName(table.Name) + busSuffix;
            string classNameBusiness = Utility.FormatClassName(table.Name) + busSuffix;
            string objBUS = "_" + table.Name + busSuffix.ToUpper();

            //Gridview to textbox
            using (StreamWriter streamWriter = new StreamWriter(Path.Combine(path, table.Name + "2.aspx.designer.cs")))
            {
                var sb = new StringBuilder();
                sb.Append("\n//------------------------------------------------------------------------------");
                sb.Append("\n// <auto-generated>");
                sb.Append("\n //     This code was generated by a tool.");
                sb.Append("\n //     Changes to this file may cause incorrect behavior and will be lost if");
                sb.Append("\n  //     the code is regenerated. ");
                sb.Append("\n // </auto-generated>");
                sb.Append("\n //------------------------------------------------------------------------------");

                sb.Append("\n namespace " + targetNamespace + ".Admin2");
                sb.Append("\n {");
                sb.Append("\n\t public partial class " + table.Name+"2");
                sb.Append("\n\t{");                
                sb.Append("\n\t protected global::System.Web.UI.WebControls.Panel PanelModify;");
                sb.Append("\n\t protected global::System.Web.UI.WebControls.GridView GridView"+table.Name+";");
                sb.Append("\n\t }");
                sb.Append("\n }");

                streamWriter.WriteLine(sb);
            }
        }
        #endregion


        ///Tao Repeater
        #region tao Repeater
        public static void CreateRepeater(string databaseName, Table table, string targetNamespace, string storedProcedurePrefix, string daoSuffix, string dtoSuffix, string path)
        {

            //string className = Utility.FormatClassName(table.Name) + daoSuffix;
            //path = Path.Combine(path, "Presentation");

            //using (StreamWriter streamWriter = new StreamWriter(Path.Combine(path, className + "Repeater.aspx")))
            //{
            //    var sb = new StringBuilder();


            //    streamWriter.WriteLine(sb);
            //}
        }

     #endregion
    }
}

